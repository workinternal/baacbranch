<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicons -->
	<link href="<?php echo $this->config->item('icon_project'); ?>" rel="icon">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/vuetify/vuetify.css" />
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/util.css">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/custom.css">
	<title><?php echo $this->config->item('project_name'); ?></title>

	<style type="text/css">
		.theme--light.v-table thead tr:first-child {
			border: 1px solid #009688 !important;
			background-color: #009688 !important;
		}
		.v-list__tile {
			margin-top: 5px !important;
			padding-right: 30px !important;
		}
		.blue-text {
			color: #3EC1D5;
		}
		.footer-main {
			padding-top: 10px;
			background-color: rgb(238, 238, 238);
			padding-bottom: 10px;
			margin-top: 10px;
		}
		.v-input__control {
    	padding-right: 10% !important;
		}
	</style>
</head>
<body>
	<div data-app="true" >
		<div class="none-show" ><?php $this->load->view('header.php');?></div>
		<div id="vuejs" >
			<v-app >
				<!-- loading -->
				<div v-show="pageLoading" class="loading-page">
					<v-content>
						<v-container fluid fill-height>
							<v-layout justify-center align-center>
								<v-progress-circular :size="200" :width="20" color="primary" indeterminate></v-progress-circular>
							</v-layout>
						</v-container>
					</v-content>
				</div>

				<v-content class="none-show" style="background-color: #eeeeee;">

					<v-container fluid  class="p-t-0 p-l-0 p-r-0 p-b-0">
						<v-layout  row wrap>
							<v-flex xs12 style="height:450px;">
								<v-carousel hide-delimiters style="height:450px;" >
									<v-carousel-item
									v-for="(item,i) in items"
									:key="i"
									:src="item.src"
									></v-carousel-item>
								</v-carousel>
							</v-flex>
							<!-- รายละเอียด ข้างล่าง-->
							<v-flex xs12 class="p-t-25 white-bg">
								<v-layout row wrap>
									<v-flex xs6>
										<v-layout row wrap>
											<v-flex xs8 offset-xs4>
												<h3 class="title">ผู้ดูแลระบบ</h3>
												<br/>
												<h4 class="pb-2">ดูแลระบบโดย <span class="blue-text">ฝ่ายอำนวยการ</span> ติดต่อ WAN 8635, 8640-41</h4>
												<h4 class="pb-2">พัฒนาโดย  <span class="blue-text">ฝ่ายปฏิบัติการเทคโนโลยีสารสนเทศ</span></h4>
												<h4 class="pb-2">กลุ่มงาน<span class="blue-text">ระบบบริหารจัดการสำนักงาน</span> WAN 8727-9, 8876</h4>
											</v-flex>
										</v-layout>
									</v-flex>
									<v-flex xs6>
										<v-layout row wrap>
											<v-flex class="text-xs-center" xs11 offset-xs1 >
												<h3 class="title">ดาวน์โหลด</h3>
												<br/>
												<h4 class="pb-2">สามารถดาวน์โหลดคู่มือได้ที่นี่ <span class="blue-text">คลิกที่นี่ </span> <i class="material-icons">chrome_reader_mode</i></h4>
												<h4 class="pb-2"><a href="http://xms2/it/wp-content/uploads/2018/08/ChromeStandaloneSetup64.exe">สามารถดาวน์โหลด Google Chrome (สำหรับเครื่อง 64 bit) ได้ที่นี่
													<span class="blue-text">คลิกที่นี่</span>
													<img src="<?=base_url() ?>/assets/images/chrome.png" / style="width: 23px;"> </a>
												</h4>
												<h4 class="pb-2"><a href="http://xms2/it/wp-content/uploads/2018/08/ChromeStandaloneSetup.exe">สามารถดาวน์โหลด Google Chrome (สำหรับเครื่อง 32 bit) ได้ที่นี่
													<span class="blue-text">คลิกที่นี่</span>
													<img src="<?=base_url() ?>/assets/images/chrome.png" / style="width: 23px;"> </a>
												</h4>
											</v-flex>
										</v-layout>
									</v-flex>
								</v-layout>
							</v-flex>

						</v-layout>
					</v-container>
					<v-flex xs12 class="text-xs-center footer-main">
						<p class="mb-1">&copy;2018 — <strong>Bank For Agriculture And Agricultural Cooperatives.</strong> </p>
						<p class="fs-10 mb-1">Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/"
							title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></p>
					</v-flex>
				</v-content>



			</v-app>
		</div> <!-- end vuejs -->
	</div> <!-- end data-app -->

</body>
<script type="text/javascript">
Vue.http.options.emulateJSON = true;
Vue.http.options.emulateHTTP = true;
var vuejs = new Vue({
  el:"#vuejs",
	data:{
		items: [
			{
				src: '<?=base_url() ?>assets/images/slide1.jpg'
			},
			{
				src: '<?=base_url() ?>assets/images/slide2.jpg'
			},
			{
				src: '<?=base_url() ?>assets/images/slide3.jpg'
			}
		],
		sessioncode: '',
		sessionname: '',
		userInfo: [],
		pageLoading: true
	},
  created (){
		this.checkLogin();
  },
  computed:
  {
  },
	mounted () {
	},
  watch: {
  },
	methods:{
		// start method
		checkLogin () {
				setTimeout(() => { this.pageLoading = false; $(".none-show").removeClass("none-show"); }, 1000)
		}
  // end methods
  }
})
</script>
</html>
