<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicons -->
	<link href="<?php echo $this->config->item('icon_project'); ?>" rel="icon">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/vuetify/vuetify.css" />
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/util.css">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/custom.css">
	<title><?php echo $this->config->item('project_name'); ?></title>

	<style type="text/css">
	 .v-card {
	 	border-radius: 10px !important;
		box-shadow: 0px 2px 1px -1px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 1px 3px 0px rgba(0,0,0,0.12) !important;
	 }
	 .card-info {
		 padding: 15px;
     background: #f2f2f2;
     border-radius: 5px;
	 }
	 .icon-head-box {
		 font-size: 30px  !important;
 	 		color: #455A64;
	 }
	 .v-list__tile__avatar {
		 min-width: 75px !important;
	 }
	 .v-avatar {
		 background-color: teal !important;
     color: white !important;
     margin-top: 15px !important;
		 font-size: 20px !important;
	 }
	 thead tr:first-child {
		 border: 1px solid #009688 !important;
		 background-color: #009688 !important;
		 /* font-family: 'Kanit', sans-serif !important; */
	 }
	 .column {
		 color: #fff !important;
     font-size: 14px !important;
	 }
	 .table-text {
		 font-family: 'Trebuchet MS' !important;
		 font-size: 15px !important;
	 }
	 .v-stepper__step .v-stepper__step--active .v-stepper__step--editable .v-stepper__step__step .primary {
		 background-color: #66BB6A !important;
     border: none !important;
	 }
	 .v-input__control {
	 		padding-right: 0 !important;
	 }
	 .v-input {
	 		padding-right: 0 !important;
	 }
	</style>
</head>
<body class="white-bg">
	<div data-app="true" class="application--light">
		<div class="none-show" ><?php $this->load->view('header.php'); ?></div>
		<div id="vuejs" class="white-bg">

			<v-app  >
				<!-- loading -->
				<div v-if="pageLoading" class="loading-page">
					<v-content>
						<v-container fluid fill-height>
							<v-layout justify-center align-center>
								<v-progress-circular :size="200" :width="20" color="primary" indeterminate></v-progress-circular>
							</v-layout>
						</v-container>
					</v-content>
				</div>
				<!-- content -->
				<div v-if="!pageLoading" class="none-show">
					<!-- content -->
					<v-content style="background-color: #fff;">
						<v-container fluid  class="p-t-0 p-l-0 p-r-0">
							<v-layout  row wrap>
									<!-- ค้นหาพนักงาน-->
									<v-flex xs3 class="m-t-15 m-b-5 text-xs-center">
										<v-layout row wrap>
											<v-flex xs10 offset-xs2>
												<v-card>
													<v-container >
														<v-layout row wrap>
															<v-flex xs12>
																<i class="material-icons icon-head-box">account_box</i>
																<h4>ค้นหาข้อมูลพนักงาน</h4>
															</v-flex>
															<v-flex xs12 class="mt-3">
																<v-text-field  label="ระบุรหัสพนักงาน" v-model="empCode" append-icon="dialpad"
																v-validate="'digits:7'" :counter="7" :error-messages="errors.collect('empcode')" data-vv-name="empcode"></v-text-field>
															</v-flex>
															<v-flex xs12>
																<v-btn color="info" round v-on:click="findEmpInfo()"><i class="material-icons">search</i> ค้นหา</v-btn>
															</v-flex>
														</v-layout>
													</v-container>
												</v-card>

												<v-card class="mt-3">
													<v-container >
														<v-layout row wrap>
															<v-flex xs12>
																<i class="material-icons icon-head-box">contacts</i>
																<h4>ข้อมูลพนักงาน</h4>
															</v-flex>
															<v-flex xs12 class="mt-3 mb-3 text-xs-left card-info" v-show="empInfo.code == ''">
																<br/>
																<br/>
																<h3 class="mb-1 text-xs-center"><i class="material-icons">warning</i> &nbsp;ไม่พบข้อมูลพนักงาน</h3>
																<br/>
																<br/>
															</v-flex>
															<v-flex xs12 v-show="empInfo.code == ''">
																<v-btn color="error" round disabled><i class="material-icons">cached</i>&nbsp;ล้างข้อมูล</v-btn>
																<v-btn color="success" round disabled ><i class="material-icons">add_circle_outline</i>&nbsp;เพิ่มสิทธิ์</v-btn>
															</v-flex>
															<v-flex xs12 class="mt-3 mb-3 text-xs-left card-info" v-show="empInfo.code != ''">
																<h4 class="mb-1">รหัสพนักงาน : {{empInfo.code}}</h4>
																<h4 class="mb-1">ชื่อ-สกุล : {{empInfo.title}}&nbsp;{{empInfo.name}}</h4>
																<h4 class="mb-1">ตำแหน่ง : {{empInfo.position}}</h4>
																<h4 class="mb-1">ระดับ : {{empInfo.level}}</h4>
																<h4 class="mb-1">สังกัด : {{empInfo.division}}</h4>
															</v-flex>
															<v-flex xs12 v-show="empInfo.code != ''">
																<v-btn color="error" round  v-on:click="clearEmpInfo()" ><i class="material-icons">cached</i>&nbsp;ล้างข้อมูล</v-btn>
																<v-btn color="success" round v-on:click="addRole()"><i class="material-icons">add_circle_outline</i>&nbsp;เพิ่มสิทธิ์</v-btn>
															</v-flex>
														</v-layout>
													</v-container>
												</v-card>
											</v-flex>
										</v-layout>

									</v-flex>
									<!-- รายชื่อคนที่มีสิทธิ์ -->
									<v-flex xs9 class="m-t-15 m-b-5 text-xs-center">
										<v-layout row wrap>
											<v-flex xs10 offset-xs1>
												<v-card>
													<v-container class="p-l-45 p-r-45">
														<v-layout row wrap>
															<v-flex xs12>
																<i class="material-icons icon-head-box">chrome_reader_mode</i>
																<h4>รายชื่อผู้มีสิทธิ์</h4>
															</v-flex>
															<v-flex xs12 class="mt-3">
																<v-text-field  label="ค้นหา" append-icon="search" v-model="search"></v-text-field>
															</v-flex>
															<v-flex xs12 class="text-xs-left">
																<v-data-table
																:headers="headers"
																:items="listUser"
																:search="search"
																v-bind:pagination.sync="pagination"
																hide-actions
																class="elevation-1"
																>
																<v-progress-linear slot="progress" color="blue" indeterminate></v-progress-linear>
																<template slot="items" slot-scope="props">
																	<td class="text-xs-center table-text">{{props.index+1}}</td>
							                    <td class="text-xs-center table-text">{{props.item.empCode}}</td>
							                    <td class="text-xs-left table-text">{{props.item.title}} {{props.item.name}}</td>
							                    <td class="text-xs-left table-text">{{props.item.position.split(" ")[0]}}</td>
							                    <td class="text-xs-center table-text">{{props.item.positionLevel}}</td>
							                    <td class="text-xs-center table-text">{{props.item.division}}</td>
							                    <td class="text-xs-center table-text">
																		<v-btn flat icon color="pink" v-on:click="removeRole(props.item.empCode, props.item.title +' '+ props.item.name)">
																			<v-icon>delete_forever</v-icon>
																		</v-btn>
							                    </td>
																</template>

															</v-data-table>
															<div class="text-xs-center">
																<v-pagination class="m-t-10 " v-model="pagination.page" :length="pages"circle></v-pagination>
															</div>
															</v-flex>
														</v-layout>
													</v-container>
												</v-card>

											</v-flex>
										</v-layout>
									</v-flex>
							</v-layout>
						</v-container>
					</v-content>

					<v-dialog v-model="dialogProgress.status" persistent max-width="500px">
						<v-card class="text-xs-center">
							<v-btn class="m-t-30" fab dark large color="white" depressed >
								<v-icon dark color="red" style="font-size:45px!important;">notification_important</v-icon>
							</v-btn>
							<v-card-title class="justify-center pt-0">
								<span class="headline"><h4>{{dialogProgress.title}}</h4></span>
							</v-card-title>
							<v-card-text class="p-t-0 p-b-0">
								<v-container grid-list-md class="p-t-0 p-b-0">
									<v-layout wrap>
										<v-flex xs12>
											<v-progress-linear :indeterminate="true"></v-progress-linear>
										</v-flex>
									</v-layout>
								</v-container>
							</v-card-text>
							<v-card-actions class="p-b-20 justify-center">
							</v-card-actions>
						</v-card>
					</v-dialog>
				</div>

			</v-app>
		</div> <!-- end vuejs -->
	</div> <!-- end data-app -->

</body>
<script type="text/javascript">
Vue.http.options.emulateJSON = true;
Vue.http.options.emulateHTTP = true;
Vue.use(VeeValidate)
var vuejs = new Vue({
  el:"#vuejs",
	$_veeValidate: {
		validator: 'new'
	},
	data:{
		pageLoading: true,
		projectName: 'ระบบงานที่ตั้งสำนักงานของธนาคาร',
		sessioncode: '',
		sessionname: '',
		dialogProgress: {status: false, title: ''},
		dictionary: {
			custom: {
				empcode: {
					digits: 'ระบุรหัสพนักงานเป็นตัวเลขเท่านั้น และความยาวต้องมี 7 ตัวอักษร'
				}
			}
		},
		listUser: [],
		empCode: '',
		empInfo: {code: '', title: '', name: '', division: '', position: '', level: ''},
		pagination: {rowsPerPage: 6},
		search: '',
		headers: [
			{
				text: 'ลำดับ',
				align: 'center',
				sortable: false,
				value: 'index',
				width: "20px"
			},
			{ text: 'รหัสพนักงาน', value: 'empCode', sortable: false, width: "20px" },
			{ text: 'ชื่อ', value: 'name', sortable: false, width: "200px" },
			{ text: 'ตำแหน่ง', value: 'position', sortable: false, width: "150px" },
			{ text: 'ระดับ', value: 'positionLevel', sortable: false },
			{ text: 'สังกัด', value: 'division', sortable: false },
			{ text: '', value: 'default', sortable: false, width: "20px" }
		]

	},
  created (){
		this.checkLogin();
		$(".none-show").removeClass("none-show");
  },
  computed:
  {
		pages () {
			return this.pagination.rowsPerPage ? Math.ceil(this.listUser.length / this.pagination.rowsPerPage) : 0
		}
  },
	mounted () {
		this.$validator.localize('en', this.dictionary);
	},
  watch: {
  },
	methods:{
		// start method
		progressDialog (status, title) {
			this.dialogProgress.status = status;
			this.dialogProgress.title = title;
		},
		checkLogin () {
			this.$http.post('<?=base_url() ?>index.php/loginController/getSession').then((response) => {
				var userInfo = response.body;
				if (userInfo.emp_code != "nodata") {
					this.sessioncode = userInfo.emp_code;
					this.sessionname = userInfo.emp_name;
					this.loadListUserAdmin();
					setTimeout(() => { this.pageLoading = false;  }, 300)
				}
				else {
					this.pageLoading = false;
					swal({
						title: 'กรุณาเข้าสู่ระบบก่อนเข้าใช้งาน',
						text: '',
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.href = '<?=base_url() ?>';
					})
				}

			}, (response) => {});
		},
		loadListUserAdmin () {
			this.listUser = [];
			this.$http.post('<?=base_url() ?>index.php/adminController/getUserAdmin').then((response) => {
				this.listUser = response.body;
			}, (response) => {});
		},
		findEmpInfo () {
			this.$validator.validateAll().then((result) => {
				if (result) {
					this.empInfo = {code: '', title: '', name: '', division: '', position: '', level: ''};
					this.$http.post('<?=base_url() ?>index.php/adminController/getUserInfo', {empCode: this.empCode}).then((response) => {
						var result = response.body;
						if(result.length > 0) {
							this.empCode = '';
							this.empInfo['code'] = result[0]['empCode'];
							this.empInfo['title'] = result[0]['title'];
							this.empInfo['name'] = result[0]['name'];
							this.empInfo['division'] = result[0]['division'];
							this.empInfo['position'] = result[0]['position'].split(' ')[0];
							this.empInfo['level'] = result[0]['positionLevel'];
						}
						else {
							swal({
								title: 'ไม่พบข้อมูลพนักงาน',
								text: 'กรุณาลองใหม่อีกครั้ง',
								type: 'warning',
								showCancelButton: false,
								confirmButtonColor: '#3085d6',
								confirmButtonText: 'ตกลง',
								allowOutsideClick: false
							})
							this.empCode = '';
						}
					}, (response) => {});
					return;
				}
			});
		},
		addRole () {
			if(this.empInfo.name == '') {
				swal({
					title: 'ข้อมูลพนักงานไม่ถูกต้อง',
					text: 'กรุณาลองใหม่อีกครั้ง',
					type: 'warning',
					showCancelButton: false,
					confirmButtonColor: '#3085d6',
					confirmButtonText: 'ตกลง',
					allowOutsideClick: false
				})
			}
			else {
				this.$http.post('<?=base_url() ?>index.php/adminController/addRole', {empCode: this.empInfo.code}).then((response) => {
					var result = response.body;
					var resultHead = result.split(':')[0];
					var resultTail = result.split(':')[1];
					if(resultHead == 'Successfully') {
						this.clearEmpInfo();
						if (resultTail == '1') {
							this.loadListUserAdmin();
							swal({
								title: 'เพิ่มสิทธิ์เรียบร้อยแล้ว',
								text: '',
								type: 'success',
								showCancelButton: false,
								confirmButtonColor: '#3085d6',
								confirmButtonText: 'ตกลง',
								allowOutsideClick: false
							})
						}
						else if (resultTail == '0') {
							swal({
								title: 'พนักงานรายนี้มีสิทธิ์อยู่แล้ว',
								text: '',
								type: 'warning',
								showCancelButton: false,
								confirmButtonColor: '#3085d6',
								confirmButtonText: 'ตกลง',
								allowOutsideClick: false
							})
						}

					}
					else {
						swal({
							title: 'ไม่สามารถเพิ่มสิทธิ์ได้',
							text: 'กรุณาลองใหม่อีกครั้ง',
							type: 'error',
							showCancelButton: false,
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'ตกลง',
							allowOutsideClick: false
						})
					}
				}, (response) => {});
			}

		},
		removeRole (empCode, empName) {
			swal({
				title: 'ยืนยันการลบสิทธิ์!',
				text: 'ของ ' + empName,
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'ตกลง',
				cancelButtonText: 'ยกเลิก',
				allowOutsideClick: true
			}).then((result) => {
				if (result.value) {
					this.$http.post('<?=base_url() ?>index.php/adminController/removeRole', {empCode: empCode}).then((response) => {
						var result = response.body;
						if(result == 'Successfully') {
							this.clearEmpInfo();
							this.loadListUserAdmin();
							swal({
								title: 'ลบสิทธิ์เรียบร้อยแล้ว',
								text: '',
								type: 'success',
								showCancelButton: false,
								confirmButtonColor: '#3085d6',
								confirmButtonText: 'ตกลง',
								allowOutsideClick: false
							})
						}
						else {
							swal({
								title: 'ไม่สามารถลบสิทธิ์ได้',
								text: 'กรุณาลองใหม่อีกครั้ง',
								type: 'error',
								showCancelButton: false,
								confirmButtonColor: '#3085d6',
								confirmButtonText: 'ตกลง',
								allowOutsideClick: false
							})
						}
					}, (response) => {});	
				}

			})

		},
		clearEmpInfo () {
			this.empInfo = {code: '', title: '', name: '', division: '', position: '', level: ''};
		}
  // end methods
  }
})
</script>
</html>
