<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicons -->
	<link href="<?php echo $this->config->item('icon_project'); ?>" rel="icon">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/vuetify/vuetify.css" />
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/util.css">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/custom.css">
	<title><?php echo $this->config->item('project_name'); ?></title>

	<style type="text/css">
		.v-stepper--alt-labels .v-stepper__step {
			flex-basis: 95% !important;
		}
		.box-area {
			background: #eee;
			padding: 30px 15px 0px 15px;
			border-radius: 5px;
		}
		.v-input__slot {
			margin: 10px 2px 2px 10px !important;
		}


		</style>
</head>
<body>
	<div data-app="true" class="application--light">
		<div class="none-show" ><?php $this->load->view('header.php'); ?></div>
		<div id="vuejs">
			<v-app  class="white-bg" >
				<!-- loading -->
				<div v-show="pageLoading" class="loading-page">
					<v-content>
						<v-container fluid fill-height>
							<v-layout justify-center align-center>
								<v-progress-circular :size="200" :width="20" color="primary" indeterminate></v-progress-circular>
							</v-layout>
						</v-container>
					</v-content>
				</div>
				<!-- content -->
				<div class="none-show">

					<!-- content -->
					<v-content style="background-color: #fff;">
						<v-container fluid  class="p-t-0 p-l-0 p-r-0">
							<v-layout  row wrap>
								<v-flex xs10 offset-xs1 class="pt-2 text-xs-center">
									<span><i class="material-icons icon-title-big">create_new_folder</i></span> <h2>สร้างสำนักงานจังหวัด</h2>
								</v-flex>
								<!-- หัวข้อ-->
								<v-flex xs10 offset-xs1 class="m-t-5 m-b-5 text-xs-center">
									<div class="p-l-50  p-r-50 p-b-50">
										<v-stepper v-model="currentStep" alt-labels>
											<v-stepper-header>
												<template v-for="n in steps">
													<v-stepper-step :edit-icon="'check'" :complete="currentStep > n" :key="`${n}-step`" :step="n" >{{headerStep[n-1]}}</v-stepper-step>
													<!-- <v-divider v-if="n !== steps" :key="n"></v-divider> -->
												</template>
											</v-stepper-header>
										<v-stepper-items>
											<!-- step 1 -->
											<v-stepper-content step="1">
												<v-card  class="mb-5 " >
													<!-- information -->
													<v-layout row wrap class="mb-2">
														<v-flex xs12 class="text-xs-left">
															<h3><v-icon large color="blue-grey darken-2" style="font-size:25px;color: #C62828 !important;">chrome_reader_mode</v-icon> <span>ข้อมูลสำนักงานจังหวัด</span></h3>
														</v-flex>
													</v-layout>
													<!-- ฝ่าย / สนจ -->
													<v-layout row wrap class="m-b-45">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ฝ่ายกิจการสาขา</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-autocomplete  box single-line  :items="listDiv" item-text="div_nme" item-value="id" v-model="odInfo.is_div" menu-props="auto"  hide-details  single-line
															v-validate="'required'"  :error-messages="errors.collect('step1.is_div')" data-vv-name="is_div" data-vv-scope="step1">
															</v-autocomplete>
														</v-flex>
													</v-layout>
													<!-- รหัสสังกัด -->
													<!-- <v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-2">รหัสสังกัด</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
																<v-text-field disabled box single-line disabled v-model="odInfo.code" ></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left pl-4">

														</v-flex>
														<v-flex xs4 class="text-xs-left">
																<v-text-field disabled box single-line disabled v-model="odInfo.division_code" ></v-text-field>
														</v-flex>
													</v-layout> -->
													<!-- ชื่อไทย / ชื่ออังกฤษ -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ชื่อสำนักงาน (ภาษาไทย)</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field  box single-line label="ภาษาไทย"  clear-icon="cancel" clearable v-model="odInfo.is_thai_n" v-validate="'required'" :error-messages="errors.collect('step1.thainame')" data-vv-name="thainame" data-vv-scope="step1" messages="ระบุเฉพาะชื่อจังหวัด"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">ชื่อสำนักงาน (ภาษาอังกฤษ)</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field  box single-line label="ภาษาอังกฤษ"  clear-icon="cancel" clearable v-model="odInfo.is_eng_n" v-validate="'required'" :error-messages="errors.collect('step1.engname')" data-vv-name="engname" data-vv-scope="step1" messages="ระบุเฉพาะชื่อจังหวัด"></v-text-field>
														</v-flex>
													</v-layout>
													<!-- ชื่อย่อ / วันที่เปิดดำเนินการ  -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ชื่อย่อสำนักงาน</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field  box single-line label="ชื่อย่อ"  v-model="odInfo.is_amp_n"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">วันที่เปิดดำเนินการ</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-menu :close-on-content-click="true" v-model="menu2" :nudge-right="40" transition="scale-transition" offset-y full-width min-width="290px">
												        <v-text-field  box single-line readonly slot="activator" v-model="odInfo.is_open_dte" append-icon="event" readonly></v-text-field>
												        <v-date-picker v-model="odInfo.is_open_dte" @input="menu2 = false" locale="th"></v-date-picker>
												      </v-menu>
														</v-flex>
													</v-layout>
													<!-- location -->
													<v-layout row wrap class="mb-2 mt-5">
														<v-flex xs12 class="text-xs-left">
															<h3><v-icon large color="blue-grey darken-2" style="font-size:25px;color: #C62828 !important;">account_balance</v-icon> <span>ที่ตั้ง</span></h3>
														</v-flex>
													</v-layout>
													<!-- จังหวัด และ อำเภอ-->
													<v-layout row wrap class="m-b-45">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">จังหวัด</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-autocomplete  box single-line  :items="listProvinceAddr" id="listProvinceAddr" item-text="name" item-value="id" v-model="odAddrBR.province" menu-props="auto" label=" -- จังหวัด --" hide-details  single-line v-on:change="getListDistrict()" v-validate="'required'" :error-messages="errors.collect('step1.addrProvince')" data-vv-name="addrProvince" data-vv-scope="step1"></v-autocomplete>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">อำเภอ</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-autocomplete   box single-line  :items="listDistrictAddr" id="listDistrictAddr" menu-props="auto" label=" -- อำเภอ --" hide-details 	single-line
															item-text="name" item-value="id" v-model="odAddrBR.amphur"  v-on:change="getListSubDistrict()" v-validate="'required'" :error-messages="errors.collect('step1.addrDist')" data-vv-name="addrDist" data-vv-scope="step1"></v-autocomplete>
														</v-flex>
													</v-layout>
													<!-- ตำบล และ รหัสไปรษณีย์ -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ตำบล</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-autocomplete  box single-line  :items="listSubDistrictAddr" id="listSubDistrictAddr" item-text="name" item-value="id" v-model="odAddrBR.tumbon"  v-on:change="getZipcode()" menu-props="auto" label=" -- ตำบล --" hide-details  single-line v-validate="'required'" :error-messages="errors.collect('step1.addrSubDist')" data-vv-name="addrSubDist" data-vv-scope="step1"></v-autocomplete>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">รหัสไปรษณีย์</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field  box single-line  v-model="odAddrBR.zipcode" disabled label="รหัสไปรษณีย์"></v-text-field>
														</v-flex>
													</v-layout>
													<!-- เลขที่ หมู่ -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">เลขที่</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field  box single-line  clear-icon="cancel" clearable label="เลขที่" v-model="odAddrBR.addr" v-validate="'required'" :error-messages="errors.collect('step1.addr')" data-vv-name="addr" data-vv-scope="step1"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">หมู่</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field  box single-line  clear-icon="cancel" clearable label="หมู่" v-model="odAddrBR.moo"></v-text-field>
														</v-flex>
													</v-layout>
													<!-- ซอย ถนน -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ซอย</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field  box single-line  clear-icon="cancel" clearable label="ซอย" v-model="odAddrBR.soi"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">ถนน</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field  box single-line  clear-icon="cancel" clearable label="ถนน" v-model="odAddrBR.street"></v-text-field>
														</v-flex>
													</v-layout>
													<!-- contract -->
													<v-layout row wrap class="mb-2 mt-5">
														<v-flex xs12 class="text-xs-left">
															<h3><v-icon large color="blue-grey darken-2" style="font-size:25px;color: #C62828 !important;">contact_phone</v-icon> <span>ติดต่อ</span></h3>
														</v-flex>
													</v-layout>
													<!-- เบอร์โทร -->
													<v-layout row wrap class="mb-3" v-for=" (item, index) in telBRInfo " :key="index">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">โทรศัพท์</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line label="โทรศัพท์"  clear-icon="cancel" clearable return-masked-value mask="#-####-####" v-model="item.number"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left " >
															<v-btn color="red" fab small dark v-on:click="removeTelephoneBR(index)" v-show="index != 0">
																<v-icon>remove</v-icon>
															</v-btn>
															<v-btn color="primary" fab small dark v-on:click="addTelephoneBR()" v-show="index == (telBRInfo.length-1)">
																<v-icon>add</v-icon>
															</v-btn>
														</v-flex>
													</v-layout>
													<!-- wan โทรสาร -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">WAN</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line label="wan"  clear-icon="cancel" clearable return-masked-value mask="####" v-model="ctrBRInfo.wan"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left">
															<h4 class="pt-3">โทรสาร</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line label="โทรสาร"  clear-icon="cancel" clearable return-masked-value mask="#-####-####" v-model="ctrBRInfo.fax" ></v-text-field>
														</v-flex>
													</v-layout>
												</v-card>
												<v-card-actions class="text-xs-center">
													<v-spacer></v-spacer>
													<v-btn round color="primary" @click.native="validateBeforeNextPage('step1')">สร้างสำนักงานจังหวัด</v-btn>
													<v-spacer></v-spacer>
												</v-card-actions>
											</v-stepper-content>
										</v-stepper-items>
									</v-stepper>
								</div>
								</v-flex>

							</v-layout>
						</v-container>
					</v-content>

					<v-dialog v-model="dialogProgress.status" persistent max-width="500px">
						<v-card class="text-xs-center">
							<v-btn class="m-t-30" fab dark large color="white" depressed >
								<v-icon dark color="red" style="font-size:45px!important;">notification_important</v-icon>
							</v-btn>
							<v-card-title class="justify-center pt-0">
								<span class="headline"><h4>{{dialogProgress.title}}</h4></span>
							</v-card-title>
							<v-card-text class="p-t-0 p-b-0">
								<v-container grid-list-md class="p-t-0 p-b-0">
									<v-layout wrap>
										<v-flex xs12>
											<v-progress-linear :indeterminate="true"></v-progress-linear>
										</v-flex>
									</v-layout>
								</v-container>
							</v-card-text>
							<v-card-actions class="p-b-20 justify-center">
							</v-card-actions>
						</v-card>
					</v-dialog>

				</div>


			</v-app>
		</div> <!-- end vuejs -->
	</div> <!-- end data-app -->
</body>
<script type="text/javascript">
Vue.http.options.emulateJSON = true;
Vue.http.options.emulateHTTP = true;
Vue.use(VeeValidate)
var vuejs = new Vue({
  el:"#vuejs",
	$_veeValidate: {
		validator: 'new'
	},
	data:{
		pageLoading: true,
		projectName: 'ระบบงานที่ตั้งสำนักงานของธนาคาร',
		sessioncode: '',
		sessionname: '',
		dialogProgress: {status: false, title: ''},
		dictionary: {
			attributes: {
				// email: 'E-mail Address'
			},
			custom: {
				is_div: { required: () => 'กรุณาเลือกฝ่ายกิจการสาขา' },
				is_province: { required: () => 'กรุณาเลือกสำนักงานจังหวัด' },
				thainame: { required: () => 'กรุณาระบุชื่อภาษาไทย' },
				engname: { required: () => 'กรุณาระบุชื่อภาษาอังกฤษ' },
				aligname: { required: () => 'กรุณาระบุชื่อย่อสาขา' },
				addrProvince: { required: () => 'กรุณาเลือกจังหวัด' },
				addrDist: { required: () => 'กรุณาเลือกอำเภอ' },
				addrSubDist: { required: () => 'กรุณาเลือกตำบล' },
				addrZipcode: { required: () => 'กรุณาระบุรหัสไปรษณีย์' },
				addr: { required: () => 'กรุณาระบุเลขที่' }
			}
		},
		currentStep: 1,
		steps: 1,
		headerStep: ['ข้อมูลสำนักงานจังหวัด'],
		listDiv: [],
		listProvince: [],
		listBranch: [],
		listType: [],
		branchDetail: [],
		odInfo: {is_div: '', is_thai_n: '', is_eng_n: '', is_open_dte: new Date().toISOString().substr(0, 10), is_amp_n: ''},
		odAddrBR: {province: '', amphur: '', tumbon: '', zipcode: '', addr: '', moo: '', soi: '', street: '', prov: '', dist: '', subdist: ''},
		ctrBRInfo: {fax: '', wan: '' },
		telBRInfo: [{number: ''}],
		menu2: '',
		listProvinceAddr: [],
		listDistrictAddr: [],
		listSubDistrictAddr: []
	},
  created (){
		this.checkLogin();
  },
  computed:
  {
  },
	mounted () {
		this.$validator.localize('en', this.dictionary);
	},
  watch: {
		steps (val) {
			if (this.currentStep > val) {
				this.currentStep = val;
			}
		},
		currentStep (check) {
			if (check == 2) {
				if(this.listProvinceUnitAddr.length == 0) {
					this.getListProvinceUnit();
				}
			}
			else if (check == 3) {
				if(this.odArea.length == 1 && this.listAddrArea[0].listProvince.length == 0) {
					this.getListProvinceArea(0);
				}
			}
		},
		copyName (check) {
			if (check == true) {
				this.odAmphur.is_thai_n = this.odInfo.is_thai_n;
				this.odAmphur.is_eng_n  = this.odInfo.is_eng_n;
			}
			else {
				this.odAmphur.is_thai_n = '';
				this.odAmphur.is_eng_n  = '';
			}
		},
		copyAddr (check) {
			if (check === true) {
				this.odAddrAM.province = this.odAddrBR.province;
				this.getListDistrictUnit();
			}
			else {
				this.odAddrAM.province = '';
				this.odAddrAM.amphur = '';
				this.odAddrAM.tumbon = '';
				this.odAddrAM.zipcode = '';
				this.odAddrAM.addr = '';
				this.odAddrAM.moo = '';
				this.odAddrAM.soi = '';
				this.odAddrAM.street = '';
			}
		},
		copyPhone (check) {
			if (check === true) {
				this.telAMInfo = this.telBRInfo;
				this.ctrAMInfo = this.ctrBRInfo;
			}
			else {
				this.ctrAMInfo = {fax: '', wan: '' };
				this.telAMInfo = [{number: ''}];
			}
		}
  },
	methods:{
		// start method
		progressDialog (status, title) {
			this.dialogProgress.status = status;
			this.dialogProgress.title = title;
		},
		checkLogin () {
			this.$http.post('<?=base_url() ?>index.php/loginController/getSession').then((response) => {
				var userInfo = response.body;
				if (userInfo.emp_code != "nodata") {
					this.sessioncode = userInfo.emp_code;
					this.sessionname = userInfo.emp_name;
					this.loadDivInfo();
					this.getListProvince();
					setTimeout(() => { this.pageLoading = false; $(".none-show").removeClass("none-show"); }, 1000)
				}
				else {
					this.pageLoading = false;
					swal({
						title: 'กรุณาเข้าสู่ระบบก่อนเข้าใช้งาน',
						text: '',
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.href = '<?=base_url() ?>';
					})
				}
			}, (response) => {});
		},
		loadDivInfo () {
			this.listDiv = [];
			this.odInfo.is_div = '';
			this.odInfo.province = '';
			this.$http.post('<?=base_url() ?>index.php/odController/loadDivInfo').then((response) => {
				this.listDiv = response.body;
				this.listDiv.unshift({div_nme: '-- ฝ่ายกิจการสาขา --', id: ''})
			}, (response) => {
			});
		},
		getListProvince () {
			this.listProvinceAddr = [];
			this.amphurAddr = [];
			this.odAddrBR.amphur = '';
			this.odAddrBR.tumbon = '';
			this.odAddrBR.zipcode = '';
			var me = this;
			this.$http.post('<?=base_url() ?>index.php/createBranchController/getListProvince').then((response) => {
				this.listProvinceAddr = response.body;
				this.listProvinceAddr.unshift({id: '', name: '-- จังหวัด --', code: ''})
				this.getListDistrict();
			}, (response) => {
			});
		},
		getListDistrict () {
			this.odAddrBR.amphur = '';
			this.odAddrBR.tumbon = '';
			this.odAddrBR.zipcode = '';
			this.$http.post('<?=base_url() ?>index.php/createBranchController/getListDistrict', {provinceID: this.odAddrBR.province}).then((response) => {
				this.listDistrictAddr = response.body;
			}, (response) => {
			});
		},
		getListSubDistrict () {
			this.odAddrBR.tumbon = '';
			this.odAddrBR.zipcode = '';
			this.$http.post('<?=base_url() ?>index.php/createBranchController/getListSubDistrict', {provinceID: this.odAddrBR.province, amphurID: this.odAddrBR.amphur}).then((response) => {
				this.listSubDistrictAddr = response.body;
			}, (response) => {});
		},
		getZipcode () {
			var me = this;
			var indexOf = _.filter(this.listSubDistrictAddr, function(o) { return o.id == me.odAddrBR.tumbon; });
			this.odAddrBR.zipcode = indexOf[0]['postcode'];
		},
		addTelephoneBR () {
			this.telBRInfo.push({number: ''});
		},
		removeTelephoneBR (index) {
			swal({
				title: 'ยืนยันการลบข้อมูลเบอร์โทรศัพท์',
				text: "",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'ตกลง',
				cancelButtonText: 'ยกเลิก',
			}).then((result) => {
				if (result.value) {
					if (this.telBRInfo.length > 1) {
						this.telBRInfo.splice(index,1)
					}
				}
			})
		},
		validateBeforeNextPage(scope) {
			this.$validator.validateAll(scope).then((result) => {
				if (result) {

						swal({
							title: 'ยืนยันการสร้างสำนักงานจังหวัด',
							text: '',
							type: 'warning',
							showCancelButton: true,
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'ตกลง',
							cancelButtonText: 'ยกเลิก',
							allowOutsideClick: false
						}).then((result) => {
							if(result.value) {

								this.odAddrBR.subdist = $('#listSubDistrictAddr').val();
								this.odAddrBR.dist = $('#listDistrictAddr').val();
								this.odAddrBR.prov = $('#listProvinceAddr').val();

								this.progressDialog(true, 'กำลังดำเนินการ กรุณารอสักครู่');
								setTimeout(() => { this.addNewProvincial(); }, 500)
							}

						})

				}
				else {
					swal({
						title: 'กรุณาตรวจสอบข้อมูลให้ถูกต้อง',
						text: '',
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.href = "#";
					})

				}
			});
		},
		addNewProvincial () {
			this.$http.post('<?=base_url() ?>index.php/createProvincialController/addNewProvincial', {odInfo: this.odInfo, odAddrBR: this.odAddrBR, telBRInfo: this.telBRInfo, ctrBRInfo: this.ctrBRInfo}).then((response) => {

				var result = response.body;
				this.progressDialog(false, null);
				if (result == 'Successfully') {
					swal({
						title: 'สร้างสำนักงานจังหวัดเรียบร้อยแล้ว',
						text: '',
						type: 'success',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.reload();
					})
				}
				else {
					swal({
						title: 'ไม่สามารถสร้างสำนักงานจังหวัดได้',
						text: 'กรุกรุณาลองใหม่อีกครั้ง',
						type: 'error',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.href = "#";
					})
				}
			}, (response) => {
			});
		}
  // end methods
  }
})
</script>
</html>
