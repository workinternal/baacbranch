<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicons -->
	<link href="<?php echo $this->config->item('icon_project'); ?>" rel="icon">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/vuetify/vuetify.css" />
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/util.css">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/custom.css">
	<title><?php echo $this->config->item('project_name'); ?></title>

	<style type="text/css">
		.theme--light.v-table thead th {
			border: 1px solid #ddd !important;
			background-color: #f2f2f2 !important;
			font-weight: bold !important;
			color: #000 !important;
			font-family: 'Trebuchet MS' !important;
			font-size: 15px !important;
		}
		table.v-table tbody td {
			border: 1px solid #ddd !important;
		}
		.v-list__tile {
			margin-top: 5px !important;
			padding-right: 30px !important;
		}
		.blue-text {
			color: #3EC1D5;
		}
		.v-input {
			padding-top: 0px !important;
    	margin-top: 0px !important;
    	padding-right: 50px !important;
		}
		.table-text {
			font-family: 'Trebuchet MS' !important;
    	font-size: 15px !important;
		}
		.box-btn {
			padding-top: 5px !important;
			height: 70px !important;
			border-radius: 5px !important;
		}
	</style>
</head>
<body class="white-bg">
	<div data-app="true" class="application--light">
		<div class="none-show" ><?php $this->load->view('header.php'); ?></div>
		<div id="vuejs" class="white-bg">

			<v-app>

				<!-- loading -->
				<div v-if="pageLoading" class="loading-page">
					<v-content>
						<v-container fluid fill-height>
							<v-layout justify-center align-center>
								<v-progress-circular :size="200" :width="20" color="primary" indeterminate></v-progress-circular>
							</v-layout>
						</v-container>
					</v-content>
				</div>
				<!-- content -->
				<div  class="none-show">
					<!-- content -->
					<v-content style="background-color: #fff;">
						<v-container fluid  class="p-t-0 p-l-0 p-r-0">
							<v-layout  row wrap>
								<!-- หัวข้อ-->
								<v-flex xs12 class="m-t-15 m-b-5 text-xs-center">
									<span><i class="material-icons icon-title-small">domain</i></span>
									<h2>รายชื่อสังกัด ที่ตั้งสำนักงาน รหัสอำเภอ/ชื่ออำเภอ (ธ.ก.ส.) และชื่ออำเภอ/ตำบล(มหาดไทย)</h2>
								</v-flex>
								<!-- หัวข้อ-->
								<v-flex xs10 offset-xs1 class="border-grey p-t-20 p-b-10 p-l-20 p-r-20">
									<v-layout  row wrap>
										<v-flex xs1>
											<h4 class="font-bold">ระบุสังกัด</h4>
										</v-flex>
										<v-flex xs11>
											<v-layout  row wrap>
												<v-flex xs1 class="p-l-5 p-t-20">
													<h4>ฝสข.</h4>
												</v-flex>
												<v-flex xs5>
													<v-autocomplete box :items="listDiv" menu-props="auto" label=" -- ฝ่ายกิจการสาขา --" hide-details single-line
													item-text="div_nme" item-value="id" v-model="odInfo.div" v-on:change="loadProvinceInfo()" id="divnme"></v-autocomplete>
												</v-flex>
												<v-flex xs1 class="p-l-5  p-t-20">
													<h4>สนจ.</h4>
												</v-flex>
												<v-flex xs5>
													<v-autocomplete box :items="listProvince" menu-props="auto" label=" -- สนจ --" hide-details 	single-line
													item-text="brname" item-value="brcode" v-model="odInfo.province" v-on:change="loadBranchInfo()" id="provnme"></v-autocomplete>
												</v-flex>
											</v-layout>
										</v-flex>
									</v-layout>
									<v-layout  row wrap class="m-t-25">
										<v-flex xs1></v-flex>
										<v-flex xs11>
											<v-layout  row wrap>
												<v-flex xs1 class="p-l-5  p-t-20">
													<h4>สาขา</h4>
												</v-flex>
												<v-flex xs5>
													<v-autocomplete box  :items="listBranch" menu-props="auto" label=" -- สาขา --" hide-details 	single-line
													item-text="brname" item-value="brcode" v-model="odInfo.branch"  id="brnnme"></v-autocomplete>
												</v-flex>
												<v-flex xs1 class="p-l-5  p-t-20">
													<h4>ประเภทสาขา</h4>
												</v-flex>
												<v-flex xs5>
													<v-autocomplete box  :items="listType" menu-props="auto" label=" -- ทุกประเภท --" hide-details 	single-line
													item-text="type_des" item-value="brnch_type" v-model="odInfo.type" ></v-autocomplete>
												</v-flex>
											</v-layout>
										</v-flex>
									</v-layout>
									<v-layout  row wrap class="m-t-25">
										<v-flex xs12 class="text-xs-center">
											<v-btn round  color="orange" dark v-on:click="loadBranchDetail()"> ค้นหา <v-icon  class="ml-2">search</v-icon> </v-btn>
											<v-menu transition="slide-x-transition" bottom right offset-x >
												<v-btn round slot="activator"  color="primary">  พิมพ์ <v-icon  dark class="ml-2">print</v-icon></v-btn>
												<v-list>
													<v-list-tile>
														<v-list-tile-title class="point-cursor">

															<a v-on:click="DoReport()"> <img src="<?=base_url() ?>assets/images/pdf.png"  height="23" class="mr-2"> PDF </a>
														</v-list-tile-title>
													</v-list-tile>
													<v-list-tile>
														<v-list-tile-title class="point-cursor">

															<a id="exportBranch"> <img src="<?=base_url() ?>assets/images/excel.png"  height="23" class="mr-2"> Excel </a>
														</v-list-tile-title>
													</v-list-tile>
												</v-list>
											</v-menu>
										</v-flex>
									</v-layout>
								</v-flex>
							</v-layout>
							<v-layout  row wrap class="m-t-10">
								<!-- ตาราง-->
								<v-flex xs12 class="m-t-10 m-l-10 m-b-10 m-r-10">

									<v-data-table
									:headers="headers"
									:items="branchDetail"
									class="elevation-1"
									hide-actions
									>
									<template slot="items" slot-scope="props">
										<td class="text-xs-center table-text"	 >{{ props.index + 1 }}</td>
										<td class="text-xs-center table-text" style="width:150px;">{{ props.item.org_ode }}</td>
										<td class="text-xs-center table-text" 	style="width:80px;">{{ props.item.org_cd }}</td>
										<td class="text-xs-left table-text"		>{{ props.item.type_des + props.item.th_org_nme}}</td>
										<td class="text-xs-left table-text"		>{{ props.item.eng_org_nme }}</td>
										<td class="text-xs-center table-text" 		style="width: 120px;">{{ dateFormat(props.item.open_date) }}</td>
										<td class="text-xs-left table-text" 	style="width: 300px;">
											<p class="m-b-10 m-t-5"		>ที่อยู่: {{ props.item.org_addr }}</p>
											<p class="m-b-10" v-show="props.item.telephone.length > 0" v-for="listPhone, index in props.item.telephone">โทรศัพท์: {{ listPhone.number }} </p>
											<p class="m-b-10" v-show=" props.item.fax != '' ">โทรสาร: {{ props.item.fax}} </p>
											<p class="m-b-10" v-show=" props.item.wan != ''" >WAN: {{ props.item.wan}} </p>
										</td>
										<td class="text-xs-cente table-textr" style="width: 120px;">{{ unitFormat(props.item.district, props.item.unit_nme) }}</td>
										<td class="text-xs-left table-text" style="width: 180px;">
											<p v-for="listunit, index in props.item.unit_list">{{listunit.tumbon}}</p>
										</td>
									</template>
								</v-data-table>

							</v-flex>
						</v-layout>
						<v-layout row wrap>
							<v-flex xs12>
								<a href="#">
									<v-btn absolute dark fixed fab 	bottom right small color="cyan" class="m-b-35" >
										<v-icon>keyboard_arrow_up</v-icon>
									</v-btn>
								</a>
							</v-flex>
						</v-layout>
					</v-container>
				</v-content>



				<v-dialog v-model="dialogProgress.status" persistent max-width="500px">
					<v-card class="text-xs-center">
						<v-btn class="m-t-30" fab dark large color="white" depressed >
							<v-icon dark color="red" style="font-size:45px!important;">notification_important</v-icon>
						</v-btn>
						<v-card-title class="justify-center pt-0">
							<span class="headline"><h4>{{dialogProgress.title}}</h4></span>
						</v-card-title>
						<v-card-text class="p-t-0 p-b-0">
							<v-container grid-list-md class="p-t-0 p-b-0">
								<v-layout wrap>
									<v-flex xs12>
										<v-progress-linear :indeterminate="true"></v-progress-linear>
									</v-flex>
								</v-layout>
							</v-container>
						</v-card-text>
						<v-card-actions class="p-b-20 justify-center">
						</v-card-actions>
					</v-card>
				</v-dialog>

			</div>

			</v-app>

			<!-- start table for export excel -->
			<div style="display:none;" >
				<table id="branchInfo" >
					<thead>
						<tr>
							<th colspan="8">{{topic}}</th>
						</tr>
						<tr>
							<th>ลำดับ</th>
							<th>รหัสสังกัด</th>
							<th>เลขที่ออกหนังสือ</th>
							<th>ชื่อสังกัด</th>
							<th>ชื่อภาษาอังกฤษ</th>
							<th>วันที่เปิดดำเนินการ</th>
							<th>ที่ตั้งสำนักงาน</th>
							<th>รหัสอำเภอและชื่ออำเภอ</th>
							<th>พื้นที่ในความรับผิดชอบ</th>
						</tr>
					</thead>
					<tbody v-for="list ,index in branchDetail">
						<tr>
							<td style="text-align:center;vertical-align: middle;">{{index + 1}}</td>
							<td style="text-align:center;vertical-align: middle;">{{list.org_ode}}</td>
							<td style="text-align:center;vertical-align: middle;">{{list.org_cd}}</td>
							<td style="vertical-align: middle;">{{list.type_des + list.th_org_nme}}</td>
							<td style="vertical-align: middle;">{{list.eng_org_nme}}</td>
							<td style="text-align:center;vertical-align: middle;">{{dateFormat(list.open_date)}}</td>
							<td style="vertical-align: middle;">
								<span>ที่อยู่: {{ list.org_addr }}</span> </br>
								<span>โทรศัพท์: <span v-for="listPhone, indexPhone in list.telephone" class="m-r-3">  {{ listPhone.number }} </span></span> </br>
								<span>โทรสาร: {{ list.fax}} </span> </br>
								<span>WAN: {{ list.wan}} </span>
							</td>
							<td style="text-align:center;vertical-align: middle;">{{unitFormat(list.district, list.unit_nme)}}</td>
							<td style="vertical-align: middle;">
								<span v-for="unit in list.unit_list">{{unit.tumbon}} </br></span>
							</td>
						</tr>
						<!-- <tr> <td colspan="8"></td> </tr> -->
					</tbody>
				</table>
			</div>
			<!-- end table for export excel -->

		</div> <!-- end vuejs -->
	</div> <!-- end data-app -->
</body>
<script type="text/javascript">
Vue.http.options.emulateJSON = true;
Vue.http.options.emulateHTTP = true;
var vuejs = new Vue({
  el:"#vuejs",
	data:{
		pageLoading: true,
		projectName: 'ระบบงานที่ตั้งสำนักงานของธนาคาร',
		sessioncode: '',
		sessionname: '',
		dialogProgress: {status: false, title: ''},
		listDiv: [],
		listProvince: [],
		listBranch: [],
		listType: [],
		branchDetail: [],
		topic: '',
		odInfo: {div: '', province: '', branch: '', type: ''},
		headers: [
			{ text: 'ลำดับ', align: 'center', sortable: false, value: 'index'},
			{ text: 'รหัสสังกัด', align: 'center', sortable: true, value: 'org_ode' },
			{ text: 'เลขที่ออกหนังสือ', align: 'center', sortable: true, value: 'org_cd' },
			{ text: 'ชื่อสังกัด', align: 'center', sortable: true, value: 'th_org_nme' },
			{ text: 'ชื่อภาษาอังกฤษ', align: 'center', sortable: true, value: 'eng_org_nme' },
			{ text: 'วันที่เปิดดำเนินการ', align: 'center', sortable: true, value: 'open_date' },
			{ text: 'ที่ตั้งสำนักงาน', align: 'center', sortable: false, value: 'org_addr' },
			{ text: 'รหัสอำเภอและชื่ออำเภอ', align: 'center', sortable: false, value: 'district' },
			{ text: 'พื้นที่ในความรับผิดชอบ', align: 'center', sortable: false, value: 'district_name' }
		]
	},
  created (){
		this.checkLogin();
  },
  computed:
  {
  },
	mounted () {
	},
	beforeDestroy () {
		clearInterval(this.interval)
	},
  watch: {
  },
	methods:{
		// start method
		progressDialog (status, title) {
			this.dialogProgress.status = status;
			this.dialogProgress.title = title;
		},
		checkLogin () {
			this.$http.post('<?=base_url() ?>index.php/loginController/getSession').then((response) => {
				var userInfo = response.body;
				if (userInfo.emp_code != "nodata") {
					this.sessioncode = userInfo.emp_code;
					this.sessionname = userInfo.emp_name;
					setTimeout(() => { this.pageLoading = false; $(".none-show").removeClass("none-show");
					this.loadDivInfo();
					this.loadTypeInfo();}, 1000)
				}
				else {
					this.pageLoading = false;
					swal({
						title: 'กรุณาเข้าสู่ระบบก่อนเข้าใช้งาน',
						text: '',
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.href = '<?=base_url() ?>';
					})
				}
			}, (response) => {});
		},
		loadDivInfo () {
			this.listDiv = [];
			this.odInfo.div = '';
			this.odInfo.province = '';
			this.odInfo.branch = '';
			this.$http.post('<?=base_url() ?>index.php/odController/loadDivInfo').then((response) => {
				this.listDiv = response.body;
				this.listDiv.unshift({div_nme: '-- ฝ่ายกิจการสาขา --', id: ''})
			}, (response) => {
			});
		},
		loadTypeInfo () {
			this.listType = [];
			this.$http.post('<?=base_url() ?>index.php/odController/loadTypeInfo').then((response) => {
				this.listType = response.body;
				this.listType.unshift({brnch_type: '', type_des: '-- ทุกประเภท --'})
			}, (response) => {
			});
		},
		loadProvinceInfo () {
			this.listProvince = [];
			this.odInfo.province = '';
			this.odInfo.branch = '';
			if(this.odInfo.div != '')
			{
				this.progressDialog(true, 'กำลังดำเนินการ กรุณารอสักครู่');
				this.$http.post('<?=base_url() ?>index.php/odController/loadProvinceInfo', {div: this.odInfo.div}).then((response) => {
					this.listProvince = response.body;
					this.listProvince.unshift({brname: '-- สนจ --', brcode: '', cat_cc: ''})
					setTimeout(() => { this.progressDialog(false, null); }, 300)
				}, (response) => {
				});
			}
		},
		loadBranchInfo () {
			this.listBranch = [];
			this.odInfo.branch = '';
			if(this.odInfo.province != '')
			{
				this.progressDialog(true, 'กำลังดำเนินการ กรุณารอสักครู่');
				this.$http.post('<?=base_url() ?>index.php/odController/loadBranchInfo', {province: this.odInfo.province}).then((response) => {
					this.listBranch = response.body;
					this.listBranch.unshift({brname: '-- สาขา --', brcode: ''})
					setTimeout(() => { this.progressDialog(false, null); }, 300)
				}, (response) => {
				});
			}
		},
		loadBranchDetail () {
			this.branchDetail = [];
			if(this.odInfo.div == '') {
				swal({
					title: 'กรุณาเลือกฝ่ายกิจการสาขา',
					text: '',
					type: 'warning',
					showCancelButton: false,
					confirmButtonColor: '#3085d6',
					confirmButtonText: 'ตกลง',
					allowOutsideClick: false
				})
			}
			else {
				this.progressDialog(true, 'กำลังดำเนินการ กรุณารอสักครู่');
				this.$http.post('<?=base_url() ?>index.php/odController/loadBrandDetail', {odInfo: this.odInfo}).then((response) => {
					this.branchDetail = response.body;
					if(this.branchDetail.length == 0) {
						this.progressDialog(false, null);
						swal({
							title: 'ไม่พบข้อมูล',
							text: '',
							type: 'warning',
							showCancelButton: false,
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'ตกลง',
							allowOutsideClick: false
						})
					}
					else {
						var div = $("#divnme").val().replace("-- ฝ่ายกิจการสาขา --", "");
						var prov = $("#provnme").val().replace("-- สนจ --", "");
						var brn = $("#brnnme").val().replace("-- สาขา --", "");
						this.topic = "รายชื่อสังกัด " + div + ' ' + prov + ' ' + brn + ' ';
					}
					setTimeout(() => { this.progressDialog(false, null); }, 2000)
				}, (response) => {});
			}
		},
		dateFormat (str) {
			if (str == '00000000')
				return '00/00/0000';
			else if (str != '' || str != null)
				return str.substr(6, 2) + '/' + str.substr(4, 2) + '/' + (parseInt(str.substr(0, 4)) + 543).toString();
			else
				return null;

		},
		unitFormat (unitCode, unitName) {
			if(unitCode != null)
				return unitCode.substr(0, 2) + '-' + unitCode.substr(2, 2) + ' ' + unitName;
			else
				return null;
		},
		DoReport() {
			this.progressDialog(true, 'กำลังดำเนินการ กรุณารอสักครู่');
			var date = new Date();
			var hours = date.getHours();
			var minutes = date.getMinutes();
			var day = date.getDate();
			var month = date.getMonth()+1;

			day = day < 10 ? '0'+day : day;
			month = month < 10 ? '0'+month : month;
			hours = hours < 10 ? '0'+hours : hours; // the hour '0' should be '12'
			minutes = minutes < 10 ? '0'+minutes : minutes;

			var strTime = hours + ':' + minutes;
			var strDT = day + "/" + month + "/" + date.getFullYear() + "  " + strTime;

			this.createPDFMake (strDT, this.branchDetail);
		},
		createPDFMake (strDT, result) {
			data = this.table(result, ['org_ode', 'ord_cd' , 'nme_show', 'eng_org_nme', 'open_date', 'org_addr', 'telephone'
			, 'fax', 'wan', 'district', 'unit_nme', 'unit_list']);
			pdfMake.fonts = {
					Angsana: {
						normal: 'angsa.ttf'
					}
			}
			var docDefinition = {
				defaultStyle: {
					font: 'Angsana',
					fontSize: 13
				},
				pageSize: 'A4',
				pageOrientation: 'landscape',
				pageMargins: [ 15, 20, 15, 35],
				content: [
					{ text: 'ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร ', style: 'header', alignment: 'center', fontSize: 16 },
					{ text: this.topic, style: 'header', alignment: 'center', fontSize: 16 },
					{ text: ' ' , style: 'header', alignment: 'center', fontSize: 14 },
					data
				],
				footer: function(page, pages) {
					return {
						columns: [
							{
								text: 'พิมพ์เมื่อวันที่ ' + strDT
							},
							{
								alignment: 'right',
								text: [
									{
										text: 'หน้า '
									},
									{
										text: page.toString()
									}
								]
							}
						],
						margin: [ 20, 0, 20, 0 ]
					}
				} //end footer

			}

			pdfMake.createPdf(docDefinition).open();
			this.progressDialog(false, null);
		},
		buildTableBody(data, columns) {
				var body = [];
				var head = [
					{
						text: 'ลำดับ',
						alignment: 'center',
						fontSize: 14
					},
					{
						text: 'รหัสสังกัด',
						alignment: 'center',
						fontSize: 14
					},
					{
						text: 'เลขที่ออกหนังสือ',
						alignment: 'center',
						fontSize: 14
					},
					{
						text: 'ชื่อสังกัด',
						alignment: 'center',
						fontSize: 14
					},
					{
						text: 'ชื่อภาษาอังกฤษ',
						alignment: 'center',
						fontSize: 14
					},
					{
						text: 'วันที่เปิดดำเนินการ',
						alignment: 'center',
						fontSize: 14
					},
					{
						text: 'ที่ตั้งสำนักงาน',
						alignment: 'center',
						fontSize: 14
					},
					{
						text: 'รหัสอำเภอและชื่ออำเภอ',
						alignment: 'center',
						fontSize: 14
					},
					{
						text: 'พื้นที่ในความรับผิดชอบ',
						alignment: 'center',
						fontSize: 14
					}
				];
				body.push(head);
				var i = 1;
				var me = this;
				data.forEach(function(row) {
						var dataRow = [];
						// เพิ่มข้อมูล โทรศัพท์ โทรสาร และ wan
						var telList = ' โทรศัพท์:  ';
						var fax = ' โทรสาร:  ' + row.fax;
						var wan = ' WAN:  ' + row.wan;
						row.telephone.forEach(function(rowTel) {
							telList = telList + rowTel.number + ', ';
						})
						//เพิ่มพื้นที่ในความรับผิดชอบ
						var listUnit = [];
						var bodyUnit = [];
						row.unit_list.forEach(function(rowUnit) {
							listUnit.push({'text': rowUnit.tumbon, 'alignment': 'left'});
						})
						bodyUnit.push(listUnit)

						dataRow.push({'text': i, 'alignment': 'center'});
						dataRow.push({'text': row.org_ode, 'alignment': 'center'});
						dataRow.push({'text': row.org_cd, 'alignment': 'center'});
						dataRow.push({'text': row.nme_show, 'alignment': 'left'});
						dataRow.push({'text': row.eng_org_nme, 'alignment': 'left'});
						dataRow.push({'text': me.dateFormat(row.open_date), 'alignment': 'center'});
						dataRow.push({ layout: 'noBorders', table: { body: [ [row.org_addr], [telList.slice(0, -2)], [fax], [wan] ] }});
						dataRow.push({'text': me.unitFormat(row.district, row.unit_nme), 'alignment': 'center'});
						dataRow.push({ layout: 'noBorders', table: {body: [bodyUnit] }  });

						body.push(dataRow);
						i = i + 1
				});

				return body;
		},
		table (data, columns) {
			var me = this
				return {
						table: {
								headerRows: 1,
								widths: [25, 55, 38, 140, 125, 40, 160, 55, 100],
								alignment: 'center',
								body: me.buildTableBody(data, columns)
						}
				};
		}
  // end methods
  }
})
</script>
<script type="text/javascript">
  $("#exportBranch").click(function(){
    $("#branchInfo").table2excel({
    name: "Excel Document Name",
    filename: vuejs.topic,
    fileext: ".xls",
    exclude_img: true,
    exclude_links: true,
    exclude_inputs: true
  });
  });
</script>
</html>
