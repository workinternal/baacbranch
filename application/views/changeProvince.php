<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicons -->
	<link href="<?php echo $this->config->item('icon_project'); ?>" rel="icon">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/vuetify/vuetify.css" />
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/util.css">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/custom.css">
	<title><?php echo $this->config->item('project_name'); ?></title>

	<style type="text/css">
		.v-stepper--alt-labels .v-stepper__step {
			flex-basis: 95% !important;
		}
		.box-area {
			background: #eee;
			padding: 30px 15px 0px 15px;
			border-radius: 5px;
		}
	</style>
</head>
<body>

	<div data-app="true" class="application--light">
		<div class="none-show" ><?php $this->load->view('header.php'); ?></div>
		<div id="vuejs">
			<v-app   class="white-bg" >
				<!-- loading -->
				<div v-show="pageLoading" class="loading-page">
					<v-content>
						<v-container fluid fill-height>
							<v-layout justify-center align-center>
								<v-progress-circular :size="200" :width="20" color="primary" indeterminate></v-progress-circular>
							</v-layout>
						</v-container>
					</v-content>
				</div>
				<!-- content -->
				<div class="none-show">

					<!-- content -->
					<v-content style="background-color: #fff;">
						<v-container fluid  class="p-t-0 p-l-0 p-r-0">
							<v-layout  row wrap>
								<v-flex xs10 offset-xs1 class="pt-2 text-xs-center">
									<span><i class="material-icons icon-title-small p-t-5 p-b-5">border_color</i></span> <h2>แก้ไขข้อมูลสาขา</h2>
								</v-flex>
								<!-- หัวข้อ-->
								<v-flex xs10 offset-xs1 class="m-t-5 m-b-5 text-xs-center">
									<div class="p-l-50  p-r-50 p-b-50">
										<v-stepper v-model="currentStep" alt-labels>
											<v-stepper-header>
												<template v-for="n in steps">
												<v-stepper-step :edit-icon="'check'" :complete="currentStep > n" :key="`${n}-step`" :step="n" >{{headerStep[n-1]}}</v-stepper-step>
													<!-- <v-divider v-if="n !== steps" :key="n"></v-divider> -->
												</template>
											</v-stepper-header>
										<v-stepper-items>
											<!-- step 1 -->
											<v-stepper-content step="1">
												<div class="text-xs-center" v-show="isSetBranch == false">
													<h4 style="color: #D32F2F;">* กรุณาเลือกสาขาที่ต้องการเปลี่ยนสำนักงานจังหวัด</h4>
														<v-btn outline round color="primary" v-on:click="openDialogSearchBranch()">ค้นหาสาขา</v-btn>
												</div>
												<v-card  class="mb-5 " v-show="isSetBranch == true">
													<!-- select branch -->
													<v-layout row wrap class="mb-2">
														<v-flex xs10 class="text-xs-right p-t-15">
															<h4 style="color: #D32F2F;">* กรุณาเลือกสาขาที่ต้องการเปลี่ยนสำนักงานจังหวัด</h4>
														</v-flex>
														<v-flex xs2 class="text-xs-center">
															<v-btn outline round color="primary" v-on:click="openDialogSearchBranch()">ค้นหาสาขา</v-btn>
														</v-flex>
													</v-layout>
													<!-- ข้อมุลสาขา -->
													<v-layout row wrap class="mb-2 mt-5">
														<v-flex xs12 class="text-xs-left">
															<h3><v-icon large color="blue-grey darken-2" style="font-size:25px;color: #C62828 !important;">chrome_reader_mode</v-icon> <span>ข้อมูลสาขาที่ต้องการเปลี่ยนสำนักงานจังหวัด</span></h3>
														</v-flex>
													</v-layout>
													<!-- รหัส และ ชื่อไทย-->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">รหัสสาขา</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field disabled box single-line   v-model="odInfo.code"  ></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">ชื่อสาขา</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field disabled box single-line   v-model="odInfo.is_thai_n"  ></v-text-field>
													</v-layout>
													<!-- information -->
													<v-layout row wrap class="mb-2">
														<v-flex xs12 class="text-xs-left">
															<h3><v-icon large color="blue-grey darken-2" style="font-size:25px;color: #C62828 !important;">chrome_reader_mode</v-icon> <span>ข้อมูลสำนักงานจังหวัด</span></h3>
														</v-flex>
													</v-layout>
													<!-- ฝ่าย / สนจ -->
													<v-layout row wrap class="m-b-45">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ฝ่ายกิจการสาขา</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-autocomplete  box single-line  :items="listDiv" item-text="div_nme" item-value="id" v-model="provinceInfo.is_div" menu-props="auto"  hide-details  single-line
															v-validate="'required'"  :error-messages="errors.collect('step1.is_div')" data-vv-name="is_div" data-vv-scope="step1" v-on:change="loadProvinceInfo()">
															</v-autocomplete>
														</v-flex>
														<v-flex xs2 class="text-xs-left">
															<h4 class="pt-3">สำนักงานจังหวัด</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-autocomplete  box single-line  :items="listProvince" item-text="brname" item-value="brcode" v-model="provinceInfo.brcode" menu-props="auto"  hide-details  single-line
															v-validate="'required'"  :error-messages="errors.collect('step1.is_br')" data-vv-name="is_br" data-vv-scope="step1" v-on:change="setProvinceInfo()">
															</v-autocomplete>
														</v-flex>
													</v-layout>
													<!-- ชื่อไทย / ชื่ออังกฤษ -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">รหัสสำนักงาน</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field disabled box single-line v-model="provinceInfo.code" ></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">ชื่อสำนักงาน</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field disabled box single-line  v-model="provinceInfo.is_thai_n"></v-text-field>
														</v-flex>
													</v-layout>
													<!-- ชื่อย่อ / วันที่เปิดดำเนินการ  -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ชื่อย่อสำนักงาน</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field disabled box single-line   v-model="provinceInfo.is_amp_n"  ></v-text-field>
														</v-flex>
													</v-layout>

												</v-card>
												<v-card-actions class="text-xs-right" v-show="isSetBranch == true">
													<v-spacer></v-spacer>
													<v-btn round color="primary" @click.native="validateBeforeNextPage('step1')">เปลี่ยนสำนักงานจังหวัด</v-btn>
													<v-spacer></v-spacer>
												</v-card-actions>
											</v-stepper-content>
										</v-stepper-items>
									</v-stepper>
								</div>
								</v-flex>

							</v-layout>
						</v-container>
					</v-content>

					<v-dialog v-model="dialogProgress.status" persistent max-width="500px">
						<v-card class="text-xs-center">
							<v-btn class="m-t-30" fab dark large color="white" depressed >
								<v-icon dark color="red" style="font-size:45px!important;">notification_important</v-icon>
							</v-btn>
							<v-card-title class="justify-center pt-0">
								<span class="headline"><h4>{{dialogProgress.title}}</h4></span>
							</v-card-title>
							<v-card-text class="p-t-0 p-b-0">
								<v-container grid-list-md class="p-t-0 p-b-0">
									<v-layout wrap>
										<v-flex xs12>
											<v-progress-linear :indeterminate="true"></v-progress-linear>
										</v-flex>
									</v-layout>
								</v-container>
							</v-card-text>
							<v-card-actions class="p-b-20 justify-center">
							</v-card-actions>
						</v-card>
					</v-dialog>

					<v-dialog v-model="dialogSearchBranch" persistent max-width="700px">
						<v-card>
							<v-card-title>
								<span class="headline"><h6>กรุณาเลือกสาขาที่ต้องการเปลี่ยนสำนักงานจังหวัด</h6></span>
							</v-card-title>
							<v-card-text>
								<v-container class="text-xs-center">
									<v-layout row wrap>
										<v-flex xs12 class="text-xs-center" style="padding-left:15%">
											<v-autocomplete
												box
												autofocus
												:items="listDivDialog"
												label="ฝ่ายกิจการสาขา"
												v-model="odInfoDialog.div"
												item-text="div_name" item-value="id"
												v-on:change="loadProvinceDialogSearch"

											></v-autocomplete>
										</v-flex>
										<v-flex xs12 class="text-xs-center" style="padding-left:15%">
											<v-autocomplete
												box
												autofocus
												:items="listProvinceDialog"
												label="สำนักงานจังหวัด"
												v-model="odInfoDialog.prov"
												item-text="brname" item-value="brcode"
												v-on:change="loadBranchDialogSearch"

											></v-autocomplete>
										</v-flex>
										<v-flex xs12 class="text-xs-center" style="padding-left:15%">
											<v-autocomplete
												box
												autofocus
												:items="listBranchDialog"
												label="สาขา"
												v-model="branchActive"
												item-text="display_name" item-value="id"
											></v-autocomplete>
										</v-flex>
									</v-layout>
								</v-container>
							</v-card-text>
							<v-card-actions>
								<v-btn color="grey darken-2" flat v-on:click="closeDialogSearchBranch()">ปิด</v-btn>
								<v-spacer></v-spacer>
								<v-btn color="blue darken-2" flat v-on:click="setBranch()">ตกลง</v-btn>
							</v-card-actions>
						</v-card>
					</v-dialog>

				</div>

			</v-app>
		</div> <!-- end vuejs -->
	</div> <!-- end data-app -->

</body>
<script type="text/javascript">
Vue.http.options.emulateJSON = true;
Vue.http.options.emulateHTTP = true;
Vue.use(VeeValidate)
var vuejs = new Vue({
  el:"#vuejs",
	$_veeValidate: {
		validator: 'new'
	},
	data:{
		pageLoading: true,
		projectName: '',
		sessioncode: '',
		sessionname: '',
		dialogProgress: {status: false, title: ''},
		dialogSearchBranch: false,
		dictionary: {
			attributes: {
				// email: 'E-mail Address'
			},
			custom: {
					is_div: { required: () => 'กรุณาเลือกฝ่ายกิจการสาขา' },
					is_br: { required: () => 'กรุณาเลือกสำนักงานจังหวัด' }
			}
		},
		currentStep: 1,
		steps: 1,
		headerStep: ['ข้อมูลสำนักงานจังหวัด', 'ข้อมูลสาขา'],
		listBranchName: [],
		branchActive: '',
		isSetBranch: false,
		listDiv: [],
		listProvince: [],
		listBranch: [],
		listDivDialog: [],
		listProvinceDialog: [],
		listBranchDialog: [],
		odInfoDialog: { div: '', prov: ''},
		branchDetail: [],
		odInfo: {is_div: '', is_div_nme: '', province: '', prov_nme: '', is_br: '', is_sbr: '', is_ch: '00', is_am: '00', is_thai_n: '', is_eng_n: '', is_open_dte: '', is_amp_n: '', code: '', prov_code: ''},
		modal: false,
		odInfoTemp: [],
		// listProvinceAddr: [],
		listDistrictAddr: [],
		listSubDistrictAddr: [],
		ctrBRInfo: {fax: '', wan: '' },
		telBRInfo: [{number: ''}],
		provinceInfo: {is_div: '', is_thai_n: '', is_eng_n: '', is_amp_n: '', is_open_dte: '', is_br: '', code: '', brcode: '', id: '', divid: ''},
		provinceAddr: {province: '', amphur: '', tumbon: '', zipcode: '', addr: '', moo: '', soi: '', street: '', prov_nme: '', dist_nme: '', subdist_nme: ''},
		provinceListProv: [],
		provinceListAmphur: [],
		provinceListTumbon: []
	},
  created (){
		this.checkLogin();
  },
  computed:
  {
  },
	mounted () {
		this.$validator.localize('th', this.dictionary);
	},
  watch: {
		steps (val) {
			if (this.currentStep > val) {
				this.currentStep = val;
			}
		},
  },
	methods:{
		// start method
		progressDialog (status, title) {
			this.dialogProgress.status = status;
			this.dialogProgress.title = title;
		},
		checkLogin () {
			this.$http.post('<?=base_url() ?>index.php/loginController/getSession').then((response) => {
				var userInfo = response.body;
				if (userInfo.emp_code != "nodata") {
					this.sessioncode = userInfo.emp_code;
					this.sessionname = userInfo.emp_name;
					this.loadDivDialogSearch();
					setTimeout(() => { this.pageLoading = false; $(".none-show").removeClass("none-show"); }, 1000)
				}
				else {
					this.pageLoading = false;
					swal({
						title: 'กรุณาเข้าสู่ระบบก่อนเข้าใช้งาน',
						text: '',
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.href = '<?=base_url() ?>';
					})
				}
			}, (response) => {});
		},
		openDialogSearchBranch () {
			setTimeout(() => {
				this.dialogSearchBranch = true;
				this.isSetBranch = false;
			}, 500)
		},
		closeDialogSearchBranch () {
			setTimeout(() => {
				this.dialogSearchBranch = false;
				if (this.branchActive != '' && this.odInfoTemp.length != 0)
					this.isSetBranch = true;
			}, 500)
		},
		loadDivDialogSearch () {
			this.listDivDialog = [];
			this.odInfoDialog.div = '';
			this.odInfoDialog.prov = '';
			this.branchActive = '';
			this.$http.post('<?=base_url() ?>index.php/odController/loadDivInfo').then((response) => {
				this.listDivDialog = response.body;
			}, (response) => {
			});
		},
		loadProvinceDialogSearch () {
			this.listProvinceDialog = [];
			this.odInfoDialog.prov = '';
			this.branchActive = '';
			if(this.odInfoDialog.div != '')
			{
				this.progressDialog(true, 'กำลังดำเนินการ กรุณารอสักครู่');
				this.$http.post('<?=base_url() ?>index.php/odController/loadProvinceInfo', {div: this.odInfoDialog.div}).then((response) => {
					this.listProvinceDialog = response.body;
					setTimeout(() => { this.progressDialog(false, null); }, 300)
				}, (response) => {
				});
			}
		},
		loadBranchDialogSearch () {
			this.listBranchDialog = [];
			this.branchActive = '';
			if(this.odInfoDialog.prov != '')
			{
				this.progressDialog(true, 'กำลังดำเนินการ กรุณารอสักครู่');
				this.$http.post('<?=base_url() ?>index.php/editBranchController/getListBranchName', {id: this.odInfoDialog.prov}).then((response) => {
					this.listBranchDialog = response.body;
					setTimeout(() => { this.progressDialog(false, null); }, 300)
				}, (response) => {
				});
			}
		},
		setBranch () {
			if (this.odInfoDialog.div == '' || this.odInfoDialog.prov == '' || this.branchActive == '') {
				swal({
					title: 'กรุณากรอกข้อมูลให้ครบถ้วน',
					text: "",
					type: 'warning',
					showCancelButton: false,
					confirmButtonColor: '#3085d6',
					confirmButtonText: 'ตกลง',
					cancelButtonText: 'ยกเลิก',
				}).then((result) => {})
			}
			else {
				this.odInfoTemp = [];
				this.odInfo = {is_div: '', is_div_nme: '', province: '', prov_nme: '', is_br: '', is_sbr: '', is_ch: '00', is_am: '00', is_thai_n: '', is_eng_n: '', is_open_dte: '', is_amp_n: '', code: ''};
				this.odAddrBR = {province: '', amphur: '', tumbon: '', zipcode: '', addr: '', moo: '', soi: '', street: '', prov_nme: '', dist_nme: '', subdist_nme: ''};
				this.ctrInfo = {fax: '', wan: '' };
				this.telInfo = [{number: ''}];
				this.dialogSearchBranch = false;
				this.progressDialog(true, 'กำลังดำเนินการ กรุณารอสักครู่');
				this.$http.post('<?=base_url() ?>index.php/editBranchController/getBranchInformation', { id: this.branchActive }).then((response) => {

					var result = response.body;

					this.odInfoTemp = result[0];
					this.odInfo.is_div_nme = this.odInfoTemp.div_nme;
					this.odInfo.prov_nme = this.odInfoTemp.brname;
					this.odInfo.is_div = '0' + 	this.odInfoTemp.is_div;
					this.odInfo.is_br =  this.odInfoTemp.is_br;
					this.odInfo.is_sbr = this.odInfoTemp.is_sbr;
					this.odInfo.is_ch =  this.odInfoTemp.is_ch;
					this.odInfo.is_am =  this.odInfoTemp.is_am;
					this.odInfo.code = this.odInfoTemp.is_br + ' - ' + this.odInfoTemp.is_sbr + ' - ' + this.odInfoTemp.is_ch + ' - ' + this.odInfoTemp.is_am;
					this.odInfo.prov_code = this.odInfoTemp.prov_code;
					this.odInfo.province = this.odInfoTemp.is_br + 'A';
					this.odInfo.is_thai_n = this.odInfoTemp.is_thai_n;
					this.odInfo.is_eng_n = this.odInfoTemp.is_eng_n;
					this.odInfo.is_amp_n = this.odInfoTemp.is_amp_n;

					this.loadDivInfo();
					// this.getProvinceListProvince();
					setTimeout(() => {
						this.isSetBranch = true;
						setTimeout(() => {
							this.progressDialog(false, null);
						}, 1000)
					}, 3000)

				}, (response) => {
				});
			}

		},
		setCodeBranch() {
			var me = this;
			var provinceCode = this.odInfo.province.slice(0, -1);
			this.getListProvince();
		},
		loadDivInfo () {
			this.listDiv = [];
			this.listProvince = [];
			this.provinceInfo.is_br = '';
			this.$http.post('<?=base_url() ?>index.php/odController/loadDivInfo').then((response) => {
				this.listDiv = response.body;
				this.listDiv.unshift({div_nme: '-- ฝ่ายกิจการสาขา --', id: ''})

			}, (response) => {
			});
		},
		loadProvinceInfo () {
			this.listProvince = [];
			this.$http.post('<?=base_url() ?>index.php/odController/loadProvinceInfo', {div: this.provinceInfo.is_div }).then((response) => {
				this.listProvince = response.body;
				this.listProvince.unshift({brname: '-- สำนักงานจังหวัด --', brcode: ''})
			}, (response) => {
			});
		},
		setProvinceInfo () {
			var provinceID = this.provinceInfo.brcode + '0000';
			this.$http.post('<?=base_url() ?>index.php/editProvincialController/getBranchInformation', {id: provinceID  }).then((response) => {
				var result = response.body;

				this.provinceInfo.code = result[0].is_br + ' - ' + result[0].is_sbr + ' - ' + result[0].is_ch + ' - ' + result[0].is_am;
				this.provinceInfo.id = result[0].prov_code;
				this.provinceInfo.is_thai_n = result[0].brname;
				this.provinceInfo.is_amp_n = result[0].is_amp_n;
				this.provinceInfo.is_br = result[0].is_br;
				this.provinceInfo.divid = result[0].is_div;
				// console.log(this.provinceInfo);
			}, (response) => {
			});
		},
		dateFormat (str) {
			if (str == null || str == '' || str == ' ') {
				return null;
			}
			else {
				var dataStr = str.substr(0, 4) + '-' + str.substr(4, 2) + '-' + str.substr(6, 2);
				return dataStr.toString();
			}
		},
		validateBeforeNextPage(scope) {
			this.$validator.validateAll(scope).then((result) => {
				if (result) {

						swal({
							title: 'ยืนยันการย้ายสำนักงานจังหวัด',
							text: '',
							type: 'warning',
							showCancelButton: true,
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'ตกลง',
							cancelButtonText: 'ยกเลิก',
							allowOutsideClick: false,
						}).then((result1) => {
							if(result1.value) {
								this.progressDialog(true, 'กำลังดำเนินการ กรุณารอสักครู่');
								setTimeout(() => { this.submitUpdateBranchInformation(); }, 500)
							}

						})


				}
				else {
					swal({
						title: 'กรุณาตรวจสอบข้อมูลให้ถูกต้อง',
						text: '',
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result1) => {
							window.location.href = "#";
					})

				}
			});
		},
		submitUpdateBranchInformation () {
			this.$http.post('<?=base_url() ?>index.php/editBranchController/changeProvince', {odInfo: this.odInfo, provinceInfo: this.provinceInfo}).then((response) => {
				console.log(response.body);
				var result = response.body;
				if (result == 'Successfully') {

					swal({
						title: 'ย้ายสำนักงานเรียบร้อยแล้ว',
						text: '',
						type: 'success',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.reload();
						this.isSetBranch = false;
					})
				}
				else {
					swal({
						title: 'ไม่สามารถย้ายสำนักงานได้',
						text: 'กรุณาลองใหม่อีกครั้ง',
						type: 'error',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.href = "#";
					})
				}
			}, (response) => {});
		}
  // end methods
  }
})
</script>
</html>
