<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicons -->
	<link href="<?php echo $this->config->item('icon_project'); ?>" rel="icon">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/vuetify/vuetify.css" />
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/util.css">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/custom.css">
	<title><?php echo $this->config->item('project_name'); ?></title>

	<style type="text/css">
		table.v-table tbody td {
			border: 1px solid #ddd !important;
		}
		.box-area {
			border: 1px solid #ddd !important;
    	padding: 20px !important;
    	border-radius: 5px !important;
    	margin-top: 20px !important;
		}
		.theme--light.v-table thead th {
    	border: 1px solid #ddd !important;
    	background-color: #f2f2f2 !important;
    	color: #000 !important;
			/* font-family: 'Kanit', sans-serif !important; */
    	font-weight: normal  !important;
			font-size: 15px;
		}
		table.v-table tbody td {
			/*font-family: 'Kanit', sans-serif !important; */
			font-size: 14px !importan;
		}
	</style>
</head>
<body>
	<div data-app="true" class="application--light">
		<div class="none-show" ><?php $this->load->view('header.php'); ?></div>
		<div id="vuejs">
			<v-app  class="white-bg" >
				<!-- loading -->
				<div v-show="pageLoading" class="loading-page">
					<v-content>
						<v-container fluid fill-height>
							<v-layout justify-center align-center>
								<v-progress-circular :size="200" :width="20" color="primary" indeterminate></v-progress-circular>
							</v-layout>
						</v-container>
					</v-content>
				</div>
				<!-- content -->
				<div class="none-show">

					<!-- content -->
					<v-content style="background-color: #fff;">
						<v-container fluid  class="p-t-0 p-l-0 p-r-0">
							<v-layout  row wrap>
								<v-flex xs10 offset-xs1 class="pt-4 text-xs-center">
									<span><i class="material-icons icon-title-big">not_interested</i></span> <h2>ยกเลิกกิจการสำนักงานจังหวัด</h2>
								</v-flex>
								<!-- หัวข้อ-->
								<v-flex xs10 offset-xs1 class="m-t-5 m-b-5 text-xs-center">
									<div class="p-l-50 p-t-0 p-r-50 p-b-50">
										<div class="text-xs-center" >
											<h4 style="color: #D32F2F;">* กรุณาเลือกสำนักงานจังหวัดที่ต้องการยกเลิกกิจการ</h4>
											<v-btn outline round color="primary" v-on:click="openDialogSearchBranch()">ค้นหาสำนักงานจังหวัด</v-btn>
										</div>
										<v-card  class="box-area" >
											<!-- ฝ่าย / สนจ -->
											<v-layout row wrap class="mb-3">
												<v-flex xs1 ></v-flex>
												<v-flex xs2 class="text-xs-left">
													<h4  class="font-bold">ฝ่ายกิจการสาขา:</h4>
												</v-flex>
												<v-flex xs4 class="text-xs-left">
													<h4 > {{odInfo.is_div_nme.replace('ฝ่ายกิจการสาขา', '')}} </h4>
												</v-flex>
											</v-layout>
											<!-- รหัสสังกัด -->
											<v-layout row wrap class="mb-3">
												<v-flex xs1 ></v-flex>
												<v-flex xs2 class="text-xs-left">
													<h4  class="font-bold">รหัสสำนักงานจังหวัด:</h4>
												</v-flex>
												<v-flex xs4 class="text-xs-left">
													<h4 > {{odInfo.code}}   <span v-show="odInfo.prov_code != ''">( {{odInfo.prov_code}} )</span></h4>
												</v-flex>
											</v-layout>
											<!-- ชื่อไทย / ชื่ออังกฤษ -->
											<v-layout row wrap class="mb-3">
												<v-flex xs1 ></v-flex>
												<v-flex xs2 class="text-xs-left">
													<h4 class="font-bold">ชื่อสำนักงานจังหวัด:</h4>
												</v-flex>
												<v-flex xs9 class="text-xs-left">
													<h4 > {{odInfo.is_thai_n}} <span v-show="odInfo.is_eng_n != ''">( {{odInfo.is_eng_n}} )</span></h4>
												</v-flex>
											</v-layout>
											<!-- ชื่อไทย / ชื่ออังกฤษ -->
											<v-layout row wrap class="mb-3">
												<v-flex xs1 ></v-flex>
												<v-flex xs2 class="text-xs-left">
													<h4  class="font-bold">ชื่อย่อสำนักงานจังหวัด:</h4>
												</v-flex>
												<v-flex xs9 class="text-xs-left">
													<h4 > {{odInfo.is_amp_n}} </h4>
												</v-flex>
											</v-layout>
											<!-- เลขที่ หมู่ -->
											<v-layout row wrap class="mb-3">
												<v-flex xs1 ></v-flex>
												<v-flex xs2 class="text-xs-left">
													<h4  class="font-bold">ที่อยู่:</h4>
												</v-flex>
												<v-flex xs9 class="text-xs-left">
													<h4  class="" v-show="odAddrBR.addr != ''">เลขที่ {{odAddrBR.addr}} หมู่ {{odAddrBR.moo}} ซอย {{odAddrBR.soi}} ถนน {{odAddrBR.street}} ตำบล {{odAddrBR.tumbon}} อำเภอ {{odAddrBR.amphur}} จังหวัด {{odAddrBR.province}} {{odAddrBR.zipcode}}</h4>
												</v-flex>
											</v-layout>
											<!-- เบอร์โทร -->
											<v-layout row wrap class="mb-3" v-for=" (item, index) in telInfo " :key="index">
												<v-flex xs1 > </v-flex>
												<v-flex xs2 class="text-xs-left">
													<h4  class="font-bold">โทรศัพท์:</h4>
												</v-flex>
												<v-flex xs3 class="text-xs-left">
													<h4  class=""> {{item.number}} </h4>
												</v-flex>
											</v-layout>
											<!-- wan โทรสาร -->
											<v-layout row wrap class="mb-3">
												<v-flex xs1 > </v-flex>
												<v-flex xs2 class="text-xs-left">
													<h4  class="font-bold">WAN:</h4>
												</v-flex>
												<v-flex xs4 class="text-xs-left">
													<h4  class=""> {{ctrInfo.wan}} </h4>
												</v-flex>
											</v-layout>
											<!-- wan โทรสาร -->
											<v-layout row wrap class="mb-3">
												<v-flex xs1 > </v-flex>
												<v-flex xs2 class="text-xs-left">
													<h4  class="font-bold">โทรสาร:</h4>
												</v-flex>
												<v-flex xs4 class="text-xs-left">
													<h4  class=""> {{ctrInfo.fax}} </h4>
												</v-flex>
											</v-layout>
										</v-card>
									</div>
								</v-flex>

								<v-flex xs10 offset-xs1 class="m-t-5 m-b-5 text-xs-center">

										<v-card  class="" >
											<!-- ตารางหน่วย-->
											<v-layout row wrap class="mb-3">
												<v-flex xs12 >
													<v-data-table
													:headers="headers"
													:items="provinceDetail"
													class="elevation-1"
													hide-actions
													>
													<template slot="items" slot-scope="props">
														<td class="text-xs-center table-text"	>{{ props.index + 1 }}</td>
														<td class="text-xs-center table-text" 	style="width:150px;">{{ props.item.org_ode }}</td>
														<td class="text-xs-left table-text"		>{{ props.item.type_des + props.item.th_org_nme}}</td>
														<td class="text-xs-left table-text"		>{{ props.item.eng_org_nme }}</td>
														<td class="text-xs-center table-text" 		style="width: 120px;">{{ dateFormat(props.item.open_date) }}</td>
														<td class="text-xs-left table-text" 	style="width: 300px;">
															<p class="m-b-10 m-t-5"		>ที่อยู่: {{ props.item.org_addr }}</p>
															<p class="m-b-10" v-show="props.item.telephone.length > 0" v-for="listPhone, index in props.item.telephone">โทรศัพท์: {{ listPhone.number }} </p>
															<p class="m-b-10" v-show=" props.item.fax != '' ">โทรสาร: {{ props.item.fax}} </p>
															<p class="m-b-10" v-show=" props.item.wan != ''" >WAN: {{ props.item.wan}} </p>
														</td>
														<td class="text-xs-cente table-textr" style="width: 120px;">{{ unitFormat(props.item.district, props.item.unit_nme) }}</td>
														<td class="text-xs-left table-text" style="width: 180px;">
															<p v-for="listunit, index in props.item.unit_list">{{listunit.tumbon}}</p>
														</td>
													</template>
												</v-data-table>
												</v-flex>
											</v-layout>
										</v-card>
										<v-card-actions v-show="isSetBranch == true">
											<v-spacer></v-spacer>
											<v-btn round color="red" dark v-on:click="closeBranch()">ยกเลิกกิจการสำนักงานจังหวัด</v-btn>
											<v-spacer></v-spacer>
										</v-card-actions>
								</v-flex>
							</v-layout>
							<v-layout row wrap>
								<v-flex xs12>
									<a href="#">
										<v-btn absolute dark fixed fab 	bottom right small color="cyan" class="m-b-35" >
											<v-icon>keyboard_arrow_up</v-icon>
										</v-btn>
									</a>
								</v-flex>
							</v-layout>
						</v-container>
					</v-content>

					<v-dialog v-model="dialogProgress.status" persistent max-width="500px">
						<v-card class="text-xs-center">
							<v-btn class="m-t-30" fab dark large color="white" depressed >
								<v-icon dark color="red" style="font-size:45px!important;">notification_important</v-icon>
							</v-btn>
							<v-card-title class="justify-center pt-0">
								<span class="headline"><h4>{{dialogProgress.title}}</h4></span>
							</v-card-title>
							<v-card-text class="p-t-0 p-b-0">
								<v-container grid-list-md class="p-t-0 p-b-0">
									<v-layout wrap>
										<v-flex xs12>
											<v-progress-linear :indeterminate="true"></v-progress-linear>
										</v-flex>
									</v-layout>
								</v-container>
							</v-card-text>
							<v-card-actions class="p-b-20 justify-center">
							</v-card-actions>
						</v-card>
					</v-dialog>

					<v-dialog v-model="dialogSearchBranch" persistent max-width="700px">
						<v-card>
							<v-card-title>
								<span class="headline"><h6>กรุณาเลือกสำนักงานจังหวัดที่ต้องการยกเลิกกิจการ</h6></span>
							</v-card-title>
							<v-card-text>
								<v-container class="text-xs-center">
									<v-layout row wrap>
										<v-flex xs12 class="text-xs-center" style="padding-left:15%">
											<v-autocomplete
												box
												autofocus
												:items="listDivDialog"
												label="ฝ่ายกิจการสาขา"
												v-model="odInfoDialog.div"
												item-text="div_name" item-value="id"
												v-on:change="loadProvinceDialogSearch"

											></v-autocomplete>
										</v-flex>
										<v-flex xs12 class="text-xs-center" style="padding-left:15%">
											<v-autocomplete
												box
												autofocus
												:items="listProvinceDialog"
												label="สำนักงานจังหวัด"
												v-model="provinceActive"
												item-text="display_name" item-value="id"

											></v-autocomplete>
										</v-flex>
									</v-layout>
								</v-container>
							</v-card-text>
							<v-card-actions>
								<v-btn color="grey darken-2" flat v-on:click="closeDialogSearchBranch()">ปิด</v-btn>
								<v-spacer></v-spacer>
								<v-btn color="blue darken-2" flat v-on:click="setBranch()">ตกลง</v-btn>
							</v-card-actions>
						</v-card>
					</v-dialog>

				</div>

			</v-app>
		</div> <!-- end vuejs -->
	</div> <!-- end data-app -->

</body>
<script type="text/javascript">
Vue.http.options.emulateJSON = true;
Vue.http.options.emulateHTTP = true;
Vue.use(VeeValidate)
var vuejs = new Vue({
  el:"#vuejs",
	$_veeValidate: {
		validator: 'new'
	},
	data:{
		pageLoading: true,
		projectName: '',
		sessioncode: '',
		sessionname: '',
		dialogProgress: {status: false, title: ''},
		dialogSearchBranch: false,
		listBranchName: [],
		provinceActive: '',
		listDivDialog: [],
		listProvinceDialog: [],
		listBranchDialog: [],
		odInfoDialog: { div: '', prov: ''},
		isSetBranch: false,
		odInfo: {is_div: '', is_div_nme: '', province: '', prov_nme: '', is_br: '', is_sbr: '', is_ch: '00', is_am: '00', is_thai_n: '', is_eng_n: '', is_open_dte: '', is_amp_n: '', code: '', prov_code: ''},
		odAddrBR: {province: '', amphur: '', tumbon: '', zipcode: '', addr: '', moo: '', soi: '', street: '', prov_nme: '', dist_nme: '', subdist_nme: ''},
		ctrInfo: {fax: '', wan: '' },
		telInfo: [{number: ''}],
		odInfoTemp: [],
		headers: [
			{ text: 'ลำดับ', align: 'center', sortable: false, value: 'index'},
			{ text: 'รหัสสังกัด', align: 'center', sortable: false, value: 'org_ode' },
			{ text: 'ชื่อสังกัด', align: 'center', sortable: false, value: 'th_org_nme' },
			{ text: 'ชื่อภาษาอังกฤษ', align: 'center', sortable: false, value: 'eng_org_nme' },
			{ text: 'วันที่เปิดดำเนินการ', align: 'center', sortable: false, value: 'open_date' },
			{ text: 'ที่ตั้งสำนักงาน', align: 'center', sortable: false, value: 'org_addr' },
			{ text: 'รหัสอำเภอและชื่ออำเภอ', align: 'center', sortable: false, value: 'district' },
			{ text: 'พื้นที่ในความรับผิดชอบ', align: 'center', sortable: false, value: 'district_name' }
		],
		provinceDetail: []

	},
  created (){
		this.checkLogin();
  },
  computed:
  {
  },
	mounted () {
	},
  watch: {
  },
	methods:{
		// start method
		progressDialog (status, title) {
			this.dialogProgress.status = status;
			this.dialogProgress.title = title;
		},
		checkLogin () {
			this.$http.post('<?=base_url() ?>index.php/loginController/getSession').then((response) => {
				var userInfo = response.body;
				if (userInfo.emp_code != "nodata") {
					this.sessioncode = userInfo.emp_code;
					this.sessionname = userInfo.emp_name;
					this.loadDivDialogSearch();
					setTimeout(() => { this.pageLoading = false; $(".none-show").removeClass("none-show"); }, 1000)
				}
				else {
					this.pageLoading = false;
					swal({
						title: 'กรุณาเข้าสู่ระบบก่อนเข้าใช้งาน',
						text: '',
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.href = '<?=base_url() ?>';
					})
				}
			}, (response) => {});
		},
		openDialogSearchBranch () {
			setTimeout(() => {
				this.dialogSearchBranch = true;
				this.isSetBranch = false;
			}, 500)
		},
		closeDialogSearchBranch () {
			setTimeout(() => {
				this.dialogSearchBranch = false;
				if (this.provinceActive != '' && this.odInfoTemp.length != 0)
					this.isSetBranch = true;
			}, 500)
		},
		loadDivDialogSearch () {
			this.listDivDialog = [];
			this.odInfoDialog.div = '';
			this.odInfoDialog.prov = '';
			this.provinceActive = '';
			this.$http.post('<?=base_url() ?>index.php/odController/loadDivInfo').then((response) => {
				this.listDivDialog = response.body;
			}, (response) => {
			});
		},
		loadProvinceDialogSearch () {
			this.listProvinceDialog = [];
			this.odInfoDialog.prov = '';
			this.provinceActive = '';
			if(this.odInfoDialog.div != '')
			{
				this.progressDialog(true, 'กำลังดำเนินการ กรุณารอสักครู่');
				this.$http.post('<?=base_url() ?>index.php/editProvincialController/getListProvinceName', {id: this.odInfoDialog.div}).then((response) => {
					this.listProvinceDialog = response.body;
					setTimeout(() => { this.progressDialog(false, null); }, 300)
				}, (response) => {
				});
			}
		},
		setBranch () {
			if (this.odInfoDialog.div == '' || this.provinceActive == '') {
				swal({
					title: 'กรุณากรอกข้อมูลให้ครบถ้วน',
					text: "",
					type: 'warning',
					showCancelButton: false,
					confirmButtonColor: '#3085d6',
					confirmButtonText: 'ตกลง',
					cancelButtonText: 'ยกเลิก',
				}).then((result) => {})
			}
			else {
				this.odInfoTemp = [];
				this.odInfo = {is_div: '', is_div_nme: '', province: '', prov_nme: '', is_br: '', is_sbr: '', is_ch: '00', is_am: '00', is_thai_n: '', is_eng_n: '', is_open_dte: '', is_amp_n: '', code: ''};
				this.odAddrBR = {province: '', amphur: '', tumbon: '', zipcode: '', addr: '', moo: '', soi: '', street: '', prov_nme: '', dist_nme: '', subdist_nme: ''};
				this.ctrInfo = {fax: '', wan: '' };
				this.telInfo = [{number: ''}];
				this.dialogSearchBranch = false;
				this.progressDialog(true, 'กำลังดำเนินการ กรุณารอสักครู่');
				// get branch info
				this.$http.post('<?=base_url() ?>index.php/editProvincialController/getBranchInformation', { id: this.provinceActive }).then((response) => {
					console.log(response.body);
					var result = response.body;
					this.odInfoTemp = result[0];
					this.odInfo.is_div_nme  = this.odInfoTemp.div_nme;
					this.odInfo.prov_nme  = this.odInfoTemp.brname;
					this.odInfo.is_div = '0' + 	this.odInfoTemp.is_div;
					this.odInfo.is_br =  this.odInfoTemp.is_br;
					this.odInfo.is_sbr = this.odInfoTemp.is_sbr;
					this.odInfo.is_ch =  this.odInfoTemp.is_ch;
					this.odInfo.is_am =  this.odInfoTemp.is_am;
					this.odInfo.code = this.odInfoTemp.is_br + ' - ' + this.odInfoTemp.is_sbr + ' - ' + this.odInfoTemp.is_ch + ' - ' + this.odInfoTemp.is_am;
					this.odInfo.prov_code = this.odInfoTemp.prov_code;
					this.odInfo.is_thai_n = this.odInfoTemp.is_thai_n;
					this.odInfo.is_eng_n = this.odInfoTemp.is_eng_n;
					this.odInfo.is_amp_n = this.odInfoTemp.is_amp_n;
					this.odInfo.is_open_dte = this.dateFormat(this.odInfoTemp.is_open_dte);
					this.odAddrBR.addr = this.odInfoTemp.addr;
					this.odAddrBR.moo = this.odInfoTemp.moo;
					this.odAddrBR.soi = this.odInfoTemp.soi;
					this.odAddrBR.street = this.odInfoTemp.street;
					this.telInfo = this.odInfoTemp.telephone;
					this.ctrInfo.wan = this.odInfoTemp.wan;
					this.ctrInfo.fax = this.odInfoTemp.fax;
					this.odAddrBR.province = this.odInfoTemp.prov;
					this.odAddrBR.amphur = this.odInfoTemp.dist;
					this.odAddrBR.tumbon = this.odInfoTemp.subdist;
					this.odAddrBR.zipcode = this.odInfoTemp.zip_code;

					this.loadProvinceDetail();
				}, (response) => {
				});

			}
		},
		loadProvinceDetail () {
			this.provInfo = {div: this.odInfoTemp.is_div, province: this.odInfoTemp.is_br+this.odInfoTemp.is_sbr, branch: '', type: ''}
			this.provinceDetail = [];
			this.progressDialog(true, 'กำลังดำเนินการ กรุณารอสักครู่');
			this.$http.post('<?=base_url() ?>index.php/odController/loadBrandDetail', {odInfo: this.provInfo}).then((response) => {
				// console.log(response.body);
				this.provinceDetail = response.body;
				setTimeout(() => {
					this.isSetBranch = true;
					setTimeout(() => {
						this.progressDialog(false, null);
					}, 1000)
				}, 3000)
			}, (response) => {});
		},
		closeBranch () {
			var me = this;
			swal({
				title: 'ยืนยันการยกเลิก ' + me.odInfo.is_thai_n,
				text: "",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'ตกลง',
				cancelButtonText: 'ยกเลิก',
			}).then((result) => {
				if (result.value) {
					var id = me.provinceActive[0]+me.provinceActive[1]+me.provinceActive[2];
					// console.log(id);
					this.$http.post('<?=base_url() ?>index.php/editProvincialController/closeProvince', { id: id }).then((response) => {
						var result = response.body;
						if (result == 'Successfully') {
							swal({
								title: 'ยกเลิกกิจการเรียบร้อยแล้ว',
								text: '',
								type: 'success',
								showCancelButton: false,
								confirmButtonColor: '#3085d6',
								confirmButtonText: 'ตกลง',
								cancelButtonText: 'ยกเลิก',
								allowOutsideClick: false
							}).then((result) => {
								window.location.reload();
							})
						}
						else {
							swal({
								title: 'ไม่สามารถยกเลิกกิจการได้',
								text: 'กรุณาลองใหม่อีกครั้ง',
								type: 'error',
								showCancelButton: false,
								confirmButtonColor: '#3085d6',
								confirmButtonText: 'ตกลง',
								cancelButtonText: 'ยกเลิก',
								allowOutsideClick: false
							}).then((result) => {
								window.location.href = "#";
							})
						}
					}, (response) => {
					});
				}
			})

		},
		dateFormat (str) {
			if (str == null || str == '' || str == ' ') {
				return null;
			}
			else {
				var dataStr = str.substr(0, 4) + '-' + str.substr(4, 2) + '-' + str.substr(6, 2);
				return dataStr.toString();
			}
		},
		unitFormat (unitCode, unitName) {
			if(unitCode != null)
				return unitCode.substr(0, 2) + '-' + unitCode.substr(2, 2) + ' ' + unitName;
			else
				return null;
		}
  // end methods
  }
})
</script>
</html>
