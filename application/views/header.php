<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicons -->
	<link href="<?php echo $this->config->item('icon_project'); ?>" rel="icon">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/vuetify/vuetify.css" />
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/util.css">
	<link href="<?=base_url() ?>assets/libs/icon/materialicons.css" rel="stylesheet">
	<link href="<?=base_url() ?>assets/libs/custom/font.css" rel="stylesheet">
	<link href="<?=base_url() ?>assets/libs/sweetalert/sweetalert2.min.css" rel="stylesheet">
	<title><?php echo $this->config->item('project_name'); ?></title>

	<style type="text/css">
		.theme--light.v-table thead tr:first-child {
			border: 1px solid #009688 !important;
			background-color: #009688 !important;
		}
		.v-list__tile {
			margin-top: 5px !important;
			padding-right: 30px !important;
		}
		.blue-text {
			color: #3EC1D5;
		}
		.v-list__tile {
			min-width: 210px !important;
		}

	</style>
</head>
<body style="height:64px;">
	<div id="header" style="height:64px;"  >
		<!-- <v-app id="hederApp"> -->

			<v-toolbar dark class="blue-green">
				<v-toolbar-title class="white--text"> <a href="<?=base_url() ?>">
					<span><img src="<?=base_url() ?>assets/images/map.png"  height="28" width="32"></span>
					<span class="fs-25" style="color:#fff;">{{projectName}}<span> </a>
				</v-toolbar-title>
				<v-spacer></v-spacer>
				<v-btn flat round>
					<a href="<?=base_url() ?>"> <h4 style="color:#fff;">หน้าหลัก</h4> </a>
				</v-btn>
				<div v-show="sessioncode !== ''">
					<!-- ข้อมูล -->
					<v-menu  left bottom offset-y >
						<v-btn flat round slot="activator">
							<h4>รายงาน</h4>
						</v-btn>
						<v-list class="p-t-0 p-b-0" >
							<a href="<?=base_url() ?>index.php/main/branch">
								<v-list-tile class="p-t-0 p-b-0 cursor-pointer"  >
									<v-btn icon>
										<v-icon class="m-t-0 m-b-0 m-l-0 m-r-0">domain</v-icon>
									</v-btn>
									<v-spacer></v-spacer>
									<v-list-tile-title>ข้อมูลสาขา</v-list-tile-title>
								</v-list-tile>
							</a>
							<a href="<?=base_url() ?>index.php/main/headOffice">
								<v-list-tile class="p-t-0 p-b-0 cursor-pointer"  >
									<v-btn icon>
										<v-icon class="m-t-0 m-b-0 m-l-0 m-r-0">location_city</v-icon>
									</v-btn>
									<v-spacer></v-spacer>
									<v-list-tile-title>ข้อมูลสำนักงานใหญ่</v-list-tile-title>
								</v-list-tile>
							</a>
						</v-list>
					</v-menu>
					<!-- สำนักงานใหญ่ -->
					<v-menu  left bottom offset-y v-show="sessionadmin === 1">
						<v-btn flat round slot="activator">
							<h4>สำนักงานใหญ่</h4>
						</v-btn>
						<v-list class="p-t-0 p-b-0" >
							<a href="<?=base_url() ?>index.php/main/createHead">
								<v-list-tile class="p-t-0 p-b-0 cursor-pointer"  >
									<v-btn icon>
										<v-icon class="m-t-0 m-b-0 m-l-0 m-r-0">create_new_folder</v-icon>
									</v-btn>
									<v-spacer></v-spacer>
									<v-list-tile-title>สร้างส่วนงาน</v-list-tile-title>
								</v-list-tile>
							</a>
							<a href="<?=base_url() ?>index.php/main/editHead">
								<v-list-tile class="p-t-0 p-b-0 cursor-pointer"  >
									<v-btn icon>
										<v-icon class="m-t-0 m-b-0 m-l-0 m-r-0">border_color</v-icon>
									</v-btn>
									<v-spacer></v-spacer>
									<v-list-tile-title>แก้ไขข้อมูลส่วนงาน</v-list-tile-title>
								</v-list-tile>
							</a>
							<a href="<?=base_url() ?>index.php/main/closeHead">
								<v-list-tile class="p-t-0 p-b-0 cursor-pointer"  >
									<v-btn icon>
										<v-icon class="m-t-0 m-b-0 m-l-0 m-r-0">not_interested</v-icon>
									</v-btn>
									<v-spacer></v-spacer>
									<v-list-tile-title>ยกเลิกส่วนงาน</v-list-tile-title>
								</v-list-tile>
							</a>
						</v-list>
					</v-menu>
					<v-menu left bottom offset-y v-show="sessionadmin === 1">
						<v-btn flat round slot="activator">
							<h4>สาขา</h4>
						</v-btn>
						<v-list dense>
							<v-menu offset-x open-on-hover left>
								<v-list-tile slot="activator" @click="">
									<v-list-tile-action >
										<v-icon>play_arrow</v-icon>
									</v-list-tile-action>
									<v-list-tile-title><h3>สำนักงานจังหวัด</h3></v-list-tile-title>
								</v-list-tile>
								<v-list class="p-t-0 p-b-0" >
									<a href="<?=base_url() ?>index.php/main/createprovincial">
										<v-list-tile class="p-t-0 p-b-0 cursor-pointer"  >
											<v-btn icon>
												<v-icon class="m-t-0 m-b-0 m-l-0 m-r-0">create_new_folder</v-icon>
											</v-btn>
											<v-spacer></v-spacer>
											<v-list-tile-title>สร้างสำนักงาน</v-list-tile-title>
										</v-list-tile>
									</a>
									<a href="<?=base_url() ?>index.php/main/editprovincial">
										<v-list-tile class="p-t-0 p-b-0 cursor-pointer"  >
											<v-btn icon>
												<v-icon class="m-t-0 m-b-0 m-l-0 m-r-0">border_color</v-icon>
											</v-btn>
											<v-spacer></v-spacer>
											<v-list-tile-title>แก้ไขข้อมูลสำนักงาน</v-list-tile-title>
										</v-list-tile>
									</a>
									<a href="<?=base_url() ?>index.php/main/closeprovincial">
										<v-list-tile class="p-t-0 p-b-0 cursor-pointer"  >
											<v-btn icon>
												<v-icon class="m-t-0 m-b-0 m-l-0 m-r-0">not_interested</v-icon>
											</v-btn>
											<v-spacer></v-spacer>
											<v-list-tile-title>ยกเลิกสำนักงาน</v-list-tile-title>
										</v-list-tile>
									</a>
								</v-list>
							</v-menu> <!-- menu branch -->
						</v-list>
		        <v-list dense>
		          <v-menu offset-x open-on-hover left>
		            <v-list-tile slot="activator" @click="">
									<v-list-tile-action >
										<v-icon>play_arrow</v-icon>
									</v-list-tile-action>
									<v-list-tile-title><h3>สาขา</h3></v-list-tile-title>
		            </v-list-tile>
								<v-list class="p-t-0 p-b-0" >
									<a href="<?=base_url() ?>index.php/main/createbranch">
										<v-list-tile class="p-t-0 p-b-0 cursor-pointer"  >
											<v-btn icon>
												<v-icon class="m-t-0 m-b-0 m-l-0 m-r-0">create_new_folder</v-icon>
											</v-btn>
											<v-spacer></v-spacer>
											<v-list-tile-title>สร้างสาขา</v-list-tile-title>
										</v-list-tile>
									</a>
									<a href="<?=base_url() ?>index.php/main/editbranch">
										<v-list-tile class="p-t-0 p-b-0 cursor-pointer"  >
											<v-btn icon>
												<v-icon class="m-t-0 m-b-0 m-l-0 m-r-0">border_color</v-icon>
											</v-btn>
											<v-spacer></v-spacer>
											<v-list-tile-title>แก้ไขข้อมูลสาขา</v-list-tile-title>
										</v-list-tile>
									</a>
									<a href="<?=base_url() ?>index.php/main/changetoprovince">
										<v-list-tile class="p-t-0 p-b-0 cursor-pointer"  >
											<v-btn icon>
												<v-icon class="m-t-0 m-b-0 m-l-0 m-r-0">home</v-icon>
											</v-btn>
											<v-spacer></v-spacer>
											<v-list-tile-title>ยกระดับสาขาเป็น สนจ.</v-list-tile-title>
										</v-list-tile>
									</a>
									<a href="<?=base_url() ?>index.php/main/changeprovincebranch">
										<v-list-tile class="p-t-0 p-b-0 cursor-pointer"  >
											<v-btn icon>
												<v-icon class="m-t-0 m-b-0 m-l-0 m-r-0">rotate_right</v-icon>
											</v-btn>
											<v-spacer></v-spacer>
											<v-list-tile-title>แก้ไข สนจ.ของสาขา</v-list-tile-title>
										</v-list-tile>
									</a>
									<a href="<?=base_url() ?>index.php/main/closebranch">
										<v-list-tile class="p-t-0 p-b-0 cursor-pointer"  >
											<v-btn icon>
												<v-icon class="m-t-0 m-b-0 m-l-0 m-r-0">not_interested</v-icon>
											</v-btn>
											<v-spacer></v-spacer>
											<v-list-tile-title>ยกเลิกสาขา</v-list-tile-title>
										</v-list-tile>
									</a>
								</v-list>
		          </v-menu> <!-- menu branch -->
						</v-list>
						<v-list dense>
							<v-menu offset-x open-on-hover left>
								<v-list-tile slot="activator" @click="">
									<v-list-tile-action >
										<v-icon>play_arrow</v-icon>
									</v-list-tile-action>
									<v-list-tile-title><h3>หน่วยอำเภอ</h3></v-list-tile-title>
								</v-list-tile>
								<v-list class="p-t-0 p-b-0" >
									<a href="<?=base_url() ?>index.php/main/createunit">
										<v-list-tile class="p-t-0 p-b-0 cursor-pointer"  >
											<v-btn icon>
												<v-icon class="m-t-0 m-b-0 m-l-0 m-r-0">create_new_folder</v-icon>
											</v-btn>
											<v-spacer></v-spacer>
											<v-list-tile-title>สร้างหน่วยอำเภอ</v-list-tile-title>
										</v-list-tile>
									</a>
									<a href="<?=base_url() ?>index.php/main/editunit">
										<v-list-tile class="p-t-0 p-b-0 cursor-pointer"  >
											<v-btn icon>
												<v-icon class="m-t-0 m-b-0 m-l-0 m-r-0">border_color</v-icon>
											</v-btn>
											<v-spacer></v-spacer>
											<v-list-tile-title>แก้ไขข้อมูลหน่วยอำเภอ</v-list-tile-title>
										</v-list-tile>
									</a>
									<a href="<?=base_url() ?>index.php/main/changetobranch">
										<v-list-tile class="p-t-0 p-b-0 cursor-pointer"  >
											<v-btn icon>
												<v-icon class="m-t-0 m-b-0 m-l-0 m-r-0">rotate_right</v-icon>
											</v-btn>
											<v-spacer></v-spacer>
											<v-list-tile-title>เปลี่ยนหน่วยอำเภอเป็นสาขา</v-list-tile-title>
										</v-list-tile>
									</a>
									<a href="<?=base_url() ?>index.php/main/closeunit">
										<v-list-tile class="p-t-0 p-b-0 cursor-pointer"  >
											<v-btn icon>
												<v-icon class="m-t-0 m-b-0 m-l-0 m-r-0">not_interested</v-icon>
											</v-btn>
											<v-spacer></v-spacer>
											<v-list-tile-title>ยกเลิกหน่วยอำเภอ</v-list-tile-title>
										</v-list-tile>
									</a>
								</v-list>
							</v-menu> <!-- menu unit -->
						</v-list>
						<v-list dense>
							<v-menu offset-x open-on-hover left>
								<v-list-tile slot="activator" @click="">
									<v-list-tile-action >
										<v-icon>play_arrow</v-icon>
									</v-list-tile-action>
									<v-list-tile-title><h3>สาขาย่อย</h3></v-list-tile-title>
								</v-list-tile>
								<v-list class="p-t-0 p-b-0" >
									<a href="<?=base_url() ?>index.php/main/createunitbranch">
										<v-list-tile class="p-t-0 p-b-0 cursor-pointer"  >
											<v-btn icon>
												<v-icon class="m-t-0 m-b-0 m-l-0 m-r-0">create_new_folder</v-icon>
											</v-btn>
											<v-spacer></v-spacer>
											<v-list-tile-title>สร้างสาขาย่อย</v-list-tile-title>
										</v-list-tile>
									</a>
									<a href="<?=base_url() ?>index.php/main/editunitbranch">
										<v-list-tile class="p-t-0 p-b-0 cursor-pointer"  >
											<v-btn icon>
												<v-icon class="m-t-0 m-b-0 m-l-0 m-r-0">border_color</v-icon>
											</v-btn>
											<v-spacer></v-spacer>
											<v-list-tile-title>แก้ไขข้อมูลสาขาย่อย</v-list-tile-title>
										</v-list-tile>
									</a>
									<a href="<?=base_url() ?>index.php/main/closeunitbranch">
										<v-list-tile class="p-t-0 p-b-0 cursor-pointer"  >
											<v-btn icon>
												<v-icon class="m-t-0 m-b-0 m-l-0 m-r-0">not_interested</v-icon>
											</v-btn>
											<v-spacer></v-spacer>
											<v-list-tile-title>ยกเลิกสาขาย่อย</v-list-tile-title>
										</v-list-tile>
									</a>
								</v-list>
							</v-menu> <!-- menu unit -->
						</v-list>
		      </v-menu>

				</div>
				<v-menu  left bottom offset-y v-show="sessioncode !== ''">
					<v-btn flat  round slot="activator"> <v-icon class="m-r-5">person</v-icon> <h4>{{sessionname}}</h4></v-btn>
					<v-list class="p-t-0 p-b-0" >
						<a href="<?=base_url() ?>index.php/main/useraccess" v-show="sessionadmin === 1">
							<v-list-tile class="p-t-0 p-b-0 cursor-pointer" >
								<v-btn icon>
									<v-icon>settings</v-icon>
								</v-btn>
								<v-spacer></v-spacer>
								<v-list-tile-title>กำหนดสิทธิ์</v-list-tile-title>
							</v-list-tile>
						</a>
						<a href="<?=base_url() ?>index.php/main/updatepassword">
							<v-list-tile class="p-t-0 p-b-0 cursor-pointer" >
								<v-btn icon>
									<v-icon>autorenew</v-icon>
								</v-btn>
								<v-spacer></v-spacer>
								<v-list-tile-title>เปลี่ยนรหัสผ่าน</v-list-tile-title>
							</v-list-tile>
						</a>
						<v-divider class="m-l-5 m-r-5"></v-divider>
						<a>
							<v-list-tile class="p-t-0 p-b-0 cursor-pointer" v-on:click="logout()">
								<v-btn icon   >
									<v-icon dark  color="pink">lock_open</v-icon>
								</v-btn>
								<v-spacer></v-spacer>
								<v-list-tile-title>ออกจากระบบ</v-list-tile-title>
							</v-list-tile>
						</a>
					</v-list>
				</v-menu>
				<v-btn flat round v-on:click="showDialogLogin()" v-show="sessioncode === ''">
					<v-icon class="m-r-5">lock</v-icon>
					<h4>เข้าสู่ระบบ</h4>
				</v-btn>
			</v-toolbar>


			<v-dialog v-model="dialogLogin"  max-width="500px">
				<v-card class="text-xs-center">
					<v-btn class="m-t-30" fab dark large color="white" depressed >
						<v-icon dark color="blue" style="font-size:60px!important;">lock</v-icon>
					</v-btn>
					<v-card-title class="justify-center">

						<span class="headline"><h4>{{projectName}}</h4></span>
					</v-card-title>
					<v-card-text class="p-t-0 p-b-0">
						<v-container grid-list-md class="p-t-0 p-b-0 ">
							<v-layout wrap>
								<v-flex xs12>
									<v-text-field label="ชื่อผู้ใช้*" required box prepend-icon="face"
									v-validate="'required'"
									:error-messages="errors.collect('name')"
									data-vv-name="name"
									required
									v-model="username"
									autofocus
									></v-text-field>
								</v-flex>
								<v-flex xs12>
									<v-text-field label="รหัสผ่าน*" type="password" required box prepend-icon="lock_open"
									v-validate="'required'"
									:error-messages="errors.collect('password')"
									data-vv-name="password"
									required
									v-model="password"
									v-on:keyup.enter="validateBeforeSubmit()"

									></v-text-field>
								</v-flex>
							</v-layout>
						</v-container>
					</v-card-text>
					<v-card-actions class="p-b-20 justify-center">
						<v-btn color="blue darken-1" outline round  v-on:click="validateBeforeSubmit()">เข้าสู่ระบบ</v-btn>
					</v-card-actions>
				</v-card>
			</v-dialog>

		<!-- </v-app> -->
	</div>

	<!-- Vue -->

	<script src="<?=base_url() ?>assets/libs/vue/vue.js"></script>
	<script src="<?=base_url() ?>assets/libs/vue-resource/vue-resource.js"></script>
	<script src="<?=base_url() ?>assets/libs/vee-validate/vee-validate.js"></script>
	<script src="<?=base_url() ?>assets/libs/vuetify/vuetify.js"></script>
	<script src="<?=base_url() ?>assets/libs/sweetalert/sweetalert2.all.min.js"></script>
	<script src="<?=base_url() ?>assets/libs/lodash/lodash.min.js"></script>
	<script src="<?=base_url() ?>assets/libs/jquery/jquery.slim.min.js" ></script>
	<script src="<?=base_url() ?>assets/libs/pdf/pdfmake.js"></script>
	<script src="<?=base_url() ?>assets/libs/pdf/vfs_fonts.js"></script>
	<script src="<?=base_url() ?>assets/libs/excel/jquery.table2excel.js"></script>
	<script src="<?=base_url() ?>assets/libs/jquery/popper.min.js" ></script>
	<script src="<?=base_url() ?>assets/libs/bootstrap/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
</body>
<script type="text/javascript">
  $(function () {
   _CheckIEbroswer()
  });
  function _CheckIEbroswer(){
    // Firefox 1.0+
    var isFirefox = typeof InstallTrigger !== 'undefined';
    // Safari 3.0+ "[object HTMLElementConstructor]"
    var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/false || !!document.documentMode;
    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;

    if (isFirefox || isSafari || isIE || isEdge) {
      window.location.href = '<?=base_url() ?>index.php/main/openWithChrome'
    }
  }
</script>
<script type="text/javascript">
Vue.http.options.emulateJSON = true;
Vue.http.options.emulateHTTP = true;
Vue.use(VeeValidate)
var header = new Vue({
  el:"#header",
	$_veeValidate: {
		validator: 'new'
	},
	data:{
		projectName: '',
		items: [
			{
				src: '<?=base_url() ?>assets/images/slide1.jpg'
			},
			{
				src: '<?=base_url() ?>assets/images/slide2.jpg'
			},
			{
				src: '<?=base_url() ?>assets/images/slide3.jpg'
			}
		],
		sessioncode: '',
		sessionname: '',
		sessionadmin: '',
		dialogLogin: false,
		dictionary: {
			attributes: {
				email: 'E-mail Address'
			},
			custom: {
				name: {
					required: () => 'ระบชื่อผู้ใช้',
					max: 'The name field may not be greater than 10 characters'
				},
				password: {
					required: () => 'ระบุรหัสผ่าน'
				}
			}
		},
		username: '',
		password: '',
		userInfo: [],
	},
  created (){
		this.checkLogin();

  },
  computed:
  {
  },
	mounted () {
		this.$validator.localize('en', this.dictionary);
	},
  watch: {
  },
	methods:{
		// start method
		checkLogin () {
			this.projectName = <?php echo json_encode($this->config->item('project_name')); ?>;
			this.$http.post('<?=base_url() ?>index.php/loginController/getSession').then((response) => {
				var userInfo = response.body;
				if (userInfo.emp_code != "nodata") {
					this.sessioncode 	= userInfo.emp_code;
					this.sessionname 	= userInfo.emp_name;
					this.sessionadmin = userInfo.is_admin;
				}
			}, (response) => {});
		},
		logout () {
			this.$http.post('<?=base_url() ?>index.php/loginController/logout').then((response) => {
				this.sessioncode  = '';
				this.sessionname  = '';
				this.sessionadmin = '';
				this.username     = '';
				this.Password			= '';
				this.menu = [];
				window.location.href = '<?=base_url() ?>';
			}, (response) => {});
		},
		validateBeforeSubmit() {
			this.$validator.validateAll().then((result) => {
				if (result) {
					this.submitLogin();
				}
			});
		},
		showDialogLogin () {
			this.dialogLogin = true;
		},
		submitLogin () {
			this.$http.post('<?=base_url() ?>index.php/loginController/checkLogin', {empcode: this.username, emppass: this.password}).then((response) => {
				console.log(response.body);
				var result = response.body;
				if (result == 'Login Success') {

					this.$http.post('<?=base_url() ?>index.php/loginController/getSession').then((response) => {
						var userInfo = response.body;
						if (userInfo != null) {
							this.sessioncode  = userInfo.emp_code;
							this.sessionname  = userInfo.emp_name;
							this.sessionadmin = userInfo.is_admin;
							this.username     = '';
							this.Password			= '';
							swal(
								'เข้าสู่ระบบเรียบร้อยแล้ว',
								'',
								'success'
							)
							this.dialogLogin = false;
						}
						else {
							swal(
								'ไม่สามารถเข้าสู่ระบบได้',
								'กรุณาลองใหม่อีกครั้ง',
								'error'
							)
						}

					}, (response) => {});
				}
				else if (result == 'Incorrect Password') {
					swal(
						'ไม่สามารถเข้าสู่ระบบได้',
						'รหัสผ่านไม่ถูกต้อง',
						'error'
					)
				}
				else if (result == 'Incorrect User') {
					swal(
						'ไม่สามารถเข้าสู่ระบบได้',
						'ไม่มีชื่อผู้ใช้นี้ กรุณาติดต่อผู้ดูแลระบบ',
						'error'
					)
				}
				//end check user
				}, (response) => {});
		}
  // end methods
  }
})
</script>
</html>
