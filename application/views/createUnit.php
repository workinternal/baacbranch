<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicons -->
	<link href="<?php echo $this->config->item('icon_project'); ?>" rel="icon">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/vuetify/vuetify.css" />
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/util.css">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/custom.css">
	<title><?php echo $this->config->item('project_name'); ?></title>

	<style type="text/css">
		.v-stepper--alt-labels .v-stepper__step {
			flex-basis: 47% !important;
		}
		.box-area {
			background: #eee;
			padding: 30px 15px 0px 15px;
			border-radius: 5px;
		}
	</style>
</head>
<body>

	<div data-app="true" class="application--light">
		<div class="none-show" ><?php $this->load->view('header.php'); ?></div>
		<div id="vuejs">
			<v-app  class="white-bg" >
				<!-- loading -->
				<div v-show="pageLoading" class="loading-page">
					<v-content>
						<v-container fluid fill-height>
							<v-layout justify-center align-center>
								<v-progress-circular :size="200" :width="20" color="primary" indeterminate></v-progress-circular>
							</v-layout>
						</v-container>
					</v-content>
				</div>
				<!-- content -->
				<div class="none-show">

					<!-- content -->
					<v-content style="background-color: #fff;">
						<v-container fluid  class="p-t-0 p-l-0 p-r-0">
							<v-layout  row wrap>
								<v-flex xs10 offset-xs1 class="pt-2 text-xs-center">
									<span><i class="material-icons icon-title-big">create_new_folder</i></span> <h2>เพิ่มหน่วยอำเภอ</h2>
								</v-flex>
								<!-- หัวข้อ-->
								<v-flex xs10 offset-xs1 class="m-t-5 m-b-5 text-xs-center">
									<div class="p-l-50 p-t-0 p-r-50 p-b-50">
										<v-stepper v-model="currentStep" alt-labels>
											<v-stepper-header>
												<template v-for="n in steps">
													<v-stepper-step :edit-icon="'check'" :complete="currentStep > n" :key="`${n}-step`" :step="n" >{{headerStep[n-1]}}</v-stepper-step>
													<!-- <v-divider v-if="n !== steps" :key="n"></v-divider> -->
												</template>
											</v-stepper-header>
										<v-stepper-items>
											<!-- step 1 -->
											<v-stepper-content step="1">
												<div class="text-xs-center" v-show="isSetBranch == false">
													<h4 style="color: #D32F2F;">* กรุณาเลือกสาขาที่ต้องการเพิ่มหน่วยอำเภอ</h4>
														<v-btn outline round color="primary" v-on:click="openDialogSearchUnitBranch()">ค้นหาสาขา</v-btn>
												</div>
												<v-card  class="mb-5" v-show="isSetBranch == true">
													<!-- select unit branch -->
													<v-layout row wrap class="mb-2">
														<v-flex xs10 class="text-xs-right p-t-15">
															<h4 style="color: #D32F2F;">* กรุณาเลือกสาขาที่ต้องการเพิ่มหน่วยอำเภอ</h4>
														</v-flex>
														<v-flex xs2 class="text-xs-center">
															<v-btn outline round color="primary" v-on:click="openDialogSearchUnitBranch()">ค้นหาสาขา</v-btn>
														</v-flex>
													</v-layout>
													<!-- information -->
													<v-layout row wrap class="mb-2">
														<v-flex xs12 class="text-xs-left">
															<h3><v-icon large color="blue-grey darken-2" style="font-size:25px;color: #C62828 !important;">chrome_reader_mode</v-icon> <span>ข้อมูลสังกัด</span></h3>
														</v-flex>
													</v-layout>
													<!-- ฝ่าย / สนจ -->
													<v-layout row wrap class="m-b-45">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ฝ่ายกิจการสาขา</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<!-- <v-autocomplete  box single-line  :items="listDiv" item-text="div_name" item-value="id" v-model="odInfo.is_div" menu-props="auto" label="Select" hide-details  single-line v-on:change="loadProvinceInfo()" ></v-autocomplete> -->
															<v-text-field disabled  box single-line v-model="odInfo.is_div_nme"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left">
															<h4 class="pt-3">สำนักงานจังหวัด</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field disabled  box single-line v-model="odInfo.prov_nme"></v-text-field>
														</v-flex>
													</v-layout>
													<!-- รหัสสังกัด -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-2">รหัสสังกัด</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field disabled  box single-line 	class="pr-2 "     v-model="odInfo.code"></v-text-field>
														</v-flex>
													</v-layout>
													<!-- ชื่อไทย / ชื่ออังกฤษ -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ชื่อสาขา (ภาษาไทย)</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field disabled box single-line label="ภาษาไทย"   v-model="odInfo.is_thai_n" ></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">ชื่อสาขา (ภาษาอังกฤษ)</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field disabled box single-line label="ภาษาอังกฤษ"   v-model="odInfo.is_eng_n" ></v-text-field>
														</v-flex>
													</v-layout>
													<!-- ชื่อย่อ / วันที่เปิดดำเนินการ  -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ชื่อย่อสาขา</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field disabled box single-line label="ชื่อย่อ"  v-model="odInfo.is_amp_n" ></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">วันที่เปิดดำเนินการ</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field disabled  box single-line v-model="odInfo.is_open_dte"></v-text-field>
														</v-flex>
													</v-layout>
													<!-- information -->
													<v-layout row wrap class="mb-2">
														<v-flex xs12 class="text-xs-left">
															<h3><v-icon large color="blue-grey darken-2" style="font-size:25px;color: #C62828 !important;">chrome_reader_mode</v-icon> <span>ข้อมูลหน่วยอำเภอ</span></h3>
														</v-flex>
													</v-layout>

													<!--  รหัสหน่วย วันที่เปิดดำเนินการ-->
													<v-layout row wrap class="mb-3">
														<!-- <v-flex xs2 class="text-xs-left  pl-4">
															<h4 class="pt-3">รหัสหน่วย</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field disabled box	 v-model="odAmphur.code" ></v-text-field>
														</v-flex> -->
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">วันที่เปิดดำเนินการ</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-menu :close-on-content-click="true" v-model="menu3" :nudge-right="40" transition="scale-transition" offset-y full-width min-width="290px">
																<v-text-field box slot="activator" v-model="odAmphur.is_open_dte" append-icon="event" readonly></v-text-field>
																<v-date-picker v-model="odAmphur.is_open_dte" @input="menu3 = false" locale="th"></v-date-picker>
															</v-menu>
														</v-flex>
													</v-layout>
													<!-- ชื่อหน่วยอำเภอ ภาษาไทย ภาษาอังกฤษ  -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ชื่อหน่วยอำเภอ (ภาษาไทย)</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line clear-icon="cancel" clearable label="ภาษาไทย" v-model="odAmphur.is_thai_n" v-validate="'required'" :error-messages="errors.collect('step1.thainame')" data-vv-name="thainame" data-vv-scope="step1" ></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">ชื่อหน่วยอำเภอ (ภาษาอังกฤษ)</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line clear-icon="cancel" clearable label="ภาษาอังกฤษ" v-model="odAmphur.is_eng_n" v-validate="'required'" :error-messages="errors.collect('step1.engname')" data-vv-name="engname" data-vv-scope="step1" ></v-text-field>
														</v-flex>
													</v-layout>
													<!-- location -->
													<v-layout row wrap class="mb-2 mt-5">
														<v-flex xs2 class="text-xs-left">
															<h3><v-icon large color="blue-grey darken-2" style="font-size:25px;color: #C62828 !important;">account_balance</v-icon> <span>ที่ตั้ง</span></h3>
														</v-flex>
													</v-layout>
													<!-- จังหวัด  อำเภอ-->
													<v-layout row wrap class="m-b-45">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">จังหวัด</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-autocomplete box :items="listProvinceUnitAddr" id="listProvinceUnitAddr" item-text="name" item-value="id" v-model="odAddrAM.province" menu-props="auto" label="-- จังหวัด --" hide-details  single-line v-on:change="getListAmphur()" v-validate="'required'" :error-messages="errors.collect('step1.addrProvince')" data-vv-name="addrProvince" data-vv-scope="step1" ></v-autocomplete>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">อำเภอ</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-autocomplete box  :items="listDistrictUnitAddr" id="listDistrictUnitAddr" menu-props="auto" label=" -- อำเภอ --" hide-details 	single-line
															item-text="name" item-value="id" v-model="odAddrAM.amphur" v-on:change="getListTumbon()" v-validate="'required'" :error-messages="errors.collect('step1.addrDist')" data-vv-name="addrDist" data-vv-scope="step1"></v-autocomplete>
														</v-flex>
													</v-layout>
													<!-- ตำบล รหัรหัสไปรษณีย์-->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ตำบล</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-autocomplete box :items="listSubDistrictUnitAddr" id="listSubDistrictUnitAddr" item-text="name" item-value="id" v-model="odAddrAM.tumbon" v-on:change="getListZipcode()" menu-props="auto" label="-- ตำบล --" hide-details  single-line v-validate="'required'" :error-messages="errors.collect('step1.addrSubDist')" data-vv-name="addrSubDist" data-vv-scope="step1"></v-autocomplete>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">รหัสไปรษณีย์</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line label="รหัสไปรษณีย์" disabled v-model="odAddrAM.zipcode"></v-text-field>
														</v-flex>
													</v-layout>
													<!-- เลขที่ หมู่ -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">เลขที่</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line clear-icon="cancel" clearable label="เลขที่" v-model="odAddrAM.addr" v-validate="'required'" :error-messages="errors.collect('step1.addr')" data-vv-name="addr" data-vv-scope="step1"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">หมู่</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line clear-icon="cancel" clearable label="หมู่" v-model="odAddrAM.moo"></v-text-field>
														</v-flex>
													</v-layout>
													<!-- ซอย ถนน -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ซอย</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line clear-icon="cancel" clearable label="ซอย" v-model="odAddrAM.soi"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">ถนน</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line clear-icon="cancel" clearable label="ถนน" v-model="odAddrAM.street"></v-text-field>
														</v-flex>
													</v-layout>
													<!-- contract -->
													<v-layout row wrap class="mb-2 mt-5">
														<v-flex xs12 class="text-xs-left">
															<h3><v-icon large color="blue-grey darken-2" style="font-size:25px;color: #C62828 !important;">contact_phone</v-icon> <span>ติดต่อ</span></h3>
														</v-flex>
													</v-layout>
													<!-- เบอร์โทร -->
													<v-layout row wrap class="mb-3" v-for=" (item, index) in telAMInfo " :key="index">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">โทรศัพท์</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line label="โทรศัพท์"  clear-icon="cancel" clearable return-masked-value mask="#-####-####" v-model="item.number"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left " >
															<v-btn color="red" fab small dark v-on:click="removeTelephone(index)" v-show="index != 0">
																<v-icon>remove</v-icon>
															</v-btn>
															<v-btn color="primary" fab small dark v-on:click="addTelephone()" v-show="index == (telAMInfo.length-1)">
																<v-icon>add</v-icon>
															</v-btn>
														</v-flex>
													</v-layout>
													<!-- wan โทรสาร -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">WAN</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line label="wan"  clear-icon="cancel" clearable return-masked-value mask="####" v-model="ctrAMInfo.wan"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left">
															<h4 class="pt-3">โทรสาร</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line label="โทรสาร"  clear-icon="cancel" clearable return-masked-value mask="#-####-####" v-model="ctrAMInfo.fax"></v-text-field>
														</v-flex>
													</v-layout>
												</v-card>
												<v-card-actions v-show="isSetBranch == true">
													<v-spacer></v-spacer>
													<v-btn round color="primary" @click.native="validateBeforeNextPage('step1')">ถัดไป</v-btn>
												</v-card-actions>
											</v-stepper-content>
											<!-- step 2 -->
											<v-stepper-content step="2">
												<v-card  class="mb-5 " >
													<!-- ข้อมูลอำเภอ -->
													<v-layout row wrap class="mb-2">
														<v-flex xs12 class="text-xs-left">
															<h3><v-icon large color="blue-grey darken-2" style="font-size:25px;color: #C62828 !important;">home</v-icon> <span>รหัสอำเภอและชื่ออำเภอ</span></h3>
														</v-flex>
													</v-layout>
													<!-- รหัสอำเภอ ชื่ออำเภอ -->
													<!-- <v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">รหัสอำเภอ</h4>
														</v-flex>
														<v-flex xs2 class="text-xs-left">
																<v-text-field disabled box single-line v-model="odUnitAmphur.code" ></v-text-field>
														</v-flex>
													</v-layout> -->
													<!-- ชื่อไทย / ชื่ออังกฤษ -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ชื่ออำเภอ (ภาษาไทย)</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line clear-icon="cancel" clearable label="ภาษาไทย" v-model="odUnitAmphur.is_thai_n" v-validate="'required'" :error-messages="errors.collect('step2.thainame')" data-vv-name="thainame" data-vv-scope="step2"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">ชื่ออำเภอ (ภาษาอังกฤษ)</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line clear-icon="cancel" clearable label="ภาษาอังกฤษ" v-model="odUnitAmphur.is_eng_n" v-validate="'required'" :error-messages="errors.collect('step2.engname')" data-vv-name="engname" data-vv-scope="step2"></v-text-field>
														</v-flex>
													</v-layout>
													<!-- area -->
													<v-layout row wrap class="mb-2 mt-5">
														<v-flex xs12 class="text-xs-left">
															<h3><v-icon large color="blue-grey darken-2" style="font-size:25px;color: #C62828 !important;">add_location</v-icon> <span>เลือกเขตพื้นที่ในความรับผิดชอบ</span></h3>
														</v-flex>
													</v-layout>
													<v-layout row wrap>
														<v-flex xs12 class="box-area m-b-10 m-t-10" v-for="(item, index) in odArea" :key="index">
															<!-- จังหวัด  อำเภอ-->
															<v-layout row wrap class="m-b-30">
																<v-flex xs1 offset-xs1 class="text-xs-left pl-4">
																	<h4 class="pt-3">จังหวัด</h4>
																</v-flex>
																<v-flex xs4 class="text-xs-left">
																	<v-autocomplete box :items="listAddrArea[index].listProvince" item-text="name" item-value="id" v-model="item.cat_cc" menu-props="auto" label="-- จังหวัด --" hide-details  single-line v-on:change="getListDistrictArea(index)"></v-autocomplete>
																</v-flex>
																<v-flex xs1 class="text-xs-left ">
																	<h4 class="pt-3">อำเภอ</h4>
																</v-flex>
																<v-flex xs4 class="text-xs-left">
																	<v-autocomplete box :items="listAddrArea[index].listAmphur" menu-props="auto" label=" -- อำเภอ --" hide-details 	single-line
																	item-text="name" item-value="id" v-model="item.cat_aa" v-on:change="getListSubDistrictArea(index)" ></v-autocomplete>
																</v-flex>
																<v-flex xs1 class="text-xs-right">
																	<v-btn flat icon color="pink" style="margin-top:-15px;" v-on:click="removeArea(index)" v-show="index > 0">
																		<v-icon>delete_forever</v-icon>
																	</v-btn>
																</v-flex>
															</v-layout>
															<!-- ตำบล รหัรหัสไปรษณีย์-->
															<v-layout row wrap class="mb-3">
																<v-flex xs10 offset-xs1 class="text-xs-left p-l-90 p-r-40">
																	<v-layout row wrap>
																		<v-flex xs12>

																			<table id="tumbonlist">
																				<tr>
																					<th class="wd-250" style="border-top-left-radius: 8px;">ชื่อตำบล</th>
																					<th class="wd-400" style="border-top-right-radius: 8px;">ยกเลิกพื้นที่ในความรับผิดชอบจากหน่วยเดิม (ถ้ามี)</th>
																				</tr>
																				<tr v-for="list, indexlist in odArea[index].cat_tt" :key="indexlist">
																					<td class="wd-250" >
																						<v-checkbox v-model="list.tumbon" :label="list.name"  color="indigo" ></v-checkbox>
																					</td>
																					<td class="wd-400">
																						<v-checkbox v-model="list.cancel" label="ยกเลิก" color="red darken-3" v-show="list.tumbon === true" ></v-checkbox>
																					</td>
																				</tr>
																			</table>


																		</v-flex>
																	</v-layout>
																</v-flex>

															</v-layout>

														</v-flex>
														<v-flex xs12>
															<!-- เพิ่มพื้นที่ -->
															<v-layout row wrap class="mb-3">
																<v-flex xs2 offset-xs10 class="text-xs-right">
																	<v-btn outline color="primary" v-on:click="addArea()"> เพิ่มพื้นที่</v-btn>
																</v-flex>
															</v-layout>
														</v-flex>
													</v-layout>
												</v-card>
												<v-card-actions>
													<v-btn  round outline color="default" @click.native="previousPage()">ย้อนกลับ</v-btn>
													<v-spacer></v-spacer>
													<v-btn round color="primary" @click.native="validateBeforeNextPage('step2')">เพิ่มหน่วยอำเภอ</v-btn>
												</v-card-actions>
											</v-stepper-content>
										</v-stepper-items>
									</v-stepper>
								</div>
								</v-flex>

							</v-layout>
						</v-container>
					</v-content>

					<v-dialog v-model="dialogProgress.status" persistent max-width="500px">
						<v-card class="text-xs-center">
							<v-btn class="m-t-30" fab dark large color="white" depressed >
								<v-icon dark color="red" style="font-size:45px!important;">notification_important</v-icon>
							</v-btn>
							<v-card-title class="justify-center pt-0">
								<span class="headline"><h4>{{dialogProgress.title}}</h4></span>
							</v-card-title>
							<v-card-text class="p-t-0 p-b-0">
								<v-container grid-list-md class="p-t-0 p-b-0">
									<v-layout wrap>
										<v-flex xs12>
											<v-progress-linear :indeterminate="true"></v-progress-linear>
										</v-flex>
									</v-layout>
								</v-container>
							</v-card-text>
							<v-card-actions class="p-b-20 justify-center">
							</v-card-actions>
						</v-card>
					</v-dialog>
					<v-dialog v-model="dialogSearchBranch" persistent max-width="700px">
						<v-card>
							<v-card-title>
								<span class="headline"><h6>กรุณาเลือกสาขาที่ต้องการเพิ่มหน่วยอำเภอ</h6></span>
							</v-card-title>
							<v-card-text>
								<v-container class="text-xs-center">
									<v-flex xs12 class="text-xs-center" style="padding-left:15%">
										<v-autocomplete
											box
											autofocus
											:items="listDivDialog"
											label="ฝ่ายกิจการสาขา"
											v-model="odInfoDialog.div"
											item-text="div_name" item-value="id"
											v-on:change="loadProvinceDialogSearch"

										></v-autocomplete>
									</v-flex>
									<v-flex xs12 class="text-xs-center" style="padding-left:15%">
										<v-autocomplete
											box
											autofocus
											:items="listProvinceDialog"
											label="สำนักงานจังหวัด"
											v-model="odInfoDialog.prov"
											item-text="brname" item-value="brcode"
											v-on:change="loadBranchDialogSearch"

										></v-autocomplete>
									</v-flex>
									<v-flex xs12 class="text-xs-center" style="padding-left:15%">
										<v-autocomplete
											box
											autofocus
											:items="listBranchDialog"
											label="สาขา"
											v-model="branchActive"
											item-text="display_name" item-value="id"
										></v-autocomplete>
									</v-flex>
								</v-container>
							</v-card-text>
							<v-card-actions>
								<v-btn color="grey darken-2" flat v-on:click="closeDialogSearchBranch()">ปิด</v-btn>
								<v-spacer></v-spacer>
								<v-btn color="blue darken-2" flat v-on:click="setUnitBranch()">ตกลง</v-btn>
							</v-card-actions>
						</v-card>
					</v-dialog>

				</div>

			</v-app>
		</div> <!-- end vuejs -->
	</div> <!-- end data-app -->

</body>
<script type="text/javascript">
Vue.http.options.emulateJSON = true;
Vue.http.options.emulateHTTP = true;
Vue.use(VeeValidate)
var vuejs = new Vue({
  el:"#vuejs",
	$_veeValidate: {
		validator: 'new'
	},
	data:{
		pageLoading: true,
		projectName: '',
		sessioncode: '',
		sessionname: '',
		dialogProgress: {status: false, title: ''},
		dialogSearchBranch: false,
		dictionary: {
			attributes: {
				// email: 'E-mail Address'
			},
			custom: {
				is_div: { required: () => 'กรุณาเลือกฝ่ายกิจการสาขา' },
				is_province: { required: () => 'กรุณาเลือกสำนักงานจังหวัด' },
				thainame: { required: () => 'กรุณาระบุชื่อภาษาไทย' },
				engname: { required: () => 'กรุณาระบุชื่อภาษาอังกฤษ' },
				aligname: { required: () => 'กรุณาระบุชื่อย่อสาขา' },
				addrProvince: { required: () => 'กรุณาเลือกจังหวัด' },
				addrDist: { required: () => 'กรุณาเลือกอำเภอ' },
				addrSubDist: { required: () => 'กรุณาเลือกตำบล' },
				addrZipcode: { required: () => 'กรุณาระบุรหัสไปรษณีย์' },
				addr: { required: () => 'กรุณาระบุเลขที่' }
			}
		},
		currentStep: 1,
		steps: 2,
		headerStep: ['ข้อมูลหน่วยอำเภอ', 'พื้นที่ในความรับผิดชอบ'],
		listDivDialog: [],
		listProvinceDialog: [],
		listBranchDialog: [],
		odInfoDialog: { div: '', prov: ''},
		branchActive: '',
		isSetBranch: false,
		odInfo: {is_div: '', is_div_nme: '', province: '', prov_nme: '', is_br: '', is_sbr: '', is_ch: '00', is_am: '00', is_thai_n: '', is_eng_n: '', is_open_dte: '', is_amp_n: '', code: ''},
		odAmphur: {is_div: '', is_br: '', is_sbr: '', is_ch: '01', is_am: '00', is_open_dte: new Date().toISOString().substr(0, 10), is_thai_n: '', is_eng_n: '', code: '00 - 0 - 00 - 00', is_div_nme: '', prov_nme: ''},
		odAddrAM: {province: '', amphur: '', tumbon: '', zipcode: '', addr: '', moo: '', soi: '', street: '', prov: '', dist: '', subdist: ''},
		odUnitAmphur: {is_ch: '', is_am: '', is_thai_n: '', is_eng_n: '', code: '00 - 00', distName: '', cat_code: ''},
		odArea: [],
		ctrAMInfo: {fax: '', wan: '' },
		telAMInfo: [{number: ''}],
		modal: false,
		menu2: false,
		menu3: false,
		listProvinceUnitAddr: [],
		listDistrictUnitAddr: [],
		listSubDistrictUnitAddr: [],
		odInfoTemp: [],
		listAddrArea: []

	},
  created (){
		this.checkLogin();
  },
  computed:
  {
  },
	mounted () {
		this.$validator.localize('th', this.dictionary);
	},
  watch: {
		steps (val) {
			if (this.currentStep > val) {
				this.currentStep = val;
			}
		},
  },
	methods:{
		// start method
		progressDialog (status, title) {
			this.dialogProgress.status = status;
			this.dialogProgress.title = title;
		},
		checkLogin () {
			this.$http.post('<?=base_url() ?>index.php/loginController/getSession').then((response) => {
				var userInfo = response.body;
				if (userInfo.emp_code != "nodata") {
					this.sessioncode = userInfo.emp_code;
					this.sessionname = userInfo.emp_name;
					this.loadDivDialogSearch();
					setTimeout(() => { this.pageLoading = false; $(".none-show").removeClass("none-show"); }, 1000)
				}
				else {
					this.pageLoading = false;
					swal({
						title: 'กรุณาเข้าสู่ระบบก่อนเข้าใช้งาน',
						text: '',
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.href = '<?=base_url() ?>';
					})
				}
			}, (response) => {});
		},
		openDialogSearchUnitBranch () {
			setTimeout(() => {
				this.dialogSearchBranch = true;
				this.isSetUnitBranch = false;
			}, 500)
		},
		closeDialogSearchBranch () {
			setTimeout(() => {
				this.dialogSearchBranch = false;
				this.isSetUnitBranch = true;
			}, 500)
		},
		loadDivDialogSearch () {
			this.listDivDialog = [];
			this.odInfoDialog.div = '';
			this.odInfoDialog.prov = '';
			this.branchActive = '';
			this.$http.post('<?=base_url() ?>index.php/odController/loadDivInfo').then((response) => {
				this.listDivDialog = response.body;
			}, (response) => {
			});
		},
		loadProvinceDialogSearch () {
			this.listProvinceDialog = [];
			this.odInfoDialog.prov = '';
			this.branchActive = '';
			if(this.odInfoDialog.div != '')
			{
				this.progressDialog(true, 'กำลังดำเนินการ กรุณารอสักครู่');
				this.$http.post('<?=base_url() ?>index.php/odController/loadProvinceInfo', {div: this.odInfoDialog.div}).then((response) => {
					this.listProvinceDialog = response.body;
					setTimeout(() => { this.progressDialog(false, null); }, 300)
				}, (response) => {
				});
			}
		},
		loadBranchDialogSearch () {
			this.listBranchDialog = [];
			this.branchActive = '';
			if(this.odInfoDialog.prov != '')
			{
				this.progressDialog(true, 'กำลังดำเนินการ กรุณารอสักครู่');
				this.$http.post('<?=base_url() ?>index.php/editBranchController/getListBranchName', {id: this.odInfoDialog.prov}).then((response) => {
					this.listBranchDialog = response.body;

					setTimeout(() => { this.progressDialog(false, null); }, 300)
				}, (response) => {
				});
			}
		},
		setUnitBranch () {
			if (this.odInfoDialog.div == '' || this.odInfoDialog.prov == '' || this.branchActive == '') {
				swal({
					title: 'กรุณากรอกข้อมูลให้ครบถ้วน',
					text: "",
					type: 'warning',
					showCancelButton: false,
					confirmButtonColor: '#3085d6',
					confirmButtonText: 'ตกลง',
					cancelButtonText: 'ยกเลิก',
				}).then((result) => {})
			}
			else {
				this.odInfoTemp = [];
				this.odAmphur = {is_div: '', is_br: '', is_sbr: '', is_ch: '01', is_am: '00', is_open_dte: new Date().toISOString().substr(0, 10), is_thai_n: '', is_eng_n: '', is_amp_n: '', code: '00 - 0 - 00 - 00', is_div_nme: '', prov_nme: ''};
				this.odAddrAM = {province: '', amphur: '', tumbon: '', zipcode: '', addr: '', moo: '', soi: '', street: '', prov: '', dist: '', subdist: ''};
				this.odUnitAmphur = {is_ch: '', is_am: '', is_thai_n: '', is_eng_n: '', code: '00 - 00', distName: '', cat_code: ''};
				this.ctrAMInfo = {fax: '', wan: '' };
				this.telAMInfo = [{number: ''}];
				this.dialogSearchBranch = false;

				this.dialogSearchBranch = false;
				this.progressDialog(true, 'กำลังดำเนินการ กรุณารอสักครู่');
				this.$http.post('<?=base_url() ?>index.php/createUnitController/getUnitBranchInformation', { id: this.branchActive }).then((response) => {

					var result = response.body;
					this.odInfoTemp = result[0];
					this.odInfo.is_div = '0' + 	this.odInfoTemp.is_div;
					this.odInfo.is_br =  this.odInfoTemp.is_br;
					this.odInfo.is_sbr = this.odInfoTemp.is_sbr;
					this.odInfo.is_ch =  this.odInfoTemp.is_ch;
					this.odInfo.is_am =  this.odInfoTemp.is_am;
					this.odInfo.code = this.odInfoTemp.is_br + ' - ' + this.odInfoTemp.is_sbr + ' - ' + this.odInfoTemp.is_ch + ' - ' + this.odInfoTemp.is_am;
					this.odInfo.is_thai_n = this.odInfoTemp.is_thai_n;
					this.odInfo.is_eng_n = this.odInfoTemp.is_eng_n;
					this.odInfo.is_amp_n = this.odInfoTemp.is_amp_n;
					this.odInfo.is_open_dte =  this.dateFormat(this.odInfoTemp.is_open_dte);
					this.odInfo.is_div_nme = this.odInfoTemp.div_nme;
					this.odInfo.prov_nme = this.odInfoTemp.brname;


					this.getListProvince();
					this.setArea();
					setTimeout(() => {
						this.isSetBranch = true;
						this.progressDialog(false, null);
					}, 3000)

				}, (response) => {
				});
			}
		},
		getListProvince () {
			this.listProvinceUnitAddr = [];
			this.amphurAddr = [];
			this.odAddrAM.amphur = '';
			this.odAddrAM.tumbon = '';
			this.odAddrAM.zipcode = '';
			var me = this;
			this.$http.post('<?=base_url() ?>index.php/createUnitController/getListProvince').then((response) => {
				this.listProvinceUnitAddr = response.body;
				if(this.isSetBranch == false) {
					this.odAddrAM.province = this.odInfoTemp.cat_cc;
					this.getListAmphur();
				}
			}, (response) => {
			});
		},
		getListAmphur () {
			var me = this;
			this.odAddrAM.amphur = '';
			this.odAddrAM.tumbon = '';
			this.odAddrAM.zipcode = '';
			this.$http.post('<?=base_url() ?>index.php/createUnitController/getListAmphur', {provinceID: this.odAddrAM.province}).then((response) => {
				this.listDistrictUnitAddr = response.body;
			}, (response) => {
			});
		},
		getListTumbon () {
			var me = this;
			this.odAddrAM.tumbon = '';
			this.odAddrAM.zipcode = '';
			var indexOf = _.findIndex(this.listDistrictUnitAddr, function(o) { return o.id == me.odAddrAM.amphur; });
			this.odAddrAM.zipcode = this.listDistrictUnitAddr[indexOf]['postcode'];
			this.$http.post('<?=base_url() ?>index.php/createUnitController/getListDistrict', {provinceID: this.odAddrAM.province, amphurID: this.odAddrAM.amphur}).then((response) => {
				this.listSubDistrictUnitAddr = response.body;
			}, (response) => {});
		},
		getListZipcode () {
			var me = this;
			var indexOf = _.filter(this.listSubDistrictUnitAddr, function(o) { return o.id == me.odAddrAM.tumbon; });
			this.odAddrAM.zipcode = indexOf[0]['postcode'];
		},
		addTelephone () {
			this.telAMInfo.push({number: ''});
		},
		removeTelephone (index) {
			swal({
				title: 'ยืนยันการลบข้อมูลเบอร์โทรศัพท์',
				text: "",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'ตกลง',
				cancelButtonText: 'ยกเลิก',
			}).then((result) => {
				if(result.value) {
					if (this.telAMInfo.length > 1) {
						this.telAMInfo.splice(index,1)
					}
				}
			})
		},
		setArea () {
			this.odArea.push({cat_cc: '', cat_aa: '', cat_tt: [], cat_name: ''});
			this.listAddrArea.push({listProvince: [], listAmphur: [], listDistrict: []});
			this.getListProvinceArea(0);
		},
		getListProvinceArea (index) {
			this.listAddrArea[index].listProvince = [];
			this.listAddrArea[index].listAmphur = [];
			this.listAddrArea[index].listDistrict = [];
			this.odArea[index].cat_aa = [];
			this.odArea[index].cat_tt = [];
			var me = this;
			this.$http.post('<?=base_url() ?>index.php/createBranchController/getListProvince').then((response) => {
				this.listAddrArea[index].listProvince  = response.body;
				this.listAddrArea[index].listProvince.unshift({id: '', name: '-- จังหวัด --', code: ''})
					if (index == 0) {
						setTimeout(() => {
							this.odArea[0].cat_cc = this.odAddrAM.province;
							this.getListDistrictArea(0);
						}, 500)
					}
					else if (index > 0) {
						this.odArea[index].cat_cc = this.odArea[index-1].cat_cc;
						this.getListDistrictArea(index);
					}
			}, (response) => {
			});
		},
		getListDistrictArea (index) {
			this.listAddrArea[index].listAmphur = [];
			this.listAddrArea[index].listDistrict = [];
			this.odArea[index].cat_aa = [];
			this.odArea[index].cat_tt = [];
			var me = this;
			this.$http.post('<?=base_url() ?>index.php/createBranchController/getListDistrict', {provinceID: this.odArea[index].cat_cc}).then((response) => {
				this.listAddrArea[index].listAmphur = response.body;
			}, (response) => {
			});
		},
		getListSubDistrictArea (index) {
			this.listAddrArea[index].listDistrict = [];
			this.odArea[index].cat_tt = [];
			this.$http.post('<?=base_url() ?>index.php/createBranchController/getListSubDistrict', {provinceID: this.odArea[index].cat_cc, amphurID: this.odArea[index].cat_aa}).then((response) => {
				this.listAddrArea[index].listDistrict = response.body;
				for(var i = 0 ; i < (this.listAddrArea[index].listDistrict.length); i++) {
					this.odArea[index].cat_tt.push({name: this.listAddrArea[index].listDistrict[i].name, tumbon: false, cancel: false, code: this.listAddrArea[index].listDistrict[i].code});
				}
			}, (response) => {});
		},
		addArea () {
			this.odArea.push({cat_cc: '', cat_aa: '', cat_tt: [], cat_name: ''});
			this.listAddrArea.push({listProvince: [], listAmphur: [], listDistrict: []});
			this.getListProvinceArea(this.odArea.length-1);
		},
		removeArea (index) {
			swal({
				title: 'ยืนยันการลบข้อมูลพื้นที่นี้',
				text: "",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'ตกลง',
				cancelButtonText: 'ยกเลิก',
			}).then((result) => {
				if(result.value) {
					if (this.odArea.length > 1) {
						this.odArea.splice(index,1);
						this.listAddrArea.splice(index,1);
					}
				}

			})
		},
		dateFormat (str) {
			if (str == null || str == '' || str == ' ') {
				return null;
			}
			else {
				var dataStr = str.substr(0, 4) + '-' + str.substr(4, 2) + '-' + str.substr(6, 2);
				return dataStr.toString();
			}
		},
		validateBeforeNextPage(scope) {
			this.$validator.validateAll(scope).then((result) => {
				if (result) {
					if(this.currentStep < this.steps) {
						this.nextPage();
					}
					else {
						swal({
							title: 'ยืนยันการเพิ่มข้อมูลหน่วยอำเภอ',
							text: '',
							type: 'warning',
							showCancelButton: true,
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'ตกลง',
							cancelButtonText: 'ยกเลิก',
							allowOutsideClick: false
						}).then((result) => {
							if(result.value) {

								this.odAddrAM.subdist = $('#listSubDistrictUnitAddr').val();
								this.odAddrAM.dist = $('#listDistrictUnitAddr').val();
								this.odAddrAM.prov = $('#listProvinceUnitAddr').val();

								this.currentStep = 1;
								this.progressDialog(true, 'กำลังดำเนินการ กรุณารอสักครู่');
								setTimeout(() => { this.submitCreateUnitBranchInformation(); }, 500)
							}

						})
					}
				}
				else {
					swal({
						title: 'กรุณาตรวจสอบข้อมูลให้ถูกต้อง',
						text: '',
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.href = "#";
					})

				}
			});
		},
		nextPage () {
			this.currentStep = this.currentStep + 1;
			window.location.href = "#";
		},
		previousPage () {
			this.currentStep = this.currentStep - 1;
			window.location.href = "#";
		},
		submitCreateUnitBranchInformation () {
			this.$http.post('<?=base_url() ?>index.php/createUnitController/createUnitBranchInformation', {odInfo: this.odInfo, odAmphur: this.odAmphur, odAddrAM: this.odAddrAM, telInfo: this.telAMInfo, ctrInfo: this.ctrAMInfo, odUnitAmphur: this.odUnitAmphur, odArea: this.odArea}).then((response) => {
				console.log(response.body);
				this.progressDialog(false, null);
				var result = response.body;
				if (result == 'Successfully') {
					swal({
						title: 'สร้างหน่วยอำเภอเรียบร้อยแล้ว',
						text: '',
						type: 'success',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.reload();
					})
				}
				else {
					swal({
						title: 'ไม่สามารถสร้างหน่วยอำเภอได้',
						text: 'กรุณาลองใหม่อีกครั้ง',
						type: 'error',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.href = "#";
					})
				}
			}, (response) => {});
		}
  // end methods
  }
})
</script>
</html>
