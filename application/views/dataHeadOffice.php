<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicons -->
	<link href="<?php echo $this->config->item('icon_project'); ?>" rel="icon">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/vuetify/vuetify.css" />
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/util.css">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/custom.css">
	<title><?php echo $this->config->item('project_name'); ?></title>

	<style type="text/css">
		/* .theme--light.v-table thead tr:first-child {
			border: 1px solid #f2f2f2 !important;
			background-color: #f2f2f2 !important;
		} */
		.theme--light.v-table thead th {
			border: 1px solid #ddd !important;
			background-color: #f2f2f2 !important;
			font-weight: bold !important;
			color: #000 !important;
			font-family: 'Trebuchet MS' !important;
			font-size: 15px !important;
		}
		table.v-table tbody td {
			border: 1px solid #ddd !important;
		}
		.v-list__tile {
			margin-top: 5px !important;
			padding-right: 30px !important;
		}
		.blue-text {
			color: #3EC1D5;
		}
		.v-input {
			padding-top: 0px !important;
    	margin-top: 0px !important;
    	padding-right: 50px !important;
		}
		.table-text {
			font-family: 'Trebuchet MS' !important;
			font-size: 15px !important;
		}
		.box-btn {
			padding-top: 5px !important;
    	height: 70px !important;
			border-radius: 5px !important;
		}
	</style>
</head>
<body class="white-bg">

	<div data-app="true" class="application--light">
		<div class="none-show"><?php $this->load->view('header.php'); ?></div>

		<div id="vuejs" class="white-bg" >
			<v-app>

				<!-- loading -->
				<div v-if="pageLoading" class="loading-page">
					<v-content>
						<v-container fluid fill-height>
							<v-layout justify-center align-center>
								<v-progress-circular :size="200" :width="20" color="primary" indeterminate></v-progress-circular>
							</v-layout>
						</v-container>
					</v-content>
				</div>
				<!-- content -->
				<div  class="none-show">
					<!-- content -->
					<v-content style="background-color: #fff;">
						<v-container fluid  class="p-t-0 p-l-0 p-r-0">
							<v-layout  row wrap>
								<!-- หัวข้อ-->
								<v-flex xs12 class="m-t-15 m-b-5 text-xs-center">
									<v-layout row wrap>
										<v-flex xs3 offset-xs4>
											<span><i class="material-icons icon-title-small">location_city</i></span>
											<h2>โครงสร้างแต่ละส่วนงานในสำนักงานใหญ่</h2>
										</v-flex>
										<v-flex xs5 class="text-xs-right p-r-20">
											<v-menu transition="slide-x-transition" bottom left offset-x >
												<v-btn slot="activator" class="box-btn" color="primary">
													<v-layout  row wrap>
														<v-flex xs12><i class="material-icons">print</i></v-flex>
														<v-flex xs12>พิมพ์</v-flex>
													</v-layout>
												</v-btn>
												<v-list>
													<v-list-tile>
														<v-list-tile-title class="point-cursor">

															<a v-on:click="DoReport()"> <img src="<?=base_url() ?>assets/images/pdf.png"  height="23" class="mr-2"> PDF </a>
														</v-list-tile-title>
													</v-list-tile>
													<v-list-tile>
														<v-list-tile-title class="point-cursor">

															<a id="exportHeadOffice"> <img src="<?=base_url() ?>assets/images/excel.png"  height="23" class="mr-2"> Excel </a>
														</v-list-tile-title>
													</v-list-tile>
												</v-list>
											</v-menu>
										</v-flex>
									</v-layout>

								</v-flex>
							</v-layout>
							<v-layout  row wrap class="">
								<!-- ตาราง-->
								<v-flex xs12 class="m-l-10 m-b-10 m-r-10">

									<v-data-table
									:headers="headers"
									:items="itemsDivision"
									class="elevation-1"
									hide-actions
									>
									<template slot="items" slot-scope="props">
										<td class="text-xs-center table-text ">{{ props.index + 1 }}</td>
										<td class="text-xs-center table-text " style="width:150px;">{{ props.item.id }} </td>
										<td class="text-xs-center table-text " >{{props.item.org_cd}}</td>
										<td class="text-xs-left table-text">{{ props.item.th_org_nme}}</td>
										<td class="text-xs-left table-text">{{ props.item.eng_org_nme }}</td>
										<td class="text-xs-center table-text">{{ props.item.shrt_org_nme }}</td>
										<!-- <td class="text-xs-left table-text">{{ props.item.mn_name }}</td> -->
										<td class="text-xs-center table-text" style="width:100px;">{{ dateFormat(props.item.start_oper_dte) }}</td>
									</template>
								</v-data-table>

							</v-flex>
						</v-layout>
						<v-layout row wrap>
							<v-flex xs12>
								<a href="#">
									<v-btn absolute dark fixed fab 	bottom right small color="cyan" class="m-b-35" >
										<v-icon>keyboard_arrow_up</v-icon>
									</v-btn>
								</a>
							</v-flex>
						</v-layout>
					</v-container>
				</v-content>


				<v-dialog v-model="dialogProgress.status" persistent max-width="500px">
					<v-card class="text-xs-center">
						<v-btn class="m-t-30" fab dark large color="white" depressed >
							<v-icon dark color="red" style="font-size:45px!important;">notification_important</v-icon>
						</v-btn>
						<v-card-title class="justify-center pt-0">
							<span class="headline"><h4>{{dialogProgress.title}}</h4></span>
						</v-card-title>
						<v-card-text class="p-t-0 p-b-0">
							<v-container grid-list-md class="p-t-0 p-b-0">
								<v-layout wrap>
									<v-flex xs12>
										<v-progress-linear :indeterminate="true"></v-progress-linear>
									</v-flex>
								</v-layout>
							</v-container>
						</v-card-text>
						<v-card-actions class="p-b-20 justify-center">
						</v-card-actions>
					</v-card>
				</v-dialog>

			</div>

			</v-app>
			<!-- start table for export excel -->
			<div style="display:none;">
				<table id="headOfficeInfo" >
					<thead>
						<tr>
							<th>ลำดับ</th>
							<th>รหัสส่วนงาน</th>
							<th>เลขที่ออกหนังสือ</th>
							<th>ชื่อส่วนงาน</th>
							<th>ชื่อภาษาอังกฤษ</th>
							<th>ชื่อย่อ</th>
							<th>วันที่เปิดดำเนินการ</th>
						</tr>
					</thead>
					<tbody v-for="list ,index in itemsDivision">
						<tr>
							<td style="text-align:center;">{{index + 1}}</td>
							<td style="text-align:center;">{{list.id}}</td>
							<td style="text-align:center;">{{list.org_cd}}</td>
							<td>{{list.th_org_nme}}</td>
							<td>{{list.eng_org_nme}}</td>
							<td style="text-align:center;">{{list.shrt_org_nme}}</td>
							<td style="text-align:center;">{{dateFormat(list.start_oper_dte)}}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- end table for export excel -->
		</div>
	</div>
	
</body>
<script type="text/javascript">
Vue.http.options.emulateJSON = true;
Vue.http.options.emulateHTTP = true;
var vuejs = new Vue({
  el:"#vuejs",
	data:{
		pageLoading: true,
		projectName: 'ระบบงานที่ตั้งสำนักงานของธนาคาร',
		sessioncode: '',
		sessionname: '',
		dialogProgress: {status: false, title: ''},
		headers: [
			{ text: 'ลำดับ', align: 'center', sortable: false, value: 'index'},
			{ text: 'รหัสส่วนงาน', align: 'center', sortable: true, value: 'id' },
			{ text: 'เลขที่ออกหนังสือ', align: 'center', sortable: true, value: 'org_cd' },
			{ text: 'ชื่อส่วนงาน', align: 'center', sortable: true, value: 'th_org_nme' },
			{ text: 'ชื่อส่วนงานภาษาอังกฤษ', align: 'center', sortable: true, value: 'eng_org_nme' },
			{ text: 'ชื่อย่อ', align: 'center', sortable: true, value: 'shrt_org_nme' },
			// { text: 'ชื่อสังกัด', align: 'center', sortable: true, value: 'mn_name' },
			{ text: 'วันที่เปิดดำเนินการ', align: 'center', sortable: true, value: 'start_oper_dte' }
		],
		itemsDivision: []
	},
  created (){
		this.checkLogin();
  },
  computed:
  {
  },
	mounted () {
	},
	beforeDestroy () {
		clearInterval(this.interval)
	},
  watch: {
  },
	methods:{
		// start method
		progressDialog (status, title) {
			this.dialogProgress.status = status;
			this.dialogProgress.title = title;
		},
		checkLogin () {
			this.$http.post('<?=base_url() ?>index.php/loginController/getSession').then((response) => {
				var userInfo = response.body;
				if (userInfo.emp_code != "nodata") {
					this.sessioncode = userInfo.emp_code;
					this.sessionname = userInfo.emp_name;
					setTimeout(() => { this.pageLoading = false; $(".none-show").removeClass("none-show"); this.loadAllDivisionInformation();}, 1000)
				}
				else {
					this.pageLoading = false;
					swal({
						title: 'กรุณาเข้าสู่ระบบก่อนเข้าใช้งาน',
						text: '',
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.href = '<?=base_url() ?>';
					})
				}
			}, (response) => {});
		},
		loadAllDivisionInformation () {
			this.progressDialog(true, 'กำลังดำเนินการ กรุณารอสักครู่');
			this.$http.post('<?=base_url() ?>index.php/headOfficeController/loadAllDivisionInformation').then((response) => {
				this.itemsDivision = response.body;
				this.progressDialog(false, null);
			}, (response) => {});
		},
		dateFormat (str) {
			if (str == '00000000')
				return '00/00/0000';
			else if (str != '' || str != null)
				return str.substr(6, 2) + '/' + str.substr(4, 2) + '/' + (parseInt(str.substr(0, 4)) + 543).toString();
			else
				return null;
		},
		DoReport() {
			this.progressDialog(true, 'กำลังดำเนินการ กรุณารอสักครู่');
			var date = new Date();
			var hours = date.getHours();
			var minutes = date.getMinutes();
			var day = date.getDate();
			var month = date.getMonth()+1;

			day = day < 10 ? '0'+day : day;
			month = month < 10 ? '0'+month : month;
			hours = hours < 10 ? '0'+hours : hours; // the hour '0' should be '12'
			minutes = minutes < 10 ? '0'+minutes : minutes;

			var strTime = hours + ':' + minutes;
			var strDT = day + "/" + month + "/" + date.getFullYear() + "  " + strTime;

			this.createPDFMake (strDT, this.itemsDivision);
		},
		createPDFMake (strDT, result) {
			data = this.table(result, ['id', 'org_cd', 'th_org_nme', 'eng_org_nme', 'shrt_org_nme', 'start_oper_dte']);
			pdfMake.fonts = {
					Angsana: {
						normal: 'angsa.ttf'
					}
			}
			var docDefinition = {
				defaultStyle: {
					font: 'Angsana',
					fontSize: 13
				},
				pageSize: 'A4',
				// pageOrientation: 'landscape',
				pageMargins: [ 15, 20, 15, 35],
				content: [
					{ text: 'ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร ', style: 'header', alignment: 'center', fontSize: 16 },
					{ text: 'โครงสร้างแต่ละส่วนงานในสำนักงานใหญ่', style: 'header', alignment: 'center', fontSize: 16 },
					{ text: 'ตั้งแต่รหัสสังกัด ' + result[0].id + ' ถึงรหัสสังกัด ' + result[result.length-1].id , style: 'header', alignment: 'center', fontSize: 14 },
					{ text: ' ' , style: 'header', alignment: 'center', fontSize: 14 },
					data
				],
				footer: function(page, pages) {
					return {
						columns: [
							{
								text: 'พิมพ์เมื่อวันที่ ' + strDT
							},
							{
								alignment: 'right',
								text: [
									{
										text: 'หน้า '
									},
									{
										text: page.toString()
									}
								]
							}
						],
						margin: [ 20, 0, 20, 0 ]
					}
				} //end footer

			}

			pdfMake.createPdf(docDefinition).open();
			this.progressDialog(false, null);
		},
		buildTableBody(data, columns) {
				var body = [];
				var head = [
					{
						text: 'ลำดับ',
						alignment: 'center',
						fontSize: 14
					},
					{
						text: 'รหัสส่วนงาน',
						alignment: 'center',
						fontSize: 14
					},
					{
						text: 'เลขที่ออกหนังสือ',
						alignment: 'center',
						fontSize: 14
					},
					{
						text: 'ชื่อส่วนงาน',
						alignment: 'center',
						fontSize: 14
					},
					{
						text: 'ชื่อส่วนงานภาษาอังกฤษ',
						alignment: 'center',
						fontSize: 14
					},
					{
						text: 'ชื่อย่อ',
						alignment: 'center',
						fontSize: 14
					},
					{
						text: 'วันที่เปิดดำเนินการ',
						alignment: 'center',
						fontSize: 14
					}
				];
				body.push(head);
				var i = 1;
				var me = this;
				data.forEach(function(row) {
						var dataRow = [];
						dataRow.push({'text': i, 'alignment': 'center'});
						dataRow.push({'text': row.id, 'alignment': 'center'});
						dataRow.push({'text': row.org_cd, 'alignment': 'center'});
						dataRow.push({'text': row.th_org_nme, 'alignment': 'center'});
						dataRow.push({'text': row.eng_org_nme, 'alignment': 'center'});
						dataRow.push({'text': row.shrt_org_nme, 'alignment': 'center'});
						dataRow.push({'text': me.dateFormat(row.start_oper_dte), 'alignment': 'center'});
						body.push(dataRow);
						i = i + 1
				});

				return body;
		},
		table (data, columns) {
			var me = this
				return {
						table: {
								headerRows: 1,
								widths: [25, 55, 68, 125, 125, 30, 73],
								alignment: 'center',
								body: me.buildTableBody(data, columns)
						}
				};
		}
  // end methods
  }
})
</script>
<script type="text/javascript">
  $("#exportHeadOffice").click(function(){
    $("#headOfficeInfo").table2excel({
    name: "Excel Document Name",
    filename: "โครงสร้างแต่ละส่วนงานในสำนักงานใหญ่",
    fileext: ".xls",
    exclude_img: true,
    exclude_links: true,
    exclude_inputs: true
  });
  });
</script>
</html>
