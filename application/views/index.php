<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicons -->

</head>
<body>
	<!-- Vue -->
	<script src="<?=base_url() ?>assets/libs/vue/vue.js"></script>
	<script src="<?=base_url() ?>assets/libs/vue-resource/vue-resource.js"></script>
</body>
<script type="text/javascript">
Vue.http.options.emulateJSON = true;
Vue.http.options.emulateHTTP = true;
var vuejs = new Vue({
  el:"#vuejs",
	data:{
	},
  created (){
		this.checkBrowser();
  },
  computed:
  {
  },
	methods:{
		// start method\
		checkBrowser () {
			// Firefox 1.0+
			var isFirefox = typeof InstallTrigger !== 'undefined';
			// Safari 3.0+ "[object HTMLElementConstructor]"
			var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
			// Internet Explorer 6-11
			var isIE = /*@cc_on!@*/false || !!document.documentMode;
			// Edge 20+
			var isEdge = !isIE && !!window.StyleMedia;

			if (isFirefox || isSafari || isIE || isEdge) {
				window.location.href = '<?=base_url() ?>index.php/main/openwithchrome'
			}
			else {
				this.relocation()
			}
		},
		relocation () {
			window.location = '<?=base_url() ?>index.php/Main/main';
		}
  // end methods
  }
})
</script>
</html>
