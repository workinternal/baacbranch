<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicons -->
	<link href="<?php echo $this->config->item('icon_project'); ?>" rel="icon">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/vuetify/vuetify.css" />
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/util.css">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/custom.css">
	<title><?php echo $this->config->item('project_name'); ?></title>

	<style type="text/css">
		table.v-table tbody td {
			border: 1px solid #ddd !important;
		}
		.box-area {
			border: 1px solid #ddd !important;
    	padding: 20px !important;
    	border-radius: 5px !important;
    	margin-top: 20px !important;
		}
		.theme--light.v-table thead th {
    	border: 1px solid #ddd !important;
    	background-color: #f2f2f2 !important;
    	color: #000 !important;
    	font-weight: normal  !important;
			font-size: 15px;
		}
		table.v-table tbody td {
			font-size: 14px !importan;
		}
	</style>
</head>
<body>

	<div data-app="true" class="application--light">
		<div class="none-show" ><?php $this->load->view('header.php'); ?></div>
		<div id="vuejs">
			<v-app id="inspire"  class="white-bg" >
				<!-- loading -->
				<div v-show="pageLoading" class="loading-page">
					<v-content>
						<v-container fluid fill-height>
							<v-layout justify-center align-center>
								<v-progress-circular :size="200" :width="20" color="primary" indeterminate></v-progress-circular>
							</v-layout>
						</v-container>
					</v-content>
				</div>
				<!-- content -->
				<div class="none-show">

					<!-- content -->
					<v-content style="background-color: #fff;">
						<v-container fluid  class="p-t-0 p-l-0 p-r-0">
							<v-layout  row wrap>
								<v-flex xs10 offset-xs1 class="pt-2 text-xs-center">
									<span><i class="material-icons icon-title-big">not_interested</i></span> <h2>ยกเลิกส่วนงาน</h2>
								</v-flex>
								<!-- หัวข้อ-->
								<v-flex xs10 offset-xs1 class="m-t-5 m-b-5 text-xs-center">
									<div class="p-l-50 p-t-0 p-r-50 p-b-50">
										<div class="text-xs-center" >
											<h4 style="color: #D32F2F;">* กรุณาเลือกส่วนงานที่ต้องการยกเลิก</h4>
											<v-btn outline round color="primary" v-on:click="opendialogSearchDivision()">ค้นหาส่วนงาน</v-btn>
										</div>
										<v-card  class="box-area" >
											<!--ชื่อไทย ชื่อังกฤษ -->
											<v-layout row wrap class="mb-3">
												<v-flex xs2 class="text-xs-left pl-4">
													<h4 class="pt-3">รหัสส่วนงาน</h4>
												</v-flex>
												<v-flex xs4 class="text-xs-left">
													<h4 class="pt-3">{{odInfo.code}}</h4>
												</v-flex>
											</v-layout>

											<!--ชื่อไทย ชื่อังกฤษ -->
											<v-layout row wrap class="mb-3">
												<v-flex xs2 class="text-xs-left pl-4">
													<h4 class="pt-3">ชื่อไทย</h4>
												</v-flex>
												<v-flex xs4 class="text-xs-left">
													<h4 class="pt-3">{{odInfo.th_org_nme}}</h4>
												</v-flex>
												<v-flex xs2 class="text-xs-left">
													<h4 class="pt-3">ชื่ออังกฤษ</h4>
												</v-flex>
												<v-flex xs4 class="text-xs-left">
													<h4 class="pt-3">{{odInfo.eng_org_nme}}</h4>
												</v-flex>
											</v-layout>

											<!--ชื่อย่อ วันเริ่ม -->
											<v-layout row wrap class="mb-3">
												<v-flex xs2 class="text-xs-left pl-4">
													<h4 class="pt-3">ชื่อย่อ</h4>
												</v-flex>
												<v-flex xs4 class="text-xs-left">
													<h4 class="pt-3">{{odInfo.shrt_org_nme}}</h4>
												</v-flex>
												<v-flex xs2 class="text-xs-left">
													<h4 class="pt-3">วันที่บังคับใช้</h4>
												</v-flex>
												<v-flex xs4 class="text-xs-left">
													<h4 class="pt-3">{{odInfo.start_oper_dte}}</h4>
												</v-flex>
											</v-layout>


											<!--ชื่อสังกัด -->
											<v-layout row wrap class="mb-3">
												<v-flex xs2 class="text-xs-left pl-4">
													<h4 class="pt-3">ชื่อสังกัด</h4>
												</v-flex>
												<v-flex xs4 class="text-xs-left">
													<h4 class="pt-3">{{odInfo.mn_name}}</h4>
												</v-flex>
												<v-flex xs2 class="text-xs-left">
													<h4 class="pt-3">ชื่อย่อสังกัด</h4>
												</v-flex>
												<v-flex xs4 class="text-xs-left">
													<h4 class="pt-3">{{odInfo.shrt_mn_name}}</h4>
												</v-flex>
											</v-layout>
										</v-card>
										<v-card-actions v-show=" isSetDivison == true ">
											<v-spacer></v-spacer>
											<v-btn round color="red" dark v-on:click="closeHeadOffice()">ยกเลิกส่วนงาน</v-btn>
											<v-spacer></v-spacer>
										</v-card-actions>
									</div>
								</v-flex>


							</v-layout>
						</v-container>
					</v-content>

					<v-dialog v-model="dialogProgress.status" persistent max-width="500px">
						<v-card class="text-xs-center">
							<v-btn class="m-t-30" fab dark large color="white" depressed >
								<v-icon dark color="red" style="font-size:45px!important;">notification_important</v-icon>
							</v-btn>
							<v-card-title class="justify-center pt-0">
								<span class="headline"><h4>{{dialogProgress.title}}</h4></span>
							</v-card-title>
							<v-card-text class="p-t-0 p-b-0">
								<v-container grid-list-md class="p-t-0 p-b-0">
									<v-layout wrap>
										<v-flex xs12>
											<v-progress-linear :indeterminate="true"></v-progress-linear>
										</v-flex>
									</v-layout>
								</v-container>
							</v-card-text>
							<v-card-actions class="p-b-20 justify-center">
							</v-card-actions>
						</v-card>
					</v-dialog>

					<v-dialog v-model="dialogSearchDivision" persistent max-width="700px">
						<v-card>
							<v-card-title>
								<span class="headline"><h6>กรุณาเลือกส่วนงานที่ต้องการยกเลิก</h6></span>
							</v-card-title>
							<v-card-text>
								<v-container class="text-xs-center">
									<v-layout row wrap>
										<v-flex xs12 class="text-xs-center" style="padding-left:15%">
											<v-autocomplete
												box
												autofocus
												:items="listDivision"
												label="ชื่อส่วนงาน"
												v-model="headOfficeID"
												item-text="div_name" item-value="id"
											></v-autocomplete>
										</v-flex>
									</v-layout>
								</v-container>
							</v-card-text>
							<v-card-actions>
								<v-btn color="grey darken-2" flat v-on:click="dialogSearchDivision = false">ปิด</v-btn>
								<v-spacer></v-spacer>
								<v-btn color="blue darken-2" flat v-on:click="setHeadOfficeInformation()">ตกลง</v-btn>
							</v-card-actions>
						</v-card>
					</v-dialog>

				</div>

			</v-app>
		</div> <!-- end vuejs -->
	</div> <!-- end data-app -->

</body>
<script type="text/javascript">
Vue.http.options.emulateJSON = true;
Vue.http.options.emulateHTTP = true;
var vuejs = new Vue({
  el:"#vuejs",
	data:{
		pageLoading: true,
		projectName: 'ระบบงานที่ตั้งสำนักงานของธนาคาร',
		sessioncode: '',
		sessionname: '',
		dialogProgress: {status: false, title: ''},
		e1: 0,
		menu1: false,
		odInfo: {id: '', code: '', th_org_nme: '', eng_org_nme: '', shrt_org_nme: '', start_oper_dte: '', mn_name: '', shrt_mn_name: '', underManager: false},
		typeMenu: [],
		dictionary: {
			custom: {
				type: { required: () => 'กรุณาเลือกประเภท' },
				thainame: { required: () => 'กรุณาระบุชื่อภาษาไทย' },
				engname: { required: () => 'กรุณาระบุชื่อภาษาอังกฤษ' },
				aligname: { required: () => 'กรุณาระบุชื่อย่อ' },
				mainname: { required: () => 'กรุณาระบุชื่อสังกัด' },
				mainnameas: { required: () => 'กรุณาระบุชื่อย่อสังกัด' }
			}
		},
		underManager: false,
		headOfficeID: '',
		dialogSearchDivision: false,
		listDivision: [],
		odInfoTemp: [],
		isSetDivison: false

	},
  created (){
		this.checkLogin();
  },
  computed:
  {
  },
	mounted () {
	},
  watch: {
  },
	methods:{
		// start method
		progressDialog (status, title) {
			this.dialogProgress.status = status;
			this.dialogProgress.title = title;
		},
		checkLogin () {
			this.$http.post('<?=base_url() ?>index.php/loginController/getSession').then((response) => {
				var userInfo = response.body;
				if (userInfo.emp_code != "nodata") {
					this.sessioncode = userInfo.emp_code;
					this.sessionname = userInfo.emp_name;
					this.loadListDivision();
					setTimeout(() => { this.pageLoading = false; $(".none-show").removeClass("none-show"); }, 1000)
				}
				else {
					this.pageLoading = false;
					swal({
						title: 'กรุณาเข้าสู่ระบบก่อนเข้าใช้งาน',
						text: '',
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.href = '<?=base_url() ?>';
					})
				}
			}, (response) => {});
		},
		opendialogSearchDivision () {
			setTimeout(() => {
				this.dialogSearchDivision = true;
				this.isSetDivison = false;
			}, 500)
		},
		closedialogSearchDivision () {
			setTimeout(() => {
				this.dialogSearchDivision = false;
				if (this.headOfficeID != '' && this.odInfoTemp.length != 0)
					this.isSetDivison = true;
			}, 500)
		},
		loadListDivision () {
			this.$http.post('<?=base_url() ?>index.php/headOfficeController/loadListDivision').then((response) => {
				this.listDivision = response.body;
			}, (response) => {});
		},
		dateFormat (str) {
			if (str == null || str == '' || str == ' ') {
				return null;
			}
			else {
				var dataStr = str.substr(0, 4) + '-' + str.substr(4, 2) + '-' + str.substr(6, 2);
				return dataStr.toString();
			}
		},
		setHeadOfficeInformation () {
			if (this.headOfficeID == '') {
				swal({
					title: 'กรุณาเลือกส่วนงาน',
					text: '',
					type: 'warning',
					showCancelButton: false,
					confirmButtonColor: '#3085d6',
					confirmButtonText: 'ตกลง',
					cancelButtonText: 'ยกเลิก',
					allowOutsideClick: false
				})
			}
			else {
				this.dialogSearchDivision = false;
				this.odInfoTemp = [];
				this.odInfo = {id: '', code: '', th_org_nme: '', eng_org_nme: '', shrt_org_nme: '', start_oper_dte: '', mn_name: '', shrt_mn_name: '', underManager: ''};
				this.progressDialog(true, 'กำลังดำเนินการ กรุณารอสักครู่');
				this.$http.post('<?=base_url() ?>index.php/headOfficeController/loadDivisionInformation', {id: this.headOfficeID}).then((response) => {
					var result = response.body;
					this.odInfoTemp 						= result[0];
					this.odInfo.code 						= this.odInfoTemp.code;
					this.odInfo.th_org_nme 			= this.odInfoTemp.th_org_nme;
					this.odInfo.oldName					= this.odInfo.th_org_nme;
					this.odInfo.eng_org_nme 		= this.odInfoTemp.eng_org_nme;
					this.odInfo.shrt_org_nme 		= this.odInfoTemp.shrt_org_nme;
					if (this.odInfoTemp.th_org_nme === this.odInfoTemp.mn_name) {
						this.odInfo.underManager 	= false;
					}
					else {
						this.odInfo.underManager 	= true;
					}
					this.odInfo.mn_name 				= this.odInfoTemp.mn_name;
					this.odInfo.shrt_mn_name 		= this.odInfoTemp.shrt_mn_name;
					this.odInfo.start_oper_dte 	= this.dateFormat(this.odInfoTemp.start_oper_dte);
					this.odInfo.id 							= this.odInfoTemp.id;

					setTimeout(() => {
						this.isSetDivison = true;
						setTimeout(() => {
							this.progressDialog(false, null);
						}, 1000)
					}, 1000)
				}, (response) => {});
			}
		},
		closeHeadOffice () {
			var me = this;
			swal({
				title: 'ยืนยันการยกเลิกกิจการ ' + me.odInfo.th_org_nme,
				text: "",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'ตกลง',
				cancelButtonText: 'ยกเลิก',
			}).then((result) => {
				if (result.value) {
					this.$http.post('<?=base_url() ?>index.php/headOfficeController/closeHeadOffice', { headOfficeID: this.headOfficeID }).then((response) => {
						var result = response.body;
						if (result == 'Successfully') {
							swal({
								title: 'ยกเลิกกิจการเรียบร้อยแล้ว',
								text: '',
								type: 'success',
								showCancelButton: false,
								confirmButtonColor: '#3085d6',
								confirmButtonText: 'ตกลง',
								cancelButtonText: 'ยกเลิก',
								allowOutsideClick: false
							}).then((result) => {
								window.location.reload();
							})
						}
						else {
							swal({
								title: 'ไม่สามารถยกเลิกกิจการได้',
								text: 'กรุณาลองใหม่อีกครั้ง',
								type: 'error',
								showCancelButton: false,
								confirmButtonColor: '#3085d6',
								confirmButtonText: 'ตกลง',
								cancelButtonText: 'ยกเลิก',
								allowOutsideClick: false
							}).then((result) => {
								window.location.href = "#";
							})
						}
					}, (response) => {
					});
				}
			})

		}
  // end methods
  }
})
</script>
</html>
