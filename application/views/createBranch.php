<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicons -->
	<link href="<?php echo $this->config->item('icon_project'); ?>" rel="icon">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/vuetify/vuetify.css" />
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/util.css">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/custom.css">
	<title><?php echo $this->config->item('project_name'); ?></title>

	<style type="text/css">
		.v-stepper--alt-labels .v-stepper__step {
			flex-basis: 31% !important;
		}
		.box-area {
			background: #eee;
			padding: 30px 15px 0px 15px;
			border-radius: 5px;
		}
		.v-input__slot {
			margin: 10px 2px 2px 10px !important;
		}


		</style>
</head>
<body>
	<div data-app="true" class="application--light">
		<div class="none-show" ><?php $this->load->view('header.php'); ?></div>
		<div id="vuejs">
			<v-app  class="white-bg" >
				<!-- loading -->
				<div v-show="pageLoading" class="loading-page">
					<v-content>
						<v-container fluid fill-height>
							<v-layout justify-center align-center>
								<v-progress-circular :size="200" :width="20" color="primary" indeterminate></v-progress-circular>
							</v-layout>
						</v-container>
					</v-content>
				</div>
				<!-- content -->
				<div class="none-show">

					<!-- content -->
					<v-content style="background-color: #fff;">
						<v-container fluid  class="p-t-0 p-l-0 p-r-0">
							<v-layout  row wrap>
								<v-flex xs10 offset-xs1 class="pt-2 text-xs-center">
									<span><i class="material-icons icon-title-big">create_new_folder</i></span> <h2>สร้างสาขาใหม่</h2>
								</v-flex>
								<!-- หัวข้อ-->
								<v-flex xs10 offset-xs1 class="m-t-5 m-b-5 text-xs-center">
									<div class="p-l-50  p-r-50 p-b-50">
										<v-stepper v-model="currentStep" alt-labels>
											<v-stepper-header>
												<template v-for="n in steps">
													<v-stepper-step :edit-icon="'check'" :complete="currentStep > n" :key="`${n}-step`" :step="n" >{{headerStep[n-1]}}</v-stepper-step>
													<!-- <v-divider v-if="n !== steps" :key="n"></v-divider> -->
												</template>
											</v-stepper-header>
										<v-stepper-items>
											<!-- step 1 -->
											<v-stepper-content step="1">
												<v-card  class="mb-5 " >
													<!-- information -->
													<v-layout row wrap class="mb-2">
														<v-flex xs12 class="text-xs-left">
															<h3><v-icon large color="blue-grey darken-2" style="font-size:25px;color: #C62828 !important;">chrome_reader_mode</v-icon> <span>ข้อมูลสังกัด</span></h3>
														</v-flex>
													</v-layout>
													<!-- ฝ่าย / สนจ -->
													<v-layout row wrap class="m-b-45">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ฝ่ายกิจการสาขา</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-autocomplete  box single-line  :items="listDiv" item-text="div_nme" item-value="id" v-model="odInfo.is_div" menu-props="auto"  hide-details  single-line v-on:change="loadProvinceInfo()"
															v-validate="'required'"  :error-messages="errors.collect('step1.is_div')" data-vv-name="is_div" data-vv-scope="step1">
															</v-autocomplete>
														</v-flex>
														<v-flex xs2 class="text-xs-left">
															<h4 class="pt-3">สำนักงานจังหวัด</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-autocomplete  box single-line  :items="listProvince" menu-props="auto" label=" -- สนจ --" hide-details 	single-line
															item-text="brname" item-value="brcode" v-model="odInfo.province" v-on:change="setCodeBranch()" v-validate="'required'" :error-messages="errors.collect('step1.is_province')" data-vv-name="is_province" data-vv-scope="step1"></v-autocomplete>
														</v-flex>
													</v-layout>
													<!-- รหัสสังกัด -->
													<!-- <v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-2">รหัสสังกัด</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
																<v-text-field disabled box single-line disabled v-model="odInfo.code" ></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left pl-4">

														</v-flex>
														<v-flex xs4 class="text-xs-left">
																<v-text-field disabled box single-line disabled v-model="odInfo.division_code" ></v-text-field>
														</v-flex>
													</v-layout> -->
													<!-- ชื่อไทย / ชื่ออังกฤษ -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ชื่อสาขา (ภาษาไทย)</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field  box single-line label="ภาษาไทย"  clear-icon="cancel" clearable v-model="odInfo.is_thai_n" v-validate="'required'" :error-messages="errors.collect('step1.thainame')" data-vv-name="thainame" data-vv-scope="step1"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">ชื่อสาขา (ภาษาอังกฤษ)</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field  box single-line label="ภาษาอังกฤษ"  clear-icon="cancel" clearable v-model="odInfo.is_eng_n" v-validate="'required'" :error-messages="errors.collect('step1.engname')" data-vv-name="engname" data-vv-scope="step1"></v-text-field>
														</v-flex>
													</v-layout>
													<!-- ชื่อย่อ / วันที่เปิดดำเนินการ  -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ชื่อย่อสาขา</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field  box single-line label="ชื่อย่อ" disabled v-model="odInfo.is_amp_n"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">วันที่เปิดดำเนินการ</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-menu :close-on-content-click="true" v-model="menu2" :nudge-right="40" transition="scale-transition" offset-y full-width min-width="290px">
												        <v-text-field  box single-line readonly slot="activator" v-model="odInfo.is_open_dte" append-icon="event" readonly></v-text-field>
												        <v-date-picker v-model="odInfo.is_open_dte" @input="menu2 = false" locale="th"></v-date-picker>
												      </v-menu>
														</v-flex>
													</v-layout>
													<!-- location -->
													<v-layout row wrap class="mb-2 mt-5">
														<v-flex xs12 class="text-xs-left">
															<h3><v-icon large color="blue-grey darken-2" style="font-size:25px;color: #C62828 !important;">account_balance</v-icon> <span>ที่ตั้ง</span></h3>
														</v-flex>
													</v-layout>
													<!-- จังหวัด และ อำเภอ-->
													<v-layout row wrap class="m-b-45">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">จังหวัด</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-autocomplete  box single-line  :items="listProvinceAddr" id="listProvinceAddr" item-text="name" item-value="id" v-model="odAddrBR.province" menu-props="auto" label=" -- จังหวัด --" hide-details  single-line v-on:change="getListDistrict()" v-validate="'required'" :error-messages="errors.collect('step1.addrProvince')" data-vv-name="addrProvince" data-vv-scope="step1"></v-autocomplete>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">อำเภอ</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-autocomplete   box single-line  :items="listDistrictAddr" id="listDistrictAddr" menu-props="auto" label=" -- อำเภอ --" hide-details 	single-line
															item-text="name" item-value="id" v-model="odAddrBR.amphur"  v-on:change="getListSubDistrict()" v-validate="'required'" :error-messages="errors.collect('step1.addrDist')" data-vv-name="addrDist" data-vv-scope="step1"></v-autocomplete>
														</v-flex>
													</v-layout>
													<!-- ตำบล และ รหัสไปรษณีย์ -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ตำบล</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-autocomplete  box single-line  :items="listSubDistrictAddr" id="listSubDistrictAddr" item-text="name" item-value="id" v-model="odAddrBR.tumbon"  v-on:change="getZipcode()" menu-props="auto" label=" -- ตำบล --" hide-details  single-line v-validate="'required'" :error-messages="errors.collect('step1.addrSubDist')" data-vv-name="addrSubDist" data-vv-scope="step1"></v-autocomplete>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">รหัสไปรษณีย์</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field  box single-line  v-model="odAddrBR.zipcode" disabled label="รหัสไปรษณีย์"></v-text-field>
														</v-flex>
													</v-layout>
													<!-- เลขที่ หมู่ -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">เลขที่</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field  box single-line  clear-icon="cancel" clearable label="เลขที่" v-model="odAddrBR.addr" v-validate="'required'" :error-messages="errors.collect('step1.addr')" data-vv-name="addr" data-vv-scope="step1"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">หมู่</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field  box single-line  clear-icon="cancel" clearable label="หมู่" v-model="odAddrBR.moo"></v-text-field>
														</v-flex>
													</v-layout>
													<!-- ซอย ถนน -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ซอย</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field  box single-line  clear-icon="cancel" clearable label="ซอย" v-model="odAddrBR.soi"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">ถนน</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field  box single-line  clear-icon="cancel" clearable label="ถนน" v-model="odAddrBR.street"></v-text-field>
														</v-flex>
													</v-layout>
													<!-- contract -->
													<v-layout row wrap class="mb-2 mt-5">
														<v-flex xs12 class="text-xs-left">
															<h3><v-icon large color="blue-grey darken-2" style="font-size:25px;color: #C62828 !important;">contact_phone</v-icon> <span>ติดต่อ</span></h3>
														</v-flex>
													</v-layout>
													<!-- เบอร์โทร -->
													<v-layout row wrap class="mb-3" v-for=" (item, index) in telBRInfo " :key="index">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">โทรศัพท์</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line label="โทรศัพท์"  clear-icon="cancel" clearable return-masked-value mask="#-####-####" v-model="item.number"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left " >
															<v-btn color="red" fab small dark v-on:click="removeTelephoneBR(index)" v-show="index != 0">
																<v-icon>remove</v-icon>
															</v-btn>
															<v-btn color="primary" fab small dark v-on:click="addTelephoneBR()" v-show="index == (telBRInfo.length-1)">
																<v-icon>add</v-icon>
															</v-btn>
														</v-flex>
													</v-layout>
													<!-- wan โทรสาร -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">WAN</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line label="wan"  clear-icon="cancel" clearable return-masked-value mask="####" v-model="ctrBRInfo.wan"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left">
															<h4 class="pt-3">โทรสาร</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line label="โทรสาร"  clear-icon="cancel" clearable return-masked-value mask="#-####-####" v-model="ctrBRInfo.fax" ></v-text-field>
														</v-flex>
													</v-layout>
												</v-card>
												<v-card-actions>
													<v-spacer></v-spacer>
													<v-btn round color="primary" @click.native="validateBeforeNextPage('step1')">ถัดไป</v-btn>
												</v-card-actions>
											</v-stepper-content>
											<!-- step 2 -->
											<v-stepper-content step="2">
												<v-card  class="mb-5" >
													<!-- information -->
													<v-layout row wrap class="mb-2">
														<v-flex xs2 class="text-xs-left">
															<h3><v-icon large color="blue-grey darken-2" style="font-size:25px;color: #C62828 !important;">chrome_reader_mode</v-icon> <span>ข้อมูลหน่วยอำเภอ</span></h3>
														</v-flex>
														<v-flex xs10 class="text-xs-left">
															<v-checkbox label="ใช้ชื่อเดียวกับสาขา" v-model="copyName"></v-checkbox>
														</v-flex>
													</v-layout>

													<!-- ชื่อหน่วยอำเภอ ภาษาไทย ภาษาอังกฤษ  -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ชื่อหน่วยอำเภอ (ภาษาไทย)</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line clear-icon="cancel" clearable label="ภาษาไทย" v-model="odAmphur.is_thai_n" v-validate="'required'" :error-messages="errors.collect('step2.thainame')" data-vv-name="thainame" data-vv-scope="step2" :disabled="copyName"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">ชื่อหน่วยอำเภอ (ภาษาอังกฤษ)</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line clear-icon="cancel" clearable label="ภาษาอังกฤษ" v-model="odAmphur.is_eng_n" v-validate="'required'" :error-messages="errors.collect('step2.distEngname')" data-vv-name="distEngname" data-vv-scope="step2" :disabled="copyName"></v-text-field>
														</v-flex>
													</v-layout>
													<!--  รหัสหน่วย วันที่เปิดดำเนินการ-->
													<v-layout row wrap class="mb-3">
														<!-- <v-flex xs2 class="text-xs-left  pl-4">
															<h4 class="pt-3">รหัสหน่วย</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field disabled box	 v-model="odAmphur.code" ></v-text-field>
														</v-flex> -->
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">วันที่เปิดดำเนินการ</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-menu :close-on-content-click="true" v-model="menu3" :nudge-right="40" transition="scale-transition" offset-y full-width min-width="290px">
																<v-text-field box slot="activator" v-model="odAmphur.is_open_dte" append-icon="event" readonly></v-text-field>
																<v-date-picker v-model="odAmphur.is_open_dte" @input="menu3 = false" locale="th"></v-date-picker>
															</v-menu>
														</v-flex>
													</v-layout>
													<!-- location -->
													<v-layout row wrap class="mb-2 mt-5">
														<v-flex xs1 class="text-xs-left">
															<h3><v-icon large color="blue-grey darken-2" style="font-size:25px;color: #C62828 !important;">account_balance</v-icon> <span>ที่ตั้ง</span></h3>
														</v-flex>
														<v-flex xs11 class="text-xs-left">
															<v-checkbox label="ใช้ที่อยู่เดียวกับสาขา" v-model="copyAddr"></v-checkbox>
														</v-flex>
													</v-layout>
													<!-- จังหวัด  อำเภอ-->
													<v-layout row wrap class="m-b-45">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">จังหวัด</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-autocomplete box :items="listProvinceUnitAddr" id="listProvinceUnitAddr" item-text="name" item-value="id" v-model="odAddrAM.province" menu-props="auto" label="-- จังหวัด --" hide-details  single-line v-on:change="getListDistrictUnit()" v-validate="'required'" :error-messages="errors.collect('step2.addrProvince')" data-vv-name="addrProvince" data-vv-scope="step2" :disabled="copyAddr"></v-autocomplete>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">อำเภอ</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-autocomplete box  :items="listDistrictUnitAddr" id="listDistrictUnitAddr" menu-props="auto" label=" -- อำเภอ --" hide-details 	single-line
															item-text="name" item-value="id" v-model="odAddrAM.amphur" v-on:change="getListSubDistrictUnit()" v-validate="'required'" :error-messages="errors.collect('step2.addrDist')" data-vv-name="addrDist" data-vv-scope="step2" :disabled="copyAddr"></v-autocomplete>
														</v-flex>
													</v-layout>
													<!-- ตำบล รหัรหัสไปรษณีย์-->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ตำบล</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-autocomplete box :items="listSubDistrictUnitAddr" id="listSubDistrictUnitAddr" item-text="name" item-value="id" v-model="odAddrAM.tumbon" v-on:change="getZipcodeUnit()" menu-props="auto" label="-- ตำบล --" hide-details  single-line v-validate="'required'" :error-messages="errors.collect('step2.addrSubDist')" data-vv-name="addrSubDist" data-vv-scope="step2" :disabled="copyAddr"></v-autocomplete>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">รหัสไปรษณีย์</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line label="รหัสไปรษณีย์" disabled v-model="odAddrAM.zipcode"></v-text-field>
														</v-flex>
													</v-layout>
													<!-- เลขที่ หมู่ -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">เลขที่</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line clear-icon="cancel" clearable label="เลขที่" v-model="odAddrAM.addr" v-validate="'required'" :error-messages="errors.collect('step2.addr')" data-vv-name="addr" data-vv-scope="step2" :disabled="copyAddr"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">หมู่</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line clear-icon="cancel" clearable label="หมู่" v-model="odAddrAM.moo" :disabled="copyAddr"></v-text-field>
														</v-flex>
													</v-layout>
													<!-- ซอย ถนน -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ซอย</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line clear-icon="cancel" clearable label="ซอย" v-model="odAddrAM.soi" :disabled="copyAddr"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left ">
															<h4 class="pt-3">ถนน</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line clear-icon="cancel" clearable label="ถนน" v-model="odAddrAM.street" :disabled="copyAddr"></v-text-field>
														</v-flex>
													</v-layout>
													<!-- contract -->
													<v-layout row wrap class="mb-2 mt-5">
														<v-flex xs1 class="text-xs-left">
															<h3><v-icon large color="blue-grey darken-2" style="font-size:25px;color: #C62828 !important;">contact_phone</v-icon> <span>ติดต่อ</span></h3>
														</v-flex>
														<v-flex xs11 class="text-xs-left">
															<v-checkbox label="ใช้เบอร์โทรศัพท์เดียวกับสาขา" v-model="copyPhone"></v-checkbox>
														</v-flex>
													</v-layout>
													<!-- เบอร์โทร -->
													<v-layout row wrap class="mb-3" v-for=" (item, index) in telAMInfo " :key="index">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">โทรศัพท์</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line label="โทรศัพท์"  clear-icon="cancel" clearable return-masked-value mask="#-####-####" v-model="item.number" :disabled="copyPhone"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left " >
															<v-btn color="red" fab small dark v-on:click="removeTelephoneAM(index)" v-show="index != 0">
																<v-icon>remove</v-icon>
															</v-btn>
															<v-btn color="primary" fab small dark v-on:click="addTelephoneAM()" v-show="index == (telAMInfo.length-1)">
																<v-icon>add</v-icon>
															</v-btn>
														</v-flex>
													</v-layout>
													<!-- wan โทรสาร -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">WAN</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line label="wan"  clear-icon="cancel" clearable return-masked-value mask="####" v-model="ctrAMInfo.wan" :disabled="copyPhone"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left">
															<h4 class="pt-3">โทรสาร</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field box single-line label="โทรสาร"  clear-icon="cancel" clearable return-masked-value mask="#-####-####" v-model="ctrAMInfo.fax" :disabled="copyPhone"></v-text-field>
														</v-flex>
													</v-layout>
												</v-card>
												<v-card-actions>
													<v-btn  round outline color="default" @click.native="previousPage()">ย้อนกลับ</v-btn>
													<v-spacer></v-spacer>
													<v-btn round color="primary" @click.native="validateBeforeNextPage('step2')">ถัดไป</v-btn>
												</v-card-actions>
											</v-stepper-content>
											<!-- step 3 -->
											<v-stepper-content step="3">
												<v-card  class="mb-5 " >
													<!-- ข้อมูลอำเภอ -->
													<v-layout row wrap class="mb-2">
														<v-flex xs12 class="text-xs-left">
															<h3><v-icon large color="blue-grey darken-2" style="font-size:25px;color: #C62828 !important;">home</v-icon> <span>ชื่ออำเภอ</span></h3>
														</v-flex>
													</v-layout>
													<!-- รหัสอำเภอ ชื่ออำเภอ -->
													<!-- <v-layout row wrap class="mb-3">
													  <v-flex xs2 class="text-xs-left pl-4">
													    <h4 class="pt-3">รหัสอำเภอ</h4>
													  </v-flex>
													  <v-flex xs2 class="text-xs-left">
													      <v-text-field disabled box single-line v-model="odUnitAmphur.code" ></v-text-field>
													  </v-flex>
													</v-layout> -->
													<!-- ชื่อไทย / ชื่ออังกฤษ -->
													<v-layout row wrap class="mb-3">
													  <v-flex xs2 class="text-xs-left pl-4">
													    <h4 class="pt-3">ชื่ออำเภอ (ภาษาไทย)</h4>
													  </v-flex>
													  <v-flex xs4 class="text-xs-left">
													    <v-text-field box single-line clear-icon="cancel" clearable label="ภาษาไทย" v-model="odUnitAmphur.is_thai_n" v-validate="'required'" :error-messages="errors.collect('step3.thainame')" data-vv-name="thainame" data-vv-scope="step3"></v-text-field>
													  </v-flex>
													  <v-flex xs2 class="text-xs-left ">
													    <h4 class="pt-3">ชื่ออำเภอ (ภาษาอังกฤษ)</h4>
													  </v-flex>
													  <v-flex xs4 class="text-xs-left">
													    <v-text-field box single-line clear-icon="cancel" clearable label="ภาษาอังกฤษ" v-model="odUnitAmphur.is_eng_n" v-validate="'required'" :error-messages="errors.collect('step3.engname')" data-vv-name="engname" data-vv-scope="step3"></v-text-field>
													  </v-flex>
													</v-layout>
													<!-- area -->
													<v-layout row wrap class="mb-2 mt-5">
														<v-flex xs12 class="text-xs-left">
															<h3><v-icon large color="blue-grey darken-2" style="font-size:25px;color: #C62828 !important;">add_location</v-icon> <span>เลือกเขตพื้นที่ในความรับผิดชอบ</span></h3>
														</v-flex>
													</v-layout>
													<v-layout row wrap>
														<v-flex xs12 class="box-area m-b-10 m-t-10" v-for="(item, index) in odArea" :key="index">
															<!-- จังหวัด  อำเภอ-->
															<v-layout row wrap class="m-b-30">
																<v-flex xs1 offset-xs1 class="text-xs-left pl-4">
																	<h4 class="pt-3">จังหวัด</h4>
																</v-flex>
																<v-flex xs4 class="text-xs-left">
																	<v-autocomplete box :items="listAddrArea[index].listProvince" item-text="name" item-value="id" v-model="item.cat_cc" menu-props="auto" label="-- จังหวัด --" hide-details  single-line v-on:change="getListDistrictArea(index)"></v-autocomplete>
																</v-flex>
																<v-flex xs1 class="text-xs-left ">
																	<h4 class="pt-3">อำเภอ</h4>
																</v-flex>
																<v-flex xs4 class="text-xs-left">
																	<v-autocomplete box :items="listAddrArea[index].listAmphur" menu-props="auto" label=" -- อำเภอ --" hide-details 	single-line
																	item-text="name" item-value="id" v-model="item.cat_aa" v-on:change="getListSubDistrictArea(index)" ></v-autocomplete>
																</v-flex>
																<v-flex xs1 class="text-xs-right">
																	<v-btn flat icon color="pink" style="margin-top:-15px;" v-on:click="removeArea(index)" v-show="index > 0">
																		<v-icon>delete_forever</v-icon>
																	</v-btn>
																</v-flex>
															</v-layout>
															<!-- ตำบล รหัรหัสไปรษณีย์-->
															<v-layout row wrap class="mb-3">
																<v-flex xs10 offset-xs1 class="text-xs-left p-l-90 p-r-40">
																	<v-layout row wrap>
																		<v-flex xs12>

																			<table id="tumbonlist">
																				<tr>
																					<th class="wd-250" style="border-top-left-radius: 8px;">ชื่อตำบล</th>
																					<th class="wd-400" style="border-top-right-radius: 8px;">ยกเลิกพื้นที่ในความรับผิดชอบจากหน่วยเดิม (ถ้ามี)</th>
																				</tr>
																				<tr v-for="list, indexlist in odArea[index].cat_tt" :key="indexlist">
																					<td class="wd-250" >
																						<v-checkbox v-model="list.tumbon" :label="list.name"  color="indigo" ></v-checkbox>
																					</td>
																					<td class="wd-400">
																						<v-checkbox v-model="list.cancel" label="ยกเลิก" color="red darken-3" v-show="list.tumbon === true" ></v-checkbox>
																					</td>
																				</tr>
																			</table>


																		</v-flex>
																	</v-layout>
																</v-flex>

															</v-layout>

														</v-flex>
														<v-flex xs12>
															<!-- เพิ่มพื้นที่ -->
															<v-layout row wrap class="mb-3">
																<v-flex xs2 offset-xs10 class="text-xs-right">
																	<v-btn outline color="primary" v-on:click="addArea()"> เพิ่มพื้นที่</v-btn>
																</v-flex>
															</v-layout>
														</v-flex>
													</v-layout>
												</v-card>
												<v-card-actions>
													<v-btn  round outline color="default" @click.native="previousPage()">ย้อนกลับ</v-btn>
													<v-spacer></v-spacer>
													<v-btn round color="primary" @click.native="validateBeforeNextPage('step3')">สร้างสาขา</v-btn>
												</v-card-actions>
											</v-stepper-content>
										</v-stepper-items>
									</v-stepper>
								</div>
								</v-flex>

							</v-layout>
						</v-container>
					</v-content>

					<v-dialog v-model="dialogProgress.status" persistent max-width="500px">
						<v-card class="text-xs-center">
							<v-btn class="m-t-30" fab dark large color="white" depressed >
								<v-icon dark color="red" style="font-size:45px!important;">notification_important</v-icon>
							</v-btn>
							<v-card-title class="justify-center pt-0">
								<span class="headline"><h4>{{dialogProgress.title}}</h4></span>
							</v-card-title>
							<v-card-text class="p-t-0 p-b-0">
								<v-container grid-list-md class="p-t-0 p-b-0">
									<v-layout wrap>
										<v-flex xs12>
											<v-progress-linear :indeterminate="true"></v-progress-linear>
										</v-flex>
									</v-layout>
								</v-container>
							</v-card-text>
							<v-card-actions class="p-b-20 justify-center">
							</v-card-actions>
						</v-card>
					</v-dialog>

				</div>


			</v-app>
		</div> <!-- end vuejs -->
	</div> <!-- end data-app -->
</body>
<script type="text/javascript">
Vue.http.options.emulateJSON = true;
Vue.http.options.emulateHTTP = true;
Vue.use(VeeValidate)
var vuejs = new Vue({
  el:"#vuejs",
	$_veeValidate: {
		validator: 'new'
	},
	data:{
		pageLoading: true,
		projectName: 'ระบบงานที่ตั้งสำนักงานของธนาคาร',
		sessioncode: '',
		sessionname: '',
		dialogProgress: {status: false, title: ''},
		dictionary: {
			attributes: {
				// email: 'E-mail Address'
			},
			custom: {
				is_div: { required: () => 'กรุณาเลือกฝ่ายกิจการสาขา' },
				is_province: { required: () => 'กรุณาเลือกสำนักงานจังหวัด' },
				thainame: { required: () => 'กรุณาระบุชื่อภาษาไทย' },
				engname: { required: () => 'กรุณาระบุชื่อภาษาอังกฤษ' },
				aligname: { required: () => 'กรุณาระบุชื่อย่อสาขา' },
				addrProvince: { required: () => 'กรุณาเลือกจังหวัด' },
				addrDist: { required: () => 'กรุณาเลือกอำเภอ' },
				addrSubDist: { required: () => 'กรุณาเลือกตำบล' },
				addrZipcode: { required: () => 'กรุณาระบุรหัสไปรษณีย์' },
				addr: { required: () => 'กรุณาระบุเลขที่' }
			}
		},
		currentStep: 1,
		steps: 3,
		headerStep: ['ข้อมูลสังกัด', 'ข้อมูลหน่วยอำเภอ', 'พื้นที่ในความรับผิดชอบ'],
		listDiv: [],
		listProvince: [],
		listBranch: [],
		listType: [],
		branchDetail: [],
		odInfo: {is_div: '', province: '', is_br: '', is_sbr: '', is_ch: '00', is_am: '00', is_thai_n: '', is_eng_n: '', is_open_dte: new Date().toISOString().substr(0, 10), is_amp_n: '', code: '00 - 0 - 00 - 00', division_code: '000000000', prov_code: '', prov_nme: '', prov_as: ''},
		odAddrBR: {province: '', amphur: '', tumbon: '', zipcode: '', addr: '', moo: '', soi: '', street: '', prov: '', dist: '', subdist: ''},
		odAmphur: {is_br: '', is_sbr: '', is_ch: '01', is_am: '00', is_open_dte: new Date().toISOString().substr(0, 10), is_thai_n: '', is_eng_n: '', code: '00 - 0 - 00 - 00'},
		odAddrAM: {province: '', amphur: '', tumbon: '', zipcode: '', addr: '', moo: '', soi: '', street: '', prov: '', dist: '', subdist: ''},
		odUnitAmphur: {is_ch: '', is_am: '', is_thai_n: '', is_eng_n: '', code: '00 - 00', distName: '', cat_code: ''},
		odArea: [{cat_cc: '', cat_aa: '', cat_tt: [], cat_name: ''}],
		ctrBRInfo: {fax: '', wan: '' },
		telBRInfo: [{number: ''}],
		ctrAMInfo: {fax: '', wan: '' },
		telAMInfo: [{number: ''}],
		modal: false,
		menu2: false,
		menu3: false,
		listProvinceAddr: [],
		listDistrictAddr: [],
		listSubDistrictAddr: [],
		listProvinceUnitAddr: [],
		listDistrictUnitAddr: [],
		listSubDistrictUnitAddr: [],
		listAddrArea: [{listProvince: [], listAmphur: [], listDistrict: []}],
		copyAddr: false,
		copyPhone: false,
		copyName: false

	},
  created (){
		this.checkLogin();
  },
  computed:
  {
  },
	mounted () {
		this.$validator.localize('en', this.dictionary);
	},
  watch: {
		steps (val) {
			if (this.currentStep > val) {
				this.currentStep = val;
			}
		},
		currentStep (check) {
			if (check == 2) {
				if(this.listProvinceUnitAddr.length == 0) {
					this.getListProvinceUnit();
				}
			}
			else if (check == 3) {
				if(this.odArea.length == 1 && this.listAddrArea[0].listProvince.length == 0) {
					this.getListProvinceArea(0);
				}
			}
		},
		copyName (check) {
			if (check == true) {
				this.odAmphur.is_thai_n = this.odInfo.is_thai_n;
				this.odAmphur.is_eng_n  = this.odInfo.is_eng_n;
			}
			else {
				this.odAmphur.is_thai_n = '';
				this.odAmphur.is_eng_n  = '';
			}
		},
		copyAddr (check) {
			if (check === true) {
				this.odAddrAM.province = this.odAddrBR.province;
				this.getListDistrictUnit();
			}
			else {
				this.odAddrAM.province = '';
				this.odAddrAM.amphur = '';
				this.odAddrAM.tumbon = '';
				this.odAddrAM.zipcode = '';
				this.odAddrAM.addr = '';
				this.odAddrAM.moo = '';
				this.odAddrAM.soi = '';
				this.odAddrAM.street = '';
			}
		},
		copyPhone (check) {
			if (check === true) {
				this.telAMInfo = this.telBRInfo;
				this.ctrAMInfo = this.ctrBRInfo;
			}
			else {
				this.ctrAMInfo = {fax: '', wan: '' };
				this.telAMInfo = [{number: ''}];
			}
		}
  },
	methods:{
		// start method
		progressDialog (status, title) {
			this.dialogProgress.status = status;
			this.dialogProgress.title = title;
		},
		checkLogin () {
			this.$http.post('<?=base_url() ?>index.php/loginController/getSession').then((response) => {
				var userInfo = response.body;
				if (userInfo.emp_code != "nodata") {
					this.sessioncode = userInfo.emp_code;
					this.sessionname = userInfo.emp_name;
					this.loadDivInfo();
					setTimeout(() => { this.pageLoading = false; $(".none-show").removeClass("none-show"); }, 1000)
				}
				else {
					this.pageLoading = false;
					swal({
						title: 'กรุณาเข้าสู่ระบบก่อนเข้าใช้งาน',
						text: '',
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.href = '<?=base_url() ?>';
					})
				}
			}, (response) => {});
		},
		loadDivInfo () {
			this.listDiv = [];
			this.odInfo.is_div = '';
			this.odInfo.province = '';
			this.$http.post('<?=base_url() ?>index.php/odController/loadDivInfo').then((response) => {
				this.listDiv = response.body;
				this.listDiv.unshift({div_nme: '-- ฝ่ายกิจการสาขา --', id: ''})
			}, (response) => {
			});
		},
		loadProvinceInfo () {
			this.listProvince = [];
			this.odInfo.province = '';
			if(this.odInfo.is_div != '')
			{
				this.progressDialog(true, 'กำลังดำเนินการ กรุณารอสักครู่');
				this.$http.post('<?=base_url() ?>index.php/odController/loadProvinceInfo', {div: this.odInfo.is_div}).then((response) => {
					this.listProvince = response.body;
					this.listProvince.unshift({brname: '-- สนจ --', brcode: '', cat_cc: '', brshrt: ''})
					setTimeout(() => { this.progressDialog(false, null); }, 300)
				}, (response) => {
				});
			}
		},
		setCodeBranch() {
			var me = this;
			var objName = _.filter(this.listProvince, function(o) { return o.brcode == me.odInfo.province; });
			var provinceCode = this.odInfo.province.slice(0, -1);
			this.odInfo.is_amp_n = objName[0].brshrt;
			this.odInfo.is_br = provinceCode;

			this.getListProvince();
		},
		getListProvince () {
			this.listProvinceAddr = [];
			this.amphurAddr = [];
			this.odAddrBR.amphur = '';
			this.odAddrBR.tumbon = '';
			this.odAddrBR.zipcode = '';
			var me = this;
			this.$http.post('<?=base_url() ?>index.php/createBranchController/getListProvince').then((response) => {
				this.listProvinceAddr = response.body;
				this.listProvinceAddr.unshift({id: '', name: '-- จังหวัด --', code: ''})
				var province = _.filter(this.listProvince, function(o) { return o.brcode == me.odInfo.province; });
				if (province.length > 0 ) {
					this.odInfo.prov_nme = province[0].brname;
					this.odInfo.prov_as = province[0].brname.replace('สำนักงาน ธ.ก.ส.จังหวัด', 'สนจ.');
					var  cat_cc = province[0].cat_cc;
					var idProvince = _.filter(this.listProvinceAddr, function(o) { return o.code == cat_cc; });
					if (idProvince.length > 0) {
						this.odAddrBR.province = idProvince[0].id;
						this.getListDistrict();
					}
				}

			}, (response) => {
			});
		},
		getListDistrict () {
			this.odAddrBR.amphur = '';
			this.odAddrBR.tumbon = '';
			this.odAddrBR.zipcode = '';
			this.$http.post('<?=base_url() ?>index.php/createBranchController/getListDistrict', {provinceID: this.odAddrBR.province}).then((response) => {
				this.listDistrictAddr = response.body;
			}, (response) => {
			});
		},
		getListSubDistrict () {
			this.odAddrBR.tumbon = '';
			this.odAddrBR.zipcode = '';
			this.$http.post('<?=base_url() ?>index.php/createBranchController/getListSubDistrict', {provinceID: this.odAddrBR.province, amphurID: this.odAddrBR.amphur}).then((response) => {
				this.listSubDistrictAddr = response.body;
			}, (response) => {});
		},
		getZipcode () {
			var me = this;
			var indexOf = _.filter(this.listSubDistrictAddr, function(o) { return o.id == me.odAddrBR.tumbon; });
			this.odAddrBR.zipcode = indexOf[0]['postcode'];
		},
		getListProvinceUnit () {
			this.listProvinceUnitAddr = [];
			this.amphurAmphurAddr = [];
			this.odAddrAM.amphur = '';
			this.odAddrAM.tumbon = '';
			this.odAddrAM.zipcode = '';
			var me = this;
			this.$http.post('<?=base_url() ?>index.php/createBranchController/getListProvince').then((response) => {
				this.listProvinceUnitAddr = response.body;
				this.listProvinceUnitAddr.unshift({id: '', name: '-- จังหวัด --', code: ''})
			}, (response) => {
			});
		},
		getListDistrictUnit () {
			this.odAddrAM.amphur = '';
			this.odAddrAM.tumbon = '';
			this.odAddrAM.zipcode = '';
			this.$http.post('<?=base_url() ?>index.php/createBranchController/getListDistrict', {provinceID: this.odAddrAM.province}).then((response) => {
				this.listDistrictUnitAddr = response.body;
				if(this.copyAddr === true) {
					this.odAddrAM.amphur = this.odAddrBR.amphur;
					this.getListSubDistrictUnit();
				}
			}, (response) => {
			});
		},
		getListSubDistrictUnit () {
			this.odAddrAM.tumbon = '';
			this.odAddrAM.zipcode = '';
			this.$http.post('<?=base_url() ?>index.php/createBranchController/getListSubDistrict', {provinceID: this.odAddrAM.province, amphurID: this.odAddrAM.amphur}).then((response) => {
				this.listSubDistrictUnitAddr = response.body;
				if(this.copyAddr === true) {
					this.odAddrAM.tumbon = this.odAddrBR.tumbon;
					this.odAddrAM.zipcode = this.odAddrBR.zipcode;
					this.odAddrAM.addr = this.odAddrBR.addr;
					this.odAddrAM.moo = this.odAddrBR.moo;
					this.odAddrAM.soi = this.odAddrBR.soi;
					this.odAddrAM.street = this.odAddrBR.street;
				}
			}, (response) => {});
		},
		getZipcodeUnit () {
			var me = this;
			var indexOf = _.filter(this.listSubDistrictUnitAddr, function(o) { return o.id == me.odAddrAM.tumbon; });
			this.odAddrAM.zipcode = indexOf[0]['postcode'];
		},
		getListProvinceArea (index) {
			this.listAddrArea[index].listProvince = [];
			this.listAddrArea[index].listAmphur = [];
		  this.listAddrArea[index].listDistrict = [];
			this.odArea[index].cat_aa = [];
			this.odArea[index].cat_tt = [];
			var me = this;
			this.$http.post('<?=base_url() ?>index.php/createBranchController/getListProvince').then((response) => {
				this.listAddrArea[index].listProvince  = response.body;
				this.listAddrArea[index].listProvince.unshift({id: '', name: '-- จังหวัด --', code: ''})
				if (index == 0) {
					setTimeout(() => {
						this.odArea[0].cat_cc = this.odAddrBR.province;
						this.getListDistrictArea(0);
					}, 500)
				}
				else if (index > 0) {
					this.odArea[index].cat_cc = this.odArea[index-1].cat_cc;
					this.getListDistrictArea(index);
				}
			}, (response) => {
			});
		},
		getListDistrictArea (index) {
			this.listAddrArea[index].listAmphur = [];
			this.listAddrArea[index].listDistrict = [];
			this.odArea[index].cat_aa = [];
			this.odArea[index].cat_tt = [];
			this.$http.post('<?=base_url() ?>index.php/createBranchController/getListDistrict', {provinceID: this.odArea[index].cat_cc}).then((response) => {
				this.listAddrArea[index].listAmphur = response.body;
			}, (response) => {
			});
		},
		getListSubDistrictArea (index) {
			this.listAddrArea[index].listDistrict = [];
			this.odArea[index].cat_tt = [];
			this.$http.post('<?=base_url() ?>index.php/createBranchController/getListSubDistrict', {provinceID: this.odArea[index].cat_cc, amphurID: this.odArea[index].cat_aa}).then((response) => {
				this.listAddrArea[index].listDistrict = response.body;
				for(var i = 0 ; i < (this.listAddrArea[index].listDistrict.length); i++) {
					this.odArea[index].cat_tt.push({name: this.listAddrArea[index].listDistrict[i].name, tumbon: false, cancel: false, code: this.listAddrArea[index].listDistrict[i].code});
				}
			}, (response) => {});
		},
		getListSubDistrictCode (index) {
			var me = this;
			var indexOf = _.findIndex(this.listAddrArea[index].listDistrict, function(o) { return o.code == me.odArea[index].cat_tt; });
			this.odArea[index].cat_name = this.listAddrArea[index].listDistrict[indexOf].name;
		},
		addArea () {
			this.odArea.push({cat_cc: '', cat_aa: '', cat_tt: [], cat_name: ''});
			this.listAddrArea.push({listProvince: [], listAmphur: [], listDistrict: []});
			this.getListProvinceArea(this.odArea.length-1);
		},
		removeArea (index) {
			swal({
				title: 'ยืนยันการลบข้อมูลพื้นที่นี้',
				text: "",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'ตกลง',
				cancelButtonText: 'ยกเลิก',
			}).then((result) => {
				if (result.value) {
					if (this.odArea.length > 1) {
						this.odArea.splice(index,1)
						this.listAddrArea.splice(index,1);
					}
				}
			})
		},
		addTelephoneBR () {
			this.telBRInfo.push({number: ''});
		},
		removeTelephoneBR (index) {
			swal({
				title: 'ยืนยันการลบข้อมูลเบอร์โทรศัพท์',
				text: "",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'ตกลง',
				cancelButtonText: 'ยกเลิก',
			}).then((result) => {
				if (result.value) {
					if (this.telBRInfo.length > 1) {
						this.telBRInfo.splice(index,1)
					}
				}
			})
		},
		addTelephoneAM () {
			this.telAMInfo.push({number: ''});
		},
		removeTelephoneAM (index) {
			swal({
				title: 'ยืนยันการลบข้อมูลเบอร์โทรศัพท์',
				text: "",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'ตกลง',
				cancelButtonText: 'ยกเลิก',
			}).then((result) => {
				if (result.value) {
					if (this.telAMInfo.length > 1) {
						this.telAMInfo.splice(index,1)
					}
				}
			})
		},
		validateBeforeNextPage (scope) {
			this.$validator.validateAll(scope).then((result) => {
				if (result) {
					if(this.currentStep < this.steps) {
						this.nextPage();
					}
					else {
						swal({
							title: 'ยืนยันการสร้างสาขา',
							text: '',
							type: 'warning',
							showCancelButton: true,
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'ตกลง',
							cancelButtonText: 'ยกเลิก',
							allowOutsideClick: false
						}).then((result) => {
							if (result.value) {
								// var me = this;
								// var objTumbon = _.filter(this.listSubDistrictAddr, function(o) { return o.id == me.odAddrBR.tumbon; });
								// this.odAddrBR.subdist = objTumbon[0].name;
								// var objAmphur = _.filter(this.listDistrictAddr, function(o) { return o.id == me.odAddrBR.amphur; });
								// this.odAddrBR.dist = objAmphur[0].name;
								// var objProvince = _.filter(this.listProvinceAddr, function(o) { return o.id == me.odAddrBR.province; });
								// this.odAddrBR.prov = objProvince[0].name;
								//
								// var objTumbonUnit = _.filter(this.listSubDistrictUnitAddr, function(o) { return o.id == me.odAddrAM.tumbon; });
								// this.odAddrAM.subdist = objTumbonUnit[0].name;
								// var objAmphurUnit = _.filter(this.listDistrictUnitAddr, function(o) { return o.id == me.odAddrAM.amphur; });
								// this.odAddrAM.dist = objAmphurUnit[0].name;
								// var objProvinceUnit = _.filter(this.listProvinceUnitAddr, function(o) { return o.id == me.odAddrAM.province; });
								// this.odAddrAM.prov = objProvinceUnit[0].name;

								this.odAddrBR.subdist = $('#listSubDistrictAddr').val();
								this.odAddrBR.dist = $('#listDistrictAddr').val();
								this.odAddrBR.prov = $('#listProvinceAddr').val();


								this.odAddrAM.subdist = $('#listSubDistrictUnitAddr').val();
								this.odAddrAM.dist = $('#listDistrictUnitAddr').val();
								this.odAddrAM.prov = $('#listProvinceUnitAddr').val();

								this.progressDialog(true, 'กำลังโหลดข้อมูล กรุณารอสักครู่...');
								setTimeout(() => { this.addNewBranch(); }, 1000)
							}

						})
					}
				}
				else {
					swal({
						title: 'กรุณาตรวจสอบข้อมูลให้ถูกต้อง',
						text: '',
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.href = "#";
					})
				}
			});

		},
		nextPage () {
			this.currentStep = this.currentStep + 1;
			window.location.href = "#";
		},
		previousPage () {
			this.currentStep = this.currentStep - 1;
			window.location.href = "#";
		},
		addNewBranch () {
			this.$http.post('<?=base_url() ?>index.php/createBranchController/addNewBranch', {odInfo: this.odInfo, odAddrBR: this.odAddrBR, telBRInfo: this.telBRInfo, ctrBRInfo: this.ctrBRInfo, odAmphur: this.odAmphur, odAddrAM: this.odAddrAM, telAMInfo: this.telAMInfo, ctrAMInfo: this.ctrAMInfo, odUnitAmphur: this.odUnitAmphur, odArea: this.odArea}).then((response) => {
				console.log(response.body);
				var result = response.body;
				this.progressDialog(false, null);
				if (result == 'Successfully') {
					swal({
						title: 'สร้างสาขาเรียบร้อยแล้ว',
						text: '',
						type: 'success',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.reload();
					})
				}
				else {
					this.progressDialog(false, null);
					swal({
						title: 'ไม่สามารถสร้างสาขาได้',
						text: 'กรุกรุณาลองใหม่อีกครั้ง',
						type: 'error',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.href = "#";
					})
				}
			}, (response) => {
			});
		}
  // end methods
  }
})
</script>
</html>
