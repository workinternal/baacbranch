<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicons -->
	<link href="<?php echo $this->config->item('icon_project'); ?>" rel="icon">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/vuetify/vuetify.css" />
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/util.css">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/custom.css">
	<title><?php echo $this->config->item('project_name'); ?></title>

	<style type="text/css">
	 .v-card {
	 	border-radius: 10px !important;
		box-shadow: 0px 2px 1px -1px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 1px 3px 0px rgba(0,0,0,0.12) !important;
	 }
	 .card-info {
		 padding: 15px;
     background: #f2f2f2;
     border-radius: 5px;
	 }
	 .icon-head-box {
		 font-size: 30px  !important;
 	 		color: #455A64;
	 }
	 .v-list__tile__avatar {
		 min-width: 75px !important;
	 }
	 .v-avatar {
		 background-color: teal !important;
     color: white !important;
     margin-top: 15px !important;
		 font-size: 20px !important;
	 }
	 thead tr:first-child {
		 border: 1px solid #009688 !important;
		 background-color: #009688 !important;
		 /* font-family: 'Kanit', sans-serif !important; */
	 }
	 .column {
		 color: #fff !important;
     font-size: 14px !important;
	 }
	 .table-text {
		 font-family: 'Trebuchet MS' !important;
		 font-size: 15px !important;
	 }
	 .v-stepper__step .v-stepper__step--active .v-stepper__step--editable .v-stepper__step__step .primary {
		 background-color: #66BB6A !important;
     border: none !important;
	 }
	 .v-input__control {
	 		padding-right: 0 !important;
	 }
	 .v-input {
	 		padding-right: 0 !important;
	 }
	</style>
</head>
<body class="white-bg">
	<div data-app="true" class="application--light">
		<div class="none-show" ><?php $this->load->view('header.php'); ?></div>
		<div id="vuejs" class="white-bg">

			<v-app  >
				<!-- loading -->
				<div v-if="pageLoading" class="loading-page">
					<v-content>
						<v-container fluid fill-height>
							<v-layout justify-center align-center>
								<v-progress-circular :size="200" :width="20" color="primary" indeterminate></v-progress-circular>
							</v-layout>
						</v-container>
					</v-content>
				</div>
				<!-- content -->
				<div v-if="!pageLoading" class="none-show">
					<!-- content -->
					<v-content style="background-color: #fff;">
						<v-container fluid  class="p-t-30 p-l-0 p-r-0 p-b-0">
							<v-layout  row wrap >
								<v-flex xs6 offset-xs3 >
									<v-layout row wrap>
										<v-flex xs8 offset-xs2 >
											<v-card class="justify-center" color="#ffffff" max-width="500" min-height="450" >
												<div class="text-center p-t-25"><img src="<?=base_url() ?>assets/images/key.png"  height="100" ></div>
												<div class="text-center p-t-5"><h2><?php echo $this->config->item('project_name'); ?></h2></div>
												<v-card-title class="p-t-5 p-b-10 justify-center">
													<h3>เปลี่ยนรหัสผ่าน</h3>
												</v-card-title>
												<v-card-text class="headline font-weight-bold">
													<v-form  class="p-r-10">
														<v-text-field style="display:none;"  name="oldPassforCheck" v-model="oldPassforCheck" ref="oldPassforCheck" ></v-text-field>
														<v-text-field box label="รหัสผ่านเก่า *" prepend-icon="lock" v-model="oldPass"
														v-validate="'required|confirmed:oldPassforCheck'" name="oldPass" data-vv-as="oldPassforCheck" :error-messages="errors.first('oldPass')"
														:type="showOldPass ? 'text' : 'password'" v-on:click:append="showOldPass = !showOldPass"
														:append-icon="showOldPass ? 'visibility_off' : 'visibility'"></v-text-field>

														<v-text-field box label="รหัสผ่านใหม่ *" prepend-icon="lock_open" v-model="newPass"  name="newPass"
														v-validate="'required'" :error-messages="errors.first('newPass')" ref="newPass"
														:type="showNewPass ? 'text' : 'password'" v-on:click:append="showNewPass = !showNewPass"
														:append-icon="showNewPass ? 'visibility_off' : 'visibility'">
														</v-text-field>

														<v-text-field box label="ยืนยันรหัสผ่านใหม *" prepend-icon="lock_open" v-model="confirmPass" name="confirmPass"
															v-validate="'required|confirmed:newPass'" :error-messages="errors.first('confirmPass')" data-vv-as="newPass" :type="showConfirmPass ? 'text' : 'password'" v-on:click:append="showConfirmPass = !showConfirmPass"
															:append-icon="showConfirmPass ? 'visibility_off' : 'visibility'">
														</v-text-field>
													</v-form>

												</v-card-text>
												<v-card-actions class="p-b-20 justify-center">
													<div class="text-xs-center">
														<v-btn round outline color="primary" dark v-on:click="updatePassword()">เปลี่ยนรหัสผ่าน</v-btn>
													</div>
												</v-card-actions>
											</v-card>
										</v-flex>
										<v-flex xs12 class="text-xs-center pt-2">
											<h4 class="mb-1" style="color:#fff;">Bank For Agriculture And Agricultural Cooperatives. </h4>
										</v-flex>
									</v-layout>
								</v-flex>
							</v-layout>
						</v-container>
					</v-content>

					<v-dialog v-model="dialogProgress.status" persistent max-width="500px">
						<v-card class="text-xs-center">
							<v-btn class="m-t-30" fab dark large color="white" depressed >
								<v-icon dark color="red" style="font-size:45px!important;">notification_important</v-icon>
							</v-btn>
							<v-card-title class="justify-center pt-0">
								<span class="headline"><h4>{{dialogProgress.title}}</h4></span>
							</v-card-title>
							<v-card-text class="p-t-0 p-b-0">
								<v-container grid-list-md class="p-t-0 p-b-0">
									<v-layout wrap>
										<v-flex xs12>
											<v-progress-linear :indeterminate="true"></v-progress-linear>
										</v-flex>
									</v-layout>
								</v-container>
							</v-card-text>
							<v-card-actions class="p-b-20 justify-center">
							</v-card-actions>
						</v-card>
					</v-dialog>
				</div>

			</v-app>
		</div> <!-- end vuejs -->
	</div> <!-- end data-app -->

</body>
<script type="text/javascript">
Vue.http.options.emulateJSON = true;
Vue.http.options.emulateHTTP = true;
Vue.use(VeeValidate)
var vuejs = new Vue({
  el:"#vuejs",
	$_veeValidate: {
		validator: 'new'
	},
	data:{
		pageLoading: true,
		sessioncode: '',
		sessionname: '',
		dialogProgress: {status: false, title: ''},
		oldPassforCheck: '',
		oldPass: '',
		newPass: '',
		confirmPass: '',
		showOldPass: false,
		showNewPass: false,
		showConfirmPass: false,
		dictionary: {
			custom: {
				oldPass: {
					required: () => 'ระบุรหัสผ่าน',
					confirmed: 'รหัสผ่านเก่าไม่ถูกต้อง'
				},
				newPass: {
					required: () => 'ระบุรหัสผ่าน',
					min: 'รหัสผ่านต้องใช้อักขระอย่างน้อย 4 ตัว',
				},
				confirmPass: {
					required: () => 'ระบุรหัสผ่าน',
					min: 'รหัสผ่านต้องใช้อักขระอย่างน้อย 4 ตัว',
					confirmed: 'รหัสผ่านไม่ตรงกัน'
				}
			}
		}

	},
  created (){
		this.checkLogin();
		$(".none-show").removeClass("none-show");
  },
  computed:
  {
		pages () {
			return this.pagination.rowsPerPage ? Math.ceil(this.listUser.length / this.pagination.rowsPerPage) : 0
		}
  },
	mounted () {
		this.$validator.localize('en', this.dictionary);
	},
  watch: {
  },
	methods:{
		// start method
		progressDialog (status, title) {
			this.dialogProgress.status = status;
			this.dialogProgress.title = title;
		},
		checkLogin () {
			this.$http.post('<?=base_url() ?>index.php/loginController/getSession').then((response) => {
				var userInfo = response.body;
				if (userInfo.emp_code != "nodata") {
					this.sessioncode = userInfo.emp_code;
					this.sessionname = userInfo.emp_name;
					this.loadOldPassword();
					setTimeout(() => { this.pageLoading = false;  }, 300)
				}
				else {
					this.pageLoading = false;
					swal({
						title: 'กรุณาเข้าสู่ระบบก่อนเข้าใช้งาน',
						text: '',
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.href = '<?=base_url() ?>';
					})
				}

			}, (response) => {});
		},
		loadOldPassword () {
			this.$http.post('<?=base_url() ?>index.php/loginController/loadUserInformation', {code: this.sessioncode}).then((response) => {
				var result = response.body;
				if (result.length > 0)
					this.oldPassforCheck = result[0].EMP_PASS;
			}, (response) => {});
		},
		updatePassword () {
			this.$validator.validateAll().then((result) => {
				if (result) {
					swal({
						title: 'ยืนยันการเปลี่ยนรหัสผ่าน',
						text: '',
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((select) => {
						if(select.value) {
							this.changePassword();
						}
					})
				}
				else {
					swal({
						title: 'กรุณาตรวจสอบข้อมูลให้ถูกต้อง',
						text: '',
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					})
				}
			});
		},
		changePassword () {
			this.$http.post('<?=base_url() ?>index.php/loginController/updatePassword', {code: this.sessioncode, securepass: this.newPass}).then((response) => {
				var result = response.body;
				if (result === 'Successfully') {
					swal({
						title: 'เปลี่ยนรหัสผ่านเรียบร้อยแล้ว',
						text: 'กรุณาเข้าสู่ระบบใหม่อีกครั้ง',
						type: 'success',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						this.$http.post('<?=base_url() ?>index.php/loginController/logout').then((response) => {
							this.sessioncode  = '';
							this.sessionname  = '';
							this.sessionadmin = '';
							this.username     = '';
							this.Password			= '';
							this.menu = [];
							window.location.href = '<?=base_url() ?>';
						}, (response) => {});
					})
				}
				else {
					swal({
						title: 'ไม่สามารถเปลี่ยนรหัสผ่านได้',
						text: 'กรุณาลองใหม่อีกครั้ง',
						type: 'error',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					})
				}
			}, (response) => {});
		}
  // end methods
  }
})
</script>
</html>
