<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicons -->
	<link href="<?php echo $this->config->item('icon_project'); ?>" rel="icon">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/vuetify/vuetify.css" />
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/util.css">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/custom.css">
	<title><?php echo $this->config->item('project_name'); ?></title>

	<style type="text/css">
		.v-stepper--alt-labels .v-stepper__step {
			flex-basis: 97% !important;
		}
	</style>
</head>
<body>
	
	<div data-app="true" class="application--light">
		<div class="none-show" ><?php $this->load->view('header.php'); ?></div>
		<div id="vuejs">
			<v-app  class="white-bg" >
				<!-- loading -->
				<div v-show="pageLoading" class="loading-page">
					<v-content>
						<v-container fluid fill-height>
							<v-layout justify-center align-center>
								<v-progress-circular :size="200" :width="20" color="primary" indeterminate></v-progress-circular>
							</v-layout>
						</v-container>
					</v-content>
				</div>
				<!-- content -->
				<div class="none-show">

					<!-- content -->
					<v-content style="background-color: #fff;">
						<v-container fluid  class="p-t-0 p-l-0 p-r-0">
							<v-layout  row wrap>
								<v-flex xs10 offset-xs1 class="pt-2 text-xs-center">
									<span><i class="material-icons icon-title-big">create_new_folder</i></span> <h2>สร้างส่วนงานใหม่</h2>
								</v-flex>
								<!-- หัวข้อ-->
								<v-flex xs10 offset-xs1 class=" m-b-5 text-xs-center">
									<div class="p-l-50  p-r-50 p-b-50">
										<v-stepper v-model="currentStep"  alt-labels>
									    <v-stepper-header>
												<v-stepper-step :edit-icon="'check'" :complete="currentStep > 1"  :step="1" >ข้อมูลส่วนงาน</v-stepper-step>
									    </v-stepper-header>

									    <v-stepper-items>
									      <v-stepper-content step="1">

													<!--ชื่อไทย ชื่อังกฤษ -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ประเภท</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-select box single-line :items="typeMenu" item-text="head_des" item-value="id"   v-validate="'required'" :error-messages="errors.collect('step1.type')" data-vv-name="type" data-vv-scope="step1" v-model="odInfo.type"></v-select>
														</v-flex>
													</v-layout>

													<!--ชื่อไทย ชื่อังกฤษ -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ชื่อไทย</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field  box single-line label="ภาษาไทย"  v-model="odInfo.th_org_nme" clear-icon="cancel" clearable v-validate="'required'" :error-messages="errors.collect('step1.thainame')" data-vv-name="thainame" data-vv-scope="step1"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left">
															<h4 class="pt-3">ชื่ออังกฤษ</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field  box single-line label="ภาษาอังกฤษ" v-model="odInfo.eng_org_nme" clear-icon="cancel" clearable  v-validate="'required'" :error-messages="errors.collect('step1.engname')" data-vv-name="engname" data-vv-scope="step1"></v-text-field>
														</v-flex>
													</v-layout>

													<!--ชื่อย่อ วันเริ่ม -->
													<v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ชื่อย่อ</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field  box single-line label="ชื่อย่อ" v-model="odInfo.shrt_org_nme" clear-icon="cancel" clearable v-validate="'required'" :error-messages="errors.collect('step1.aligname')" data-vv-name="aligname" data-vv-scope="step1"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left">
															<h4 class="pt-3">วันที่เปิดดำเนินการ</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-menu :close-on-content-click="true" v-model="menu1" :nudge-right="40" transition="scale-transition" offset-y full-width min-width="290px">
																<v-text-field  box single-line readonly slot="activator" v-model="odInfo.start_oper_dte" append-icon="event" readonly></v-text-field>
																<v-date-picker v-model="odInfo.start_oper_dte" @input="menu1 = false" locale="th"></v-date-picker>
															</v-menu>
														</v-flex>
													</v-layout>

													<!-- <v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
														</v-flex>
														<v-flex xs6 class="text-xs-left p-t-15">
															<v-checkbox label="ขึ้นตรงผู้จัดการ" v-model="odInfo.underManager" color="red darken-3"></v-checkbox>
														</v-flex>
													</v-layout> -->

													<!--ชื่อสังกัด -->
													<!-- <v-layout row wrap class="mb-3">
														<v-flex xs2 class="text-xs-left pl-4">
															<h4 class="pt-3">ชื่อสังกัด</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field  box single-line label="ชื่อสังกัด"  v-model="odInfo.mn_name" clear-icon="cancel" clearable v-validate="'required'" :error-messages="errors.collect('step1.mainname')" data-vv-name="mainname" data-vv-scope="step1"></v-text-field>
														</v-flex>
														<v-flex xs2 class="text-xs-left">
															<h4 class="pt-3">ชื่อย่อสังกัด</h4>
														</v-flex>
														<v-flex xs4 class="text-xs-left">
															<v-text-field  box single-line label="ชื่อย่อสังกัด" v-model="odInfo.shrt_mn_name" clear-icon="cancel" clearable  v-validate="'required'" :error-messages="errors.collect('step1.mainnameas')" data-vv-name="mainnameas" data-vv-scope="step1"></v-text-field>
														</v-flex>
													</v-layout> -->

													<v-layout row wrap class="mb-3 pr-5">
														<v-spacer></v-spacer>
														<v-btn round color="primary" @click.native="validateBeforeSubmit('step1')">สร้าง{{odInfo.type_des}}</v-btn>
													</v-layout>

									      </v-stepper-content>
									    </v-stepper-items>
									  </v-stepper>

									</v-card>
									</div>
								</v-flex>

							</v-layout>
						</v-container>
					</v-content>

					<v-dialog v-model="dialogProgress.status" persistent max-width="500px">
						<v-card class="text-xs-center">
							<v-btn class="m-t-30" fab dark large color="white" depressed >
								<v-icon dark color="red" style="font-size:45px!important;">notification_important</v-icon>
							</v-btn>
							<v-card-title class="justify-center pt-0">
								<span class="headline"><h4>{{dialogProgress.title}}</h4></span>
							</v-card-title>
							<v-card-text class="p-t-0 p-b-0">
								<v-container grid-list-md class="p-t-0 p-b-0">
									<v-layout wrap>
										<v-flex xs12>
											<v-progress-linear :indeterminate="true"></v-progress-linear>
										</v-flex>
									</v-layout>
								</v-container>
							</v-card-text>
							<v-card-actions class="p-b-20 justify-center">
							</v-card-actions>
						</v-card>
					</v-dialog>

				</div>

			</v-app>
		</div> <!-- end vuejs -->
	</div> <!-- end data-app -->

</body>
<script type="text/javascript">
Vue.http.options.emulateJSON = true;
Vue.http.options.emulateHTTP = true;
Vue.use(VeeValidate)
var vuejs = new Vue({
  el:"#vuejs",
	$_veeValidate: {
		validator: 'new'
	},
	data:{
		pageLoading: true,
		projectName: 'ระบบงานที่ตั้งสำนักงานของธนาคาร',
		sessioncode: '',
		sessionname: '',
		dialogProgress: {status: false, title: ''},
		currentStep: 0,
		menu1: false,
		odInfo: {th_org_nme: '', eng_org_nme: '', shrt_org_nme: '', start_oper_dte: new Date().toISOString().substr(0, 10), type: '', mn_name: '', shrt_mn_name: '', underManager: false, type_des: ''},
		typeMenu: [],
		dictionary: {
			custom: {
				type: { required: () => 'กรุณาเลือกประเภท' },
				thainame: { required: () => 'กรุณาระบุชื่อภาษาไทย' },
				engname: { required: () => 'กรุณาระบุชื่อภาษาอังกฤษ' },
				aligname: { required: () => 'กรุณาระบุชื่อย่อ' },
				mainname: { required: () => 'กรุณาระบุชื่อสังกัด' },
				mainnameas: { required: () => 'กรุณาระบุชื่อย่อสังกัด' }
			}
		},
	},
  created (){
		this.checkLogin();
  },
  computed:
  {
  },
	mounted () {
		this.$validator.localize('en', this.dictionary);
	},
	watch: {
		'odInfo.type' (val) {
			var objType 					= _.filter(this.typeMenu, function(o) { return o.id == val; });
			if (objType.length == 1) {
				if (objType[0]['head_des'] == 'อื่นๆ')
					this.odInfo.type_des 	= '';
				else
					this.odInfo.type_des 	= objType[0]['head_des'];
			}

		},
		'odInfo.underManager' (val) {
			if (val === true) {
				this.odInfo.mn_name = 'ส่วนขึ้นตรงต่อผู้จัดการ';
				this.odInfo.shrt_mn_name = 'ส่วนขึ้นตรงต่อผู้จัดการ';
			}
			else {
				this.odInfo.mn_name = this.odInfo.th_org_nme;
				this.odInfo.shrt_mn_name = this.odInfo.shrt_org_nme;
			}
		},
		'odInfo.th_org_nme' (val) {
				this.odInfo.mn_name = this.odInfo.th_org_nme;
		},
		'odInfo.shrt_org_nme' (val) {
				this.odInfo.shrt_mn_name = this.odInfo.shrt_org_nme;
		}
  },
	methods:{
		// start method
		progressDialog (status, title) {
			this.dialogProgress.status = status;
			this.dialogProgress.title = title;
		},
		checkLogin () {
			this.$http.post('<?=base_url() ?>index.php/loginController/getSession').then((response) => {
				var userInfo = response.body;
				if (userInfo.emp_code != "nodata") {
					this.sessioncode = userInfo.emp_code;
					this.sessionname = userInfo.emp_name;
					this.loadTypeHeadOffice();
					setTimeout(() => { this.pageLoading = false; $(".none-show").removeClass("none-show"); }, 1000)
				}
				else {
					this.pageLoading = false;
					swal({
						title: 'กรุณาเข้าสู่ระบบก่อนเข้าใช้งาน',
						text: '',
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.href = '<?=base_url() ?>';
					})
				}
			}, (response) => {});
		},
		loadTypeHeadOffice () {
			this.$http.post('<?=base_url() ?>index.php/headOfficeController/loadTypeHeadOffice').then((response) => {
				this.typeMenu = response.body;
				this.typeMenu.unshift({id: '', head_des: '-- ประเภท --'});
			}, (response) => {});
		},
		validateBeforeSubmit (scope) {
			this.$validator.validateAll(scope).then((result) => {
				var me 			= this;
				if (result) {
					swal({
						title: 'ยืนยันการสร้าง ' + me.odInfo.type_des + me.odInfo.th_org_nme,
						text: '',
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						if (result.value) {
							this.progressDialog(true, 'กำลังดำเนินการ กรุณารอสักครู่');
							setTimeout(() => { this.addNewDivision(); }, 1000)
						}
					})
				}
				else {
					swal({
						title: 'กรุณาตรวจสอบข้อมูลให้ถูกต้อง',
						text: '',
						type: 'warning',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.href = "#";
					})
				}
			});

		},
		addNewDivision () {
			var me 			= this;
			this.$http.post('<?=base_url() ?>index.php/headOfficeController/addNewDivision', {odInfo: this.odInfo}).then((response) => {
				var result = response.body;
				this.progressDialog(false, null);
				if (result == 'Successfully') {
					swal({
						title: 'สร้าง '+ me.odInfo.type_des + me.odInfo.th_org_nme + ' เรียบร้อยแล้ว',
						text: '',
						type: 'success',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.reload();
					})
				}
				else {
					swal({
						title: 'ไม่สามารถสร้าง ' + me.odInfo.type_des + me.odInfo.th_org_nme + ' ได้',
						text: 'กรุกรุณาลองใหม่อีกครั้ง',
						type: 'error',
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'ตกลง',
						cancelButtonText: 'ยกเลิก',
						allowOutsideClick: false
					}).then((result) => {
						window.location.href = "#";
					})
				}
			}, (response) => {});

		}
  // end methods
  }
})
</script>
</html>
