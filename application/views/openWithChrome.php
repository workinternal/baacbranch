<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicons -->
	<link href="<?=base_url() ?>/assets/images/baac.png" rel="icon">
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/vuetify/vuetify.css" />
	<link rel="stylesheet" href="<?=base_url() ?>assets/libs/custom/util.css">
	<link href="<?=base_url() ?>assets/libs/icon/materialicons.css" rel="stylesheet">
	<link href="<?=base_url() ?>assets/libs/custom/font.css" rel="stylesheet">
	<link href="<?=base_url() ?>assets/libs/sweetalert/sweetalert2.min.css" rel="stylesheet">
	<title><?php echo $this->config->item('project_name'); ?></title>

	<style type="text/css">
		.v-card__title {
			justify-content: center !important;
		}
		.icon-notice {
			font-size: 120px;
    	color: #B71C1C;
		}

	</style>
</head>
<body>
	<div id="vuejs">
		<v-app id="inspire">


			<v-content>
				<v-container fluid  class="p-t-100 p-l-0 p-r-0">
					<v-layout  row wrap >
						<v-flex xs6 offset-xs3 style="background-color: #ffffff; height: 450px;" >
							<v-layout row >
								<v-flex xs10 offset-xs-1 >
									<v-card 	height="450px">


									<v-card-title primary-title >
										<v-layout  row wrap class="mt-5">
											<v-flex xs12 class="text-xs-center">
												<div style="width: 100%">
													<img src="<?=base_url() ?>assets/images/error.png"  height="150" >
												</div>
											</v-flex>
											<v-flex xs12 class="text-xs-center">
												<h2><?php echo $this->config->item('project_name'); ?></h2>
											</v-flex>
											<v-flex xs12 class="text-xs-center pt-5">
												<h3>ระบบจะมีประสิทธิภาพสูงสุดเมื่อใช้กับเบราว์เซอร์ Google Chrome</h3>
												<h3 class="mt-1">
													<a href="http://xms2/it/wp-content/uploads/2018/08/ChromeStandaloneSetup64.exe">
													สามารถดาวน์โหลด Google Chrome ได้ <span style="color: #B71C1C;"> ที่นี</span>
													<span><img src="<?=base_url() ?>assets/images/chrome.png" width="30px"/></span>
													</a>
												</h3>
											</v-flex>
										</v-layout>



									</v-card-title>

									<v-card-actions>

									</v-card-actions>
								</v-card>
							</v-flex>
						</v-layout>
						</v-flex>
					</v-layout>
				</v-container>
			</v-content>



		</v-app>
	</div>

	<!-- Vue -->

	<script src="<?=base_url() ?>assets/libs/vue/vue.js"></script>
	<script src="<?=base_url() ?>assets/libs/vue-resource/vue-resource.js"></script>
	<script src="<?=base_url() ?>assets/libs/vee-validate/vee-validate.js"></script>
	<script src="<?=base_url() ?>assets/libs/vuetify/vuetify.js"></script>
	<script src="<?=base_url() ?>assets/libs/sweetalert/sweetalert2.all.min.js"></script>
	<script src="<?=base_url() ?>assets/libs/jquery/jquery.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="<?=base_url() ?>assets/libs/jquery/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="<?=base_url() ?>assets/libs/bootstrap/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</body>
<script type="text/javascript">
Vue.http.options.emulateJSON = true;
Vue.http.options.emulateHTTP = true;
Vue.use(VeeValidate)
var vuejs = new Vue({
  el:"#vuejs",
	$_veeValidate: {
		validator: 'new'
	},
	data:{
		show: false
	},
  created (){
  },
  computed:
  {
  },
	mounted () {
	},
  watch: {
  },
	methods:{
		// start method
  // end methods
  }
})
</script>
</html>
