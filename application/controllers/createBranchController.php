<?php

defined('BASEPATH') or exit('No direct script access allowed');

class createBranchController extends CI_Controller
{
  public function __construct()
  {
    ini_set('date.timezone', 'Asia/Bangkok');
    parent::__construct();

    $this->load->model('createBranchModel');
  }



  public function getListProvince()
  {
    echo $this->createBranchModel->getListProvince();
  }

  public function getListDistrict()
  {
    $provinceID = $this->input->post('provinceID');
    echo $this->createBranchModel->getListDistrict($provinceID);
  }

  public function getListSubDistrict()
  {
    $provinceID = $this->input->post('provinceID');
    $amphurID = $this->input->post('amphurID');
    echo $this->createBranchModel->getListSubDistrict($provinceID, $amphurID);
  }




  public function getCurrentBranchCode()
  {
    $province = $this->input->post('province');
    echo $this->createBranchModel->getCurrentBranchCode($province);
  }

  public function getCurrentAmphur()
  {
    $province = $this->input->post('province');
    echo $this->createBranchModel->getCurrentAmphur($province);
  }

  public function addNewBranch()
  {
    $odInfo = $this->input->post('odInfo');
    $odAddrBR = $this->input->post('odAddrBR');
    $telBRInfo = $this->input->post('telBRInfo');
    $ctrBRInfo = $this->input->post('ctrBRInfo');
    $odAmphur = $this->input->post('odAmphur');
    $odAddrAM = $this->input->post('odAddrAM');
    $telAMInfo = $this->input->post('telAMInfo');
    $ctrAMInfo = $this->input->post('ctrAMInfo');
    $odUnitAmphur = $this->input->post('odUnitAmphur');
    $odArea = $this->input->post('odArea');
    echo $this->createBranchModel->addNewBranch($odArea, $odInfo, $odAddrBR, $telBRInfo, $ctrBRInfo, $odAmphur, $odAddrAM, $telAMInfo, $ctrAMInfo, $odUnitAmphur);
  }

  public function changetoNewBranch()
  {
    $odInfo = $this->input->post('odInfo');
    $odAddrBR = $this->input->post('odAddrBR');
    $telBRInfo = $this->input->post('telBRInfo');
    $ctrBRInfo = $this->input->post('ctrBRInfo');
    $odAmphur = $this->input->post('odAmphur');
    $odAddrAM = $this->input->post('odAddrAM');
    $telAMInfo = $this->input->post('telAMInfo');
    $ctrAMInfo = $this->input->post('ctrAMInfo');
    $odUnitAmphur = $this->input->post('odUnitAmphur');
    $odArea = $this->input->post('odArea');
    $oldUnit = $this->input->post('oldUnit');
    $brcodeOld = $this->input->post('brcodeOld');
    $checkArea = $this->input->post('checkArea');

    echo $this->createBranchModel->changetoNewBranch($oldUnit, $odArea, $odInfo, $odAddrBR, $telBRInfo, $ctrBRInfo, $odAmphur, $odAddrAM, $telAMInfo, $ctrAMInfo, $odUnitAmphur, $brcodeOld, $checkArea);
  }




}

?>
