<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Main extends CI_Controller
{
  public function __construct()
  {
    ini_set('date.timezone', 'Asia/Bangkok');
    parent::__construct();

    // $this->load->model('Report_model');
  }

  public function index()
  {
    $this->load->view('index.php');
  }

  public function main()
  {
    $this->load->view('main.php');
  }

  public function openwithchrome()
  {
    $this->load->view('openWithChrome.php');
  }

  public function branch()
  {
    $this->load->view('dataBranch.php');
  }

  public function createbranch()
  {
    $this->load->view('createBranch.php');
  }

  public function useraccess()
  {
    $this->load->view('useraccess.php');
  }

  public function header()
  {
    $this->load->view('header.php');
  }

  public function editbranch()
  {
    $this->load->view('editbranch.php');
  }

  public function closebranch()
  {
    $this->load->view('closeBranch.php');
  }

  public function editunit()
  {
    $this->load->view('editUnit.php');
  }

  public function createunit()
  {
    $this->load->view('createunit.php');
  }

  public function closeunit()
  {
    $this->load->view('closeunit.php');
  }

  public function changetobranch()
  {
    $this->load->view('changetoBranch.php');
  }

  public function createHead()
  {
    $this->load->view('createHeadOffice.php');
  }

  public function editHead()
  {
    $this->load->view('editHeadOffice.php');
  }

  public function closeHead()
  {
    $this->load->view('closeHeadOffice.php');
  }

  public function headOffice()
  {
    $this->load->view('dataHeadOffice.php');
  }

  public function updatepassword()
  {
    $this->load->view('passwordReset.php');
  }

  public function createunitbranch()
  {
    $this->load->view('createUnitBranch.php');
  }

  public function editunitbranch()
  {
    $this->load->view('editUnitBranch.php');
  }

  public function closeunitbranch()
  {
    $this->load->view('closeUnitBranch.php');
  }

  public function createprovincial()
  {
    $this->load->view('createProvincial.php');
  }

  public function editprovincial()
  {
    $this->load->view('editProvincial.php');
  }

  public function closeprovincial()
  {
    $this->load->view('closeProvincial.php');
  }

  public function changetoprovince()
  {
    $this->load->view('changetoProvince.php');
  }

  public function changeprovincebranch()
  {
    $this->load->view('changeProvince.php');
  }
















}

?>
