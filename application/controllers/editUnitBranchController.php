<?php

defined('BASEPATH') or exit('No direct script access allowed');

class editUnitBranchController extends CI_Controller
{
  public function __construct()
  {
    ini_set('date.timezone', 'Asia/Bangkok');
    parent::__construct();

    $this->load->model('editUnitBranchModel');
  }



  public function getListProvince()
  {
    echo $this->editUnitBranchModel->getListProvince();
  }

  public function getListAmphur()
  {
    $provinceID = $this->input->post('provinceID');
    echo $this->editUnitBranchModel->getListAmphur($provinceID);
  }

  public function getListDistrict()
  {
    $provinceID = $this->input->post('provinceID');
    $amphurID = $this->input->post('amphurID');
    echo $this->editUnitBranchModel->getListDistrict($provinceID, $amphurID);
  }

  public function getUnitBranchInformation()
  {
    $id = $this->input->post('id');
    echo $this->editUnitBranchModel->getUnitBranchInformation($id);
  }

  public function getListBranchName()
  {
    $id = $this->input->post('id');
    echo $this->editUnitBranchModel->getListBranchName($id);
  }

  public function getListUnitBranchName()
  {
    $id = $this->input->post('id');
    echo $this->editUnitBranchModel->getListUnitBranchName($id);
  }

  public function updateUnitBranchInformation()
  {
    $odInfo = $this->input->post('odInfo');
    $odAddrBR = $this->input->post('odAddrBR');
    $telInfo = $this->input->post('telInfo');
    $ctrInfo = $this->input->post('ctrInfo');
    echo $this->editUnitBranchModel->updateUnitBranchInformation($odInfo, $odAddrBR, $telInfo, $ctrInfo);
  }

  public function closeUnitBranch()
  {
    $id = $this->input->post('id');
    echo $this->editUnitBranchModel->closeUnitBranch($id);
  }

  public function getUnitBranchInformationforClose()
  {
    $id = $this->input->post('id');
    echo $this->editUnitBranchModel->getUnitBranchInformationforClose($id);
  }






}

?>
