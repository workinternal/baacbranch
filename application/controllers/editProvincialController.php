<?php

defined('BASEPATH') or exit('No direct script access allowed');

class editProvincialController extends CI_Controller
{
  public function __construct()
  {
    ini_set('date.timezone', 'Asia/Bangkok');
    parent::__construct();

    $this->load->model('editProvincialModel');
  }



  public function getListProvince()
  {
    echo $this->editProvincialModel->getListProvince();
  }

  public function getListAmphur()
  {
    $provinceID = $this->input->post('provinceID');
    echo $this->editProvincialModel->getListAmphur($provinceID);
  }

  public function getListDistrict()
  {
    $provinceID = $this->input->post('provinceID');
    $amphurID = $this->input->post('amphurID');
    echo $this->editProvincialModel->getListDistrict($provinceID, $amphurID);
  }

  public function getBranchInformation()
  {
    $id = $this->input->post('id');
    echo $this->editProvincialModel->getBranchInformation($id);
  }

  public function getListProvinceName()
  {
    $id = $this->input->post('id');
    echo $this->editProvincialModel->getListProvinceName($id);
  }

  public function updateProvinceInformation()
  {
    $odInfo = $this->input->post('odInfo');
    $odAddrBR = $this->input->post('odAddrBR');
    $telInfo = $this->input->post('telInfo');
    $ctrInfo = $this->input->post('ctrInfo');
    echo $this->editProvincialModel->updateProvinceInformation($odInfo, $odAddrBR, $telInfo, $ctrInfo);
  }


  public function closeProvince()
  {
    $id = $this->input->post('id');
    echo $this->editProvincialModel->closeProvince($id);
  }
  







}

?>
