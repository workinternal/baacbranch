<?php

defined('BASEPATH') or exit('No direct script access allowed');

class createProvincialController extends CI_Controller
{
  public function __construct()
  {
    ini_set('date.timezone', 'Asia/Bangkok');
    parent::__construct();

    $this->load->model('createProvincialModel');
  }



  public function getListProvince()
  {
    echo $this->createProvincialModel->getListProvince();
  }

  public function getListDistrict()
  {
    $provinceID = $this->input->post('provinceID');
    echo $this->createProvincialModel->getListDistrict($provinceID);
  }

  public function getListSubDistrict()
  {
    $provinceID = $this->input->post('provinceID');
    $amphurID = $this->input->post('amphurID');
    echo $this->createProvincialModel->getListSubDistrict($provinceID, $amphurID);
  }

  public function addNewProvincial()
  {
    $odInfo = $this->input->post('odInfo');
    $odAddrBR = $this->input->post('odAddrBR');
    $telBRInfo = $this->input->post('telBRInfo');
    $ctrBRInfo = $this->input->post('ctrBRInfo');
    echo $this->createProvincialModel->addNewProvincial($odInfo, $odAddrBR, $telBRInfo, $ctrBRInfo);
  }

  public function changetoNewBranch()
  {
    $odInfo = $this->input->post('odInfo');
    $odAddrBR = $this->input->post('odAddrBR');
    $telBRInfo = $this->input->post('telBRInfo');
    $ctrBRInfo = $this->input->post('ctrBRInfo');
    $odAmphur = $this->input->post('odAmphur');
    $odAddrAM = $this->input->post('odAddrAM');
    $telAMInfo = $this->input->post('telAMInfo');
    $ctrAMInfo = $this->input->post('ctrAMInfo');
    $odUnitAmphur = $this->input->post('odUnitAmphur');
    $odArea = $this->input->post('odArea');
    $oldUnit = $this->input->post('oldUnit');

    echo $this->createProvincialModel->changetoNewBranch($oldUnit, $odArea, $odInfo, $odAddrBR, $telBRInfo, $ctrBRInfo, $odAmphur, $odAddrAM, $telAMInfo, $ctrAMInfo, $odUnitAmphur);
  }


  public function changetoProvincial()
  {
    $odInfo = $this->input->post('odInfo');
    $provinceInfo = $this->input->post('provinceInfo');
    $provinceAddr = $this->input->post('provinceAddr');
    $telBRInfo = $this->input->post('telBRInfo');
    $ctrBRInfo = $this->input->post('ctrBRInfo');
    echo $this->createProvincialModel->changetoProvincial($odInfo, $provinceInfo, $provinceAddr, $telBRInfo, $ctrBRInfo);
  }




}

?>
