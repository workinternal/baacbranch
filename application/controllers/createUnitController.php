<?php

defined('BASEPATH') or exit('No direct script access allowed');

class createUnitController extends CI_Controller
{
  public function __construct()
  {
    ini_set('date.timezone', 'Asia/Bangkok');
    parent::__construct();

    $this->load->model('createUnitModel');
  }



  public function getListProvince()
  {
    echo $this->createUnitModel->getListProvince();
  }

  public function getListAmphur()
  {
    $provinceID = $this->input->post('provinceID');
    echo $this->createUnitModel->getListAmphur($provinceID);
  }

  public function getListDistrict()
  {
    $provinceID = $this->input->post('provinceID');
    $amphurID = $this->input->post('amphurID');
    echo $this->createUnitModel->getListDistrict($provinceID, $amphurID);
  }

  public function getUnitBranchInformation()
  {
    $id = $this->input->post('id');
    echo $this->createUnitModel->getUnitBranchInformation($id);
  }

  public function getListBranchName()
  {
    echo $this->createUnitModel->getListBranchName();
  }

  public function createUnitBranchInformation()
  {
    $odInfo = $this->input->post('odInfo');
    $odAmphur = $this->input->post('odAmphur');
    $odAddrAM = $this->input->post('odAddrAM');
    $telInfo = $this->input->post('telInfo');
    $ctrInfo = $this->input->post('ctrInfo');
    $odUnitAmphur = $this->input->post('odUnitAmphur');
    $odArea = $this->input->post('odArea');
    echo $this->createUnitModel->createUnitBranchInformation($odInfo, $odAmphur, $odAddrAM, $telInfo, $ctrInfo, $odUnitAmphur, $odArea);
  }

  public function getCurrenUnitBranchCode()
  {
    $province = $this->input->post('province');
    $branch = $this->input->post('branch');
    echo $this->createUnitModel->getCurrenUnitBranchCode($province, $branch);
  }

  public function getCurrenUnitAreaCode()
  {
    $province = $this->input->post('province');
    echo $this->createUnitModel->getCurrenUnitAreaCode($province);
  }






}

?>
