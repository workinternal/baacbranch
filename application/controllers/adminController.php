<?php

defined('BASEPATH') or exit('No direct script access allowed');

class adminController extends CI_Controller
{
  public function __construct()
  {
    ini_set('date.timezone', 'Asia/Bangkok');
    parent::__construct();

    $this->load->model('adminModel');
  }

  public function getUserAdmin()
  {
    echo $this->adminModel->getUserAdmin();
  }

  public function getUserInfo()
  {
    $empCode = $this->input->post('empCode');
    echo $this->adminModel->getUserInfo($empCode);
  }

  public function addRole()
  {
    $empCode = $this->input->post('empCode');
    echo $this->adminModel->addRole($empCode);
  }

  public function removeRole()
  {
    $empCode = $this->input->post('empCode');
    echo $this->adminModel->removeRole($empCode);
  }




}

?>
