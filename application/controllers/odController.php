<?php

defined('BASEPATH') or exit('No direct script access allowed');

class odController extends CI_Controller
{
  public function __construct()
  {
    ini_set('date.timezone', 'Asia/Bangkok');
    parent::__construct();

    $this->load->model('odModel');
  }

  public function loadBrandDetail()
  {
    $odInfo = $this->input->post('odInfo');
    echo $this->odModel->loadBrandDetail($odInfo);
  }

  public function loadDivInfo()
  {
    echo $this->odModel->loadDivInfo();
  }

  public function loadProvinceInfo()
  {
    $div = $this->input->post('div');
    echo $this->odModel->loadProvinceInfo($div);
  }

  public function loadBranchInfo()
  {
    $province = $this->input->post('province');
    echo $this->odModel->loadBranchInfo($province);
  }

  public function loadTypeInfo()
  {
    echo $this->odModel->loadTypeInfo();
  }



}

?>
