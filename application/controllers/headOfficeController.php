<?php

defined('BASEPATH') or exit('No direct script access allowed');

class headOfficeController extends CI_Controller
{
  public function __construct()
  {
    ini_set('date.timezone', 'Asia/Bangkok');
    parent::__construct();

    $this->load->model('headOfficeModel');
  }

  public function loadTypeHeadOffice()
  {
    echo $this->headOfficeModel->loadTypeHeadOffice();
  }

  public function addNewDivision()
  {
    $odInfo = $this->input->post('odInfo');
    echo $this->headOfficeModel->addNewDivision($odInfo);
  }

  public function loadListDivision()
  {
    echo $this->headOfficeModel->loadListDivision();
  }

  public function loadDivisionInformation()
  {
    $id = $this->input->post('id');
    echo $this->headOfficeModel->loadDivisionInformation($id);
  }

  public function editDivision()
  {
    $headOfficeID = $this->input->post('headOfficeID');
    $odInfo = $this->input->post('odInfo');
    echo $this->headOfficeModel->editDivision($headOfficeID ,$odInfo);
  }

  public function closeHeadOffice()
  {
    $headOfficeID = $this->input->post('headOfficeID');
    echo $this->headOfficeModel->closeHeadOffice($headOfficeID);
  }

  public function loadAllDivisionInformation()
  {
    echo $this->headOfficeModel->loadAllDivisionInformation();
  }




}

?>
