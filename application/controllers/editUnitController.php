<?php

defined('BASEPATH') or exit('No direct script access allowed');

class editUnitController extends CI_Controller
{
  public function __construct()
  {
    ini_set('date.timezone', 'Asia/Bangkok');
    parent::__construct();

    $this->load->model('editUnitModel');
  }



  public function getListProvince()
  {
    echo $this->editUnitModel->getListProvince();
  }

  public function getListAmphur()
  {
    $provinceID = $this->input->post('provinceID');
    echo $this->editUnitModel->getListAmphur($provinceID);
  }

  public function getListDistrict()
  {
    $provinceID = $this->input->post('provinceID');
    $amphurID = $this->input->post('amphurID');
    echo $this->editUnitModel->getListDistrict($provinceID, $amphurID);
  }

  public function getUnitBranchInformation()
  {
    $id = $this->input->post('id');
    echo $this->editUnitModel->getUnitBranchInformation($id);
  }

  public function getListBranchName()
  {
    $id = $this->input->post('id');
    echo $this->editUnitModel->getListBranchName($id);
  }

  public function getListUnitBranchName()
  {
    $id = $this->input->post('id');
    echo $this->editUnitModel->getListUnitBranchName($id);
  }

  public function updateUnitBranchInformation()
  {
    $odInfo = $this->input->post('odInfo');
    $odAddrBR = $this->input->post('odAddrBR');
    $telInfo = $this->input->post('telInfo');
    $ctrInfo = $this->input->post('ctrInfo');
    $odUnitAmphur = $this->input->post('odUnitAmphur');
    $odArea = $this->input->post('odArea');
    echo $this->editUnitModel->updateUnitBranchInformation($odInfo, $odAddrBR, $telInfo, $ctrInfo, $odUnitAmphur, $odArea);
  }

  public function closeUnitBranch()
  {
    $id = $this->input->post('id');
    echo $this->editUnitModel->closeUnitBranch($id);
  }

  public function getUnitBranchInformationforClose()
  {
    $id = $this->input->post('id');
    echo $this->editUnitModel->getUnitBranchInformationforClose($id);
  }






}

?>
