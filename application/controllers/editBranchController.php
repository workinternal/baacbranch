<?php

defined('BASEPATH') or exit('No direct script access allowed');

class editBranchController extends CI_Controller
{
  public function __construct()
  {
    ini_set('date.timezone', 'Asia/Bangkok');
    parent::__construct();

    $this->load->model('editBranchModel');
  }



  public function getListProvince()
  {
    echo $this->editBranchModel->getListProvince();
  }

  public function getListAmphur()
  {
    $provinceID = $this->input->post('provinceID');
    echo $this->editBranchModel->getListAmphur($provinceID);
  }

  public function getListDistrict()
  {
    $provinceID = $this->input->post('provinceID');
    $amphurID = $this->input->post('amphurID');
    echo $this->editBranchModel->getListDistrict($provinceID, $amphurID);
  }

  public function getBranchInformation()
  {
    $id = $this->input->post('id');
    echo $this->editBranchModel->getBranchInformation($id);
  }

  public function getListBranchName()
  {
    $id = $this->input->post('id');
    echo $this->editBranchModel->getListBranchName($id);
  }

  public function updateBranchInformation()
  {
    $odInfo = $this->input->post('odInfo');
    $odAddrBR = $this->input->post('odAddrBR');
    $telInfo = $this->input->post('telInfo');
    $ctrInfo = $this->input->post('ctrInfo');
    echo $this->editBranchModel->updateBranchInformation($odInfo, $odAddrBR, $telInfo, $ctrInfo);
  }


  public function closeBranch()
  {
    $id = $this->input->post('id');
    echo $this->editBranchModel->closeBranch($id);
  }

  public function getAmphurUnderControl()
  {
    $id = $this->input->post('id');
    echo $this->editBranchModel->getAmphurUnderControl($id);
  }

  public function changeProvince()
  {
    $odInfo = $this->input->post('odInfo');
    $provinceInfo = $this->input->post('provinceInfo');
    echo $this->editBranchModel->changeProvince($odInfo, $provinceInfo);
  }







}

?>
