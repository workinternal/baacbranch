<?php

defined('BASEPATH') or exit('No direct script access allowed');

class createUnitBranchController extends CI_Controller
{
  public function __construct()
  {
    ini_set('date.timezone', 'Asia/Bangkok');
    parent::__construct();

    $this->load->model('createUnitBranchModel');
  }



  public function getListProvince()
  {
    echo $this->createUnitBranchModel->getListProvince();
  }

  public function getListAmphur()
  {
    $provinceID = $this->input->post('provinceID');
    echo $this->createUnitBranchModel->getListAmphur($provinceID);
  }

  public function getListDistrict()
  {
    $provinceID = $this->input->post('provinceID');
    $amphurID = $this->input->post('amphurID');
    echo $this->createUnitBranchModel->getListDistrict($provinceID, $amphurID);
  }

  public function getUnitBranchInformation()
  {
    $id = $this->input->post('id');
    echo $this->createUnitBranchModel->getUnitBranchInformation($id);
  }

  public function getListBranchName()
  {
    echo $this->createUnitBranchModel->getListBranchName();
  }

  public function createUnitBranchInformation()
  {
    $odInfo = $this->input->post('odInfo');
    $odAmphur = $this->input->post('odAmphur');
    $odAddrAM = $this->input->post('odAddrAM');
    $telInfo = $this->input->post('telInfo');
    $ctrInfo = $this->input->post('ctrInfo');
    echo $this->createUnitBranchModel->createUnitBranchInformation($odInfo, $odAmphur, $odAddrAM, $telInfo, $ctrInfo);
  }






}

?>
