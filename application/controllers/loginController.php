<?php

defined('BASEPATH') or exit('No direct script access allowed');

class loginController extends CI_Controller
{
  public function __construct()
  {
    ini_set('date.timezone', 'Asia/Bangkok');
    parent::__construct();

    $this->load->model('loginModel');
  }

  public function checkLogin()
  {
    $empcode = $this->input->post('empcode');
    $emppass = $this->input->post('emppass');
    echo $this->loginModel->checkLogin($empcode, $emppass);
  }

  public function getSession()
  {
    echo $this->loginModel->getSession();
  }

  public function logout()
  {
    echo $this->loginModel->logout();
  }

  public function loadUserInformation()
  {
    $code = $this->input->post('code');
    echo $this->loginModel->loadUserInformation($code);
  }

  public function updatePassword()
  {
    $code = $this->input->post('code');
    $securepass = $this->input->post('securepass');
    echo $this->loginModel->updatePassword($code, $securepass);
  }




}

?>
