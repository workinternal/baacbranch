<?php
class adminModel extends CI_Model
{
  var $mssql;
  public function __construct()
  {
    parent::__construct();
    //use DT database connection
    $this->mssql = $this->load->database ( 'DT', TRUE );
  }



  public function getUserAdmin () {
    $queryStr = "SELECT M.EMP_CODE, M.IS_ADMIN, Z.*
      FROM OD_MEMBER M
      RIGHT JOIN (
        SELECT A.EMP_CODE AS EMPCODE, A.TITLE,A.FNAME,A.LNAME,B.DIVISION_AS, C.POSITION_NAME, A.POSITION_LEVEL
        FROM MGTBASE.dbo.Personal A
        INNER JOIN MGTBASE.dbo.Division B ON A.DIVISION_CODE=B.DIVISION_CODE
        INNER JOIN MGTBASE.dbo.Position C ON A.POSITION_CODE=C.POSITION_CODE
        LEFT JOIN MGTBASE.DBO.CBS_DIVISION_CODE D ON A.DIVISION_CODE = D.DIVISION_CODE
      ) as Z on Z.EMPCODE = M.EMP_CODE
      WHERE  M.IS_ACTIVE = 1 AND M.IS_ADMIN = 1";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['empCode'] = $row['EMP_CODE'];
        $resultbuff['title'] = $row['TITLE'];
        $resultbuff['name'] = $row['FNAME']." ".$row['LNAME'];
        $resultbuff['division'] = $row['DIVISION_AS'];
        $resultbuff['position'] = $row['POSITION_NAME'];
        $resultbuff['positionLevel'] = $row['POSITION_LEVEL'];
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);

  }

  public function getUserInfo ($empCode) {
    $queryStr = "SELECT A.EMP_CODE AS EMPCODE, A.TITLE,A.FNAME,A.LNAME,B.DIVISION_AS, C.POSITION_NAME, A.POSITION_LEVEL
        FROM MGTBASE.dbo.Personal A
        INNER JOIN MGTBASE.dbo.Division B ON A.DIVISION_CODE=B.DIVISION_CODE
        INNER JOIN MGTBASE.dbo.Position C ON A.POSITION_CODE=C.POSITION_CODE
        LEFT JOIN MGTBASE.DBO.CBS_DIVISION_CODE D ON A.DIVISION_CODE = D.DIVISION_CODE
        WHERE A.EMP_CODE = '$empCode'";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['empCode'] = $row['EMPCODE'];
        $resultbuff['title'] = $row['TITLE'];
        $resultbuff['name'] = $row['FNAME']." ".$row['LNAME'];
        $resultbuff['division'] = $row['DIVISION_AS'];
        $resultbuff['position'] = $row['POSITION_NAME'];
        $resultbuff['positionLevel'] = $row['POSITION_LEVEL'];
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function addRole ($empCode) {
    $result    = "";
    $addPeople = $this->mssql->query("INSERT INTO OD_MEMBER (EMP_CODE, EMP_PASS, IS_ACTIVE)
    SELECT '$empCode', '$empCode', 1
    WHERE NOT EXISTS(
      SELECT EMP_CODE
      FROM OD_MEMBER
      WHERE EMP_CODE = '$empCode'
    )");

    $updateRole = $this->mssql->query("UPDATE OD_MEMBER
      SET IS_ADMIN = 1
      WHERE EMP_CODE = '$empCode' AND NOT EXISTS (SELECT EMP_CODE FROM OD_MEMBER
        WHERE EMP_CODE = '$empCode' AND IS_ADMIN = 1);");

    $log = $this->mssql->affected_rows();

    if($updateRole){
      $result = "Successfully:".$log;
    }else{
      $result = "Failed:0";
    }

    $this->mssql->close();
    return $result;
  }

  public function removeRole ($empCode) {
    $result = "";
    $updateRole = $this->mssql->query("UPDATE OD_MEMBER SET IS_ADMIN = NULL WHERE EMP_CODE = '$empCode'");

    if($updateRole){
      $result = "Successfully";
    }else{
      $result = "Failed";
    }

    $this->mssql->close();
    return $result;
  }

}
?>
