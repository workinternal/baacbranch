<?php
class loginModel extends CI_Model
{
  var $mssql;
  public function __construct()
  {
    parent::__construct();
    //use DT database connection
    $this->mssql = $this->load->database ( 'DT', TRUE );
    $this->mgtbase = $this->load->database ( 'mgtbase', TRUE );
  }


  public function logout()
  {
    $this->session->unset_userdata('od_emp_code');
    $this->session->unset_userdata('od_emp_name');
    $this->session->unset_userdata('od_division_code');
    $this->session->unset_userdata('od_division_name');
    $this->session->unset_userdata('od_position_code');
    $this->session->unset_userdata('od_position_name');
    return 'Logout Success';
  }

  public function getSession()
  {
    $user = $this->session->userdata('od_emp_code');
    $result = array();
    if (isset($user)) {
      $result = array(
        "emp_code"        => $this->session->userdata('od_emp_code'),
        "emp_name"        => $this->session->userdata('od_emp_name'),
        "division_code"   => $this->session->userdata('od_division_code'),
        "division_name"   => $this->session->userdata('od_division_name'),
        "position_code"   => $this->session->userdata('od_position_code'),
        "position_name"   => $this->session->userdata('od_position_name'),
        "is_admin"        => $this->session->userdata('od_is_admin')
      );
      return json_encode($result,JSON_UNESCAPED_UNICODE);
    }
    else {
      $result = array("emp_code" => "nodata");
      return json_encode($result,JSON_UNESCAPED_UNICODE);
    }
  }

  public function checkLogin ($employeeID, $employeePass) {
    $queryStr = " SELECT EMP_CODE, EMP_PASS, IS_ADMIN  FROM OD_MEMBER  WHERE EMP_CODE = '$employeeID' ";
    $query = $this->mssql->query($queryStr)->result_array();
    $this->mssql->close();
      if (sizeof($query) > 0) {
        $mapEmpCode     = $query[0]['EMP_CODE'];
        $mapEmpPass     = $query[0]['EMP_PASS'];
        $isadmin        = $query[0]['IS_ADMIN'];
        if ($employeePass == $mapEmpPass) {
          $queryStr = " SELECT A.EMP_CODE AS EMPCODE, A.FNAME, A.LNAME, A.DIVISION_CODE, B.DIVISION_NAME, B.DIVISION_AS, B.DIVISION_MN_CODE
                        , D.ORG_CD, A.POSITION_CODE, B.DIVISION_AREA, C.POSITION_NAME, C.POSITION_NAME_LEVEL
                        FROM MGTBASE.dbo.Personal A
                        INNER JOIN MGTBASE.dbo.Division B ON A.DIVISION_CODE=B.DIVISION_CODE
                        INNER JOIN MGTBASE.dbo.Position C ON A.POSITION_CODE=C.POSITION_CODE
                        LEFT JOIN MGTBASE.DBO.CBS_DIVISION_CODE D ON A.DIVISION_CODE = D.DIVISION_CODE
                        WHERE A.EMP_CODE = '$employeeID'";
          $queryInfo = $this->mgtbase->query($queryStr)->result_array();
          $this->mgtbase->close();
          //ถ้ามี user ใน mgtbase
          if (sizeof($queryInfo) > 0) {
            $mgtEmpcode = $queryInfo[0]['EMPCODE'];
            $empname    = $queryInfo[0]['FNAME']." ".$queryInfo[0]['LNAME'];
            $divcode    = $queryInfo[0]['DIVISION_CODE'];
            $divname    = $queryInfo[0]['DIVISION_NAME'];
            $poscode    = $queryInfo[0]['POSITION_CODE'];
            $posname    = explode(" ",$queryInfo[0]['POSITION_NAME'])[0]." ".explode(" ", $queryInfo[0]['POSITION_NAME_LEVEL'])[1];

            $this->session->od_emp_code       = $mgtEmpcode;
            $this->session->od_emp_name       = $empname;
            $this->session->od_division_code  = $divcode;
            $this->session->od_division_name  = $divname;
            $this->session->od_position_code  = $poscode;
            $this->session->od_position_name  = $posname;
            $this->session->od_is_admin       = $isadmin;
            return "Login Success";
          }
          else {
            return "Incorrect User";
          }

        }
        elseif ($employeePass != $mapEmpPass) {
          return "Incorrect Password";
        }
      }
      else {  //ไม่มี user ใน mapuser เช็คต่อใน mgtbase
        $queryStr = " SELECT A.EMP_CODE AS EMPCODE, A.FNAME, A.LNAME, A.DIVISION_CODE, B.DIVISION_NAME, B.DIVISION_AS, B.DIVISION_MN_CODE
                      , D.ORG_CD, A.POSITION_CODE, B.DIVISION_AREA, C.POSITION_NAME, C.POSITION_NAME_LEVEL
                      FROM MGTBASE.dbo.Personal A
                      INNER JOIN MGTBASE.dbo.Division B ON A.DIVISION_CODE=B.DIVISION_CODE
                      INNER JOIN MGTBASE.dbo.Position C ON A.POSITION_CODE=C.POSITION_CODE
                      LEFT JOIN MGTBASE.DBO.CBS_DIVISION_CODE D ON A.DIVISION_CODE = D.DIVISION_CODE
                      WHERE A.EMP_CODE = '$employeeID'";
        $query = $this->mgtbase->query($queryStr)->result_array();
        $this->mgtbase->close();
        //ถ้ามี user ใน mgtbase
        if (sizeof($query) > 0) {
          $mgtEmpcode = $query[0]['EMPCODE'];
          $empname    = $query[0]['FNAME']." ".$query[0]['LNAME'];
          $divcode    = $query[0]['DIVISION_CODE'];
          $divname    = $query[0]['DIVISION_NAME'];
          $poscode    = $query[0]['POSITION_CODE'];
          $posname    = explode(" ",$query[0]['POSITION_NAME'])[0]." ".explode(" ",$query[0]['POSITION_NAME_LEVEL'])[1];

          $addNewEmployee = $this->mssql->query("INSERT INTO OD_MEMBER (EMP_CODE, EMP_PASS, IS_ACTIVE, IS_ADMIN)
          VALUES ('$mgtEmpcode', '$mgtEmpcode', '1', '0') ");
          if ($addNewEmployee) {
            if ($employeePass == $mgtEmpcode) {
              $this->session->od_emp_code       = $mgtEmpcode;
              $this->session->od_emp_name       = $empname;
              $this->session->od_division_code  = $divcode;
              $this->session->od_division_name  = $divname;
              $this->session->od_position_code  = $poscode;
              $this->session->od_position_name  = $posname;
              $this->session->od_is_admin       = "0";
              return "Login Success";
            }
            else {
              return "Incorrect Password";
            }
          }
          else {
            return "Incorrect User";
          }
        }
      }

  }

  public function loadUserInformation($code)
  {

    $queryStr = "SELECT EMP_CODE, EMP_PASS FROM OD_MEMBER WHERE EMP_CODE = '$code'";
    $query = $this->mssql->query($queryStr)->result_array();
    $result = array();
    $resultbuff = array();
    foreach ($query as $row) {
      $resultbuff['EMP_CODE'] = $row['EMP_CODE'];
      $resultbuff['EMP_PASS'] = $row['EMP_PASS'];
      array_push($result, $resultbuff);
    }
    $this->mssql->close();
    return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function updatePassword ($code, $securepass) {
    $updatePassword= $this->mssql->query("UPDATE OD_MEMBER
      SET
      EMP_PASS = '$securepass'
      WHERE EMP_CODE = '$code'");

    $this->mssql->close();
    if ($updatePassword) {
      return 'Successfully';
    }
    else {
      return 'Failed';
    }
  }


}
?>
