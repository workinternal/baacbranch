<?php
class editBranchModel extends CI_Model
{
  var $mssql;
  public function __construct()
  {
    parent::__construct();
    //use DT database connection
    $this->mssql = $this->load->database ( 'DT', TRUE );
  }


  public function getListProvince () {
    $queryStr = "SELECT PROVINCE, CAT_CC FROM ADDRESS_CODE GROUP BY PROVINCE, CAT_CC ORDER BY PROVINCE";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['CAT_CC'];
        $resultbuff['code'] = $row['CAT_CC'];
        $resultbuff['name'] = $row['PROVINCE'];
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getListAmphur ($provinceID) {
    $queryStr = "SELECT REPLACE(DISTRICT, 'อำเภอ', '') DISTRICT, CAT_AA FROM ADDRESS_CODE WHERE CAT_CC = '$provinceID' GROUP BY DISTRICT, CAT_AA ORDER BY CAT_AA";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['CAT_AA'];
        $resultbuff['code'] = $row['CAT_AA'];
        $resultbuff['name'] = $row['DISTRICT'];

        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getListDistrict ($provinceID, $amphurID) {
    $queryStr = "SELECT SUB_DISTRICT, CAT_TT, POSTCODE FROM ADDRESS_CODE WHERE CAT_CC = '$provinceID' AND CAT_AA = '$amphurID' GROUP BY SUB_DISTRICT, CAT_TT, POSTCODE  ORDER BY CAT_TT";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['CAT_TT'];
        $resultbuff['code'] = $row['CAT_TT'];
        $resultbuff['name'] = $row['SUB_DISTRICT'];
        $resultbuff['postcode'] = $row['POSTCODE'];
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getBranchInformation ($id) {
    $queryStr = "SELECT DIVNME.brnch_act_dept_nme AS div_nme, BRNME.brname, ORGNME.is_div, ORGNME.is_br, ORGNME.is_sbr, ORGNME.is_ch, ORGNME.is_am
                , ORGNME.is_thai_n, ORGNME.is_eng_n, ORGNME.is_amp_n, ORGNME.is_open_dte
                , ORGADDR.addr, ORGADDR.moo, ORGADDR.soi, ORGADDR.street, ORGADDR.subdist, ORGADDR.dist, ORGADDR.prov, ORGADDR.zip_code
                , DIVI.DIVISION_CODE
                , ADDR.CAT_CC, ADDR.CAT_AA, ADDR.CAT_TT
                FROM od_br_name  AS ORGNME
                LEFT JOIN od_org_addr AS ORGADDR ON ORGNME.is_br = ORGADDR.ind_brnch_cd AND ORGNME.is_sbr = ORGADDR.dept_cd
                LEFT JOIN Division AS DIVI ON ( '48' + ORGNME.is_br + ORGNME.is_sbr + ORGNME.is_ch + ORGNME.is_am ) = DIVI.DIVISION_CODE
                JOIN od_brnch_act_dept AS DIVNME ON CONVERT(INT, ORGNME.is_div) = CONVERT(INT, DIVNME.brnch_act_dept_cd)
                JOIN  tb_brmapprofile AS BRNME ON (ORGNME.is_br + 'A') = brcode AND ORGNME.is_ch = ORGADDR.div_cd
                JOIN ADDRESS_CODE AS ADDR ON ORGADDR.prov = ADDR.PROVINCE AND ORGADDR.dist = ADDR.DISTRICT AND ORGADDR.subdist = ADDR.SUB_DISTRICT
                WHERE ORGNME.is_br + ORGNME.is_sbr + ORGNME.is_ch + ORGNME.is_am = '$id'";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['div_nme'] = $row['div_nme'];
        $resultbuff['brname'] = $row['brname'];
        $resultbuff['is_div'] = $row['is_div'];
        $resultbuff['is_br'] = $row['is_br'];
        $resultbuff['is_sbr'] = $row['is_sbr'];
        $resultbuff['is_ch'] = $row['is_ch'];
        $resultbuff['is_am'] = $row['is_am'];
        $resultbuff['is_thai_n'] = $row['is_thai_n'];
        $resultbuff['is_eng_n'] = $row['is_eng_n'];
        $resultbuff['is_amp_n'] = $row['is_amp_n'];
        $resultbuff['is_open_dte'] = $row['is_open_dte'];
        $resultbuff['addr'] = $row['addr'];
        $resultbuff['moo'] = $row['moo'];
        $resultbuff['soi'] = $row['soi'];
        $resultbuff['street'] = $row['street'];
        $resultbuff['subdist'] = $row['subdist'];
        $resultbuff['dist'] = $row['dist'];
        $resultbuff['prov'] = $row['prov'];
        $resultbuff['zip_code'] = $row['zip_code'];
        $resultbuff['cat_cc'] = $row['CAT_CC'];
        $resultbuff['cat_aa'] = $row['CAT_AA'];
        $resultbuff['cat_tt'] = $row['CAT_TT'];
        $resultbuff['prov_code'] = $row['DIVISION_CODE'];
        $resultbuff['telephone'] = array();
        $queryTel = "SELECT number FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' AND lcn_cd = '02' AND phone_fax_cd = '1' ";
        $query = $this->mssql->query($queryTel)->result_array();
        $tempTel = array();
        foreach ($query as $row) {
          $tempTel['number'] = $row['number'];

          array_push($resultbuff['telephone'], $tempTel);
        }
        $resultbuff['fax'] = "";
        $queryFax = "SELECT number FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' AND lcn_cd = '02' AND phone_fax_cd = '2' ";
        $query = $this->mssql->query($queryFax)->result_array();
        $tempTel = array();
        foreach ($query as $row) {
          $resultbuff['fax'] = $row['number'];
        }
        $resultbuff['wan'] = "";
        $queryFax = "SELECT number FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' AND lcn_cd = '15' AND phone_fax_cd = '1' ";
        $query = $this->mssql->query($queryFax)->result_array();
        $tempTel = array();
        foreach ($query as $row) {
          $resultbuff['wan'] = $row['number'];
        }
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getListBranchName ($id) {
    $br = substr($id, 0, -1);
    $queryStr = "SELECT is_br + is_sbr + is_ch + is_am AS id,
                is_br + '-' + is_sbr + '-' +is_ch + '-' +is_am AS branch, 'สาขา' + is_thai_n AS name
                  FROM od_br_name WHERE is_br = '$br' AND brnch_type = '2' AND is_status = '1' ORDER BY is_br, is_sbr, is_ch, is_am";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['id'];
        $resultbuff['branch'] = $row['branch'];
        $resultbuff['name'] = $row['name'];
        $resultbuff['display_name'] = $row['name'].' : '.$row['branch'];
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function updateBranchInformation($odInfo, $odAddrBR, $telInfo, $ctrInfo) {
    $sessionName = $this->session->userdata('od_emp_code');
    $todayTime = date("Y-m-d");
    $id =  $odInfo['is_br'].$odInfo['is_sbr'].$odInfo['is_ch'].$odInfo['is_am'];
    $brcode = $odInfo['is_br'].$odInfo['is_sbr'];
    $is_thai_n = $odInfo['is_thai_n'];
    $is_eng_n = $odInfo['is_eng_n'];
    $is_amp_n = $odInfo['is_amp_n'];
    $is_open_dte = str_replace('-', '', $odInfo['is_open_dte']);

    $updateBR = $this->mssql->query("UPDATE od_br_name
      SET
      is_thai_n = '$is_thai_n',
      is_eng_n = '$is_eng_n',
      is_amp_n = '$is_amp_n',
      is_open_dte = '$is_open_dte',
      updated_by = '$sessionName',
      last_update = '$todayTime'
      WHERE is_br + is_sbr + is_ch + is_am = '$id'");

      $addr = $odAddrBR['addr'];
      $moo = $odAddrBR['moo'];
      $soi = $odAddrBR['soi'];
      $street = $odAddrBR['street'];
      $subdist = str_replace(' ', '', $odAddrBR['subdist_nme']);
      $dist = str_replace('-', ' ', $odAddrBR['dist_nme']);
      $prov = str_replace(' ', '', $odAddrBR['prov_nme']);
      $zip_code = $odAddrBR['zipcode'];


    $updateAddr = $this->mssql->query("UPDATE od_org_addr
      SET
      addr = '$addr',
      moo = '$moo',
      soi = '$soi',
      street = '$street',
      subdist = '$subdist',
      dist = '$dist',
      prov = '$prov',
      zip_code = '$zip_code',
      updated_by = '$sessionName',
      last_update = '$todayTime'
      WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id'");

      $ind_brnch_cd = $odInfo['is_br'];
      $dept_cd = $odInfo['is_sbr'];
      $div_cd = $odInfo['is_ch'];
      $subdiv_cd = $odInfo['is_am'];
      $removePhone = $this->mssql->query("DELETE FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' ");
      $n = 0;
      foreach ($telInfo as $value) {
        $number = str_replace('-', '', $value['number']);
        if ($n == 0) {
          $addPhone = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
                                    phone_fax_cd, lcn_cd, updated_by, last_update)
                                    VALUES ( '$ind_brnch_cd', '$dept_cd', '$div_cd', '$subdiv_cd', '$number',
                                      '1', '02', '$sessionName', '$todayTime' ) ");
        }
        elseif ($n > 0 && $number != '') {
          $addPhone = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
                                    phone_fax_cd, lcn_cd, updated_by, last_update)
                                    VALUES ( '$ind_brnch_cd', '$dept_cd', '$div_cd', '$subdiv_cd', '$number',
                                      '1', '02', '$sessionName', '$todayTime' ) ");
        }
        $n = $n + 1;
      }
      $fax = str_replace('-', '',  $ctrInfo['fax']);
      $wan = $ctrInfo['wan'];
      $addFax = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
                                phone_fax_cd, lcn_cd, updated_by, last_update)
                                VALUES (
                                  '$ind_brnch_cd', '$dept_cd', '$div_cd', '$subdiv_cd', '$fax',
                                  '2', '02', '$sessionName', '$todayTime' ) ");

      $addWan = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
                                phone_fax_cd, lcn_cd, updated_by, last_update)
                                VALUES (
                                  '$ind_brnch_cd', '$dept_cd', '$div_cd', '$subdiv_cd', '$wan',
                                  '1', '15', '$sessionName', '$todayTime' ) ");
      $is_thai = 'สาขา'.$is_thai_n;
      $mapProfile = $this->mssql->query("UPDATE tb_brmapprofile SET brname = '$is_thai'  WHERE brcode = '$brcode'");

      $division_code = $odInfo['prov_code'];
      $cost_name = 'ประจำ'.$is_thai;
      $division = $this->mssql->query("UPDATE Division SET DIVISION_NAME = '$is_thai' , DIVISION_AS = '$is_thai',
                      COST_NAME = '$cost_name' WHERE DIVISION_CODE = '$division_code'");

      $result = "";
      if($updateBR && $updateAddr && $addPhone && $addFax && $addWan && $mapProfile && $division){
        $result = "Successfully";
      }else{
        $result = "Failed";
      }

      $this->mssql->close();
      return $result;
  }

  public function getAmphurUnderControl ($id) {
    $queryStr = "SELECT ORGNME.is_div, ORGNME.is_br, ORGNME.is_sbr, ORGNME.is_ch, ORGNME.is_am
                , ORGNME.is_thai_n, ORGNME.is_eng_n, ORGNME.is_amp_n, ORGNME.is_open_dte
                , ORGADDR.addr, ORGADDR.moo, ORGADDR.soi, ORGADDR.street, ORGADDR.subdist, ORGADDR.dist, ORGADDR.prov, ORGADDR.zip_code
                , ORGNMETB.is_ch AS TB_is_ch, ORGNMETB.is_am AS TB_is_am, ORGNMETB.is_thai_n AS TB_is_thai_n, ORGNMETB.is_eng_n AS TB_is_eng_n
                FROM od_br_name  AS ORGNME
                LEFT JOIN od_org_addr AS ORGADDR ON ORGNME.is_br = ORGADDR.ind_brnch_cd AND ORGNME.is_sbr = ORGADDR.dept_cd
                JOIN od_link_br_tumbon LNK ON ORGNME.is_br = LNK.ind_brnch_cd AND ORGNME.is_sbr = LNK.dept_cd AND ORGNME.is_ch = LNK.div_cd AND ORGNME.is_am = LNK.subdiv_cd
                JOIN od_br_name_tumbon AS ORGNMETB ON ORGNME.is_br = ORGNMETB.is_br AND ORGNME.is_sbr = ORGNMETB.is_sbr AND LNK.fldoff_dist_cd = (ORGNMETB.is_ch + ORGNMETB.is_am)
                AND ORGNME.is_ch = ORGADDR.div_cd WHERE ORGNME.is_br + ORGNME.is_sbr  = '$id'";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['is_div'] = $row['is_div'];
        $resultbuff['is_br'] = $row['is_br'];
        $resultbuff['is_sbr'] = $row['is_sbr'];
        $resultbuff['is_ch'] = $row['is_ch'];
        $resultbuff['is_am'] = $row['is_am'];
        $resultbuff['is_thai_n'] = $row['is_thai_n'];
        $resultbuff['is_eng_n'] = $row['is_eng_n'];
        $resultbuff['is_amp_n'] = $row['is_amp_n'];
        $resultbuff['is_open_dte'] = $row['is_open_dte'];
        $resultbuff['addr'] = $row['addr'];
        $resultbuff['moo'] = $row['moo'];
        $resultbuff['soi'] = $row['soi'];
        $resultbuff['street'] = $row['street'];
        $resultbuff['subdist'] = $row['subdist'];
        $resultbuff['dist'] = $row['dist'];
        $resultbuff['prov'] = $row['prov'];
        $resultbuff['zip_code'] = $row['zip_code'];
        $resultbuff['TB_is_ch'] = $row['TB_is_ch'];
        $resultbuff['TB_is_am'] = $row['TB_is_am'];
        $resultbuff['TB_is_thai_n'] = $row['TB_is_thai_n'];
        $resultbuff['TB_is_eng_n'] = $row['TB_is_eng_n'];
        $is_br = $row['is_br'];
        $is_sbr = $row['is_sbr'];
        $is_ch = $row['TB_is_ch'];
        $is_am = $row['TB_is_am'];

        $resultbuff['telephone'] = array();
        $queryTel = "SELECT number FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' AND lcn_cd = '02' AND phone_fax_cd = '1' ";
        $query = $this->mssql->query($queryTel)->result_array();
        $tempTel = array();
        foreach ($query as $row) {
          $tempTel['number'] = $row['number'];

          array_push($resultbuff['telephone'], $tempTel);
        }
        $resultbuff['fax'] = "";
        $queryFax = "SELECT number FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' AND lcn_cd = '02' AND phone_fax_cd = '2' ";
        $query = $this->mssql->query($queryFax)->result_array();
        $tempTel = array();
        foreach ($query as $row) {
          $resultbuff['fax'] = $row['number'];
        }
        $resultbuff['wan'] = "";
        $queryFax = "SELECT number FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' AND lcn_cd = '15' AND phone_fax_cd = '1' ";
        $query = $this->mssql->query($queryFax)->result_array();
        $tempTel = array();
        foreach ($query as $row) {
          $resultbuff['wan'] = $row['number'];
        }

        $resultbuff['unit_list'] = array();
        if ($row['TB_is_ch'] != null) {
          $info = $row['TB_is_ch'].$row['TB_is_am'];
          $unitbuff = array();
          $queryUnit = $this->mssql->query(" SELECT cat_tt, cat_mm, cat_desc  FROM bmc_tumbon WHERE is_ch + is_am = '$info' ")->result_array();
          $i = 0;
          foreach ($queryUnit as $data) {
            if($data['cat_tt'] == '00' && $data['cat_mm'] == '00')
              $unitbuff['tumbon'] = 'อ.'.$data['cat_desc'];
            else
              $unitbuff['tumbon'] = ' - ต.'.$data['cat_desc'];
            array_push($resultbuff['unit_list'], $unitbuff);
            $i = $i + 1;
          }
        }

        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function closeBranch($id) {
    $sessionName = $this->session->userdata('od_emp_code');
    $todayTime = date("Y-m-d");

    $updateBR = $this->mssql->query("UPDATE od_br_name
      SET
      is_status = '9',
      updated_by = '$sessionName',
      last_update = '$todayTime'
      WHERE is_br + is_sbr = '$id'");


      $result = "";
      if($updateBR){
        $result = "Successfully";
      }else{
        $result = "Failed";
      }

      $this->mssql->close();
      return $result;
  }

  public function changeProvince ($odInfo, $provinceInfo) {
    $sessionName = $this->session->userdata('od_emp_code');
    $todayTime = date("Y-m-d");
    $new_is_div = $provinceInfo['divid'];
    $new_is_br = $provinceInfo['is_br'];
    $new_is_amp_n = $provinceInfo['is_amp_n'];
    $oldBranch_is_br = $odInfo['is_br'];
    $oldBranch_is_sbr = $odInfo['is_sbr'];
    $oldBranch_is_ch = $odInfo['is_ch'];
    $oldBranch_is_am = $odInfo['is_am'];

    $queryStr = "SELECT item_no FROM od_br_name WHERE is_br = '$oldBranch_is_br' AND is_sbr = '$oldBranch_is_sbr' AND  is_ch = '$oldBranch_is_ch' AND is_am = '$oldBranch_is_am' ";
    $query = $this->mssql->query($queryStr)->result_array();
    $item_no = $query[0]['item_no'];

    $update_od_br_name = $this->mssql->query("UPDATE od_br_name
      SET
      is_div = '$new_is_div',
      is_br = '$new_is_br',
      is_sbr =
      (
      		SELECT (CASE WHEN b.is_sbr IS NULL THEN '0' ELSE sbn.alphabet END)
          FROM
          (SELECT is_br, is_sbr FROM od_br_name WHERE is_br = '$new_is_br' AND is_sbr = 'A' ) a
           LEFT JOIN (SELECT TOP 1 is_br, is_sbr FROM od_br_name WHERE is_br = '$new_is_br' AND is_sbr != 'A' ORDER BY is_sbr DESC) b  ON a.is_br = b.is_br
           LEFT JOIN SORTING_BRANCH sb ON b.is_sbr = sb.alphabet
           LEFT JOIN SORTING_BRANCH sbn ON (sb.item_no + 1) = sbn.item_no
      ),
      updated_by = '$sessionName',
      last_update = '$todayTime'
      WHERE is_br  = '$oldBranch_is_br' AND is_sbr = '$oldBranch_is_sbr' ");


    $queryStr = "SELECT is_br, is_sbr FROM od_br_name WHERE item_no = '$item_no' ";
    $query = $this->mssql->query($queryStr)->result_array();
    $is_br_insert = $query[0]['is_br'];
    $is_sbr_insert = $query[0]['is_sbr'];

    $update_od_br_name = $this->mssql->query("UPDATE od_br_name
      SET
      is_amp_n = '$new_is_amp_n',
      updated_by = '$sessionName',
      last_update = '$todayTime'
      WHERE is_br  = '$is_br_insert' AND is_sbr = '$is_sbr_insert' AND is_ch = '00' AND is_am = '00' ");

    $update_od_br_name_tumbon = $this->mssql->query("UPDATE od_br_name_tumbon
      SET
      is_div = '$new_is_div',
      is_br = '$is_br_insert',
      is_sbr = '$is_sbr_insert',
      updated_by = '$sessionName',
      last_update = '$todayTime'
      WHERE is_br  = '$oldBranch_is_br' AND is_sbr = '$oldBranch_is_sbr' ");

    $update_od_org_addr = $this->mssql->query("UPDATE od_org_addr
      SET
      ind_brnch_cd = '$is_br_insert',
      dept_cd = '$is_sbr_insert',
      updated_by = '$sessionName',
      last_update = '$todayTime'
      WHERE ind_brnch_cd  = '$oldBranch_is_br' AND dept_cd = '$oldBranch_is_sbr' ");

    $update_od_phone_fax = $this->mssql->query("UPDATE od_phone_fax
      SET
      ind_brnch_cd = '$is_br_insert',
      dept_cd = '$is_sbr_insert',
      updated_by = '$sessionName',
      last_update = '$todayTime'
      WHERE ind_brnch_cd  = '$oldBranch_is_br' AND dept_cd = '$oldBranch_is_sbr' ");

    $update_bmc_tumbon = $this->mssql->query("UPDATE bmc_tumbon
      SET
      is_br = '$is_br_insert',
      is_sbr = '$is_sbr_insert'
      WHERE is_br  = '$oldBranch_is_br' AND is_sbr = '$oldBranch_is_sbr' ");

    $update_od_link_br_tumbon = $this->mssql->query("UPDATE od_link_br_tumbon
      SET
      ind_brnch_cd = '$is_br_insert',
      dept_cd = '$is_sbr_insert',
      updated_by = '$sessionName',
      last_update = '$todayTime'
      WHERE ind_brnch_cd  = '$oldBranch_is_br' AND dept_cd = '$oldBranch_is_sbr' ");

    $insert_brcode = $is_br_insert.$is_sbr_insert;
    $check_brcode =  $oldBranch_is_br.$oldBranch_is_sbr;
    $divprofile = '';
    if($new_is_div == '10')
      $divprofile = '80018';
    else {
      $divprofile = '800'.$new_is_div;
    }
    $update_tb_brmapprofile = $this->mssql->query("UPDATE tb_brmapprofile
      SET
      brcode = '$insert_brcode',
      div = '$new_is_div',
      divprofile = '$divprofile'
      WHERE brcode = '$check_brcode' ");

    $division_code = $provinceInfo['id'];
    $is_thai = 'สนจ.'.$provinceInfo['is_thai_n'];
    $br_division_code = '48'.$is_br_insert.$is_sbr_insert.'0000';
    $div_is_thai = 'สำนักงาน ธ.ก.ส.จังหวัด'.$provinceInfo['is_thai_n'];
    $check_division = '48'.$check_brcode.'0000';
    $update_division = $this->mssql->query("UPDATE Division
      SET
      DIVISION_CODE = '$br_division_code',
      DIVISION_MN_CODE = '$division_code',
      DIVISION_MN_NAME = '$div_is_thai',
      DIVISION_MN_AS = '$is_thai',
      DIVISION_AREA = '$new_is_div'
      WHERE DIVISION_CODE = '$check_division' ");

    if($update_od_br_name && $update_od_br_name_tumbon && $update_od_org_addr && $update_od_phone_fax && $update_bmc_tumbon && $update_od_link_br_tumbon && $update_tb_brmapprofile && $update_division){
      $result = "Successfully";
    }else{
      $result = "Failed";
    }

    $this->mssql->close();
    return $result;
  }



}
?>
