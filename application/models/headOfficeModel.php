<?php
class headOfficeModel extends CI_Model
{
  var $mssql;
  public function __construct()
  {
    parent::__construct();
    //use DT database connection
    $this->mssql = $this->load->database ( 'DT', TRUE );
  }

  public function loadTypeHeadOffice ()
  {
    $queryStr = "SELECT headOffice_type, type_des FROM od_headoffice_type";

    $query = $this->mssql->query($queryStr)->result_array();
    $result = array();
    $resultbuff = array();
    foreach ($query as $row) {
      $resultbuff['id'] = $row['headOffice_type'];
      $resultbuff['head_des'] = $row['type_des'];
      array_push($result, $resultbuff);
    }

    $this->mssql->close();
    return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function addNewDivision ($odInfo)
  {

    $sessionName        = $this->session->userdata('od_emp_code');
    $todayTime          = date("Y-m-d");
    $brnch_type         = $odInfo['type'];
    $name_title         = $odInfo['type_des'];
    $div_th_name        = $name_title.$odInfo['th_org_nme'];
    $div_en_name        = $odInfo['eng_org_nme'];
    $div_shrt_name      = $odInfo['shrt_org_nme'];
    $div_start_oper_dte = str_replace('-', '', $odInfo['start_oper_dte']);
    $div_shrt_mn_name   = $odInfo['shrt_mn_name'];
    $underManager       = $odInfo['underManager'];
    // return $underManager;
    // insert new branch
    $addNewHeadOffice = $this->mssql->query("INSERT INTO od_headoffice_name (is_code, is_div, is_br, is_sbr, is_ch, is_am,
        is_thai_n, is_eng_n, is_amp_n, is_grp, is_reg, is_open_dte, is_region, is_poor, is_zone, is_status,
        is_business, is_exp_dte, updated_by, last_update, is_div_group, org_cd, brnch_type)
      SELECT '0', '0', A.lindNew+A.rindNew , A.deptNew, '00', '00',
       '$div_th_name', '$div_en_name', '$div_shrt_name', '000', '00000', '$div_start_oper_dte', '0', '0', '00', '1',
       '', '00000000',  '$sessionName', '$todayTime', '0', A.org_cd, '$brnch_type'
      FROM (
        SELECT TOP 1
        (CASE WHEN (DEPT.itemOld = DEPT.maxItem) AND (RIND.itemOld = RIND.maxItem)  THEN LIND.charNew ELSE  LIND.charOld END) AS lindNew,
        (CASE WHEN (DEPT.itemOld = DEPT.maxItem) AND (RIND.itemOld = RIND.maxItem) THEN RIND.minChar
        WHEN (DEPT.itemOld = DEPT.maxItem) AND (RIND.itemOld < RIND.maxItem) THEN RIND.charNew
        ELSE RIND.charOld END) AS rindNew,
        (CASE WHEN DEPT.itemOld = DEPT.maxItem THEN DEPT.minChar ELSE DEPT.charNew END) AS deptNew
        , (CASE WHEN '$brnch_type' = '4' THEN NULL ELSE (SELECT MAX(org_cd)+1 FROM od_headoffice_name ) END) AS org_cd
        FROM od_headoffice_name od
        LEFT JOIN (
          SELECT s.item_no AS itemOld, s.alphabet AS charOld, sn.item_no AS itemNew, sn.alphabet AS charNew,
          (SELECT MAX(item_no) FROM SORTING_BRANCH) AS maxItem, (SELECT TOP 1 alphabet FROM SORTING_BRANCH ORDER BY item_no ASC) AS minChar
          FROM  SORTING_BRANCH s
          LEFT JOIN SORTING_BRANCH sn ON (s.item_no + 1) = sn.item_no
        ) AS DEPT ON od.is_sbr = DEPT.charOld
        LEFT JOIN (
          SELECT s.item_no AS itemOld, s.alphabet AS charOld, sn.item_no AS itemNew, sn.alphabet AS charNew,
          (SELECT MAX(item_no) FROM SORTING_HEADOFFICE) AS maxItem, (SELECT TOP 1 alphabet FROM SORTING_HEADOFFICE ORDER BY item_no ASC) AS minChar
          FROM  SORTING_HEADOFFICE s
          LEFT JOIN SORTING_HEADOFFICE sn ON (s.item_no + 1) = sn.item_no
        ) AS RIND ON RIGHT(od.is_br, 1) = RIND.charOld
        LEFT JOIN (
          SELECT s.item_no AS itemOld, s.alphabet AS charOld, sn.item_no AS itemNew, sn.alphabet AS charNew
          FROM  SORTING_BRANCH s
          LEFT JOIN SORTING_BRANCH sn ON (s.item_no + 1) = sn.item_no
        ) AS LIND ON LEFT(od.is_br,1) = LIND.charOld
        ORDER BY od.is_br DESC, od.is_sbr DESC ) A ");

    $insert_id = $this->mssql->insert_id();
    $querySBR  = "SELECT is_br, is_sbr, is_ch, is_am FROM od_headoffice_name  WHERE item_no = $insert_id ";
    $query     = $this->mssql->query($querySBR)->result_array();
    $ind_brnch_cd = $query[0]['is_br'];
    $dept_cd      = $query[0]['is_sbr'];
    $div_cd       = '00';
    $subdiv_cd    = '00';

    $div_code       = '48'.$ind_brnch_cd.$dept_cd.$div_cd.$subdiv_cd;
    $div_mn_code    = '';
    $div_mn_name    = '';
    if ($underManager == 'true') {
      $div_mn_code  = '4800A0000';
      $div_mn_name  = $odInfo['mn_name'];
    }
    else {
      $div_mn_code  = $div_code;
      $div_mn_name  = $name_title.$odInfo['mn_name'];
    }


    $addNewDivision = $this->mssql->query("INSERT INTO Division (DIVISION_CODE, DIVISION_NAME, DIVISION_AS, DIVISION_MN_CODE,
      DIVISION_MN_NAME, DIVISION_MN_AS, COST_CODE, COST_NAME, DIVISION_AREA )
      VALUES ('$div_code', '$div_th_name', '$div_shrt_name', '$div_mn_code',
              '$div_mn_name', '$div_shrt_mn_name', '', '', '0') ");

    $regionName = 'ภาค'.$odInfo['th_org_nme'];
    if ($brnch_type == '1') {
      $addNewRegion = $this->mssql->query("INSERT INTO od_brnch_act_dept (brnch_act_dept_cd, brnch_act_dept_nme, brnch_act_dept_dtl,
              updated_by, last_update, brnch_code, brnch_sht)
              SELECT TOP 1 CONVERT(int, brnch_act_dept_cd)+1, '$div_th_name', '$regionName',
              '$sessionName', '$todayTime', '$div_code', '$div_shrt_mn_name'
              FROM od_brnch_act_dept ORDER BY brnch_act_dept_cd DESC ");
    }


      if($addNewHeadOffice && $addNewDivision){
        $result = "Successfully";
      }else{
        $result = "Failed";
      }

      $this->mssql->close();
      return $result;

  }

  public function loadListDivision ()
  {
    $queryStr = "SELECT DIVISION_CODE, is_thai_n, is_br+is_sbr+is_ch+is_am AS id
      FROM od_headoffice_name
      LEFT JOIN Division ON DIVISION_CODE = '48'+is_br+is_sbr+is_ch+is_am
      WHERE is_status = 1 ORDER BY DIVISION_CODE ";

    $query = $this->mssql->query($queryStr)->result_array();
    $result = array();
    $resultbuff = array();
    foreach ($query as $row) {
      $resultbuff['id'] = $row['id'];
      $resultbuff['code'] = $row['DIVISION_CODE'];
      $resultbuff['div_name'] = $row['is_thai_n'];
      array_push($result, $resultbuff);
    }

    $this->mssql->close();
    return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function loadDivisionInformation ($id)
  {
    $queryStr = "SELECT is_br + '-' + is_sbr + '-' + is_ch + '-' + is_am AS id, is_thai_n, is_eng_n, is_amp_n, is_open_dte,
      DIVISION_MN_NAME, DIVISION_MN_AS, DIVISION_CODE, brnch_type, type_des
      FROM od_headoffice_name LEFT JOIN Division ON DIVISION_CODE = '48'+is_br+is_sbr+is_ch+is_am
      LEFT JOIN od_headoffice_type ON brnch_type = headOffice_type
      WHERE is_br+is_sbr+is_ch+is_am = '$id'ORDER BY is_br, is_sbr, is_ch, is_am ";

    $query = $this->mssql->query($queryStr)->result_array();
    $result = array();
    $resultbuff = array();
    foreach ($query as $row) {
      $resultbuff['id'] = $row['id'];
      $resultbuff['code'] = $row['DIVISION_CODE'];
      $resultbuff['th_org_nme'] = $row['is_thai_n'];
      $resultbuff['eng_org_nme'] = $row['is_eng_n'];
      $resultbuff['shrt_org_nme'] = $row['is_amp_n'];
      $resultbuff['start_oper_dte'] = $row['is_open_dte'];
      $resultbuff['mn_name'] = $row['DIVISION_MN_NAME'];
      $resultbuff['shrt_mn_name'] = $row['DIVISION_MN_AS'];
      $resultbuff['type'] = $row['brnch_type'];
      $resultbuff['type_des'] = $row['type_des'];
      array_push($result, $resultbuff);
    }

    $this->mssql->close();
    return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function editDivision ($headOfficeID ,$odInfo)
  {

    $sessionName        = $this->session->userdata('od_emp_code');
    $todayTime          = date("Y-m-d");
    $brnch_type         = $odInfo['type'];
    $name_title         = $odInfo['type_des'];
    $div_th_name        = $name_title.$odInfo['th_org_nme'];
    $div_en_name        = $odInfo['eng_org_nme'];
    $div_shrt_name      = $odInfo['shrt_org_nme'];
    $div_start_oper_dte = str_replace('-', '', $odInfo['start_oper_dte']);
    $div_shrt_mn_name   = $odInfo['shrt_mn_name'];
    $underManager       = $odInfo['underManager'];
    $div_code           = $odInfo['code'];
    // insert new branch

    $updateHeadOffice = $this->mssql->query("UPDATE od_headoffice_name SET is_thai_n = '$div_th_name', is_eng_n = '$div_en_name',
       is_amp_n = '$div_shrt_name', is_open_dte = '$div_start_oper_dte', updated_by = '$sessionName', last_update = '$todayTime',
       brnch_type = '$brnch_type'
       WHERE is_br+is_sbr+is_ch+is_am = '$headOfficeID' ");

    $div_mn_code    = '';
    $div_mn_name    = '';
    if ($underManager == 'true') {
      $div_mn_code  = '4800A0000';
      $div_mn_name  = $odInfo['mn_name'];
    }
    else {
      $div_mn_code  = $div_code;
      $div_mn_name  = $name_title.$odInfo['mn_name'];
    }


    $updateDivision = $this->mssql->query("UPDATE Division SET DIVISION_NAME = '$div_th_name', DIVISION_AS = '$div_shrt_name',
      DIVISION_MN_NAME = '$div_mn_name', DIVISION_MN_AS = '$div_shrt_mn_name', COST_NAME = '' WHERE DIVISION_CODE = '$div_code' ");

    $regionName = 'ภาค'.$odInfo['th_org_nme'];
    $updateRegion = $this->mssql->query("UPDATE od_brnch_act_dept SET brnch_act_dept_nme = '$div_th_name', brnch_act_dept_dtl = '$regionName',
      brnch_sht = '$div_shrt_name' WHERE brnch_code = '$div_code' ");


    if($updateHeadOffice && $updateDivision){
      $result = "Successfully";
    }else{
      $result = "Failed";
    }

    $this->mssql->close();
    return $result;

  }

  public function closeHeadOffice ($headOfficeID)
  {
    $updateHeadOffice = $this->mssql->query("UPDATE od_headoffice_name SET is_status = '9' WHERE is_br+is_sbr+is_ch+is_am = '$headOfficeID' ");


      if($updateHeadOffice){
        $result = "Successfully";
      }else{
        $result = "Failed";
      }

      $this->mssql->close();
      return $result;
  }

  public function loadAllDivisionInformation ()
  {
    $queryStr = "SELECT is_br + '-' + is_sbr + '-' + is_ch + '-' + is_am AS id, org_cd,
      is_thai_n, is_eng_n, is_amp_n, is_open_dte,
      DIVISION_MN_NAME, DIVISION_MN_AS, DIVISION_CODE, brnch_type, type_des
      FROM od_headoffice_name LEFT JOIN Division ON DIVISION_CODE = '48'+is_br+is_sbr+is_ch+is_am
      LEFT JOIN od_headoffice_type ON brnch_type = headOffice_type
      WHERE is_status = '1' ORDER BY is_br, is_sbr, is_ch, is_am ";

    $query = $this->mssql->query($queryStr)->result_array();
    $result = array();
    $resultbuff = array();
    foreach ($query as $row) {
      $resultbuff['id'] = $row['id'];
      $resultbuff['org_cd'] = $row['org_cd'];
      $resultbuff['code'] = $row['DIVISION_CODE'];
      $resultbuff['th_org_nme'] = $row['is_thai_n'];
      $resultbuff['eng_org_nme'] = $row['is_eng_n'];
      $resultbuff['shrt_org_nme'] = $row['is_amp_n'];
      $resultbuff['start_oper_dte'] = $row['is_open_dte'];
      $resultbuff['mn_name'] = $row['DIVISION_MN_NAME'];
      $resultbuff['shrt_mn_name'] = $row['DIVISION_MN_AS'];
      $resultbuff['type'] = $row['brnch_type'];
      $resultbuff['type_des'] = $row['type_des'];
      array_push($result, $resultbuff);
    }

    $this->mssql->close();
    return json_encode($result,JSON_UNESCAPED_UNICODE);
  }



}
?>
