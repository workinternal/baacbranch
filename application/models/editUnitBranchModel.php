<?php
class editUnitBranchModel extends CI_Model
{
  var $mssql;
  public function __construct()
  {
    parent::__construct();
    //use DT database connection
    $this->mssql = $this->load->database ( 'DT', TRUE );
  }



  public function getListProvince () {
    $queryStr = "SELECT PROVINCE, CAT_CC FROM ADDRESS_CODE GROUP BY PROVINCE, CAT_CC ORDER BY PROVINCE";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['CAT_CC'];
        $resultbuff['code'] = $row['CAT_CC'];
        $resultbuff['name'] = $row['PROVINCE'];
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getListAmphur ($provinceID) {
    $queryStr = "SELECT REPLACE(DISTRICT, 'อำเภอ', '') DISTRICT, CAT_AA FROM ADDRESS_CODE WHERE CAT_CC = '$provinceID' GROUP BY DISTRICT, CAT_AA ORDER BY CAT_AA";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['CAT_AA'];
        $resultbuff['code'] = $row['CAT_AA'];
        $resultbuff['name'] = $row['DISTRICT'];

        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getListDistrict ($provinceID, $amphurID) {
    $queryStr = "SELECT SUB_DISTRICT, CAT_TT, POSTCODE FROM ADDRESS_CODE WHERE CAT_CC = '$provinceID' AND CAT_AA = '$amphurID' GROUP BY SUB_DISTRICT, CAT_TT, POSTCODE  ORDER BY CAT_TT";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['CAT_TT'];
        $resultbuff['code'] = $row['CAT_TT'];
        $resultbuff['name'] = $row['SUB_DISTRICT'];
        $resultbuff['postcode'] = $row['POSTCODE'];
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getUnitBranchInformation ($id) {
    $queryStr = "SELECT DIVNME.brnch_act_dept_nme AS div_nme, BRNME.brname, ORGNME.is_div, ORGNME.is_br, ORGNME.is_sbr, ORGNME.is_ch, ORGNME.is_am
                , ORGNME.is_thai_n, ORGNME.is_eng_n, ORGNME.is_amp_n, ORGNME.is_open_dte
                , ORGADDR.addr, ORGADDR.moo, ORGADDR.soi, ORGADDR.street, ORGADDR.subdist, ORGADDR.dist, ORGADDR.prov, ORGADDR.zip_code
                , ADDR.CAT_CC, ADDR.CAT_AA, ADDR.CAT_TT
                FROM od_br_name  AS ORGNME
                LEFT JOIN od_org_addr AS ORGADDR ON ORGNME.is_br = ORGADDR.ind_brnch_cd AND ORGNME.is_sbr = ORGADDR.dept_cd
                JOIN od_brnch_act_dept AS DIVNME ON CONVERT(INT, ORGNME.is_div) = CONVERT(INT, DIVNME.brnch_act_dept_cd)
                JOIN  tb_brmapprofile AS BRNME ON (ORGNME.is_br + 'A') = brcode AND ORGNME.is_ch = ORGADDR.div_cd
                JOIN ADDRESS_CODE AS ADDR ON ORGADDR.prov = ADDR.PROVINCE AND ORGADDR.dist = ADDR.DISTRICT AND ORGADDR.subdist = ADDR.SUB_DISTRICT
                WHERE ORGNME.is_br + ORGNME.is_sbr + ORGNME.is_ch + ORGNME.is_am = '$id'";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['div_nme'] = $row['div_nme'];
        $resultbuff['brname'] = $row['brname'];
        $resultbuff['is_div'] = str_replace(' ','',$row['is_div']);
        $resultbuff['is_br'] = $row['is_br'];
        $resultbuff['is_sbr'] = $row['is_sbr'];
        $resultbuff['is_ch'] = $row['is_ch'];
        $resultbuff['is_am'] = $row['is_am'];
        $resultbuff['is_thai_n'] = $row['is_thai_n'];
        $resultbuff['is_eng_n'] = $row['is_eng_n'];
        $resultbuff['is_amp_n'] = $row['is_amp_n'];
        $resultbuff['is_open_dte'] = $row['is_open_dte'];
        $resultbuff['addr'] = $row['addr'];
        $resultbuff['moo'] = $row['moo'];
        $resultbuff['soi'] = $row['soi'];
        $resultbuff['street'] = $row['street'];
        $resultbuff['subdist'] = $row['subdist'];
        $resultbuff['dist'] = $row['dist'];
        $resultbuff['prov'] = $row['prov'];
        $resultbuff['zip_code'] = $row['zip_code'];
        $resultbuff['cat_cc'] = $row['CAT_CC'];
        $resultbuff['cat_aa'] = $row['CAT_AA'];
        $resultbuff['cat_tt'] = $row['CAT_TT'];

        $resultbuff['telephone'] = array();
        $queryTel = "SELECT number FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' AND lcn_cd = '02' AND phone_fax_cd = '1' ";
        $query = $this->mssql->query($queryTel)->result_array();
        $tempTel = array();
        foreach ($query as $row) {
          $tempTel['number'] = $row['number'];

          array_push($resultbuff['telephone'], $tempTel);
        }
        $resultbuff['fax'] = "";
        $queryFax = "SELECT number FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' AND lcn_cd = '02' AND phone_fax_cd = '2' ";
        $query = $this->mssql->query($queryFax)->result_array();
        $tempTel = array();
        foreach ($query as $row) {
          $resultbuff['fax'] = $row['number'];
        }
        $resultbuff['wan'] = "";
        $queryFax = "SELECT number FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' AND lcn_cd = '15' AND phone_fax_cd = '1' ";
        $query = $this->mssql->query($queryFax)->result_array();
        $tempTel = array();
        foreach ($query as $row) {
          $resultbuff['wan'] = $row['number'];
        }

        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getListBranchName ($id) {
    $br = substr($id, 0, -1);
    $queryStr = "SELECT is_br + is_sbr AS id,
                is_br + '-' + is_sbr + '-' +is_ch + '-' +is_am AS branch, 'สาขา' + is_thai_n AS name
                  FROM od_br_name WHERE is_br = '$br' AND brnch_type = '2' AND is_status = '1' ORDER BY is_br, is_sbr, is_ch, is_am";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['id'];
        $resultbuff['branch'] = $row['branch'];
        $resultbuff['name'] = $row['name'];
        $resultbuff['display_name'] = $row['name'].' : '.$row['branch'];
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getListUnitBranchName ($id) {
    $queryStr = "SELECT is_br + is_sbr + is_ch + is_am AS id,
                is_br + '-' + is_sbr + '-' +is_ch + '-' +is_am AS branch, 'สาขาย่อย' + is_thai_n AS name
                  FROM od_br_name WHERE is_br + is_sbr = '$id' AND ( brnch_type = 'a' OR brnch_type = '6') AND is_status = '1'  ORDER BY is_br, is_sbr, is_ch, is_am ";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['id'];
        $resultbuff['unitbranch'] = $row['branch'];
        $resultbuff['name'] = $row['name'];
        $resultbuff['display_name'] = $row['name'].' : '.$row['branch'];
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function updateUnitBranchInformation($odInfo, $odAddrBR, $telInfo, $ctrInfo) {
      $sessionName = $this->session->userdata('od_emp_code');
      $todayTime = date("Y-m-d");
      $id =  $odInfo['is_br'].$odInfo['is_sbr'].$odInfo['is_ch'].$odInfo['is_am'];

      $is_thai_n = $odInfo['is_thai_n'];
      $is_eng_n = $odInfo['is_eng_n'];
      $is_amp_n = "";
      $is_open_dte = str_replace('-', '', $odInfo['is_open_dte']);

      $updateAM = $this->mssql->query("UPDATE od_br_name
        SET
        is_thai_n = '$is_thai_n',
        is_eng_n = '$is_eng_n',
        is_amp_n = '$is_amp_n',
        is_open_dte = '$is_open_dte',
        updated_by = '$sessionName',
        last_update = '$todayTime'
        WHERE is_br + is_sbr + is_ch + is_am = '$id'");

      $addr = $odAddrBR['addr'];
      $moo = $odAddrBR['moo'];
      $soi = $odAddrBR['soi'];
      $street = $odAddrBR['street'];
      $subdist = str_replace(' ', '', $odAddrBR['subdist']);
      $dist = str_replace('-', ' ', $odAddrBR['dist']);
      $prov = str_replace(' ', '', $odAddrBR['prov']);
      $zip_code = $odAddrBR['zipcode'];


      $updateAddr = $this->mssql->query("UPDATE od_org_addr
        SET
        addr = '$addr',
        moo = '$moo',
        soi = '$soi',
        street = '$street',
        subdist = '$subdist',
        dist = '$dist',
        prov = '$prov',
        zip_code = '$zip_code',
        updated_by = '$sessionName',
        last_update = '$todayTime'
        WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id'");

      $ind_brnch_cd = $odInfo['is_br'];
      $dept_cd = $odInfo['is_sbr'];
      $div_cd = $odInfo['is_ch'];
      $subdiv_cd = $odInfo['is_am'];
      $removePhone = $this->mssql->query("DELETE FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' ");
      $n = 0;
      foreach ($telInfo as $value) {
        $number = str_replace('-', '', $value['number']);
        if ($n == 0) {
          $addPhone = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
                                    phone_fax_cd, lcn_cd, updated_by, last_update)
                                    VALUES ( '$ind_brnch_cd', '$dept_cd', '$div_cd', '$subdiv_cd', '$number',
                                      '1', '02', '$sessionName', '$todayTime' ) ");
        }
        elseif ($n > 0 && $number != '') {
          $addPhone = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
                                    phone_fax_cd, lcn_cd, updated_by, last_update)
                                    VALUES ( '$ind_brnch_cd', '$dept_cd', '$div_cd', '$subdiv_cd', '$number',
                                      '1', '02', '$sessionName', '$todayTime' ) ");
        }
        $n = $n + 1;
      }
      $fax = str_replace('-', '',  $ctrInfo['fax']);
      $wan = $ctrInfo['wan'];
      $addFax = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
                                phone_fax_cd, lcn_cd, updated_by, last_update)
                                VALUES (
                                  '$ind_brnch_cd', '$dept_cd', '$div_cd', '$subdiv_cd', '$fax',
                                  '2', '02', '$sessionName', '$todayTime' ) ");

      $addWan = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
                                phone_fax_cd, lcn_cd, updated_by, last_update)
                                VALUES (
                                  '$ind_brnch_cd', '$dept_cd', '$div_cd', '$subdiv_cd', '$wan',
                                  '1', '15', '$sessionName', '$todayTime' ) ");


        $result = "";
        if($updateAM && $updateAddr && $addPhone && $addFax && $addWan){
          $result = "Successfully";
        }else{
          $result = "Failed";
        }

        $this->mssql->close();
        return $result;
    }

  public function getUnitBranchInformationforClose ($id) {
      $queryStr = "SELECT DIV.DIVISION_CODE, DIVNME.brnch_act_dept_nme AS div_nme, BRNME.brname
                  , HEADNME.is_br + ' - ' + HEADNME.is_sbr + ' - ' + HEADNME.is_ch + ' - ' + HEADNME.is_am AS br_code
                  , HEADNME.is_thai_n AS br_th_name, HEADNME.is_eng_n AS br_en_name, HEADNME.is_amp_n AS br_sht_name
                  , ORGNME.is_div, ORGNME.is_br, ORGNME.is_sbr, ORGNME.is_ch, ORGNME.is_am
                  , ORGNME.is_thai_n, ORGNME.is_eng_n, ORGNME.is_amp_n, ORGNME.is_open_dte
                  , ORGADDR.addr, ORGADDR.moo, ORGADDR.soi, ORGADDR.street, ORGADDR.subdist, ORGADDR.dist, ORGADDR.prov, ORGADDR.zip_code
                  FROM od_br_name  AS ORGNME
                  LEFT JOIN od_org_addr AS ORGADDR ON ORGNME.is_br = ORGADDR.ind_brnch_cd AND ORGNME.is_sbr = ORGADDR.dept_cd
                  JOIN od_brnch_act_dept AS DIVNME ON CONVERT(INT, ORGNME.is_div) = CONVERT(INT, DIVNME.brnch_act_dept_cd)
                  JOIN  tb_brmapprofile AS BRNME ON (ORGNME.is_br + 'A') = brcode
                  JOIN Division AS DIV ON ('48' + ORGNME.is_br + ORGNME.is_sbr + '0000') = DIV.DIVISION_CODE
                  JOIN od_br_name AS HEADNME ON ( ORGNME.is_br+ORGNME.is_sbr+'0000' ) = (HEADNME.is_br + HEADNME.is_sbr + HEADNME.is_ch + HEADNME.is_am)
                  AND ORGNME.is_ch = ORGADDR.div_cd WHERE ORGNME.is_br + ORGNME.is_sbr + ORGNME.is_ch + ORGNME.is_am = '$id'";
        $query = $this->mssql->query($queryStr)->result_array();
        $result = array();
        $resultbuff = array();
        foreach ($query as $row) {
            $resultbuff['div_code'] = $row['DIVISION_CODE'];
            $resultbuff['br_code'] = $row['br_code'];
            $resultbuff['br_th_name'] = $row['br_th_name'];
            $resultbuff['br_en_name'] = $row['br_en_name'];
            $resultbuff['br_sht_name'] = $row['br_sht_name'];
            $resultbuff['div_nme'] = $row['div_nme'];
            $resultbuff['brname'] = $row['brname'];
            $resultbuff['is_div'] = $row['is_div'];
            $resultbuff['is_br'] = $row['is_br'];
            $resultbuff['is_sbr'] = $row['is_sbr'];
            $resultbuff['is_ch'] = $row['is_ch'];
            $resultbuff['is_am'] = $row['is_am'];
            $resultbuff['is_thai_n'] = $row['is_thai_n'];
            $resultbuff['is_eng_n'] = $row['is_eng_n'];
            $resultbuff['is_amp_n'] = $row['is_amp_n'];
            $resultbuff['is_open_dte'] = $row['is_open_dte'];
            $resultbuff['addr'] = $row['addr'];
            $resultbuff['moo'] = $row['moo'];
            $resultbuff['soi'] = $row['soi'];
            $resultbuff['street'] = $row['street'];
            $resultbuff['subdist'] = $row['subdist'];
            $resultbuff['dist'] = $row['dist'];
            $resultbuff['prov'] = $row['prov'];
            $resultbuff['zip_code'] = $row['zip_code'];


            $resultbuff['telephone'] = array();
            $queryTel = "SELECT number FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' AND lcn_cd = '02' AND phone_fax_cd = '1' ";
            $query = $this->mssql->query($queryTel)->result_array();
            $tempTel = array();
            foreach ($query as $row) {
              $tempTel['number'] = $row['number'];

              array_push($resultbuff['telephone'], $tempTel);
            }
            $resultbuff['fax'] = "";
            $queryFax = "SELECT number FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' AND lcn_cd = '02' AND phone_fax_cd = '2' ";
            $query = $this->mssql->query($queryFax)->result_array();
            $tempTel = array();
            foreach ($query as $row) {
              $resultbuff['fax'] = $row['number'];
            }
            $resultbuff['wan'] = "";
            $queryFax = "SELECT number FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' AND lcn_cd = '15' AND phone_fax_cd = '1' ";
            $query = $this->mssql->query($queryFax)->result_array();
            $tempTel = array();
            foreach ($query as $row) {
              $resultbuff['wan'] = $row['number'];
            }

            array_push($result, $resultbuff);
        }

        $this->mssql->close();
        return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function closeUnitBranch($id) {
      $sessionName = $this->session->userdata('od_emp_code');
      $todayTime = date("Y-m-d");

      $updateBR = $this->mssql->query("UPDATE od_br_name
        SET
        is_status = '9',
        updated_by = '$sessionName',
        last_update = '$todayTime'
        WHERE is_br + is_sbr + is_ch + is_am = '$id'");


        $result = "";
        if($updateBR){
          $result = "Successfully";
        }else{
          $result = "Failed";
        }

        $this->mssql->close();
        return $result;
  }






}
?>
