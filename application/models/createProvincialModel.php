<?php
class createProvincialModel extends CI_Model
{
  var $mssql;
  public function __construct()
  {
    parent::__construct();
    //use DT database connection
    $this->mssql = $this->load->database ( 'DT', TRUE );
  }



  public function getListProvince () {
    $queryStr = "SELECT PROVINCE, CAT_CC FROM ADDRESS_CODE GROUP BY PROVINCE, CAT_CC ORDER BY PROVINCE";
      $query  = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['CAT_CC'];
        $resultbuff['code'] = $row['CAT_CC'];
        $resultbuff['name'] = $row['PROVINCE'];
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getListDistrict ($provinceID) {
    $queryStr = "SELECT REPLACE(DISTRICT, 'อำเภอ', '') DISTRICT, CAT_AA FROM ADDRESS_CODE WHERE CAT_CC = '$provinceID' GROUP BY DISTRICT, CAT_AA ORDER BY CAT_AA";
      $query  = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['CAT_AA'];
        $resultbuff['code'] = $row['CAT_AA'];
        $resultbuff['name'] = $row['DISTRICT'];

        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getListSubDistrict ($provinceID, $amphurID) {
    $queryStr = "SELECT SUB_DISTRICT, CAT_TT, POSTCODE FROM ADDRESS_CODE WHERE CAT_CC = '$provinceID' AND CAT_AA = '$amphurID' GROUP BY SUB_DISTRICT, CAT_TT, POSTCODE  ORDER BY CAT_TT";
      $query  = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['CAT_TT'];
        $resultbuff['code'] = $row['CAT_TT'];
        $resultbuff['name'] = $row['SUB_DISTRICT'];
        $resultbuff['postcode'] = $row['POSTCODE'];
        $resultbuff['cancel'] = false;
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function addNewProvincial ($odInfo, $odAddrBR, $telBRInfo, $ctrBRInfo) {
    $sessionName = $this->session->userdata('od_emp_code');
    $todayTime = date("Y-m-d");
    $brnch_act_dept_cd = $odInfo['is_div'];
    if ($brnch_act_dept_cd == '10') {
      $is_div = $odInfo['is_div'];
    }
    else {
      $is_div = str_replace('0', '', $odInfo['is_div']);
    }
    // $is_br = $odInfo['is_br'];
    $is_sbr = 'A';
    $is_ch = '00';
    $is_am = '00';
    $is_thai_n = 'สำนักงาน ธ.ก.ส.จังหวัด'.$odInfo['is_thai_n'];
    $is_eng_n = $odInfo['is_eng_n'].' BAAC PROV.OFF';
    $is_amp_n = $odInfo['is_amp_n'];
    $is_open_dte = str_replace('-', '',  $odInfo['is_open_dte']);
    // insert new branch
    $addNewBranch = $this->mssql->query("INSERT INTO od_br_name (is_div, is_br, is_sbr, is_ch, is_am, is_thai_n, is_eng_n,
      is_amp_n, is_grp, is_reg, is_open_dte, is_region, is_poor, is_zone, is_status,
      is_business, is_exp_dte, updated_by, last_update, is_div_group, org_cd,
      brnch_type, is_code)
      SELECT TOP 1 '$is_div', REPLACE(sbn.alphabet, 'A', ''), RIGHT(sbn.alphabet, 1), '$is_ch', '$is_am', '$is_thai_n', '$is_eng_n',
      '$is_amp_n', '000', '00000', '$is_open_dte', '', '0', '00', '1',
      '', '00000000', '$sessionName', '$todayTime', '',
      (SELECT CONVERT(varchar, MAX(CONVERT(INT, REPLACE(org_cd, 'จ', '')))+1)+'จ' FROM od_br_name WHERE org_cd != ''),
       '1', '0'
      FROM od_br_name od
      LEFT JOIN SORTING_PROVINCIAL sb ON od.is_br+od.is_sbr = sb.alphabet
      LEFT JOIN SORTING_PROVINCIAL sbn ON (sb.item_no + 1) = sbn.item_no
      WHERE  od.is_sbr = 'A'
      ORDER BY od.is_br DESC");

    $insert_id = $this->mssql->insert_id();
    $querySBR = "SELECT is_br, is_sbr FROM od_br_name  WHERE item_no = $insert_id ";
    $query = $this->mssql->query($querySBR)->result_array();
    $is_br_insert = $query[0]['is_br'];
    $is_sbr_insert = $query[0]['is_sbr'];

    $addr =  $odAddrBR['addr'];
    $moo =  $odAddrBR['moo'];
    $soi =  $odAddrBR['soi'];
    $street =  $odAddrBR['street'];
    $subdist =  $odAddrBR['subdist'];
    $dist =  $odAddrBR['dist'];
    $prov =  $odAddrBR['prov'];
    $zip_code =  $odAddrBR['zipcode'];
    // insert new address for new branch
    $addNewBranchAddr = $this->mssql->query("INSERT INTO od_org_addr (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, brnch_act_dept_cd, addr,
      moo, soi, street, subdist, dist, prov, zip_code, updated_by, last_update)
      VALUES ( '$is_br_insert', '$is_sbr_insert', '$is_ch', '$is_am', '$brnch_act_dept_cd', '$addr',
        '$moo', '$soi', '$street', '$subdist', '$dist', '$prov', '$zip_code', '$sessionName', '$todayTime' ) ");

    $n = 0;
    foreach ($telBRInfo as $value) {
      $number = str_replace('-', '', $value['number']);
      //insert phone number for new branch
      if ($n == 0) {
        $addPhone = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
          phone_fax_cd, lcn_cd, updated_by, last_update)
          VALUES ( '$is_br_insert', '$is_sbr_insert', '$is_ch', '$is_am', '$number',
            '1', '02', '$sessionName', '$todayTime' ) ");
      }
      elseif ($n > 0 && $number != '') {
        $addPhone = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
          phone_fax_cd, lcn_cd, updated_by, last_update)
          VALUES ( '$is_br_insert', '$is_sbr_insert', '$is_ch', '$is_am', '$number',
            '1', '02', '$sessionName', '$todayTime' ) ");
      }
      $n = $n + 1;
    }

    $fax = str_replace('-', '',  $ctrBRInfo['fax']);
                //insert fax number for new branch
    $addFax = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
      phone_fax_cd, lcn_cd, updated_by, last_update)
      VALUES ( '$is_br_insert', '$is_sbr_insert', '$is_ch', '$is_am', '$fax',
        '2', '02', '$sessionName', '$todayTime' ) ");

    $wan = $ctrBRInfo['wan'];
    //insert wan number for new branch
    $addWan = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
      phone_fax_cd, lcn_cd, updated_by, last_update)
      VALUES (
        '$is_br_insert', '$is_sbr_insert', '$is_ch', '$is_am', '$wan',
        '1', '15', '$sessionName', '$todayTime' ) ");

    $brcode =  $is_br_insert.$is_sbr_insert;
    $divprofile = '';
    if($is_div == '10')
      $divprofile = '80018';
    else {
      $divprofile = '800'.$is_div;
    }

    $profile = '90'.$is_br_insert;
    $addMapProfile = $this->mssql->query("INSERT INTO tb_brmapprofile (brcode, profilecode, acccode, atmcode, brname, div, divprofile)
      VALUES ('$brcode', '$profile', '', '', '$is_thai_n', '$is_div', '$divprofile') ");

    $division_code = '48'.$is_br_insert.$is_sbr_insert.'0000';
    $is_thai = 'สนจ.'.$odInfo['is_thai_n'];
    $cost_name = 'ประจำสนจ.'.$odInfo['is_thai_n'];

    $addDivision = $this->mssql->query("INSERT INTO Division (DIVISION_CODE, DIVISION_NAME, DIVISION_AS,
      DIVISION_MN_CODE, DIVISION_MN_NAME, DIVISION_MN_AS,
      COST_CODE, COST_NAME, DIVISION_AREA)
      SELECT '$division_code', '$is_thai_n', '$is_thai',
      brnch_code, 'ฝสข.'+brnch_act_dept_dtl, brnch_sht,
      '', '$cost_name', '$is_div'
      FROM od_brnch_act_dept
      WHERE brnch_act_dept_cd = '$brnch_act_dept_cd' ");

    $addShortName = $this->mssql->query("INSERT INTO short_name_branch (brcode, brname, brshrt, ampcode)
    SELECT '$brcode', '$is_thai_n', '$is_amp_n', (MAX(ampcode)+1) FROM short_name_branch ");


    if($addNewBranch && $addNewBranchAddr && $addPhone && $addFax && $addWan && $addMapProfile && $addDivision && $addShortName){
      $result = "Successfully";
    }else{
      $result = "Failed";
    }

    $this->mssql->close();
    return $result;

  }


  public function changetoProvincial ($odInfo, $provinceInfo, $provinceAddr, $telBRInfo, $ctrBRInfo) {
    $sessionName = $this->session->userdata('od_emp_code');
    $todayTime = date("Y-m-d");
    $brnch_act_dept_cd = $provinceInfo['is_div'];
    if ($brnch_act_dept_cd == '10') {
      $is_div = $provinceInfo['is_div'];
    }
    else {
      $is_div = str_replace('0', '', $provinceInfo['is_div']);
    }
    // $is_br = $odInfo['is_br'];
    $is_sbr = 'A';
    $is_ch = '00';
    $is_am = '00';
    $is_thai_n = 'สำนักงาน ธ.ก.ส.จังหวัด'.$provinceInfo['is_thai_n'];
    $is_eng_n = $provinceInfo['is_eng_n'].' BAAC PROV.OFF';
    $is_amp_n = $provinceInfo['is_amp_n'];
    $is_open_dte = str_replace('-', '',  $provinceInfo['is_open_dte']);
    $old_id = $odInfo['is_br'].$odInfo['is_sbr'].$odInfo['is_ch'].$odInfo['is_am'];
    // insert new branch
    $addNewBranch = $this->mssql->query("INSERT INTO od_br_name (is_div, is_br, is_sbr, is_ch, is_am, is_thai_n, is_eng_n,
      is_amp_n, is_grp, is_reg, is_open_dte, is_region, is_poor, is_zone, is_status,
      is_business, is_exp_dte, updated_by, last_update, is_div_group, org_cd,
      brnch_type, is_code)
      SELECT TOP 1 '$is_div', REPLACE(sbn.alphabet, 'A', ''), RIGHT(sbn.alphabet, 1), '$is_ch', '$is_am', '$is_thai_n', '$is_eng_n',
      '$is_amp_n', '000', '00000', '$is_open_dte', '', '0', '00', '1',
      '', '00000000', '$sessionName', '$todayTime', '',
      ( ( SELECT REPLACE(org_cd, ' ', '')  FROM od_br_name WHERE is_br+is_sbr+is_ch+is_am = '$old_id' ) + 'จ' ),
       '1', '0'
      FROM od_br_name od
      LEFT JOIN SORTING_PROVINCIAL sb ON od.is_br+od.is_sbr = sb.alphabet
      LEFT JOIN SORTING_PROVINCIAL sbn ON (sb.item_no + 1) = sbn.item_no
      WHERE  od.is_sbr = 'A'
      ORDER BY od.is_br DESC");

    $insert_id = $this->mssql->insert_id();
    $querySBR = "SELECT is_br, is_sbr FROM od_br_name  WHERE item_no = $insert_id ";
    $query = $this->mssql->query($querySBR)->result_array();
    $is_br_insert = $query[0]['is_br'];
    $is_sbr_insert = $query[0]['is_sbr'];

    $addr =  $provinceAddr['addr'];
    $moo =  $provinceAddr['moo'];
    $soi =  $provinceAddr['soi'];
    $street =  $provinceAddr['street'];
    $subdist =  $provinceAddr['subdist_nme'];
    $dist =  $provinceAddr['dist_nme'];
    $prov =  $provinceAddr['prov_nme'];
    $zip_code =  $provinceAddr['zipcode'];
    // insert new address for new branch
    $addNewBranchAddr = $this->mssql->query("INSERT INTO od_org_addr (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, brnch_act_dept_cd, addr,
      moo, soi, street, subdist, dist, prov, zip_code, updated_by, last_update)
      VALUES ( '$is_br_insert', '$is_sbr_insert', '$is_ch', '$is_am', '$brnch_act_dept_cd', '$addr',
        '$moo', '$soi', '$street', '$subdist', '$dist', '$prov', '$zip_code', '$sessionName', '$todayTime' ) ");

    $n = 0;
    foreach ($telBRInfo as $value) {
      $number = str_replace('-', '', $value['number']);
      //insert phone number for new branch
      if ($n == 0) {
        $addPhone = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
          phone_fax_cd, lcn_cd, updated_by, last_update)
          VALUES ( '$is_br_insert', '$is_sbr_insert', '$is_ch', '$is_am', '$number',
            '1', '02', '$sessionName', '$todayTime' ) ");
      }
      elseif ($n > 0 && $number != '') {
        $addPhone = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
          phone_fax_cd, lcn_cd, updated_by, last_update)
          VALUES ( '$is_br_insert', '$is_sbr_insert', '$is_ch', '$is_am', '$number',
            '1', '02', '$sessionName', '$todayTime' ) ");
      }
      $n = $n + 1;
    }

    $fax = str_replace('-', '',  $ctrBRInfo['fax']);
                //insert fax number for new branch
    $addFax = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
      phone_fax_cd, lcn_cd, updated_by, last_update)
      VALUES ( '$is_br_insert', '$is_sbr_insert', '$is_ch', '$is_am', '$fax',
        '2', '02', '$sessionName', '$todayTime' ) ");

    $wan = $ctrBRInfo['wan'];
    //insert wan number for new branch
    $addWan = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
      phone_fax_cd, lcn_cd, updated_by, last_update)
      VALUES (
        '$is_br_insert', '$is_sbr_insert', '$is_ch', '$is_am', '$wan',
        '1', '15', '$sessionName', '$todayTime' ) ");

    $brcode =  $is_br_insert.$is_sbr_insert;
    $divprofile = '';
    if($is_div == '10')
      $divprofile = '80018';
    else {
      $divprofile = '800'.$is_div;
    }

    $profile = '90'.$is_br_insert;
    $addMapProfile = $this->mssql->query("INSERT INTO tb_brmapprofile (brcode, profilecode, acccode, atmcode, brname, div, divprofile)
      VALUES ('$brcode', '$profile', '', '', '$is_thai_n', '$is_div', '$divprofile') ");

    $division_code = '48'.$is_br_insert.$is_sbr_insert.'0000';
    $is_thai = 'สนจ.'.$provinceInfo['is_thai_n'];
    $cost_name = 'ประจำสนจ.'.$provinceInfo['is_thai_n'];

    $addDivision = $this->mssql->query("INSERT INTO Division (DIVISION_CODE, DIVISION_NAME, DIVISION_AS,
      DIVISION_MN_CODE, DIVISION_MN_NAME, DIVISION_MN_AS,
      COST_CODE, COST_NAME, DIVISION_AREA)
      SELECT '$division_code', '$is_thai_n', '$is_thai',
      brnch_code, 'ฝสข.'+brnch_act_dept_dtl, brnch_sht,
      '', '$cost_name', '$is_div'
      FROM od_brnch_act_dept
      WHERE brnch_act_dept_cd = '$brnch_act_dept_cd' ");

    $brcodeShort =  $is_br_insert.$is_sbr_insert;
    $addShortName = $this->mssql->query("INSERT INTO short_name_branch (brcode, brname, brshrt, ampcode)
    SELECT '$brcodeShort', '$is_thai_n', '$is_amp_n', (MAX(ampcode)+1) FROM short_name_branch ");

    // update ข้อมูลของสาขา
    $oldBranch_is_br = $odInfo['is_br'];
    $oldBranch_is_sbr = $odInfo['is_sbr'];     //$is_div
    $update_od_br_name = $this->mssql->query("UPDATE od_br_name
      SET
      is_div = '$is_div',
      is_br = '$is_br_insert',
      is_sbr = '0',
      updated_by = '$sessionName',
      last_update = '$todayTime'
      WHERE is_br  = '$oldBranch_is_br' AND is_sbr = '$oldBranch_is_sbr' ");

    $update_od_br_name = $this->mssql->query("UPDATE od_br_name
      SET
      is_amp_n = '$is_amp_n',
      updated_by = '$sessionName',
      last_update = '$todayTime'
      WHERE is_br  = '$is_br_insert' AND is_sbr = '0' AND is_ch = '00' AND is_am = '00' ");

    $update_od_br_name_tumbon = $this->mssql->query("UPDATE od_br_name_tumbon
      SET
      is_div = '$is_div',
      is_br = '$is_br_insert',
      is_sbr = '0',
      updated_by = '$sessionName',
      last_update = '$todayTime'
      WHERE is_br  = '$oldBranch_is_br' AND is_sbr = '$oldBranch_is_sbr' ");

    $update_od_org_addr = $this->mssql->query("UPDATE od_org_addr
      SET
      ind_brnch_cd = '$is_br_insert',
      dept_cd = '0',
      updated_by = '$sessionName',
      last_update = '$todayTime'
      WHERE ind_brnch_cd  = '$oldBranch_is_br' AND dept_cd = '$oldBranch_is_sbr' ");

    $update_od_phone_fax = $this->mssql->query("UPDATE od_phone_fax
      SET
      ind_brnch_cd = '$is_br_insert',
      dept_cd = '0',
      updated_by = '$sessionName',
      last_update = '$todayTime'
      WHERE ind_brnch_cd  = '$oldBranch_is_br' AND dept_cd = '$oldBranch_is_sbr' ");

    $update_bmc_tumbon = $this->mssql->query("UPDATE bmc_tumbon
      SET
      is_br = '$is_br_insert',
      is_sbr = '0'
      WHERE is_br  = '$oldBranch_is_br' AND is_sbr = '$oldBranch_is_sbr' ");

    $update_od_link_br_tumbon = $this->mssql->query("UPDATE od_link_br_tumbon
      SET
      ind_brnch_cd = '$is_br_insert',
      dept_cd = '0',
      updated_by = '$sessionName',
      last_update = '$todayTime'
      WHERE ind_brnch_cd  = '$oldBranch_is_br' AND dept_cd = '$oldBranch_is_sbr' ");

    $insert_brcode = $is_br_insert.'0';
    $check_brcode =  $oldBranch_is_br.$oldBranch_is_sbr;
    $update_tb_brmapprofile = $this->mssql->query("UPDATE tb_brmapprofile
      SET
      brcode = '$insert_brcode',
      div = '$is_div',
      divprofile = '$divprofile'
      WHERE brcode = '$check_brcode' ");

    $is_thai = 'สนจ.'.$provinceInfo['is_thai_n'];
    $br_division_code = '48'.$is_br_insert.'0'.'0000';
    $div_is_thai = 'สำนักงาน ธ.ก.ส.จังหวัด'.$provinceInfo['is_thai_n'];
    $check_division = '48'.$check_brcode.'0000';
    $update_division = $this->mssql->query("UPDATE Division
      SET
      DIVISION_CODE = '$br_division_code',
      DIVISION_MN_CODE = '$division_code',
      DIVISION_MN_NAME = '$div_is_thai',
      DIVISION_MN_AS = '$is_thai',
      DIVISION_AREA = '$is_div'
      WHERE DIVISION_CODE = '$check_division' ");

    if($addNewBranch && $addNewBranchAddr && $addPhone && $addFax && $addWan && $addMapProfile && $addDivision && $addShortName){
      $result = "Successfully";
    }else{
      $result = "Failed";
    }

    $this->mssql->close();
    return $result;
  }
}
?>
