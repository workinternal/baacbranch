<?php
class createBranchModel extends CI_Model
{
  var $mssql;
  public function __construct()
  {
    parent::__construct();
    //use DT database connection
    $this->mssql = $this->load->database ( 'DT', TRUE );
  }



  public function getListProvince () {
    $queryStr = "SELECT PROVINCE, CAT_CC FROM ADDRESS_CODE GROUP BY PROVINCE, CAT_CC ORDER BY PROVINCE";
      $query  = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['CAT_CC'];
        $resultbuff['code'] = $row['CAT_CC'];
        $resultbuff['name'] = $row['PROVINCE'];
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getListDistrict ($provinceID) {
    $queryStr = "SELECT REPLACE(DISTRICT, 'อำเภอ', '') DISTRICT, CAT_AA FROM ADDRESS_CODE WHERE CAT_CC = '$provinceID' GROUP BY DISTRICT, CAT_AA ORDER BY CAT_AA";
      $query  = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['CAT_AA'];
        $resultbuff['code'] = $row['CAT_AA'];
        $resultbuff['name'] = $row['DISTRICT'];

        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getListSubDistrict ($provinceID, $amphurID) {
    $queryStr = "SELECT SUB_DISTRICT, CAT_TT, POSTCODE FROM ADDRESS_CODE WHERE CAT_CC = '$provinceID' AND CAT_AA = '$amphurID' GROUP BY SUB_DISTRICT, CAT_TT, POSTCODE  ORDER BY CAT_TT";
      $query  = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['CAT_TT'];
        $resultbuff['code'] = $row['CAT_TT'];
        $resultbuff['name'] = $row['SUB_DISTRICT'];
        $resultbuff['postcode'] = $row['POSTCODE'];
        $resultbuff['cancel'] = false;
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getCurrentBranchCode ($province) {
    $queryStr = "SELECT TOP 1 od.is_sbr, sb.alphabet, sb.item_no, sbn.item_no AS item_new, sbn.alphabet AS alphabet_new
                FROM od_br_name od
                LEFT JOIN SORTING_BRANCH sb ON od.is_sbr = sb.alphabet
                LEFT JOIN SORTING_BRANCH sbn ON (sb.item_no + 1) = sbn.item_no
                WHERE od.is_br = '$province' AND od.is_sbr != 'A'
                ORDER BY od.is_sbr DESC";
      $query  = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['alphabet_new'];
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getCurrentAmphur ($province) {
    $queryStr = "SELECT TOP 1 (CASE WHEN is_am <  9 THEN '0' + CONVERT(VARCHAR, is_am+1) ELSE CONVERT(VARCHAR, is_am+1) END) am,
        is_ch, is_am FROM od_br_name_tumbon WHERE is_br = '$province'  ORDER BY is_am DESC";
      $query  = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['is_ch'] = $row['is_ch'];
        $resultbuff['is_am'] = $row['am'];
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function addNewBranch ($odArea, $odInfo, $odAddrBR, $telBRInfo, $ctrBRInfo, $odAmphur, $odAddrAM, $telAMInfo, $ctrAMInfo, $odUnitAmphur) {
      $sessionName = $this->session->userdata('od_emp_code');
      $todayTime = date("Y-m-d");
      $brnch_act_dept_cd = $odInfo['is_div'];
      if ($brnch_act_dept_cd == '10') {
        $is_div = $odInfo['is_div'];
      }
      else {
        $is_div = str_replace('0', '', $odInfo['is_div']);
      }
      $is_br = $odInfo['is_br'];

      $is_ch = $odInfo['is_ch'];
      $is_am = $odInfo['is_am'];
      $is_thai_n = $odInfo['is_thai_n'];
      $is_eng_n = $odInfo['is_eng_n'];
      $is_amp_n = $odInfo['is_amp_n'];
      $is_open_dte = str_replace('-', '',  $odInfo['is_open_dte']);

      if ($is_br == '' || $is_br == null) {
        return 'Failed';
      }
      // insert new branch
      $addNewBranch = $this->mssql->query("INSERT INTO od_br_name (is_div, is_br, is_sbr, is_ch, is_am, is_thai_n, is_eng_n,
        is_amp_n, is_grp, is_reg, is_open_dte, is_region, is_poor, is_zone, is_status,
        is_business, is_exp_dte, updated_by, last_update, is_div_group, org_cd, brnch_type, is_code)
        SELECT '$is_div', '$is_br', (CASE WHEN b.is_sbr IS NULL THEN '0' ELSE sbn.alphabet END) , '$is_ch', '$is_am', '$is_thai_n', '$is_eng_n',
        '$is_amp_n', '000', '00000', '$is_open_dte', '', '0', '00', '1',
        '', '00000000', '$sessionName', '$todayTime', '', '', '2', '0'
        FROM
        (SELECT is_br, is_sbr FROM od_br_name WHERE is_br = '$is_br' AND is_sbr = 'A' ) a
        LEFT JOIN (SELECT TOP 1 is_br, is_sbr FROM od_br_name WHERE is_br = '$is_br' AND is_sbr != 'A' ORDER BY is_sbr DESC) b  ON a.is_br = b.is_br
        LEFT JOIN SORTING_BRANCH sb ON b.is_sbr = sb.alphabet
        LEFT JOIN SORTING_BRANCH sbn ON (sb.item_no + 1) = sbn.item_no ");

      $insert_id = $this->mssql->insert_id();
      $querySBR = "SELECT is_br, is_sbr, is_ch, is_am FROM od_br_name  WHERE item_no = $insert_id ";
      $query = $this->mssql->query($querySBR)->result_array();
      $is_sbr_insert = $query[0]['is_sbr'];
      $br_code_new = $query[0]['is_br'].$query[0]['is_sbr'].$query[0]['is_ch'].$query[0]['is_am'];

      $addr =  $odAddrBR['addr'];
      $moo =  $odAddrBR['moo'];
      $soi =  $odAddrBR['soi'];
      $street =  $odAddrBR['street'];
      $subdist =  $odAddrBR['subdist'];
      $dist =  $odAddrBR['dist'];
      $prov =  $odAddrBR['prov'];
      $zip_code =  $odAddrBR['zipcode'];
      // insert new address for new branch
      $addNewBranchAddr = $this->mssql->query("INSERT INTO od_org_addr (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, brnch_act_dept_cd, addr,
        moo, soi, street, subdist, dist, prov, zip_code, updated_by, last_update)
        VALUES ( '$is_br', '$is_sbr_insert', '$is_ch', '$is_am', '$brnch_act_dept_cd', '$addr',
          '$moo', '$soi', '$street', '$subdist', '$dist', '$prov', '$zip_code', '$sessionName', '$todayTime' ) ");

      $n = 0;
      foreach ($telBRInfo as $value) {
        $number = str_replace('-', '', $value['number']);
        //insert phone number for new branch
        if ($n == 0) {
          $addPhone = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
            phone_fax_cd, lcn_cd, updated_by, last_update)
            VALUES ( '$is_br', '$is_sbr_insert', '$is_ch', '$is_am', '$number',
              '1', '02', '$sessionName', '$todayTime' ) ");
        }
        elseif ($n > 0 && $number != '') {
          $addPhone = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
            phone_fax_cd, lcn_cd, updated_by, last_update)
            VALUES ( '$is_br', '$is_sbr_insert', '$is_ch', '$is_am', '$number',
              '1', '02', '$sessionName', '$todayTime' ) ");
        }
        $n = $n + 1;
      }

      $fax = str_replace('-', '',  $ctrBRInfo['fax']);
                  //insert fax number for new branch
      $addFax = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
        phone_fax_cd, lcn_cd, updated_by, last_update)
        VALUES ( '$is_br', '$is_sbr_insert', '$is_ch', '$is_am', '$fax',
          '2', '02', '$sessionName', '$todayTime' ) ");

      $wan = $ctrBRInfo['wan'];
      //insert wan number for new branch
      $addWan = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
        phone_fax_cd, lcn_cd, updated_by, last_update)
        VALUES (
          '$is_br', '$is_sbr_insert', '$is_ch', '$is_am', '$wan',
          '1', '15', '$sessionName', '$todayTime' ) ");

      $amphur_is_br = $is_br;
      $amphur_is_sbr = $is_sbr_insert;
      $amphur_is_ch = $odAmphur['is_ch'];
      $amphur_is_am = $odAmphur['is_am'];
      $amphur_is_thai_n = $odAmphur['is_thai_n'];
      $amphur_is_eng_n = $odAmphur['is_eng_n'];
      $amphur_is_open_dte = str_replace('-', '',  $odAmphur['is_open_dte']);
      // insert new unit
      $addNewUnit = $this->mssql->query("INSERT INTO od_br_name (is_div, is_br, is_sbr, is_ch, is_am, is_thai_n, is_eng_n,
        is_amp_n, is_grp, is_reg, is_open_dte, is_region, is_poor, is_zone, is_status,
        is_business, is_exp_dte, updated_by, last_update, is_div_group, org_cd, brnch_type, is_code)
        VALUES ( '$is_div', '$amphur_is_br', '$amphur_is_sbr', '$amphur_is_ch', '$amphur_is_am', '$amphur_is_thai_n', '$amphur_is_eng_n',
          '', '000', '00000', '$amphur_is_open_dte', '', '0', '00', '1',
          '', '00000000', '$sessionName', '$todayTime', '', '', '7', '0') ");

      $amphur_addr =  $odAddrAM['addr'];
      $amphur_moo =  $odAddrAM['moo'];
      $amphur_soi =  $odAddrAM['soi'];
      $amphur_street =  $odAddrAM['street'];
      $amphur_subdist =  $odAddrAM['subdist'];
      $amphur_dist =  $odAddrAM['dist'];
      $amphur_prov =  $odAddrAM['prov'];
      $amphur_zip_code =  $odAddrAM['zipcode'];
      // insert new address for new branch
      $addNewUnitAddr = $this->mssql->query("INSERT INTO od_org_addr (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, brnch_act_dept_cd, addr,
        moo, soi, street, subdist, dist, prov, zip_code,
        updated_by, last_update)
        VALUES ( '$amphur_is_br', '$amphur_is_sbr', '$amphur_is_ch', '$amphur_is_am', '$brnch_act_dept_cd', '$amphur_addr',
          '$amphur_moo', '$amphur_soi', '$amphur_street', '$amphur_subdist', '$amphur_dist', '$amphur_prov', '$amphur_zip_code',
          '$sessionName', '$todayTime' ) ");

      $m = 0;
      foreach ($telAMInfo as $value) {
        $numberUnit = str_replace('-', '', $value['number']);
        //insert phone number for new unit
        if ($m == 0) {
          $addPhoneUnit = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
            phone_fax_cd, lcn_cd, updated_by, last_update)
            VALUES ( '$amphur_is_br', '$amphur_is_sbr', '$amphur_is_ch', '$amphur_is_am', '$numberUnit',
              '1', '02', '$sessionName', '$todayTime' ) ");
        }
        elseif ($m > 0 && $numberUnit != '') {
          $addPhoneUnit = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
            phone_fax_cd, lcn_cd, updated_by, last_update)
            VALUES ( '$amphur_is_br', '$amphur_is_sbr', '$amphur_is_ch', '$amphur_is_am', '$numberUnit',
              '1', '02', '$sessionName', '$todayTime' ) ");
        }
        $m = $m + 1;
      }

      $faxUnit = str_replace('-', '',  $ctrAMInfo['fax']);
      //insert fax number for new unit
      $addFaxUnit = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
        phone_fax_cd, lcn_cd, updated_by, last_update)
        VALUES (
          '$amphur_is_br', '$amphur_is_sbr', '$amphur_is_ch', '$amphur_is_am', '$faxUnit',
          '2', '02', '$sessionName', '$todayTime' ) ");

      $wanUnit = $ctrAMInfo['wan'];
      //insert wan number for new unit
      $addWanUnit = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
        phone_fax_cd, lcn_cd, updated_by, last_update)
        VALUES (
          '$amphur_is_br', '$amphur_is_sbr', '$amphur_is_ch', '$amphur_is_am', '$wanUnit',
          '1', '15', '$sessionName', '$todayTime' ) ");

      $tumbon_is_thai_n = $odUnitAmphur['is_thai_n'];
      $tumbon_is_eng_n = $odUnitAmphur['is_eng_n'];

      $provcode = $is_br.'A';
      // insert new tumbon
      $addNewUnitArea = $this->mssql->query("INSERT INTO od_br_name_tumbon (is_div, is_br, is_sbr,
        is_ch, is_am,
        is_thai_n, is_eng_n,
        is_amp_n, is_grp, is_reg, is_open_dte, is_region, is_poor, is_zone, is_status,
        is_business, is_exp_dte, updated_by, last_update, is_div_group, is_code)
        SELECT '$is_div', '$amphur_is_br', '$amphur_is_sbr',
        ampcode, (CASE WHEN is_am IS NULL THEN '01' WHEN is_am <  9 THEN '0' + CONVERT(VARCHAR, is_am+1) ELSE CONVERT(VARCHAR, is_am+1) END),
        '$tumbon_is_thai_n', '$tumbon_is_eng_n',
        '', '000', '00000', '$amphur_is_open_dte', '', '0', '00', '1',
        '', '00000000', '$sessionName', '$todayTime', '', '0'
        FROM short_name_branch
        LEFT JOIN (SELECT is_ch, MAX(is_am) is_am FROM od_br_name_tumbon GROUP BY is_ch) AS MX ON ampcode = is_ch
        WHERE brcode = '$provcode'  ");

      $insert_id_tumbon = $this->mssql->insert_id();
      $querySBR = "SELECT is_ch, is_am  FROM od_br_name_tumbon  WHERE item_no = $insert_id_tumbon ";
      $query = $this->mssql->query($querySBR)->result_array();
      $tumbon_is_ch = $query[0]['is_ch'];
      $tumbon_is_am = $query[0]['is_am'];
      $tumbon_is_offset = $tumbon_is_ch.$tumbon_is_am;

      $addMapUnitArea = $this->mssql->query("INSERT INTO od_link_br_tumbon (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, fldoff_dist_cd, updated_by, last_update)
        VALUES ('$amphur_is_br', '$amphur_is_sbr', '$amphur_is_ch', '$amphur_is_am', '$tumbon_is_offset', '$sessionName', '$todayTime') ");

      if (sizeof($odArea) > 0) {
        for ($x = 0; $x < sizeof($odArea); $x++) {
          $cat_cc = $odArea[$x]['cat_cc'];
          $cat_aa = $odArea[$x]['cat_aa'];
          $addMainArea = $this->mssql->query("INSERT INTO bmc_tumbon (is_br, is_sbr, is_ch, is_am, is_thai_n,
            cat_cc, cat_aa, cat_tt, cat_mm, cat_desc)
            SELECT TOP 1 '$amphur_is_br', '$amphur_is_sbr', '$tumbon_is_ch', '$tumbon_is_am', '$tumbon_is_thai_n',
            CAT_CC, CAT_AA, '00', '00', REPLACE(DISTRICT, 'อำเภอ', '')
            FROM ADDRESS_CODE
            WHERE CAT_CC = '$cat_cc' AND CAT_AA = '$cat_aa' ");

            if( sizeof($odArea[$x]['cat_tt']) > 0) {

              for ($y = 0; $y < sizeof($odArea[$x]['cat_tt']); $y++) {

                $cat_tt = $odArea[$x]['cat_tt'][$y]['code'];
                $select = $odArea[$x]['cat_tt'][$y]['tumbon'];
                $cancel = $odArea[$x]['cat_tt'][$y]['cancel'];
                if($select == 'true') {

                  if ($cancel == 'true') { //ลบตำบล หากเลือกยกเลิกจากพื้นที่เก่า
                    $removeUnitArea = $this->mssql->query("DELETE FROM bmc_tumbon WHERE CAT_CC = '$cat_cc' AND CAT_AA = '$cat_aa' AND CAT_TT = '$cat_tt' ");
                  }
                  $addSubArea = $this->mssql->query("INSERT INTO bmc_tumbon (is_br, is_sbr, is_ch, is_am, is_thai_n,
                    cat_cc, cat_aa, cat_tt, cat_mm, cat_desc)
                    SELECT TOP 1 '$amphur_is_br', '$amphur_is_sbr', '$tumbon_is_ch', '$tumbon_is_am', '$tumbon_is_thai_n',
                    CAT_CC, CAT_AA, CAT_TT, '00', SUB_DISTRICT
                    FROM ADDRESS_CODE
                    WHERE CAT_CC = '$cat_cc' AND CAT_AA = '$cat_aa' AND CAT_TT = '$cat_tt' ");
                } //end if select

              } //end for

            } // end if
          } // end for
        } //end if


      $brcode =  $is_br.$is_sbr_insert;
      $divprofile = '';
      if($is_div == '10')
        $divprofile = '80018';
      else {
        $divprofile = '800'.$is_div;
      }

      $is_thai = 'สาขา'.$is_thai_n;
      if ($is_sbr_insert == '0') {
        $addMapProfile = $this->mssql->query("INSERT INTO tb_brmapprofile (brcode, profilecode, acccode, atmcode,brname, div, divprofile)
          SELECT TOP 1 '$brcode', (SELECT SUBSTRING(org_cd, 1, len(org_cd)-1) FROM od_br_name  WHERE is_br = '$is_br' AND is_sbr = 'A'), '',
           (SELECT SUBSTRING(org_cd, 1, len(org_cd)-1) FROM od_br_name  WHERE is_br = '$is_br' AND is_sbr = 'A'), '$is_thai', '$is_div', '$divprofile' ");
      }
      else {
        $addMapProfile = $this->mssql->query("INSERT INTO tb_brmapprofile (brcode, profilecode, acccode, atmcode,brname, div, divprofile)
        SELECT '$brcode', MAX(CONVERT(INT, REPLACE(org_cd, 'จ', '')))+1, '', MAX(CONVERT(INT, REPLACE(org_cd, 'จ', '')))+1, '$is_thai', '$is_div', '$divprofile'
        FROM od_br_name WHERE org_cd != '' ");
      }


      $updateORGCD = $this->mssql->query("UPDATE od_br_name SET org_cd = br.profilecode
          FROM (SELECT profilecode FROM tb_brmapprofile WHERE brcode = '$brcode') AS br
          WHERE is_br+is_sbr+is_ch+is_am = '$br_code_new' ");

      $division_code = '48'.$is_br.$is_sbr_insert.'0000';
      $div_mn_name = $odInfo['prov_nme'];
      $div_mn_as = $odInfo['prov_as'];
      $div_mn_code = '48'.$is_br.'A'.'0000';
      $cost_name = 'ประจำ'.$is_thai;

      $addDivision = $this->mssql->query("INSERT INTO Division (DIVISION_CODE, DIVISION_NAME, DIVISION_AS,
                      DIVISION_MN_CODE, DIVISION_MN_NAME, DIVISION_MN_AS,
                      COST_CODE, COST_NAME, DIVISION_AREA)
                      VALUES ( '$division_code', '$is_thai', '$is_thai',
                               '$div_mn_code', '$div_mn_name', '$div_mn_as',
                               '', '$cost_name', '$is_div') ");


      if($addNewBranch && $addNewBranchAddr && $addPhone && $addFax && $addWan && $addNewUnit && $addNewUnitAddr && $addPhoneUnit && $addFaxUnit && $addWanUnit && $addNewUnitArea && $addMapUnitArea && $addMapProfile && $addDivision){
        $result = "Successfully";
      }else{
        $result = "Failed";
      }

      $this->mssql->close();
      return $result;

  }

  public function changetoNewBranch ($oldUnit, $odArea, $odInfo, $odAddrBR, $telBRInfo, $ctrBRInfo, $odAmphur, $odAddrAM, $telAMInfo, $ctrAMInfo, $odUnitAmphur, $brcodeOld, $checkArea) {
    $sessionName = $this->session->userdata('od_emp_code');
    $todayTime = date("Y-m-d");

    $updateBR = $this->mssql->query("UPDATE od_br_name
      SET
      is_status = '9',
      updated_by = '$sessionName',
      last_update = '$todayTime'
      WHERE is_br + is_sbr + is_ch + is_am = '$oldUnit'");

    $brnch_act_dept_cd = $odInfo['is_div'];
    if ($brnch_act_dept_cd == '10') {
      $is_div = $odInfo['is_div'];
    }
    else {
      $is_div = str_replace('0', '', $odInfo['is_div']);
    }
    $is_br = $odInfo['is_br'];
    // $is_sbr = $odInfo['is_sbr'];
    $is_ch = $odInfo['is_ch'];
    $is_am = $odInfo['is_am'];
    $is_thai_n = $odInfo['is_thai_n'];
    $is_eng_n = $odInfo['is_eng_n'];
    $is_amp_n = $odInfo['is_amp_n'];
    $is_open_dte = str_replace('-', '',  $odInfo['is_open_dte']);
    // insert new branch
    $addNewBranch = $this->mssql->query("INSERT INTO od_br_name (is_div, is_br, is_sbr, is_ch, is_am, is_thai_n, is_eng_n,
      is_amp_n, is_grp, is_reg, is_open_dte, is_region, is_poor, is_zone, is_status,
      is_business, is_exp_dte, updated_by, last_update, is_div_group, org_cd, brnch_type, is_code)
      SELECT '$is_div', '$is_br', (CASE WHEN b.is_sbr IS NULL THEN '0' ELSE sbn.alphabet END) , '$is_ch', '$is_am', '$is_thai_n', '$is_eng_n',
      '$is_amp_n', '000', '00000', '$is_open_dte', '', '0', '00', '1',
      '', '00000000', '$sessionName', '$todayTime', '', '', '2', '0'
      FROM
      (SELECT is_br, is_sbr FROM od_br_name WHERE is_br = '$is_br' AND is_sbr = 'A' ) a
      LEFT JOIN (SELECT TOP 1 is_br, is_sbr FROM od_br_name WHERE is_br = '$is_br' AND is_sbr != 'A' ORDER BY is_sbr DESC) b  ON a.is_br = b.is_br
      LEFT JOIN SORTING_BRANCH sb ON b.is_sbr = sb.alphabet
      LEFT JOIN SORTING_BRANCH sbn ON (sb.item_no + 1) = sbn.item_no ");

    $insert_id = $this->mssql->insert_id();
    $querySBR = "SELECT is_br, is_sbr, is_ch, is_am FROM od_br_name  WHERE item_no = $insert_id ";
    $query = $this->mssql->query($querySBR)->result_array();
    $is_sbr_insert = $query[0]['is_sbr'];
    $br_code_new = $query[0]['is_br'].$query[0]['is_sbr'].$query[0]['is_ch'].$query[0]['is_am'];

    $addChangeHistory = $this->mssql->query("INSERT INTO UNIT_BRANCH_HISTORY (unit_code, br_code, updated_by, last_update)
      VALUES ( '$oldUnit', '$br_code_new', '$sessionName', '$todayTime' ) ");

    $addr =  $odAddrBR['addr'];
    $moo =  $odAddrBR['moo'];
    $soi =  $odAddrBR['soi'];
    $street =  $odAddrBR['street'];
    $subdist =  $odAddrBR['subdist'];
    $dist =  $odAddrBR['dist'];
    $prov =  $odAddrBR['prov'];
    $zip_code =  $odAddrBR['zipcode'];
    // insert new address for new branch
    $addNewBranchAddr = $this->mssql->query("INSERT INTO od_org_addr (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, brnch_act_dept_cd, addr,
      moo, soi, street, subdist, dist, prov, zip_code, updated_by, last_update)
      VALUES ( '$is_br', '$is_sbr_insert', '$is_ch', '$is_am', '$brnch_act_dept_cd', '$addr',
        '$moo', '$soi', '$street', '$subdist', '$dist', '$prov', '$zip_code', '$sessionName', '$todayTime' ) ");

    $n = 0;
    foreach ($telBRInfo as $value) {
      $number = str_replace('-', '', $value['number']);
      //insert phone number for new branch
      if ($n == 0) {
        $addPhone = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
          phone_fax_cd, lcn_cd, updated_by, last_update)
          VALUES ( '$is_br', '$is_sbr_insert', '$is_ch', '$is_am', '$number',
            '1', '02', '$sessionName', '$todayTime' ) ");
      }
      elseif ($n > 0 && $number != '') {
        $addPhone = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
          phone_fax_cd, lcn_cd, updated_by, last_update)
          VALUES ( '$is_br', '$is_sbr_insert', '$is_ch', '$is_am', '$number',
            '1', '02', '$sessionName', '$todayTime' ) ");
      }
      $n = $n + 1;
    }

    $fax = str_replace('-', '',  $ctrBRInfo['fax']);
                //insert fax number for new branch
    $addFax = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
      phone_fax_cd, lcn_cd, updated_by, last_update)
      VALUES ( '$is_br', '$is_sbr_insert', '$is_ch', '$is_am', '$fax',
        '2', '02', '$sessionName', '$todayTime' ) ");

    $wan = $ctrBRInfo['wan'];
    //insert wan number for new branch
    $addWan = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
      phone_fax_cd, lcn_cd, updated_by, last_update)
      VALUES (
        '$is_br', '$is_sbr_insert', '$is_ch', '$is_am', '$wan',
        '1', '15', '$sessionName', '$todayTime' ) ");

    $amphur_is_br = $odAmphur['is_br'];
    $amphur_is_sbr = $is_sbr_insert;
    $amphur_is_ch = $odAmphur['is_ch'];
    $amphur_is_am = $odAmphur['is_am'];
    $amphur_is_thai_n = $odAmphur['is_thai_n'];
    $amphur_is_eng_n = $odAmphur['is_eng_n'];
    $amphur_is_open_dte = str_replace('-', '',  $odAmphur['is_open_dte']);
    // insert new unit
    $addNewUnit = $this->mssql->query("INSERT INTO od_br_name (is_div, is_br, is_sbr, is_ch, is_am, is_thai_n, is_eng_n,
      is_amp_n, is_grp, is_reg, is_open_dte, is_region, is_poor, is_zone, is_status,
      is_business, is_exp_dte, updated_by, last_update, is_div_group, org_cd, brnch_type, is_code)
      VALUES ( '$is_div', '$amphur_is_br', '$amphur_is_sbr', '$amphur_is_ch', '$amphur_is_am', '$amphur_is_thai_n', '$amphur_is_eng_n',
        '', '000', '00000', '$amphur_is_open_dte', '', '0', '00', '1',
        '', '00000000', '$sessionName', '$todayTime', '', '', '7', '0') ");

    $amphur_addr =  $odAddrAM['addr'];
    $amphur_moo =  $odAddrAM['moo'];
    $amphur_soi =  $odAddrAM['soi'];
    $amphur_street =  $odAddrAM['street'];
    $amphur_subdist =  $odAddrAM['subdist'];
    $amphur_dist =  $odAddrAM['dist'];
    $amphur_prov =  $odAddrAM['prov'];
    $amphur_zip_code =  $odAddrAM['zipcode'];
    // insert new address for new branch
    $addNewUnitAddr = $this->mssql->query("INSERT INTO od_org_addr (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, brnch_act_dept_cd, addr,
      moo, soi, street, subdist, dist, prov, zip_code,
      updated_by, last_update)
      VALUES ( '$amphur_is_br', '$amphur_is_sbr', '$amphur_is_ch', '$amphur_is_am', '$brnch_act_dept_cd', '$amphur_addr',
        '$amphur_moo', '$amphur_soi', '$amphur_street', '$amphur_subdist', '$amphur_dist', '$amphur_prov', '$amphur_zip_code',
        '$sessionName', '$todayTime' ) ");

    $m = 0;
    foreach ($telAMInfo as $value) {
      $numberUnit = str_replace('-', '', $value['number']);
      //insert phone number for new unit
      if ($m == 0) {
        $addPhoneUnit = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
          phone_fax_cd, lcn_cd, updated_by, last_update)
          VALUES ( '$amphur_is_br', '$amphur_is_sbr', '$amphur_is_ch', '$amphur_is_am', '$numberUnit',
            '1', '02', '$sessionName', '$todayTime' ) ");
      }
      elseif ($m > 0 && $numberUnit != '') {
        $addPhoneUnit = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
          phone_fax_cd, lcn_cd, updated_by, last_update)
          VALUES ( '$amphur_is_br', '$amphur_is_sbr', '$amphur_is_ch', '$amphur_is_am', '$numberUnit',
            '1', '02', '$sessionName', '$todayTime' ) ");
      }
      $m = $m + 1;
    }

    $faxUnit = str_replace('-', '',  $ctrAMInfo['fax']);
    //insert fax number for new unit
    $addFaxUnit = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
      phone_fax_cd, lcn_cd, updated_by, last_update)
      VALUES (
        '$amphur_is_br', '$amphur_is_sbr', '$amphur_is_ch', '$amphur_is_am', '$faxUnit',
        '2', '02', '$sessionName', '$todayTime' ) ");

    $wanUnit = $ctrAMInfo['wan'];
    //insert wan number for new unit
    $addWanUnit = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
      phone_fax_cd, lcn_cd, updated_by, last_update)
      VALUES (
        '$amphur_is_br', '$amphur_is_sbr', '$amphur_is_ch', '$amphur_is_am', '$wanUnit',
        '1', '15', '$sessionName', '$todayTime' ) ");




    $tumbon_is_thai_n = $odUnitAmphur['is_thai_n'];
    $tumbon_is_eng_n = $odUnitAmphur['is_eng_n'];

    if ($checkArea == 'true') {
      $tumbon_is_ch = $odUnitAmphur['is_ch'];
      $tumbon_is_am = (int)$odUnitAmphur['is_am'];
      $addNewUnitArea = $this->mssql->query("INSERT INTO od_br_name_tumbon (is_div, is_br, is_sbr,
        is_ch, is_am,
        is_thai_n, is_eng_n,
        is_amp_n, is_grp, is_reg, is_open_dte, is_region, is_poor, is_zone, is_status,
        is_business, is_exp_dte, updated_by, last_update, is_div_group, is_code)
        VALUES ( '$is_div', '$amphur_is_br', '$amphur_is_sbr',
        $tumbon_is_ch, (CASE WHEN $tumbon_is_am <=  9 THEN '0' + CONVERT(VARCHAR, $tumbon_is_am) ELSE CONVERT(VARCHAR, $tumbon_is_am) END),
        '$tumbon_is_thai_n', '$tumbon_is_eng_n',
        '', '000', '00000', '$amphur_is_open_dte', '', '0', '00', '1',
        '', '00000000', '$sessionName', '$todayTime', '', '0' ) ");
      $insert_id_tumbon = $this->mssql->insert_id();
    }
    elseif ($checkArea == 'false') {
      $provcode = $brcodeOld.'A';
      // insert new tumbon
      $addNewUnitArea = $this->mssql->query("INSERT INTO od_br_name_tumbon (is_div, is_br, is_sbr,
        is_ch, is_am,
        is_thai_n, is_eng_n,
        is_amp_n, is_grp, is_reg, is_open_dte, is_region, is_poor, is_zone, is_status,
        is_business, is_exp_dte, updated_by, last_update, is_div_group, is_code)
        SELECT '$is_div', '$amphur_is_br', '$amphur_is_sbr',
        ampcode, (CASE WHEN is_am IS NULL THEN '01' WHEN is_am <  9 THEN '0' + CONVERT(VARCHAR, is_am+1) ELSE CONVERT(VARCHAR, is_am+1) END),
        '$tumbon_is_thai_n', '$tumbon_is_eng_n',
        '', '000', '00000', '$amphur_is_open_dte', '', '0', '00', '1',
        '', '00000000', '$sessionName', '$todayTime', '', '0'
        FROM short_name_branch
        LEFT JOIN (SELECT is_ch, MAX(is_am) is_am FROM od_br_name_tumbon GROUP BY is_ch) AS MX ON ampcode = is_ch
        WHERE brcode = '$provcode' ");


      $insert_id_tumbon = $this->mssql->insert_id();
    }

    $querySBR = "SELECT is_ch, is_am  FROM od_br_name_tumbon  WHERE item_no = $insert_id_tumbon ";
    $query = $this->mssql->query($querySBR)->result_array();
    $tumbon_is_ch = $query[0]['is_ch'];
    $tumbon_is_am = $query[0]['is_am'];
    $tumbon_is_offset = $tumbon_is_ch.$tumbon_is_am;

    $addMapUnitArea = $this->mssql->query("INSERT INTO od_link_br_tumbon (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, fldoff_dist_cd, updated_by, last_update)
      VALUES ('$amphur_is_br', '$amphur_is_sbr', '$amphur_is_ch', '$amphur_is_am', '$tumbon_is_offset', '$sessionName', '$todayTime') ");


    if (sizeof($odArea) > 0) {
      for ($x = 0; $x < sizeof($odArea); $x++) {
        $cat_cc = $odArea[$x]['cat_cc'];
        $cat_aa = $odArea[$x]['cat_aa'];
        $addMainArea = $this->mssql->query("INSERT INTO bmc_tumbon (is_br, is_sbr, is_ch, is_am, is_thai_n,
          cat_cc, cat_aa, cat_tt, cat_mm, cat_desc)
          SELECT TOP 1 '$amphur_is_br', '$amphur_is_sbr', '$tumbon_is_ch', '$tumbon_is_am', '$tumbon_is_thai_n',
          CAT_CC, CAT_AA, '00', '00', REPLACE(DISTRICT, 'อำเภอ', '')
          FROM ADDRESS_CODE
          WHERE CAT_CC = '$cat_cc' AND CAT_AA = '$cat_aa' ");

          if( sizeof($odArea[$x]['cat_tt']) > 0) {

            for ($y = 0; $y < sizeof($odArea[$x]['cat_tt']); $y++) {

              $cat_tt = $odArea[$x]['cat_tt'][$y]['code'];
              $select = $odArea[$x]['cat_tt'][$y]['tumbon'];
              $cancel = $odArea[$x]['cat_tt'][$y]['cancel'];
              if($select == 'true') {

                if ($cancel == 'true') { //ลบตำบล หากเลือกยกเลิกจากพื้นที่เก่า
                  $removeUnitArea = $this->mssql->query("DELETE FROM bmc_tumbon WHERE CAT_CC = '$cat_cc' AND CAT_AA = '$cat_aa' AND CAT_TT = '$cat_tt' ");
                }
                $addSubArea = $this->mssql->query("INSERT INTO bmc_tumbon (is_br, is_sbr, is_ch, is_am, is_thai_n,
                  cat_cc, cat_aa, cat_tt, cat_mm, cat_desc)
                  SELECT TOP 1 '$amphur_is_br', '$amphur_is_sbr', '$tumbon_is_ch', '$tumbon_is_am', '$tumbon_is_thai_n',
                  CAT_CC, CAT_AA, CAT_TT, '00', SUB_DISTRICT
                  FROM ADDRESS_CODE
                  WHERE CAT_CC = '$cat_cc' AND CAT_AA = '$cat_aa' AND CAT_TT = '$cat_tt' ");
                } //end if select

              } //end for

            } // end if
          } // end for
        } //end if

      $brcode =  $is_br.$is_sbr_insert;
      $divprofile = '';
      if($is_div == '10')
        $divprofile = '80018';
      else {
        $divprofile = '800'.$is_div;
      }

      $is_thai = 'สาขา'.$is_thai_n;
      if ($is_sbr_insert == '0') {
        $addMapProfile = $this->mssql->query("INSERT INTO tb_brmapprofile (brcode, profilecode, acccode, atmcode,brname, div, divprofile)
          SELECT TOP 1 '$brcode', (SELECT SUBSTRING(org_cd, 1, len(org_cd)-1) FROM od_br_name  WHERE is_br = '$is_br' AND is_sbr = 'A'), '',
           (SELECT SUBSTRING(org_cd, 1, len(org_cd)-1) FROM od_br_name  WHERE is_br = '$is_br' AND is_sbr = 'A'), '$is_thai', '$is_div', '$divprofile' ");
      }
      else {
        $addMapProfile = $this->mssql->query("INSERT INTO tb_brmapprofile (brcode, profilecode, acccode, atmcode,brname, div, divprofile)
        SELECT '$brcode', MAX(CONVERT(INT, REPLACE(org_cd, 'จ', '')))+1, '', MAX(CONVERT(INT, REPLACE(org_cd, 'จ', '')))+1, '$is_thai', '$is_div', '$divprofile'
        FROM od_br_name WHERE org_cd != '' ");
      }


      $updateORGCD = $this->mssql->query("UPDATE od_br_name SET org_cd = br.profilecode
          FROM (SELECT profilecode FROM tb_brmapprofile WHERE brcode = '$brcode') AS br
          WHERE is_br+is_sbr+is_ch+is_am = '$br_code_new' ");

      $division_code = '48'.$is_br.$is_sbr_insert.$is_ch.$is_am;
      $div_mn_name = $odInfo['prov_nme'];
      $div_mn_as = $odInfo['prov_as'];
      $div_mn_code = '48'.$is_br.'A'.$is_ch.$is_am;
      $cost_name = 'ประจำ'.$is_thai;

      $addDivision = $this->mssql->query("INSERT INTO Division (DIVISION_CODE, DIVISION_NAME, DIVISION_AS,
                      DIVISION_MN_CODE, DIVISION_MN_NAME, DIVISION_MN_AS,
                      COST_CODE, COST_NAME, DIVISION_AREA)
                      VALUES ( '$division_code', '$is_thai', '$is_thai',
                               '$div_mn_code', '$div_mn_name', '$div_mn_as',
                               '', '$cost_name', '$is_div') ");


      if($addNewBranch && $addNewBranchAddr && $addPhone && $addFax && $addWan && $addNewUnit && $addNewUnitAddr && $addPhoneUnit && $addFaxUnit && $addWanUnit && $addNewUnitArea && $addMapUnitArea && $addMapProfile && $addDivision){
        $result = "Successfully";
      }else{
        $result = "Failed";
      }

      $this->mssql->close();
      return $result;

  }


}
?>
