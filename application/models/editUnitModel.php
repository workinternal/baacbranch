<?php
class editUnitModel extends CI_Model
{
  var $mssql;
  public function __construct()
  {
    parent::__construct();
    //use DT database connection
    $this->mssql = $this->load->database ( 'DT', TRUE );
  }



  public function getListProvince () {
    $queryStr = "SELECT PROVINCE, CAT_CC FROM ADDRESS_CODE GROUP BY PROVINCE, CAT_CC ORDER BY PROVINCE";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['CAT_CC'];
        $resultbuff['code'] = $row['CAT_CC'];
        $resultbuff['name'] = $row['PROVINCE'];
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getListAmphur ($provinceID) {
    $queryStr = "SELECT REPLACE(DISTRICT, 'อำเภอ', '') DISTRICT, CAT_AA FROM ADDRESS_CODE WHERE CAT_CC = '$provinceID' GROUP BY DISTRICT, CAT_AA ORDER BY CAT_AA";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['CAT_AA'];
        $resultbuff['code'] = $row['CAT_AA'];
        $resultbuff['name'] = $row['DISTRICT'];

        array_push($result, $resultbuff);
      }
      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getListDistrict ($provinceID, $amphurID) {
    $queryStr = "SELECT SUB_DISTRICT, CAT_TT, POSTCODE FROM ADDRESS_CODE WHERE CAT_CC = '$provinceID' AND CAT_AA = '$amphurID' GROUP BY SUB_DISTRICT, CAT_TT, POSTCODE  ORDER BY CAT_TT";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['CAT_TT'];
        $resultbuff['code'] = $row['CAT_TT'];
        $resultbuff['name'] = $row['SUB_DISTRICT'];
        $resultbuff['postcode'] = $row['POSTCODE'];
        array_push($result, $resultbuff);
      }
      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getUnitBranchInformation ($id) {
    $queryStr = "SELECT DIVNME.brnch_act_dept_nme AS div_nme, BRNME.brname, ORGNME.is_div, ORGNME.is_br, ORGNME.is_sbr, ORGNME.is_ch, ORGNME.is_am
                , ORGNME.is_thai_n, ORGNME.is_eng_n, ORGNME.is_amp_n, ORGNME.is_open_dte
                , ORGADDR.addr, ORGADDR.moo, ORGADDR.soi, ORGADDR.street, ORGADDR.subdist, ORGADDR.dist, ORGADDR.prov, ORGADDR.zip_code
                , ORGNMETB.is_ch AS TB_is_ch, ORGNMETB.is_am AS TB_is_am, ORGNMETB.is_thai_n AS TB_is_thai_n, ORGNMETB.is_eng_n AS TB_is_eng_n
                , ADDR.CAT_CC, ADDR.CAT_AA, ADDR.CAT_TT
                FROM od_br_name  AS ORGNME
                LEFT JOIN od_org_addr AS ORGADDR ON ORGNME.is_br = ORGADDR.ind_brnch_cd AND ORGNME.is_sbr = ORGADDR.dept_cd
                JOIN od_brnch_act_dept AS DIVNME ON CONVERT(INT, ORGNME.is_div) = CONVERT(INT, DIVNME.brnch_act_dept_cd)
                JOIN  tb_brmapprofile AS BRNME ON (ORGNME.is_br + 'A') = brcode
                LEFT JOIN od_link_br_tumbon LNK ON ORGNME.is_br = LNK.ind_brnch_cd AND ORGNME.is_sbr = LNK.dept_cd AND ORGNME.is_ch = LNK.div_cd AND ORGNME.is_am = LNK.subdiv_cd
                JOIN od_br_name_tumbon AS ORGNMETB ON ORGNME.is_br = ORGNMETB.is_br AND ORGNME.is_sbr = ORGNMETB.is_sbr AND LNK.fldoff_dist_cd = (ORGNMETB.is_ch + ORGNMETB.is_am)
                AND ORGNME.is_ch = ORGADDR.div_cd
                JOIN ADDRESS_CODE AS ADDR ON ORGADDR.prov = ADDR.PROVINCE AND ORGADDR.dist = ADDR.DISTRICT AND ORGADDR.subdist = ADDR.SUB_DISTRICT
                WHERE ORGNME.is_br + ORGNME.is_sbr + ORGNME.is_ch + ORGNME.is_am = '$id'";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['div_nme'] = $row['div_nme'];
        $resultbuff['brname'] = $row['brname'];
        $resultbuff['is_div'] = $row['is_div'];
        $resultbuff['is_br'] = $row['is_br'];
        $resultbuff['is_sbr'] = $row['is_sbr'];
        $resultbuff['is_ch'] = $row['is_ch'];
        $resultbuff['is_am'] = $row['is_am'];
        $resultbuff['is_thai_n'] = $row['is_thai_n'];
        $resultbuff['is_eng_n'] = $row['is_eng_n'];
        $resultbuff['is_amp_n'] = $row['is_amp_n'];
        $resultbuff['is_open_dte'] = $row['is_open_dte'];
        $resultbuff['addr'] = $row['addr'];
        $resultbuff['moo'] = $row['moo'];
        $resultbuff['soi'] = $row['soi'];
        $resultbuff['street'] = $row['street'];
        $resultbuff['subdist'] = $row['subdist'];
        $resultbuff['dist'] = $row['dist'];
        $resultbuff['prov'] = $row['prov'];
        $resultbuff['zip_code'] = $row['zip_code'];
        $resultbuff['cat_cc'] = $row['CAT_CC'];
        $resultbuff['cat_aa'] = $row['CAT_AA'];
        $resultbuff['cat_tt'] = $row['CAT_TT'];
        $resultbuff['TB_is_ch'] = $row['TB_is_ch'];
        $resultbuff['TB_is_am'] = $row['TB_is_am'];
        $resultbuff['TB_is_thai_n'] = $row['TB_is_thai_n'];
        $resultbuff['TB_is_eng_n'] = $row['TB_is_eng_n'];
        $is_br = $row['is_br'];
        $is_sbr = $row['is_sbr'];
        $is_ch = $row['TB_is_ch'];
        $is_am = $row['TB_is_am'];

        $resultbuff['telephone'] = array();
        $queryTel = "SELECT number FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' AND lcn_cd = '02' AND phone_fax_cd = '1' ";
        $query = $this->mssql->query($queryTel)->result_array();
        $tempTel = array();
        foreach ($query as $row) {
          $tempTel['number'] = $row['number'];

          array_push($resultbuff['telephone'], $tempTel);
        }
        $resultbuff['fax'] = "";
        $queryFax = "SELECT number FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' AND lcn_cd = '02' AND phone_fax_cd = '2' ";
        $query = $this->mssql->query($queryFax)->result_array();
        $tempTel = array();
        foreach ($query as $row) {
          $resultbuff['fax'] = $row['number'];
        }
        $resultbuff['wan'] = "";
        $queryFax = "SELECT number FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' AND lcn_cd = '15' AND phone_fax_cd = '1' ";
        $query = $this->mssql->query($queryFax)->result_array();
        $tempTel = array();
        foreach ($query as $row) {
          $resultbuff['wan'] = $row['number'];
        }

        $resultbuff['area'] = array();
        $queryArea = "SELECT is_thai_n, cat_cc AS prov_code, cat_aa AS dist_code
        , cat_tt AS subdist_code, cat_desc
        FROM  bmc_tumbon
        WHERE is_br = '$is_br' AND is_sbr = '$is_sbr' AND is_ch = '$is_ch' AND is_am = '$is_am' AND cat_tt = '00' AND cat_mm = '00'";
        $query = $this->mssql->query($queryArea)->result_array();
        $tempArea = array();
        foreach ($query as $row) {
          $tempArea['cat_name'] = $row['cat_desc'];
          $tempArea['cat_cc'] = $row['prov_code'];
          $tempArea['cat_aa'] = $row['dist_code'];
          $tempArea['cat_tt'] = array();
          $cat_pro = $row['prov_code'];
          $cat_amp = $row['dist_code'];
          $querySubArea = "SELECT cat_tt FROM  bmc_tumbon WHERE is_br = '$is_br' AND is_sbr = '$is_sbr' AND is_ch = '$is_ch' AND is_am = '$is_am'
          AND cat_cc = '$cat_pro' AND cat_aa = '$cat_amp'";
          $querySub = $this->mssql->query($querySubArea)->result_array();
          $tempCat = array();
          foreach ($querySub as $data) {
            $tempCat['code'] = $data['cat_tt'];
            array_push($tempArea['cat_tt'], $tempCat);
          }

          array_push($resultbuff['area'], $tempArea);
        }

        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getListBranchName ($id) {
    $br = substr($id, 0, -1);
    $queryStr = "SELECT is_br + is_sbr AS id,
                is_br + '-' + is_sbr + '-' +is_ch + '-' +is_am AS branch, 'สาขา' + is_thai_n AS name
                  FROM od_br_name WHERE is_br = '$br' AND brnch_type = '2' AND is_status = '1' ORDER BY is_br, is_sbr, is_ch, is_am";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['id'];
        $resultbuff['branch'] = $row['branch'];
        $resultbuff['name'] = $row['name'];
        $resultbuff['display_name'] = $row['name'].' : '.$row['branch'];
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getListUnitBranchName ($id) {
    $queryStr = "SELECT is_br + is_sbr + is_ch + is_am AS id,
                is_br + '-' + is_sbr + '-' +is_ch + '-' +is_am AS branch, 'หน่วยอำเภอ' + is_thai_n AS name
                  FROM od_br_name WHERE is_br + is_sbr = '$id' AND brnch_type = '7' AND is_status = '1'  ORDER BY is_br, is_sbr, is_ch, is_am ";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['id'];
        $resultbuff['unitbranch'] = $row['branch'];
        $resultbuff['name'] = $row['name'];
        $resultbuff['display_name'] = $row['name'].' : '.$row['branch'];
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function updateUnitBranchInformation($odInfo, $odAddrBR, $telInfo, $ctrInfo, $odUnitAmphur, $odArea) {
      $sessionName = $this->session->userdata('od_emp_code');
      $todayTime = date("Y-m-d");
      $id =  $odInfo['is_br'].$odInfo['is_sbr'].$odInfo['is_ch'].$odInfo['is_am'];

      $is_thai_n = $odInfo['is_thai_n'];
      $is_eng_n = $odInfo['is_eng_n'];
      $is_amp_n = "";
      $is_open_dte = str_replace('-', '', $odInfo['is_open_dte']);

      $updateAM = $this->mssql->query("UPDATE od_br_name
        SET
        is_thai_n = '$is_thai_n',
        is_eng_n = '$is_eng_n',
        is_amp_n = '$is_amp_n',
        is_open_dte = '$is_open_dte',
        updated_by = '$sessionName',
        last_update = '$todayTime'
        WHERE is_br + is_sbr + is_ch + is_am = '$id'");

      $addr = $odAddrBR['addr'];
      $moo = $odAddrBR['moo'];
      $soi = $odAddrBR['soi'];
      $street = $odAddrBR['street'];
      $subdist = str_replace(' ', '', $odAddrBR['subdist']);
      $dist = str_replace('-', ' ', $odAddrBR['dist']);
      $prov = str_replace(' ', '', $odAddrBR['prov']);
      $zip_code = $odAddrBR['zipcode'];


      $updateAddr = $this->mssql->query("UPDATE od_org_addr
        SET
        addr = '$addr',
        moo = '$moo',
        soi = '$soi',
        street = '$street',
        subdist = '$subdist',
        dist = '$dist',
        prov = '$prov',
        zip_code = '$zip_code',
        updated_by = '$sessionName',
        last_update = '$todayTime'
        WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id'");

      $ind_brnch_cd = $odInfo['is_br'];
      $dept_cd = $odInfo['is_sbr'];
      $div_cd = $odInfo['is_ch'];
      $subdiv_cd = $odInfo['is_am'];
      $removePhone = $this->mssql->query("DELETE FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' ");
      $n = 0;
      foreach ($telInfo as $value) {
        $number = str_replace('-', '', $value['number']);
        if ($n == 0) {
          $addPhone = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
                                    phone_fax_cd, lcn_cd, updated_by, last_update)
                                    VALUES ( '$ind_brnch_cd', '$dept_cd', '$div_cd', '$subdiv_cd', '$number',
                                      '1', '02', '$sessionName', '$todayTime' ) ");
        }
        elseif ($n > 0 && $number != '') {
          $addPhone = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
                                    phone_fax_cd, lcn_cd, updated_by, last_update)
                                    VALUES ( '$ind_brnch_cd', '$dept_cd', '$div_cd', '$subdiv_cd', '$number',
                                      '1', '02', '$sessionName', '$todayTime' ) ");
        }
        $n = $n + 1;
      }
      $fax = str_replace('-', '',  $ctrInfo['fax']);
      $wan = $ctrInfo['wan'];
      $addFax = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
                                phone_fax_cd, lcn_cd, updated_by, last_update)
                                VALUES (
                                  '$ind_brnch_cd', '$dept_cd', '$div_cd', '$subdiv_cd', '$fax',
                                  '2', '02', '$sessionName', '$todayTime' ) ");

      $addWan = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
                                phone_fax_cd, lcn_cd, updated_by, last_update)
                                VALUES (
                                  '$ind_brnch_cd', '$dept_cd', '$div_cd', '$subdiv_cd', '$wan',
                                  '1', '15', '$sessionName', '$todayTime' ) ");

      $is_thai_n_area = $odUnitAmphur['is_thai_n'];
      $is_eng_n_area = $odUnitAmphur['is_eng_n'];
      $temp_area = str_replace(' - ', '', $odUnitAmphur['code']);
      $is_id_area = $odInfo['is_br'].$odInfo['is_sbr'].$temp_area;
      $updateUnitArea = $this->mssql->query("UPDATE od_br_name_tumbon
        SET
        is_thai_n = '$is_thai_n_area',
        is_eng_n = '$is_eng_n_area',
        is_open_dte = '$is_open_dte',
        updated_by = '$sessionName',
        last_update = '$todayTime'
        WHERE is_br + is_sbr + is_ch + is_am = '$is_id_area'");

      $removeUnitArea = $this->mssql->query("DELETE FROM bmc_tumbon WHERE is_br + is_sbr + is_ch + is_am = '$is_id_area' ");
      $amphur_is_br = $odInfo['is_br'];
      $amphur_is_sbr = $odInfo['is_sbr'];
      $tumbon_is_ch = $odUnitAmphur['is_ch'];
      $tumbon_is_am = $odUnitAmphur['is_am'];

      if (sizeof($odArea) > 0) {
        for ($x = 0; $x < sizeof($odArea); $x++) {
          $cat_cc = $odArea[$x]['cat_cc'];
          $cat_aa = $odArea[$x]['cat_aa'];
          $addMainArea = $this->mssql->query("INSERT INTO bmc_tumbon (is_br, is_sbr, is_ch, is_am, is_thai_n,
            cat_cc, cat_aa, cat_tt, cat_mm, cat_desc)
            SELECT TOP 1 '$amphur_is_br', '$amphur_is_sbr', '$tumbon_is_ch', '$tumbon_is_am', '$is_thai_n_area',
            CAT_CC, CAT_AA, '00', '00', REPLACE(DISTRICT, 'อำเภอ', '')
            FROM ADDRESS_CODE
            WHERE CAT_CC = '$cat_cc' AND CAT_AA = '$cat_aa' ");

            if( sizeof($odArea[$x]['cat_tt']) > 0) {

              for ($y = 0; $y < sizeof($odArea[$x]['cat_tt']); $y++) {

                $cat_tt = $odArea[$x]['cat_tt'][$y]['code'];
                $select = $odArea[$x]['cat_tt'][$y]['tumbon'];
                $cancel = $odArea[$x]['cat_tt'][$y]['cancel'];

                if($select == 'true') {

                  if ($cancel == 'true') { //ลบตำบล หากเลือกยกเลิกจากพื้นที่เก่า
                    $removeUnitArea = $this->mssql->query("DELETE FROM bmc_tumbon WHERE CAT_CC = '$cat_cc' AND CAT_AA = '$cat_aa' AND CAT_TT = '$cat_tt' ");
                  }

                  $addSubArea = $this->mssql->query("INSERT INTO bmc_tumbon (is_br, is_sbr, is_ch, is_am, is_thai_n,
                    cat_cc, cat_aa, cat_tt, cat_mm, cat_desc)
                    SELECT TOP 1 '$amphur_is_br', '$amphur_is_sbr', '$tumbon_is_ch', '$tumbon_is_am', '$is_thai_n_area',
                    CAT_CC, CAT_AA, CAT_TT, '00', SUB_DISTRICT
                    FROM ADDRESS_CODE
                    WHERE CAT_CC = '$cat_cc' AND CAT_AA = '$cat_aa' AND CAT_TT = '$cat_tt' ");
                }//end if select

              } //end for
            } // end if
          } // end for
        } //end if

        $result = "";
        if($updateAM && $updateAddr && $addPhone && $addFax && $addWan && $updateUnitArea){
          $result = "Successfully";
        }else{
          $result = "Failed";
        }

        $this->mssql->close();
        return $result;
    }

  public function getUnitBranchInformationforClose ($id) {
      $queryStr = "SELECT DIV.DIVISION_CODE, DIVNME.brnch_act_dept_nme AS div_nme, BRNME.brname
                  , HEADNME.is_br + ' - ' + HEADNME.is_sbr + ' - ' + HEADNME.is_ch + ' - ' + HEADNME.is_am AS br_code
                  , HEADNME.is_thai_n AS br_th_name, HEADNME.is_eng_n AS br_en_name, HEADNME.is_amp_n AS br_sht_name
                  , ORGNME.is_div, ORGNME.is_br, ORGNME.is_sbr, ORGNME.is_ch, ORGNME.is_am
                  , ORGNME.is_thai_n, ORGNME.is_eng_n, ORGNME.is_amp_n, ORGNME.is_open_dte
                  , ORGADDR.addr, ORGADDR.moo, ORGADDR.soi, ORGADDR.street, ORGADDR.subdist, ORGADDR.dist, ORGADDR.prov, ORGADDR.zip_code
                  , ORGNMETB.is_ch AS TB_is_ch, ORGNMETB.is_am AS TB_is_am, ORGNMETB.is_thai_n AS TB_is_thai_n, ORGNMETB.is_eng_n AS TB_is_eng_n
                  FROM od_br_name  AS ORGNME
                  LEFT JOIN od_org_addr AS ORGADDR ON ORGNME.is_br = ORGADDR.ind_brnch_cd AND ORGNME.is_sbr = ORGADDR.dept_cd
                  JOIN od_brnch_act_dept AS DIVNME ON CONVERT(INT, ORGNME.is_div) = CONVERT(INT, DIVNME.brnch_act_dept_cd)
                  JOIN  tb_brmapprofile AS BRNME ON (ORGNME.is_br + 'A') = brcode
                  LEFT JOIN od_link_br_tumbon LNK ON ORGNME.is_br = LNK.ind_brnch_cd AND ORGNME.is_sbr = LNK.dept_cd AND ORGNME.is_ch = LNK.div_cd AND ORGNME.is_am = LNK.subdiv_cd
                  LEFT JOIN od_br_name_tumbon AS ORGNMETB ON ORGNME.is_br = ORGNMETB.is_br AND ORGNME.is_sbr = ORGNMETB.is_sbr AND LNK.fldoff_dist_cd = (ORGNMETB.is_ch + ORGNMETB.is_am)
                  LEFT JOIN Division AS DIV ON ('48' + ORGNME.is_br + ORGNME.is_sbr + '0000') = DIV.DIVISION_CODE
                  JOIN od_br_name AS HEADNME ON ( ORGNME.is_br+ORGNME.is_sbr+'0000' ) = (HEADNME.is_br + HEADNME.is_sbr + HEADNME.is_ch + HEADNME.is_am)
                  AND ORGNME.is_ch = ORGADDR.div_cd WHERE ORGNME.is_br + ORGNME.is_sbr + ORGNME.is_ch + ORGNME.is_am = '$id'";
        $query = $this->mssql->query($queryStr)->result_array();
        $result = array();
        $resultbuff = array();
        foreach ($query as $row) {
            $resultbuff['div_code'] = $row['DIVISION_CODE'];
            $resultbuff['br_code'] = $row['br_code'];
            $resultbuff['br_th_name'] = $row['br_th_name'];
            $resultbuff['br_en_name'] = $row['br_en_name'];
            $resultbuff['br_sht_name'] = $row['br_sht_name'];
            $resultbuff['div_nme'] = $row['div_nme'];
            $resultbuff['brname'] = $row['brname'];
            $resultbuff['is_div'] = $row['is_div'];
            $resultbuff['is_br'] = $row['is_br'];
            $resultbuff['is_sbr'] = $row['is_sbr'];
            $resultbuff['is_ch'] = $row['is_ch'];
            $resultbuff['is_am'] = $row['is_am'];
            $resultbuff['is_thai_n'] = $row['is_thai_n'];
            $resultbuff['is_eng_n'] = $row['is_eng_n'];
            $resultbuff['is_amp_n'] = $row['is_amp_n'];
            $resultbuff['is_open_dte'] = $row['is_open_dte'];
            $resultbuff['addr'] = $row['addr'];
            $resultbuff['moo'] = $row['moo'];
            $resultbuff['soi'] = $row['soi'];
            $resultbuff['street'] = $row['street'];
            $resultbuff['subdist'] = $row['subdist'];
            $resultbuff['dist'] = $row['dist'];
            $resultbuff['prov'] = $row['prov'];
            $resultbuff['zip_code'] = $row['zip_code'];
            $resultbuff['TB_is_ch'] = $row['TB_is_ch'];
            $resultbuff['TB_is_am'] = $row['TB_is_am'];
            $resultbuff['TB_is_thai_n'] = $row['TB_is_thai_n'];
            $resultbuff['TB_is_eng_n'] = $row['TB_is_eng_n'];
            $is_br = $row['is_br'];
            $is_sbr = $row['is_sbr'];
            $is_ch = $row['TB_is_ch'];
            $is_am = $row['TB_is_am'];

            $resultbuff['telephone'] = array();
            $queryTel = "SELECT number FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' AND lcn_cd = '02' AND phone_fax_cd = '1' ";
            $query = $this->mssql->query($queryTel)->result_array();
            $tempTel = array();
            foreach ($query as $row) {
              $tempTel['number'] = $row['number'];

              array_push($resultbuff['telephone'], $tempTel);
            }
            $resultbuff['fax'] = "";
            $queryFax = "SELECT number FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' AND lcn_cd = '02' AND phone_fax_cd = '2' ";
            $query = $this->mssql->query($queryFax)->result_array();
            $tempTel = array();
            foreach ($query as $row) {
              $resultbuff['fax'] = $row['number'];
            }
            $resultbuff['wan'] = "";
            $queryFax = "SELECT number FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' AND lcn_cd = '15' AND phone_fax_cd = '1' ";
            $query = $this->mssql->query($queryFax)->result_array();
            $tempTel = array();
            foreach ($query as $row) {
              $resultbuff['wan'] = $row['number'];
            }

            $resultbuff['area'] = array();
            $queryArea = "SELECT BMC.is_br, BMC.is_sbr, BMC.is_ch, BMC.is_am, BMC.is_thai_n,
            TMB.cat_cc, TMB.cat_aa, TMB.cat_tt, TMB.cat_mm, BMC.cat_desc, TMB.cat_desc AS subdist_code
            FROM  bmc_tumbon BMC
            LEFT JOIN (SELECT * FROM bmc_tumbon WHERE cat_tt != '00') TMB ON
            BMC.is_br = TMB.is_br AND BMC.is_sbr = TMB.is_sbr AND BMC.is_ch = TMB.is_ch AND BMC.is_am = TMB.is_am AND BMC.cat_cc = TMB.cat_cc
            AND BMC.cat_aa = TMB.cat_aa
            WHERE BMC.is_br = '$is_br' AND BMC.is_sbr = '$is_sbr' AND BMC.is_ch = '$is_ch' AND BMC.is_am = '$is_am' AND BMC.cat_tt = '00'
            ORDER BY BMC.cat_cc, BMC.cat_aa, BMC.cat_tt, BMC.cat_mm ";
            $query = $this->mssql->query($queryArea)->result_array();
            $tempArea = array();
            foreach ($query as $row) {
              $tempArea['cat_amp'] = $row['cat_desc'];
              $tempArea['cat_name'] = $row['subdist_code'];
              $tempArea['cat_cc'] = $row['cat_cc'];
              $tempArea['cat_aa'] = $row['cat_aa'];
              $tempArea['cat_tt'] = $row['cat_tt'];
              array_push($resultbuff['area'], $tempArea);
            }

            array_push($result, $resultbuff);
        }

        $this->mssql->close();
        return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function closeUnitBranch($id) {
      $sessionName = $this->session->userdata('od_emp_code');
      $todayTime = date("Y-m-d");

      $updateBR = $this->mssql->query("UPDATE od_br_name
        SET
        is_status = '9',
        updated_by = '$sessionName',
        last_update = '$todayTime'
        WHERE is_br + is_sbr + is_ch + is_am = '$id'");


        $result = "";
        if($updateBR){
          $result = "Successfully";
        }else{
          $result = "Failed";
        }

        $this->mssql->close();
        return $result;
  }






}
?>
