<?php
class createUnitBranchModel extends CI_Model
{
  var $mssql;
  public function __construct()
  {
    parent::__construct();
    //use DT database connection
    $this->mssql = $this->load->database ( 'DT', TRUE );
  }



  public function getListProvince () {
    $queryStr = "SELECT PROVINCE, CAT_CC FROM ADDRESS_CODE GROUP BY PROVINCE, CAT_CC ORDER BY PROVINCE";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['CAT_CC'];
        $resultbuff['code'] = $row['CAT_CC'];
        $resultbuff['name'] = $row['PROVINCE'];
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getListAmphur ($provinceID) {
    $queryStr = "SELECT REPLACE(DISTRICT, 'อำเภอ', '') DISTRICT, CAT_AA FROM ADDRESS_CODE WHERE CAT_CC = '$provinceID' GROUP BY DISTRICT, CAT_AA ORDER BY CAT_AA";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['CAT_AA'];
        $resultbuff['code'] = $row['CAT_AA'];
        $resultbuff['name'] = $row['DISTRICT'];

        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getListDistrict ($provinceID, $amphurID) {
    $queryStr = "SELECT SUB_DISTRICT, CAT_TT, POSTCODE FROM ADDRESS_CODE WHERE CAT_CC = '$provinceID' AND CAT_AA = '$amphurID' GROUP BY SUB_DISTRICT, CAT_TT, POSTCODE  ORDER BY CAT_TT";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['CAT_TT'];
        $resultbuff['code'] = $row['CAT_TT'];
        $resultbuff['name'] = $row['SUB_DISTRICT'];
        $resultbuff['postcode'] = $row['POSTCODE'];
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getUnitBranchInformation ($id) {
    $queryStr = "SELECT DIVNME.brnch_act_dept_nme AS div_nme, BRNME.brname, ORGNME.is_div, ORGNME.is_br, ORGNME.is_sbr, ORGNME.is_ch, ORGNME.is_am,
        ORGNME.is_thai_n, ORGNME.is_eng_n, ORGNME.is_amp_n, ORGNME.is_open_dte, ADDR.CAT_CC
        FROM od_br_name  AS ORGNME
        LEFT JOIN od_brnch_act_dept AS DIVNME ON CONVERT(INT, ORGNME.is_div) = CONVERT(INT, DIVNME.brnch_act_dept_cd)
        LEFT JOIN  tb_brmapprofile AS BRNME ON (ORGNME.is_br + 'A') = brcode
        LEFT JOIN ( SELECT PROVINCE, CAT_CC FROM ADDRESS_CODE GROUP BY PROVINCE,CAT_CC ) ADDR ON REPLACE(BRNME.brname, 'สำนักงาน ธ.ก.ส.จังหวัด', '') = ADDR.PROVINCE
        WHERE ORGNME.is_br + ORGNME.is_sbr + ORGNME.is_ch + ORGNME.is_am = '$id'";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['div_nme'] = $row['div_nme'];
        $resultbuff['brname'] = $row['brname'];
        $resultbuff['is_div'] = $row['is_div'];
        $resultbuff['is_br'] = $row['is_br'];
        $resultbuff['is_sbr'] = $row['is_sbr'];
        $resultbuff['is_ch'] = $row['is_ch'];
        $resultbuff['is_am'] = $row['is_am'];
        $resultbuff['is_thai_n'] = $row['is_thai_n'];
        $resultbuff['is_eng_n'] = $row['is_eng_n'];
        $resultbuff['is_amp_n'] = $row['is_amp_n'];
        $resultbuff['is_open_dte'] = $row['is_open_dte'];
        $resultbuff['cat_cc'] = $row['CAT_CC'];
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function getListBranchName () {
    $queryStr = "SELECT is_br + is_sbr + is_ch + is_am AS id,
                is_br + '-' + is_sbr + '-' +is_ch + '-' +is_am AS branch, 'สาขา' + is_thai_n AS name
                  FROM od_br_name WHERE brnch_type = '2'";
      $query = $this->mssql->query($queryStr)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['id'] = $row['id'];
        $resultbuff['branch'] = $row['branch'];
        $resultbuff['name'] = $row['name'];
        $resultbuff['display_name'] = $row['name'].' : '.$row['branch'];
        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function createUnitBranchInformation($odInfo, $odAmphur, $odAddrAM, $telInfo, $ctrInfo) {
    $sessionName = $this->session->userdata('od_emp_code');
    $todayTime = date("Y-m-d");

    $brnch_act_dept_cd = $odInfo['is_div'];
    if ($brnch_act_dept_cd == '10') {
      $is_div = $odInfo['is_div'];
    }
    else {
      $is_div = str_replace('0', '', $odInfo['is_div']);
    }
    $amphur_is_thai_n = $odAmphur['is_thai_n'];
    $amphur_is_eng_n  = $odAmphur['is_eng_n'];
    $amphur_is_open_dte      = str_replace('-', '', $odAmphur['is_open_dte']);
    $amphur_is_br     = $odInfo['is_br'];
    $amphur_is_sbr    = $odInfo['is_sbr'];

    $string = "INSERT INTO od_br_name (is_div, is_br, is_sbr,
      is_ch, is_am,
      is_thai_n, is_eng_n,
      is_amp_n, is_grp, is_reg, is_open_dte, is_region, is_poor, is_zone, is_status,
      is_business, is_exp_dte, updated_by, last_update, is_div_group, org_cd, brnch_type, is_code)
      SELECT TOP 1 '$is_div', '$amphur_is_br', '$amphur_is_sbr',
      (CASE WHEN is_ch+1 < 10 THEN '0' + CONVERT(varchar,is_ch+1) ELSE CONVERT(varchar,is_ch+1) END), '00',
      '$amphur_is_thai_n', '$amphur_is_eng_n',
      '', '000', '00000', '$amphur_is_open_dte', '', '0', '00', '1',
      '', '00000000', '$sessionName', '$todayTime', '', '', 'a', '0'
      FROM od_br_name od
      WHERE od.is_br = '$amphur_is_br' AND od.is_sbr = '$amphur_is_sbr'
      ORDER BY od.is_ch DESC";
    // insert new unit
    $addNewUnit = $this->mssql->query("INSERT INTO od_br_name (is_div, is_br, is_sbr,
      is_ch, is_am,
      is_thai_n, is_eng_n,
      is_amp_n, is_grp, is_reg, is_open_dte, is_region, is_poor, is_zone, is_status,
      is_business, is_exp_dte, updated_by, last_update, is_div_group, org_cd, brnch_type, is_code)
      SELECT TOP 1 '$is_div', '$amphur_is_br', '$amphur_is_sbr',
      (CASE WHEN is_ch+1 < 10 THEN '0' + CONVERT(varchar,is_ch+1) ELSE CONVERT(varchar,is_ch+1) END), '00',
      '$amphur_is_thai_n', '$amphur_is_eng_n',
      '', '000', '00000', '$amphur_is_open_dte', '', '0', '00', '1',
      '', '00000000', '$sessionName', '$todayTime', '', '', 'a', '0'
      FROM od_br_name od
      WHERE od.is_br = '$amphur_is_br' AND od.is_sbr = '$amphur_is_sbr'
      ORDER BY od.is_ch DESC");

    $insert_id = $this->mssql->insert_id();
    $querySBR = "SELECT is_ch, is_am FROM od_br_name  WHERE item_no = $insert_id ";
    $query = $this->mssql->query($querySBR)->result_array();
    $amphur_is_ch     = $query[0]['is_ch'];
    $amphur_is_am     = $query[0]['is_am'];

    $amphur_addr =  $odAddrAM['addr'];
    $amphur_moo =  $odAddrAM['moo'];
    $amphur_soi =  $odAddrAM['soi'];
    $amphur_street =  $odAddrAM['street'];
    $amphur_subdist =  $odAddrAM['subdist'];
    $amphur_dist =  $odAddrAM['dist'];
    $amphur_prov =  $odAddrAM['prov'];
    $amphur_zip_code =  $odAddrAM['zipcode'];
    // insert new address for new branch
    $addNewUnitAddr = $this->mssql->query("INSERT INTO od_org_addr (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, brnch_act_dept_cd, addr,
      moo, soi, street, subdist, dist, prov, zip_code,
      updated_by, last_update)
      VALUES ( '$amphur_is_br', '$amphur_is_sbr', '$amphur_is_ch', '$amphur_is_am', '$brnch_act_dept_cd', '$amphur_addr',
        '$amphur_moo', '$amphur_soi', '$amphur_street', '$amphur_subdist', '$amphur_dist', '$amphur_prov', '$amphur_zip_code',
        '$sessionName', '$todayTime' ) ");

    for ($p = 0; $p < sizeof($telInfo); $p++) {
      $numberUnit = str_replace('-', '', $telInfo[$p]['number']);
      //insert phone number for new unit
      if ($p == 0) {
        $addPhoneUnit = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
          phone_fax_cd, lcn_cd, updated_by, last_update)
          VALUES ( '$amphur_is_br', '$amphur_is_sbr', '$amphur_is_ch', '$amphur_is_am', '$numberUnit',
            '1', '02', '$sessionName', '$todayTime' ) ");
      }
      elseif ($p > 0 && $numberUnit != '') {
        $addPhoneUnit = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
          phone_fax_cd, lcn_cd, updated_by, last_update)
          VALUES ( '$amphur_is_br', '$amphur_is_sbr', '$amphur_is_ch', '$amphur_is_am', '$numberUnit',
            '1', '02', '$sessionName', '$todayTime' ) ");
      }

    }

    $faxUnit = str_replace('-', '',  $ctrInfo['fax']);
    //insert fax number for new unit
    $addFaxUnit = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
      phone_fax_cd, lcn_cd, updated_by, last_update)
      VALUES (
        '$amphur_is_br', '$amphur_is_sbr', '$amphur_is_ch', '$amphur_is_am', '$faxUnit',
        '2', '02', '$sessionName', '$todayTime' ) ");

    $wanUnit = $ctrInfo['wan'];
    //insert wan number for new unit
    $addWanUnit = $this->mssql->query("INSERT INTO od_phone_fax (ind_brnch_cd, dept_cd, div_cd, subdiv_cd, number,
      phone_fax_cd, lcn_cd, updated_by, last_update)
      VALUES (
        '$amphur_is_br', '$amphur_is_sbr', '$amphur_is_ch', '$amphur_is_am', '$wanUnit',
        '1', '15', '$sessionName', '$todayTime' ) ");


    $result = "";
    if($addNewUnit && $addNewUnitAddr && $addPhoneUnit && $addFaxUnit && $addWanUnit){
      $result = "Successfully";
    }else{
      $result = "Failed";
    }

    $this->mssql->close();
    return $result;

  }



}
?>
