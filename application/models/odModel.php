<?php

class odModel extends CI_Model
{
  var $mssql;
  public function __construct()
  {
    parent::__construct();
    //use DT database connection
    $this->mssql = $this->load->database ( 'DT', TRUE );
  }

  public function loadDivInfo()
  {

    $queryStr = "SELECT brnch_act_dept_cd AS id, brnch_act_dept_nme AS div_nme,  brnch_act_dept_nme AS div_name FROM od_brnch_act_dept
                 WHERE brnch_act_dept_cd != '00'";

    $query = $this->mssql->query($queryStr)->result_array();
    $result = array();
    $resultbuff = array();
    foreach ($query as $row) {
      $resultbuff['id'] = $row['id'];
      $resultbuff['div_nme'] = $row['div_nme'];
      $resultbuff['div_name'] = $row['div_name'];
      array_push($result, $resultbuff);
    }

    $this->mssql->close();
    return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function loadProvinceInfo($div)
  {
    $queryStr = "SELECT TB.brcode, TB.brname , cat_cc, ST.brshrt
    FROM tb_brmapprofile TB
    LEFT JOIN bmc_tumbon ON left(TB.brcode,2) = is_br
    LEFT JOIN short_name_branch ST ON TB.brcode = ST.brcode
    LEFT JOIN od_br_name OD ON TB.brcode = OD.is_br+OD.is_sbr
    WHERE CONVERT(int, div) = $div AND TB.brcode like '%A' AND is_status = 1
    GROUP BY TB.brcode, TB.brname , cat_cc, ST.brshrt";

    $query = $this->mssql->query($queryStr)->result_array();
    $result = array();
    $resultbuff = array();
    foreach ($query as $row) {
      $resultbuff['brcode'] = $row['brcode'];
      $resultbuff['brname'] = $row['brname'];
      $resultbuff['cat_cc'] = $row['cat_cc'];
      $resultbuff['brshrt'] = $row['brshrt'];
      array_push($result, $resultbuff);
    }

    $this->mssql->close();
    return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function loadBranchInfo($province)
  {
    $queryStr = "SELECT TB.brcode AS brcode, TB.brname, OD.is_status
        FROM tb_brmapprofile TB
        LEFT JOIN od_br_name OD ON TB.brcode = OD.is_br + OD.is_sbr
        WHERE TB.brcode not like '%A' AND left(TB.brcode,2) + 'A' = '$province' AND OD.is_status = 1
        ORDER BY TB.brcode";

    $query = $this->mssql->query($queryStr)->result_array();
    $result = array();
    $resultbuff = array();
    foreach ($query as $row) {
      $resultbuff['brcode'] = $row['brcode'];
      $resultbuff['brname'] = $row['brname'];
      array_push($result, $resultbuff);
    }

    $this->mssql->close();
    return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function loadTypeInfo()
  {
    $queryStr = "SELECT brnch_type, type_des FROM od_brnch_type";

    $query = $this->mssql->query($queryStr)->result_array();
    $result = array();
    $resultbuff = array();
    foreach ($query as $row) {
      $resultbuff['brnch_type'] = $row['brnch_type'];
      $resultbuff['type_des'] = $row['type_des'];
      array_push($result, $resultbuff);
    }
    $this->mssql->close();
    return json_encode($result,JSON_UNESCAPED_UNICODE);
  }

  public function loadBrandDetail($odInfo)
  {
    $div = $odInfo['div'];
    $province = rtrim($odInfo['province'],"A");
    $branch = $odInfo['branch'];
    $type = $odInfo['type'];
    $strQuery = "SELECT OD.* FROM (
      SELECT ORGNME.is_br, ORGNME.is_sbr, ORGNME.is_ch, ORGNME.is_am
			, ORGNME.is_br + '-' + ORGNME.is_sbr + '-' + ORGNME.is_ch + '-' + ORGNME.is_am AS org_ode, ORGNME.is_status
      , ORGNME.brnch_type, ORGNME.is_amp_n, ORGNME.org_cd
      , (CASE WHEN BRTYPE.brnch_type = '1' THEN '' WHEN BRTYPE.brnch_type = '5' THEN 'สาขา' ELSE BRTYPE.type_des END) AS type_des
      , ORGNME.is_thai_n, ORGNME.is_eng_n, ORGNME.is_div AS div, ORGNME.is_open_dte AS open_date
      , ORGADDR.addr + (CASE WHEN ORGADDR.moo != '' THEN ' ม.' + ORGADDR.moo ELSE '' END)
      + (CASE WHEN ORGADDR.soi != '' THEN ' ' + ORGADDR.soi ELSE '' END)
      + (CASE WHEN ORGADDR.street != '' THEN ' ' + ORGADDR.street ELSE '' END)
      + ' ต.' + ORGADDR.subdist + ' อ.' + dist + ' จ.' + prov + ' ' + zip_code AS org_addr
      , FDIST.fldoff_dist_cd AS district
      , BRNME.is_thai_n AS unit_nme
      FROM od_br_name  AS ORGNME
      LEFT JOIN od_org_addr AS ORGADDR ON ORGNME.is_br = ORGADDR.ind_brnch_cd AND ORGNME.is_sbr = ORGADDR.dept_cd AND ORGNME.is_ch = ORGADDR.div_cd
      LEFT JOIN od_link_br_tumbon  FDIST ON ORGNME.is_br = FDIST.ind_brnch_cd AND ORGNME.is_sbr = FDIST.dept_cd AND ORGNME.is_ch = FDIST.div_cd AND ORGNME.is_am = FDIST.subdiv_cd
      LEFT JOIN od_br_name_tumbon BRNME ON ORGNME.is_br =  BRNME.is_br AND ORGNME.is_sbr = BRNME.is_sbr AND FDIST.fldoff_dist_cd = (BRNME.is_ch+BRNME.is_am)
      LEFT JOIN od_brnch_type BRTYPE ON ORGNME.brnch_type = BRTYPE.brnch_type ) AS OD ";

    if ( $div != '' && $province == '' && $branch == '') {
      $strQuery  = $strQuery." WHERE OD.is_status = '1' AND OD.div = $div ";
    }
    elseif ( $div != '' && $province != '' && $branch == '') {
      $strQuery  = $strQuery." WHERE OD.is_status = '1' AND OD.div = $div AND OD.is_br = '$province' ";
    }
    elseif ( $div != '' && $province != '' && $branch != '') {
      $strQuery  = $strQuery." WHERE OD.is_status = '1' AND OD.div = $div AND OD.is_br = '$province' AND OD.is_br + OD.is_sbr = '$branch' ";
    }

    if ($type != '') {
      $strQuery = $strQuery." AND OD.brnch_type = '$type' ";
    }

    $strQuery = $strQuery." ORDER BY  OD.is_br, OD.is_sbr, OD.is_ch, OD.is_am";
    $query = $this->mssql->query($strQuery)->result_array();
      $result = array();
      $resultbuff = array();
      foreach ($query as $row) {
        $resultbuff['org_ode'] = $row['org_ode'];
        $resultbuff['brnch_type'] = $row['brnch_type'];
        $resultbuff['org_cd'] =  $row['is_amp_n'].str_replace(" ","", $row['org_cd']);
        $resultbuff['type_des'] = $row['type_des'];
        $resultbuff['th_org_nme'] = $row['is_thai_n'];
        $resultbuff['eng_org_nme'] = $row['is_eng_n'];
        $resultbuff['nme_show'] = str_replace(" ","", $row['type_des']).str_replace(" ","", $row['is_thai_n']);
        $resultbuff['open_date'] = $row['open_date'];
        $resultbuff['org_addr'] = $row['org_addr'];
        $resultbuff['district'] = $row['district'];
        $resultbuff['unit_nme'] = $row['unit_nme'];
        $resultbuff['div'] = $row['div'];
        $resultbuff['unit_list'] = array();
        if ($row['district'] != null) {
          $info = $row['district'];
          $unitbuff = array();
          $br = $row['is_br'];
          $sbr = $row['is_sbr'];
          $queryUnit = $this->mssql->query(" SELECT cat_tt, cat_mm, cat_desc  FROM bmc_tumbon WHERE is_br = '$br' AND is_sbr = '$sbr' AND is_ch + is_am = '$info' ORDER BY cat_aa, cat_tt ASC ")->result_array();
          $i = 0;
            foreach ($queryUnit as $data) {
              if($data['cat_tt'] == '00' && $data['cat_mm'] == '00')
                $unitbuff['tumbon'] = 'อ.'.$data['cat_desc'];
              else
                $unitbuff['tumbon'] = ' - ต.'.$data['cat_desc'];
              array_push($resultbuff['unit_list'], $unitbuff);
              $i = $i + 1;
            }
        }
        $id = $row['is_br'].$row['is_sbr'].$row['is_ch'].$row['is_am'];
        $resultbuff['telephone'] = array();
        $queryTel = "SELECT number FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' AND lcn_cd = '02' AND phone_fax_cd = '1' ";
        $query = $this->mssql->query($queryTel)->result_array();
        $tempTel = array();
        foreach ($query as $row) {
          $tempTel['number'] = substr_replace($row['number'], '-', 1, 0);
          $tempTel['number'] = substr_replace($tempTel['number'], '-', 6, 0);
          // $tempTel['number'] = $row['number'];
          array_push($resultbuff['telephone'], $tempTel);
        }

        $resultbuff['fax'] = "";
        $queryFax = "SELECT number FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' AND lcn_cd = '02' AND phone_fax_cd = '2' ";
        $query = $this->mssql->query($queryFax)->result_array();
        $tempTel = array();
        foreach ($query as $row) {
          $resultbuff['fax'] = substr_replace($row['number'], '-', 1, 0);
          $resultbuff['fax'] = substr_replace($resultbuff['fax'], '-', 6, 0);
          // $resultbuff['fax'] = $row['number'];
        }
        $resultbuff['wan'] = "";
        $queryFax = "SELECT number FROM od_phone_fax WHERE ind_brnch_cd + dept_cd + div_cd + subdiv_cd = '$id' AND lcn_cd = '15' AND phone_fax_cd = '1' ";
        $query = $this->mssql->query($queryFax)->result_array();
        $tempTel = array();
        foreach ($query as $row) {
          $resultbuff['wan'] = $row['number'];
        }

        array_push($result, $resultbuff);
      }

      $this->mssql->close();
      return json_encode($result,JSON_UNESCAPED_UNICODE);
  }



}
?>
